
from collections import defaultdict
from functools import wraps

from pyodide_mkdocs_theme.pyodide_macros import html_builder as Html
from pyodide_mkdocs_theme.pyodide_macros import PyodideMacrosPlugin




def lien_exo(env: PyodideMacrosPlugin):
    """
    Crée automatiquement le lien vers l'exercice contenu dans le dossier indiqué,
    quelle que soit la localisation du fichier actuel.

    @txt:           Texte à afficher pour le lien
    @target:        Nom du dossier contenant l'exercice à cibler
    @tail="":       Pour cibler un autre fichier que index.md dans le répertoie cible (images, svg, ...)
    @new_tab=False: Le lien s'ouvre dans un nouvel onglet

    @returns: une balise `<a>` avec le bon lien relatif, et le contenu de `txt_lien`

    @throws:
        * Au démarrage du build: FileExistsError, si deux dossier d'exercices sont trouvés avec
          des noms identiques
        * À la construction des pages: FileNotFoundError, si le dossier ciblé n'est pas trouvé.
    """

    # Build the paths dict
    EXOS = {}
    explore = defaultdict(set)
    for file in env.docs_dir_path.rglob("index.md"):

        # skip home:
        if file.parent == env.docs_dir_path:
            continue

        parts = file.relative_to(env.docs_dir_path).parts[:-1]
        name = parts[-1]
        tail_path = '/'.join(parts)
        explore[name].add(tail_path)
        EXOS[name] = tail_path

    oops = ''.join(
        f'    { name }:' + ''.join(f"\n\t{env.docs_dir}/{p}" for p in paths)
            for name,paths in explore.items()
            if len(paths) > 1
    )
    if oops:
        raise FileExistsError(
            "Des noms de dossiers identiques contenant des fichiers 'index.md' ont été trouvés. "
            "Il n'est pas possible de construire le dictionnaire pour la macro "
            f"{ lien_exo.__name__ }.\nNoms et fichiers concernés:\n{ oops }\n"
        )


    @wraps(lien_exo)
    def wrapped(txt:str, target:str, tail:str="", new_tab:bool=False):
        """
        Abstraction des liens internes à la documentation
        Frédéric Zinelli
        """
        if target not in EXOS:
            raise FileNotFoundError(f"""Pas d'exercice trouvé dans un dossier nommé {target!r}""")
        up = env.level_up_from_current_page()
        
        extra_args = {"target": '_blank'} if new_tab else {}
        link =  Html.a(txt, href=f'{ up }/{ EXOS[target] }/{ tail }', **extra_args)
                # Use fo tail works because directory_urls=true
        return link


    return wrapped
