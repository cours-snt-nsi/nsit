"""
#########################################################
# Fichiers ajoutés pour Codex                           #
# Création de la page de recherche : fonction recherche #
# Gestion des tags : variable env.conf['tagsValides']   #
#########################################################
"""

from datetime import datetime, date
import json
import re

from typing import Dict, List, Tuple
from functools import wraps
from dataclasses import dataclass, field
from pathlib import Path

import markdown
from mkdocs.exceptions import BuildError

from pyodide_mkdocs_theme.pyodide_macros import PyodideMacrosPlugin


ExByCategory = Dict[str, List["Exercice"]]

DiffData = Tuple[str, str]
""" (category ID (ASCII + no spaces), category name (not ASCII + spaces)) """


EXAM_PATH = (
    "exam"  # exam-type exercises are placed in this directory "docs/<EXAM_PATH>/."
)
DRAFT_TAG = (
    "brouillon"  # those exercises are far from being ready. You don't want to show them
)
DEFAULT_DIFF = 400


# In order:
DIFFICULTY_CATEGORIES: List[DiffData] = [
    ("debutant", "débutant"),
    ("facile", "facile"),
    ("moyen", "moyen"),
    ("difficile", "difficile"),
    ("non-renseigne", "non renseigné"),
]
OTHER_CATEGORIES = (
    "autre",
    "brouillon",
    "exam",
)

ALL_CATEGORIES = OTHER_CATEGORIES + tuple(cat_id for cat_id, _ in DIFFICULTY_CATEGORIES)

VALIDATED_DIR = "exercices"


@dataclass
class Exercice:
    """Regrouping the data for a file/problem targeted by the search engine"""

    path: Path  # mandatory

    title: str = "Pas de titre :("
    stripped_title: str = "pasdetitre"
    difficulty: int = DEFAULT_DIFF
    tags: List[str] = field(default_factory=list)
    is_exercise: bool = True  # "validé" ou "en travaux"
    is_validated: bool = False  # dans "./exercices/..."
    is_draft: bool = False  # "TROP en travaux"... XD
    is_exam: bool = False  # exam-type exercises, to be ignored in some occasion
    href: str = ""
    diff_category_data: DiffData = ("", "")
    index_file_last_update: str = "01/01/2024"  # exercise's last update dd/mm/yyyyy


def recherche(env: PyodideMacrosPlugin, cat_dict: ExByCategory):
    """
    Création d'une page de recherche
    Nicolas Revéret
    """

    # ajout du dictionnaire des tags valides à la variable d'environnement
    # ce dictionnaire devient accessible dans Jinja en faisant {{ config.tagsValides }}
    # voir tags.html pour son utilisation
    env.conf["tagsValides"] = env.variables["tagsValides"]

    @wraps(recherche)
    def wrapped():

        rendu = [
            """
<table class="table hover stripe row-border" id="tableau-recherche" style="width:100%;">
    <thead class="">
        <tr>
            <th>Exercice</th>
            <th>Difficulté</th>
            <th>Tags</th>
            <th>Mise à jour</th>
            <th>valeurDifficultes</th>
            <th>classeDifficultes</th>
            <th>timestamp</th>
        </tr>
    </thead>
    <tbody>
"""
        ]

        is_dev_docs = env.docs_dir == "dev_docs"
        for cat_id, _ in DIFFICULTY_CATEGORIES:
            for exo in cat_dict[cat_id]:
                if not exo.is_draft or is_dev_docs:
                    tags = [f"<span class='md-tag'>{ t }</span>" for t in exo.tags]
                    start, end = (
                        (1, -1)
                        if exo.title[0] in "\"'" and exo.title[-1] in "\"'"
                        else (0, len(exo.title))
                    )

                    dd, mm, yyyy = exo.index_file_last_update.split("/")
                    # Formatting is guaranteed to be "../../....", here.

                    rendu.extend(
                        (
                            "<tr class='table-row'><td>",
                            f"<a href={exo.href} class='md-tag' data-sort='{exo.stripped_title}'>{ exo.title[start:end] }</a>",
                            "</td><td>",
                            f"""<span class='md-tag {exo.diff_category_data[0]}'>{exo.diff_category_data[1]}</span>""",
                            "</td><td>",
                            " ".join(tags),
                            "</td><td>",
                            exo.index_file_last_update,
                            "</td><td>",
                            str(exo.difficulty),
                            "</td><td>",
                            str(exo.difficulty // 100),
                            "</td><td>",
                            yyyy + mm + dd,
                            "</td></tr>",
                        )
                    )

        rendu.append("</tbody></table>")
        return "".join(rendu)

    return wrapped


def liste_exercices(_: PyodideMacrosPlugin, cat_dict: ExByCategory):
    """
    Création d'un objet JS :
    {
        "debutant": [[titre de l'exercice, lien], [...]]] ,
        "facile": [[titre de l'exercice, lien], [...]]] ,
        "moyen": [[titre de l'exercice, lien], [...]]] ,
        "difficile": [[titre de l'exercice, lien], [...]]] ,
    }
    listant tous les exercices validés classés par difficulté
    Cet objet `exercices` est disponible en JS dans la page appelant cette macro


    Nicolas Revéret
    """

    @wraps(liste_exercices)
    def wrapped():
        exercices_dict = {
            cat_id: []
            for cat_id, _ in DIFFICULTY_CATEGORIES
            if cat_id != "non-renseigne"
        }
        for category, exercices in exercices_dict.items():
            for exo in cat_dict[category]:
                if exo.is_validated:
                    exercices.append((exo.title, exo.href))

        return "<script>var exercices = " + json.dumps(exercices_dict) + "</script>"

    return wrapped


def dico_pages(env: PyodideMacrosPlugin) -> ExByCategory:
    """
    Création d'une liste référençant les pages de la documentation
    Nicolas Revéret
    """

    dico_tags = env.variables["tagsValides"]

    docs = env.docs_dir_path

    validated_exo_path = (
        f"{VALIDATED_DIR}/*/*.md"  # NOTE: validation starts from the right
    )

    # Extract all index.md files, except the one at the root directory of the website:
    path_filter = (
        "**/index.md" if env.docs_dir == "dev_docs" else "*/**/index.md"
    )
    exercices = [Exercice(path) for path in docs.glob(path_filter)]

    exos_by_category = {cat_id: [] for cat_id in ALL_CATEGORIES}
    for exo in exercices:

        with open(exo.path, "r", encoding="utf-8") as file:
            text = file.read()

        md = markdown.Markdown(extensions=["meta"])
        md.convert(text)
        metas = md.Meta  # pylint: disable=no-member

        if "title" in metas:
            exo.title = metas["title"][0]
            exo.stripped_title = re.sub(r"\W", "", exo.title)

        if "difficulty" in metas:
            exo.difficulty = int(metas["difficulty"][0])

        if "tags" in metas:
            exo.tags = [t[2:].strip() for t in metas["tags"] if t]
            for tag in tuple(exo.tags):
                if tag not in dico_tags:
                    exo.tags.remove(tag)
                    raise BuildError(
                        f"[Tag incorrect] - l'exercice {exo.path} contient le tag '{tag}' incorrect"
                    )

        if "maj" in metas:
            update = metas["maj"][0]
            try:
                dd, mm, yyyy = map(int, update.split("/"))
                if yyyy < 100:     # Assume "YY" format only
                    yyyy += 2000
                if yyyy > datetime.now().year:
                    raise ValueError()

                date(yyyy,mm,dd)        # validation purpose only

            except ValueError:
                maj = metas and metas.get('maj', "(no maj date...)")
                raise BuildError(
                    f"[Date incorrecte] - l'exercice {exo.path} contient un format de date de maj "
                    f"incorrecte : {maj}. Le format attendu est jj/mm/aaaa. ou jj/mm/aa pour les dates > 1999."
                ) from None

            exo.index_file_last_update = f"{ dd :0>2}/{ mm :0>2}/{ yyyy }"

        exo.is_exercise = bool(exo.tags)

        exo.is_validated = exo.path.match(validated_exo_path)

        exo.is_draft = "brouillon" in exo.tags

        exo.is_exam = EXAM_PATH in exo.path.parts

        exo.diff_category_data = DIFFICULTY_CATEGORIES[exo.difficulty // 100]

        exo_dir = exo.path.relative_to(docs).parent
        url = f"{ env.site_url }{ '/'.join(exo_dir.parts) }/"
        exo.href = url

        # We fill the dict with this target
        if exo.is_draft:
            exos_by_category["brouillon"].append(exo)
        elif exo.is_exam:
            exos_by_category["exam"].append(exo)
        elif exo.is_exercise:
            diff_cat_id = exo.diff_category_data[0]
            exos_by_category[diff_cat_id].append(exo)
        else:
            exos_by_category["autre"].append(exo)

    return exos_by_category
