
from pyodide_mkdocs_theme.pyodide_macros import PyodideMacrosPlugin

from . import (
    liens_internes,
    recherche,
    insertion_remarque,
    includer,
)



def define_env(env:PyodideMacrosPlugin):
    """Macro hook function (equivalent to on_config plugin hook)"""


    def build_macros_for(macros_factories):
        """Register all the functions as macros"""
        for func in macros_factories:
            macro = func(env)
            env.macro(macro)

    # ------------------------------------------------
    # CodEx specific

    pages = recherche.dico_pages(env)

    env.macro(recherche.recherche(env, pages))
    env.macro(recherche.liste_exercices(env, pages))

    build_macros_for([
        insertion_remarque.interdiction,
        insertion_remarque.remarque,
        insertion_remarque.version_ep,
        liens_internes.lien_exo,
        includer.double_IDE,
    ])
