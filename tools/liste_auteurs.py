import markdown
from pathlib import Path
import os


sujets = {
    f"{path}/{file}" : {} for path,dirs,files in os.walk("./docs")
        for file in files
        if file=='sujet.md' and "exam" not in path
}
auteurs = set()
for adresse in sujets:
    try:
        with open(adresse, "r", encoding="utf-8") as fichier_énoncé:
            text = fichier_énoncé.read()
        md = markdown.Markdown(extensions=["meta"])
        md.convert(text)
        metas = md.Meta             # pylint: disable=no-member
        if "author" in metas:
            sujets[adresse]["author"] = [a[2:] if a[0] == "-" else a for a in metas["author"] if a]
            for a in sujets[adresse]["author"]:
                auteurs.add(a)
    except FileNotFoundError:
        pass

for k, v in sujets.items():
    print(f"{k}: {v}")

for a in sorted(auteurs, key=lambda x: " ".join(x.split(" ")[1:])):
    print(a)
