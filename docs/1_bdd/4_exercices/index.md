---
author: Nicolas Revéret
title: Exercices
---

# Exercices sur les bases de données

[Ce site propose différents exercices sur les bases de donnée et le langage `SQL`.](https://nreveret.forge.apps.education.fr/exercices_bdd/01_lycee/lycee/){:target="_blank"}

Vous y trouverez différents exercices sur les thèmes suivants :

* 🔨 Création de base de données, insertion et suppression de valeurs

* 🐭 Requêtes sur une seule table,

* 🐈 Requêtes sur plusieurs tables,

* 🐯 Exemples complets.


Le site n'a pas vocation à être un cours sur les bases de données ni sur le langage `SQL`. Il propose simplement des exercices de rédaction d'instructions `SQL` dans le cadre du programme de la [spécialité NSI](https://eduscol.education.fr/document/30010/download).

Notez que dans chaque section sauf la dernière, le premier exercice proposé présente les nouvelles instructions `SQL` utilisées.

La dernière section propose des exercices reprenant l'ensemble des instructions présentées dans les précédentes.

Les commandes de base de `SQL` sont rappelées dans le [:bulb: memento `SQL`](memento_sql.md).