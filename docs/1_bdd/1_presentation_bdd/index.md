---
title: Présentation des bases de données
author: N. Revéret
---

???+ note "Site dédié"

    Vous trouverez un cours sur le SQL à [cette adresse](https://nreveret.forge.apps.education.fr/exercices_bdd/).

En première, nous avons manipulé des [_données en tables_](https://nreveret.forge.apps.education.fr/donnees_en_table/), le plus souvent contenues dans des fichiers _csv_.

Cette structure de données, bien que pratique et facilitant l'échange de données entre utilisateurs, ne permet bien souvent pas de décrire facilement la complexité des relations existant entre différentes informations.

Dans le cas de données plus compliquées, on peut donc utiliser une **base de données**. Différents modèles existent mais le plus utilisé reste la **base de données relationnelle**. C'est ce modèle que nous allons étudier ci-dessous.

Seule, une base de données est assez inutile. On la couple souvent avec un [**Système de Gestion de la Base de Données**](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_gestion_de_base_de_donn%C3%A9es) (SGBD) qui permet d'interagir avec celle-ci en ajoutant des valeurs, cherchant des données... Le SGBD a aussi en charge les accès simultanés à la base de données (plusieurs utilisateurs peuvent l'interroger ou la modifier en même temps) ainsi que la gestion des différentes sauvegardes de celle-ci.

On pourra retenir les exemples suivants :

* `MySQL` : le plus connu et courant ;

* `SQlite` : bibliothèque écrite en `C` dont il existe aussi une version Python (`sqlite3`).

![MySql et SQLite](MySQL-vs-SQLite.jpg){width=40% .center .autolight .autolight}

## Des relations et des tables

### Tables et attributs

???+ info "Table ou relation"

    Une base de données relationnelle est construite à l'aide de **relations** que l'on appelle communément des **tables**. 

    On peut visualiser une relation sous la forme d'un tableau à double-entrée :

    * la première ligne comporte les entêtes, les **attributs** de la table ;

    * les lignes suivantes comprennent les **données** ou **entrées**.
    
Voici par exemple une table `Eleves` :

<center>

| `id` | `nom`  | `prenom` | `naissance` | `classe` |
| ---- | ------ | -------- | ----------- | -------- |
| 1    | Durand | Marie    | 03/03/03    | Term-1   |
| 2    | Ergand | Renaud   | 25/04/03    | Term-2   |
| 3    | Simon  | Caroline | 03/06/03    | Term-1   |

</center>

Lorsqu'on définit un attribut, on précise son type : cela permet de préciser quelles sont les valeurs possibles. Par exemple lorsque l'utilisateur ajoute des données, le SGBD vérifie que celles-ci correspondent bien aux types indiqués.

???+ info "Contrainte de domaine"

    L'adéquation entre le type (ou *domaine*) indiqué et la valeur d'un attribut est appelé « **contrainte de domaine ».

Dans le cas de l'exemple précédent, les types des attributs sont :

* `id` : type entier `INT`
* `nom`, `prenom`, `classe` : type texte `VARCHAR`
* `naissance` : type date `DATE`

### Clé primaire

Par principe, une table ne peut pas contenir deux lignes parfaitement identiques. L'exemple ci-dessous sera refusé par le SGBD :

<center>

| `id` | `nom`  | `prenom` | `naissance` | `classe` |
| ---- | ------ | -------- | ----------- | -------- |
| 1    | Durand | Marie    | 03/03/03    | Term-1   |
| 2    | Ergand | Renaud   | 25/04/03    | Term-2   |
| 3    | Simon  | Caroline | 03/06/03    | Term-1   |
| 3    | Simon  | Caroline | 03/06/03    | Term-1   |

</center>

On pourrait toutefois imaginer que deux élèves de même nom, prénom, nés le même jour soient dans la même classe (si, si, c'est possible !). Dans ce cas comment gérer notre table ?

Il sera possible de les distinguer en utilisant leur `id` : cette valeur numérique est **unique** pour chaque ligne : deux lignes ne peuvent avoir le même `id`.

Le plus souvent on va même jusqu'à préciser que l'`id` est la **clé primaire** (`PRIMARY KEY` dans les SGBD) de notre table.

???+ info "Clé primaire"

    Une **clé primaire** est un attribut permettant d'identifier sans aucune ambiguïté une donnée dans une relation.
    
    Une table ne contient qu'une seule clé primaire...mais une clé primaire peut-être composée.
    
    Par exemple le couple `(niveau, numéro)` permet de définir sans ambiguïté une classe du lycée. Ainsi `(T, 3)` décrit la "Terminale 3" alors que `(1, 3)` la "Première 3".

???+ info "Contrainte d'intégrité"

    Le caractère *unique* de la clé primaire et le fait qu'elle permette d'identifier chaque donnée sans ambiguïté est appelé « **contrainte d'intégrité ».
    
## Plusieurs tables

Complétons la table précédente en ajoutant les professeurs principaux :

<center>

| `id` | `nom`  | `prenom` | `naissance` | `classe` | `prof_principal` |
| ---- | ------ | -------- | ----------- | -------- | ---------------- |
| 1    | Durand | Marie    | 03/03/03    | Term-1   | M. Ayrault       |
| 2    | Ergand | Renaud   | 25/04/03    | Term-2   | M. Beyrault      |
| 3    | Simon  | Caroline | 03/06/03    | Term-1   | M. Ayrault       |

</center>

Nous obtenons une nouvelle table valide mais contenant des répétitions : est-il vraiment utile de préciser pour chaque élève que M. Ayrault est le professeur principal de la Terminale 1 ?

Il serait plus judicieux de créer une seconde table `Classes` :

<center>

| `id` | `classe` | `prof_principal` |
| ---- | -------- | ---------------- |
| 1    | Term-1   | M. Ayrault       |
| 2    | Term-2   | M. Beyrault      |

</center>

On le voit, dans cette seconde table, une ligne correspond à une classe et le nom du professeur principal n'est alors indiqué qu'une seule fois.

Ici aussi l'attribut `id` sert de clé primaire.


???+ note "Mêmes noms d'attributs"
    
    On peut s'interroger sur le nom des attributs : est-ce que le SGBD ne va pas confondre les deux colonnes `id` ? Non, à condition d'être explicite en précisant `Eleves.id` ou `Classes.id` selon que l'on souhaite parler de l'un ou l'autre.

On peut donc utiliser l'`id` de la classe dans la première table :

<center>

| `id` | `nom`  | `prenom` | `naissance` | `classe` |
| ---- | ------ | -------- | ----------- | -------- |
| 1    | Durand | Marie    | 03/03/03    | 1        |
| 2    | Ergand | Renaud   | 25/04/03    | 2        |
| 3    | Simon  | Caroline | 03/06/03    | 1        |

</center>

Comment le SGBD va-t'il savoir que la classe 1 de la table `Eleves` est la 1 de la table `Classes` ? 

???+ info "Clé étrangère"

    On utilise une **clé étrangère** (`FOREIGN KEY`) : la clé étrangère de l'attribut `Eleves.classe` est `Classes.id`.
    
    On indique ainsi que la valeur de la colonne `classe` de la table `Eleves` correspond à l'`id` de la tables `Classes`.

???+ note "Un pour un ou tous pour un"

    Un élève appartient à une classe mais une classe compte plusieurs élèves. On parle de relation **un pour plusieurs**. 
    
    On peut aussi imaginer des relations **un pour un** : un élève a une place précise dans le plan de classe et une place correspond à un élève unique.

## Modèle de données relationnel

Travailler avec une base de données, créer une base de données nécessitent donc de bien comprendre sa structure, les relations entre les différentes tables et attributs.

Cette structure peut être décrite facilement à l'aide du **modèle de données** ou **schéma relationnel**. On peut les fournir sous forme de  texte ou de schéma.

* <code>eleves(#id, nom, prenom, ,naissance, classe, <span style="color:red">spe_1</span>, <span style="color:red">spe_2</span>)</code>

* <code>matieres(<span style="color:red">#id</span>, nom)</code>

* <code>classes(<span style="color:red">#id</span>, nom, <span style="color:red">prof_principal</span>)</code>

* <code>cours(<span style="color:red">#id</span>, <span style="color:red">matiere</span>, <span style="color:red">professeur</span>)</code>

* <code>professeurs(<span style="color:red">#id</span>, nom)</code>

Chaque ligne correspond à une table. Les clés primaires sont préfixés avec `#`. Les clés étrangères sont indiquées en couleur.

On peut voir qu'il existe cinq tables liées les unes avec les autres.

Ainsi un élève :

* fait partie d'une classe (grâce à la clé externe entre `eleves.classe` et `classes.id`)
* suit un cours en première spécialité (correspondance entre `eleves.spe_1` et `cours.id`)

Les cours (qui correspondent ici à des groupes d'une matière) sont associés :

* à des élèves
* à une matière (`cours.matiere = matieres.id`)
* à un professeur (`cours.professeur = professeurs.id`)


Voici le schéma représentant la même base de donnée :

![Schéma relationnel](Schema_DB_Lycee.png){width=60% .center .autolight}

Les `1` et les `*` sur les relations entre les tables indiquent le type de relation :

* le `1` indique que cet attribut correspond à une unique entrée dans l'autre table (un élève est dans **une** classe)
* le `*` permet de dire que cet attribut peut se retrouver plusieurs fois (une classe comporte **plusieurs** élèves)

???+ note "Convention"

    On trouve souvent le symbole $\infty$ à la place de `*` dans les schémas.