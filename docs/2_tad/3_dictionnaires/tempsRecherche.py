import matplotlib.pyplot as plt
from time import perf_counter_ns
from statistics import mean

class Element:
    """
    Classe décrivant les éléments utilisés dans les listes châinées
    Un élément possède :
        - une donnée (de type quelconque)
        - un successeur (un autre élément)
    """

    def __init__(self, donnee):
        self.donnee = donnee
        self.successeur = None

    def afficher(self):
        print(str(self.donnee) + " --> " + str(self.successeur), end="\n\n")


class Liste_Chainee:
    """
    Classe décrivant une liste simplement chaînée
    La liste est caractérisée par sa seule tête
    """

    def __init__(self):
        """
        Constructeur : renvoie la liste vide
        """
        self.tete = None

    def inserer(self, nouveau):
        """
        Insertion d'un élément à la tête de la liste
        """
        nouveau.successeur = self.tete
        self.tete = nouveau

    def supprimer(self, element):
        """
        Suppression de l'élément indiqué en argument
        """
        precedent = None
        x = self.tete

        while x != element:
            precedent = x
            x = x.successeur

        if precedent == None:
            self.tete = x.successeur
        else:
            precedent.successeur = x.successeur

    def afficher(self):
        """
        Affichage de la liste
        """
        s = ""

        pointeur = self.tete

        while pointeur != None:
            s += str(pointeur.donnee) + " --> "
            pointeur = pointeur.successeur

        print(s + "Vide", end="\n\n")

x = []
t_liste_chainee = []
t_liste = []
t_dict = []

for n in range(10, 2000, 10) :
    x.append(n)

    # Création dictionnaire
    dico = { 1 : 'a' for k in range(n-1)}
    dico[2] = 'b'

    t_start = perf_counter_ns()
    dico[2]
    t_end = perf_counter_ns()
    t_dict.append(t_end - t_start)


    # Création liste classique
    liste = ['a' for k in range(n-1)]
    liste[-1] = 'b'

    t_start = perf_counter_ns()
    liste[-1]
    t_end = perf_counter_ns()
    t_liste.append(t_end - t_start)

    t_s = []
    for i in range(10) :
        # Création liste
        liste = Liste_Chainee()
        liste.inserer(Element('b'))
        for i in range(n-1) :
            liste.inserer(Element('a'))
        elt = liste.tete
        t_start = perf_counter_ns()
        while elt.donnee != 'b' :
            elt = elt.successeur
        t_end = perf_counter_ns()
        t_s.append(t_end - t_start)
    t_liste_chainee.append(mean(t_s))


fig, ax = plt.subplots()

ax.plot(x, t_dict, label="Dictionnaire")
ax.plot(x, t_liste, label="Liste python (env. tableau)")
ax.plot(x, t_liste_chainee, label="Liste chaînée")
plt.title("Temps d'accès en fonction de la taille des données")
plt.xlabel("Taille des données")
props = dict(facecolor='white', alpha=0.5)
plt.text(0.65, 0.20, "Les dictionnaires et les listes\n sont confondus", transform=ax.transAxes, fontsize=10,
        verticalalignment='top', bbox=props)
plt.ylabel("Temps en ns")
plt.legend()
plt.show()