---
author: Mireille Coilhac, Nicolas Revéret
title: Bilan
---

# 🏁 Bilan

??? abstract "Dictionnaire : définition"

    Un dictionnaire est un ensemble **non ordonné** de couples (clé: valeur) notés `#!py clé: valeur`.

    Chaque **clé** est présente de façon **unique** dans le dictionnaire.

    Un dictionnaire s'écrit avec des **accolades**, les différents couples sont séparés par des **virgules** : `#!py {clé : valeur, autre_clé: autre_valeur, ...}`.

    Afin de pouvoir être hachées, les clés doivent être de types **immuables** : elles ne peuvent pas être modifiées.
    
    On peut choisir comme clé des nombres, des chaînes de caractères, des tuples d'éléments de types immuables,  mais **jamais des listes ni des dictionnaires**.

    En revanche, une valeur peut être de n'importe quel type : nombre, liste, chaîne de caractères, tuple, ou même dictionnaire.


??? abstract "Présence d'une clé"

    `#!py cle in dico` renvoie `#!py True` si la clé `#!py cle` existe dans `#!py dico` et `#!py False` sinon.  
    
    `#!py cle not in dico` renvoie `#!py True` si la clé `#!py cle` n'existe pas dans `#!py dico` et `#!py False` sinon. 

    ```pycon
    >>> dico = {"table": 1, "assiette": 6, "couverts": 6, "plat": 1}
    >>> "couverts" in dico
    True
    >>> "verre" in dico
    False
    ```

??? abstract "Accéder à une valeur"

    `#!py dico[cle]` renvoie la valeur associée à `cle` si elle est présente dans le dictionnaire.
    
    **Attention**, si la clé n'est pas présente dans le dictionnaire, cette instruction génère une erreur `#!py KeyError` !

    ```pycon
    >>> dico = {"table": 1, "assiette": 6, "couverts": 6, "plat": 1}
    >>> dico["table"]
    1
    >>> "verre" in dico
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    KeyError: 'verre'
    ```
    

??? abstract "Modifier et ajouter une valeur"

    Si la clé existe déjà `#!py dico[cle] = valeur` modifie la valeur associée.
    
    Si la clé n'existe pas, `#!py dico[cle] = valeur` ajoute la paire `#!py (cle, valeur)`.

    ```pycon
    >>> dico = {"table": 1, "assiette": 6, "couverts": 6, "plat": 1}
    >>> dico["plat"] = 2
    >>> dico["verre"] = 6
    >>> dico
    {'table': 1, 'assiette': 6, 'couverts': 6, 'plat': 2, 'verre': 6}
    ```

??? abstract "Supprimer une valeur"

    `#!py del dico[cle]` supprime le couple `(cle, valeur)` de `#!py dico`.
  
    `#!py dico.pop(cle)` supprime le couple (`cle`, `valeur`) de `#!py dico` et renvoie la valeur associée.

    ```pycon
    >>> dico = {"table": 1, "assiette": 6, "couverts": 6, "plat": 1}
    >>> del dico["plat"]
    >>> valeur = dico.pop("table")
    >>> dico
    {'assiette': 6, 'couverts': 6}
    >>> valeur
    1
    ```
    
??? abstract "Longueur d'un dictionnaire"

    `#!py len(dico)` renvoie le nombre de couples `(clé: valeur)` du dictionnaire.

    ```pycon
    >>> dico = {"table": 1, "assiette": 6, "couverts": 6, "plat": 1}
    >>> len(dico)
    4
    ```

??? abstract "Parcourir un dictionnaire"

    Le parcours d'un dictionnaire se fait sur ses clés en écrivant :

    ```python
    for cle in dico :
        # action à répéter
    ```

    Depuis `Python 3.6`, il est garanti que le parcours d'un dictionnaire se fait dans l'ordre d'ajout des couples `(clé: valeur)`. Si l'on n'est pas sûr de la version de `Python` utilisée, on ne peut pas être sûr de l'ordre dans lequel s'effectuent ces différents parcours.


    ```pycon
    >>> dico = {"table": 1, "assiette": 6, "couverts": 6, "plat": 1}
    >>> for element in dico:
    ...     print(element)
    table
    assiette
    couverts
    plat
    ```

??? abstract  "Listes des clés, des valeurs, des couples `(clé, valeur)`"

    On peut obtenir une liste `#!py cles` de toutes les clés de ce dictionnaire ainsi :

    ```pycon
    >>> dico = {"table": 1, "assiette": 6, "couverts": 6, "plat": 1}
    >>> cles = [cle for cle in dico]
    >>> cles
    ['table', 'assiette', 'couverts', 'plat']
    ```

    On peut obtenir une liste `#!py valeurs` de toutes les valeurs de ce dictionnaire ainsi :

    ```pycon
    >>> dico = {"table": 1, "assiette": 6, "couverts": 6, "plat": 1}
    >>> valeurs = [dico[cle] for cle in dico]
    >>> valeurs
    [1, 6, 6, 1]
    ```

    On peut obtenir **une** liste des couples `(clé: valeur)` de ce dictionnaire ainsi :

    ```pycon
    >>> dico = {"table": 1, "assiette": 6, "couverts": 6, "plat": 1}
    >>> couples = [(cle, dico[cle]) for cle in dico]
    >>> couples
    [('table', 1), ('assiette', 6), ('couverts', 6), ('plat', 1)]
    ```

??? abstract  "Méthodes `#!py keys`, `#!py values` et `#!py items`"

    `#!py keys`, `#!py values` et `#!py items` sont des **vues** du dictionnaire.

    `#!py dico.keys()` permet d'accéder à toutes les clés de `#!py dico`.

    ```pycon
    >>> dico = {"table": 1, "assiette": 6, "couverts": 6, "plat": 1}
    >>> cles = list(dico.keys())
    >>> cles
    ['table', 'assiette', 'couverts', 'plat']
    ```
  
    `#!py dico.values()` permet d'accéder à toutes les valeurs de `#!py dico`.

    ```pycon
    >>> dico = {"table": 1, "assiette": 6, "couverts": 6, "plat": 1}
    >>> valeurs = list(dico.values())
    >>> valeurs
    [1, 6, 6, 1]
    ```

    `#!py dico.items()` permet*  d'accéder à tous les tuples (clé: valeur) de `#!py dico`.

    ```pycon
    >>> dico = {"table": 1, "assiette": 6, "couverts": 6, "plat": 1}
    >>> couples = list(dico.items())
    >>> couples
    [('table', 1), ('assiette', 6), ('couverts', 6), ('plat', 1)]
    >>> for (cle, valeur) in dico.items():
    ...     print("Clé =", cle, " // Valeur =", valeur)
    Clé = table // Valeur = 1
    Clé = assiette // Valeur = 6
    Clé = couverts // Valeur = 6
    Clé = plat // Valeur = 1
    ```