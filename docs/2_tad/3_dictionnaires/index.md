---
title: Dictionnaires
---

???+ note "Site dédié"

    Vous trouverez un cours sur les dictionnaires à [cette adresse](https://nreveret.forge.apps.education.fr/dictionnaires/).

Un dictionnaire est une structure de données permettant le stockage tout comme les tableaux et les listes chaînées. Mais contrairement aux tableaux, les éléments d'un dictionnaires ne sont pas identifiés à l'aide d'un *indice* mais à l'aide d'une *clé*.

Un dictionnaire est donc un ensemble de `{clé : valeur}`.

![Clés et Valeurs](dict.png){width=40% .center .autolight}

???+ abstract  "Rappels des opérations en Python"

    Soit `#!py mon_dico` un dictionnaire. Soit `#!py valeur` la valeur associée à `#!py cle` :

    * `#!py cle in mon_dico` renvoie `#!py True` si la clé `cle` existe dans `mon_dico` et `#!py False` sinon ;

    * `#!py mon_dico[cle]` renvoie la valeur associée à `cle` si elle est présente dans le dictionnaire ;
    
        **Attention**, si la clé n'est pas présente dans le dictionnaire, cette instruction génère une erreur `#!py KeyError` !

    * Si la clé existe déjà `#!py mon_dico[cle] = valeur` modifie la valeur associée ;
    
    * Si la clé n'existe pas, `#!py mon_dico[cle] = valeur` ajoute la paire `#!py (cle, valeur)` ;

    * `#!py del mon_dico[cle]` supprime le couple `(cle, valeur)` de `#!py mon_dico ;
  
    * `#!py mon_dico.pop(cle)` supprime le couple (`cle`, `valeur`) de `#!py mon_dico` et renvoie la valeur associée ;
    
    * `#!py len(mon_dico)` renvoie le nombre de couples `(clé: valeur)` du dictionnaire ;
    
    * Parcours des clés (1) : `#!py for cle in mon_dico:` ;
    
    * Parcours des clés (2) : `#!py for cle in mon_dico.keys():` ;
    
    * Parcours des valeurs : `#!py for valeur in mon_dico.values():` ; 
    
    * Parcours des couples (clé, valeur) : `#!py for (cle, valeur) in mon_dico.items():`.

## Interface

En tant que structure de stockage, le dictionnaire doit implémenter les opérations suivantes :

* *ajouter* : ajouter une nouvelle paire `clé : valeurs` au dictionnaire
* *modifier* : modifier la `valeur` associée à une `clé`. Il n'est pas possible de modifier la `clé` (on doit la supprimer et en créer une nouvelle)
* *supprimer* : supprimer la `clé` (et donc la valeur qui lui est associée)
* *rechercher* : rechercher une `valeur` à partir d'une `clé`

On retrouve les méthodes classiques déjà rencontrées dans les listes chaînées. La différence peut se faire sur le coût des opérations.

## Coût des opérations

Concernant le temps d'accès à un élément précis, considérons le pire des cas : accéder à la dernière valeur de la structure.

* dans le cas d'une liste chaînée, il faut lire l'ensemble des valeurs à partir de la *tête*. La complexité est linéaire.

* dans le cas d'un tableau (mis en œuvre par les `list` en Python), l'accès se fait avec `#!py tab[len(tab) - 1]`. Si les cellules du tableau ont toutes la même taille en bits, la détermination de l'adresse mémoire est facile : il suffit de partir de l'adresse de la première cellule et d'ajouter la taille d'une valeur multipliée par le nombre de cellules $adresse_0 + taille \times indice$. Que le tableau soit petit ou grand, il n'y a que deux opérations (une multiplication et une addition) à faire. Le temps d'accès est donc constant ;

* pour un dictionnaire, le coût d'un accès dépend de son implantation. La plus courante est la table de hachage. Dans ce cas, accéder à la valeur associée à une clé nécessite de déterminer, à l'aide de la clé, l'indice de la cellule contenant cette valeur. Dans le meilleur des cas, ce calcul se fait en temps constant : que la clé ait été ajouté au début ou à la fin lors de la construction, que le dictionnaire contienne peu ou beaucoup de valeurs, ce calcul prend toujours (en moyenne) autant de temps. Le temps d'accès est donc encore constant.

![Comparaison des temps d'accès](recherche.png){width=60% .center .autolight}

La recherche d'une valeur précise permet de distinguer les trois structures :

* chercher une valeur dans un tableau ou une liste chaînée nécessite, dans le pire des cas, de lire l'ensemble des valeurs. La complexité est donc linéaire $\mathcal{O}(n)$ en la taille des données ;
* dans le cas d'un dictionnaire on calcule l'indice de la cellule qui pourrait contenir la valeur cherchée. On vérifie alors directement que celle-ci contient effectivement la valeur souhaitée. Le temps de recherche est donc constant.

Ces informations et d'autres sont résumées dans le tableau ci-dessous précisant les coûts de différentes opérations :

<center>

| Opération              | Liste chaînée | Tableau       | Dictionnaire  |
| :--------------------- | :------------ | :------------ | :------------ |
| Ajout d'une valeur     | Coût linéaire | Coût constant | Coût constant |
| Accès à une valeur     | Coût linéaire | Coût constant | Coût constant |
| Recherche d'une valeur | Coût linéaire | Coût constant | Coût constant |

</center>

## Implantation avec des tables de hachages

L'implémentation des dictionnaires se fait le plus souvent à l'aide de **tables de hachage**. C'est le cas en Python.

![Table de hachage](hash_table.png){width=60% .center .autolight}

L'idée est d'utiliser une **fonction de hachage**. On ne décrira pas ici fonctionnement précis.

???+ note "Fonction de hachage"
    
    Une fonction de hachage :

    * est une fonction prenant en argument la clé du dictionnaire et renvoyant un nombre de taille précise (ce nombre peut ensuite être transformé en un indice dans la table) ;
    
    * essaie au maximum de faire en sorte que deux clés différentes ne soient pas associées au même indice.

Il peut arriver (et il est même certain que cela arrivera !) que deux clés pointent vers deux indices identiques. On parle alors de **collision**. Dans ce cas, l'implémentation de la table de hachage cherche un autre indice disponible (en regardant par exemple si la cellule située juste après est vide).

???+ note "*Hash*"

    Les fonctions de hachage sont très utilisées en informatique. On les retrouve par exemple pour vérifier l'intégrité d'un fichier après un téléchargement (le fichier téléchargé est-il bien celui qui était sur le serveur) ou pour stocker les mots de passe dans une base de données.
    
    En effet il est bien souvent compliqué connaissant le *hash* d'une donnée de retrouver celle-ci : si un pirate récupère la table des mots de passe *hashés*, il ne pourra pas remonter au mots de passe en clair.
