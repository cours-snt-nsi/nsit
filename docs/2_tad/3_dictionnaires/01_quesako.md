---
author: Mireille Coilhac, Nicolas Revéret
title: Premiers pas
---

# 🏁 Premiers pas

## La ferme de Gaston - Listes

Gaston possède des lapins, des vaches, des cochons et des chevaux. Il désire créer un « document » (avec Python 😊) lui permettant de gérer le nombre fluctuant d'animaux. Par exemple, il doit noter qu'il possède 5 lapins, 7 vaches, 2 cochons et 4 chevaux.

Comment peut-il faire ? Quelle structure python que vous connaissez va-t-il pouvoir utiliser ?

???+ success "Solution"

    * On peut imaginer utiliser deux listes :
        ```python
        liste_animaux = ["lapin", "vache", "cochon", "cheval"]
        liste_effectifs = [5, 7, 2, 4]
        ```

    * On peut imaginer utiliser des listes de listes comme par exemple :
        ```python
        ferme_gaston_1 = [["lapin", 5], ["vache", 7], ["cochon", 2], ["cheval", 4]]
        ferme_gaston_2 = [["lapin", "vache", "cochon", "cheval"], [5, 7, 2, 4]]
        ```

    * On peut aussi imaginer utiliser des listes de tuples comme par exemple :
        ```python
        ferme_gaston_3 = [("lapin", 5), ("vache", 7), ("cochon", 2), ("cheval", 4)]
        ```

    * Vous avez peut-être eu d'autres idées... Il est tout à fait possible de faire autrement !


???+ note "Aidez Gaston"
    Finalement, Gaston a choisi la liste de listes :

    ```python
    ferme_gaston_1 = [["lapin", 5], ["vache", 7], ["cochon", 2], ["cheval", 4]]
    ```
    Mais il est distrait, il n'a pas la liste sous les yeux, et ne sait plus combien d'animaux de chaque catégorie !

    Aidez-le  en complétant la fonction suivante qui prend en paramètres la liste `ferme` décrivant une ferme ainsi qu'un nom d'animal.

    Cette fonction renvoie
    le nombre d'animaux présents dans la ferme. Si l'animal n'est pas présent, la fonction renvoie `#!py 0`.

    {{IDE('../scripts/fct_nbre_animaux')}}

    ??? success "Solution"
        ```python
        def nb_animaux(ferme, animal):
            for liste in ferme:
                if liste[0] == animal:
                    return liste[1]
            return 0
        ```

## La ferme de Gaston - Dictionnaires

Gustave vient d'expliquer à Gaston, son ami, qu'il peut utiliser une structure appelée « dictionnaire ». L'efficacité d'une recherche dans un dictionnaire est bien supérieure à celle qu'on obtiendrait avec une liste de listes. C'est un des intérêts majeurs de cette nouvelle structure.

Voilà comment on construit le dictionnaire `#!py ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4}`.

Cela permet d'associer à chaque animal, par exemple à `#!py "vache"`, le nombre correspondant.

Dans cet exemple `#!py "vache"` s'appelle une **clé** et `#!py 7` est la **valeur** qui lui est associée.

!!! abstract  "À retenir"

    Un dictionnaire est constitué de paires `#!py clé : valeur`.

    Il s'écrit avec des accolades et des virgules entre chaque couple : `#!py {clé : valeur, autre_clé : autre_valeur}`.

???+ question

    Donner une autre clé du dictionnaire `#!py ferme_gaston` et la valeur qui lui est associée :

    - « *La clé ... est associée à la valeur ...* ».
   
    - « *La valeur ... est associée à la clé ...* ».

    ??? success "Solution"
        * La clé `#!py "lapin"` est associée à la valeur `#!py 5`.
        * La clé `#!py "cochon"` est associée à la valeur `#!py 2`.
        * La valeur `#!py 4`. est associée à la clé `#!py "cheval"`.


???+ question "Aider Gaston à retrouver la mémoire"

    Tester le script ci-dessous, et ajouter vos propres essais :

    {{IDE('../scripts/ferme_1')}}


## Gaston achète des lapins

Gaston vient d'acheter 6 lapins. Il décide de les ajouter à son dictionnaire :

{{IDE('../scripts/ferme_2')}}

??? question "Que s'est-il passé ?"

    Dans un dictionnaire, il ne peut pas y avoir deux clés identiques. Python n'en a gardé qu'une...

!!! abstract  "À retenir"

    Chaque **clé** est présente de façon **unique** dans le dictionnaire.

## Gaston met à jour son dictionnaire

Gaston a bien compris qu'il ne pouvait pas y avoir deux clés identiques dans son dictionnaire.
Après avoir acheté ses lapins, il va donc tout simplement modifier son dictionnaire, avec l'aide de Gustave.

Tester le script ci-dessous :

{{IDE('../scripts/ferme_3')}}

Il aurait pu tout simplement rajouter 6 lapins. Tester le script ci-dessous :

{{IDE('../scripts/ferme_5')}}


## Gaston utilise un dictionnaire de secours

Gaston est parti vérifier qu'il ne manquait pas d'animaux dans sa ferme, mais il a oublié son dictionnaire chez lui.

Gustave est toujours là, et lui fournit un dictionnaire qu'il a reconstitué de mémoire : `ferme_gaston_secours`

Les dictionnaires suivants sont-ils identiques ?

* `#!py ferme_gaston = {"lapin": 11, "vache": 7, "cochon": 2, "cheval": 4}` et

* `#!py ferme_gaston_secours = {"cochon": 2, "lapin": 11, "cheval": 4, "vache": 7}`.

Tester le script ci-dessous :

{{IDE('../scripts/ferme_4')}}

!!! abstract  "À retenir"

    Un dictionnaire est un ensemble **non ordonné de couples `clé, valeur`**.

    Deux dictionnaires sont **égaux** si et seulement si ils contiennent les mêmes couples `clé, valeur` **peu importe l'ordre**.

## Gaston part au marché

Gaston veut acheter des pommes et des poires au marché. Il est très méthodique et très fier d'avoir appris à utiliser les dictionnaires `Python`.

Il décide donc de constituer un dictionnaire dans lequel il va noter, au fur et à mesures qu'il les voit sur les étalages, les différentes variétés de pommes et de poires avec leur prix au kilo.

???+ question "Couac..."

    Tester le script ci-dessous :

    {{IDE('../scripts/marche_1')}}

    ??? warning "Que s'est-il passé ?"

        On obtient une erreur : `#!py TypeError: unhashable type: 'list'`.

        Les clés d'un dictionnaire doivent être hachables. `Python` doit être capable de calculer un entier (le *hash*) à partir de la valeur de la clé.
        
        Ici on utilise des listes qui sont des objets "mutable" de `Python`. Un tel objet n'est *a priori* pas hachable car en cas de modification, la valeur du *hash* risque d'être elle aussi modifiée.

???+ question "Aidez Gaston"

    Il est impossible d'utiliser des listes comme clés d'un dictionnaires car ce sont des objets mutables.

    Il est par contre possible d'utiliser des chaînes de caractères ou des tuples !

    Créer un dictionnaire valide avec les renseignements notés par Gaston.

    {{IDE('../scripts/marche_2')}}

    ??? success "Solution"

        * Avec des tuples :
        
            ```python
            fruits = {
                ("pomme", "Pink"): 2.1,
                ("poire", "Williams"): 2.5,
                ("pomme", "Golden"): 3.0,
                ("poire", "Conférence"): 2.8,
            }
            ```
        
        * Avec des chaînes de caractères (1) :
            ```python
            fruits = {
                "pomme Pink": 2.1,
                "poire Williams": 2.5,
                "pomme Golden": 3.0,
                "poire Conférence": 2.8,
            }
            ```
        
        * Avec des chaînes de caractères (2) :
            ```python
            fruits = {
                "Pink": ["pomme", 2.1],
                "Williams": ["poire", 2.5],
                "Golden": ["pomme", 3.0],
                "Conférence": ["poire", 2.8],
            }
            ```

        * Vous avez peut-être eu d'autres idées... Il est tout à fait possible de faire autrement !

## Bilan

!!! abstract  "À retenir"

    Un dictionnaire est un ensemble **non ordonné** de couples (clé: valeur) notés `#!py clé: valeur`.

    Chaque **clé** est présente de façon **unique** dans le dictionnaire.

    Un dictionnaire s'écrit avec des **accolades**, les différents couples sont séparés par des **virgules** : `#!py {clé: valeur, autre_clé: autre_valeur, ...}`.

    Afin de pouvoir être hachées, les clés doivent être de types **immuables** : elles ne peuvent pas être modifiées.
    
    On peut choisir comme clé des nombres, des chaînes de caractères, des tuples d'éléments de types immuables,  mais **jamais des listes ni des dictionnaires**.

    En revanche, une valeur peut être de n'importe quel type : nombre, liste, chaîne de caractères, tuple, ou même dictionnaire.

!!! warning "Attention : les dictionnaires sont mutables"

    A l'instar des listes, `Python` autorise de modifier la valeur d'un dictionnaire au fil d'un programme. On dit que ce sont des objets mutables.
    
    Cet aspect permet de mettre à jour des valeurs ce qui est très pratique. Il faut toutefois se méfier de ce comportement lorsque l'on effectue des copies de dictionnaires. Observez le comportement de ce script :

    <iframe src="https://pythontutor.com/visualize.html#code=%23%20Cr%C3%A9ation%20d'un%20dictionnaire%0Adico%20%3D%20%7B1%3A%20%22un%22,%202%3A%20%22deux%22%7D%0A%0A%23%20On%20le%20copie%20dansvers%20un%20autre%20dictionnaire%0Aautre_dico%20%3D%20dico%0A%0A%23%20On%20met%20%C3%A0%20jour%20l'autre%20dictionnaire%0Aautre_dico%5B3%5D%20%3D%20%22trois%22%0A%0Aprint%28dico%29&cumulative=false&heapPrimitives=nevernest&mode=edit&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false" width="100%" height="600" frameborder="0"></iframe>

    L'explication est simple : `dico` et `autre_dico` sont un seul et même objet `Python` (ils ont le même identifiant) :
    
    ```pycon
    >>> dico = {1: "un", 2: "deux"}
    >>> autre_dico = dico
    >>> id(dico)
    139633253889600
    >>> id(autre_dico)
    139633253889600
    ```

