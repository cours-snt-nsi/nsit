class Maillon:
    """
    Classe décrivant les maillons utilisés dans les files
    Un maillon possède :
        - une donnée (de type quelconque)
        - un successeur (un autre élément)
    """

    def __init__(self, donnee):
        self.donnee = donnee
        self.successeur = None

    def __repr__(self):
        """Affichage"""
        resultat = f"{self.donnee}"
        if self.successeur is not None:
            resultat += f", {self.successeur}"
        return resultat


class File:
    """
    Classe décrivant une file
    La file est caractérisée par sa tête (pour le défilement)
    et sa queue (pour l'enfilement)
    """

    def __init__(self):
        """Constructeur : renvoie la file vide"""
        self.tete = None
        self.queue = None

    def est_vide(self):
        """Détermine si la file est vide ou non"""
        return self.tete is None

    def enfile(self, x):
        """Enfile la valeur x"""
        # Cas particulie : la file est vide
        nouveau = Maillon(x)
        if self.est_vide():
            self.queue = nouveau
            self.tete = nouveau
        else:
            self.queue.successeur = nouveau
            self.queue = nouveau

    def defile(self):
        """Supprime et renvoie la valeur en tête de la file"""
        if self.est_vide():
            raise ValueError("La file est vide")

        # Cas particulier : la file ne contient qu'un élément
        if self.tete.successeur is None:
            maillon = self.tete
            self.tete = None
            self.queue = None
            return maillon.donnee

        maillon = self.tete
        self.tete = self.tete.successeur
        return maillon.donnee

    def __repr__(self):
        """Affichage"""
        if self.est_vide():
            return "(Tête) [] (Queue)"

        return f"(Tête) [{self.tete}] (Queue)"


file = File()
print(file)

file.enfile(1)
print(file)

file.enfile(2)
print(file)

file.enfile(3)
print(file)

element = file.defile()
print(element)
print(file)
