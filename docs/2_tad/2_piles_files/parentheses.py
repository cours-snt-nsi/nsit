OUVRANTS = {
    "(": ")",
    "[": "]",
    "{": "}"
}

FERMANTS = {
    ")": "(",
    "]": "[",
    "}": "{"
}

def parenthesage_correct(expression) :
    """Détermine si une expression est bien formée du point de vue des parenthèses"""
    pile = []

    for caractere in expression :
        if caractere in OUVRANTS:
            pile.append(caractere)  # on empile les ouvrants
        elif caractere in FERMANTS:
            if pile == []:  # la pile est vide : il y a trop de fermants
                return False
            else:
                dernier_ouvrant = pile.pop()
                if FERMANTS[caractere] != dernier_ouvrant:  # le fermant ne correspond pas à l'ouvrant
                    return False
    return pile == []  # la pile doit être vide

expression = "()"
assert parenthesage_correct(expression) is True

expression = "([])"
assert parenthesage_correct(expression) is True

expression = "({}[])"
assert parenthesage_correct(expression) is True

expression = "([]))"  # trop de fermants
assert parenthesage_correct(expression) is False

expression = "(}"  # fermant incorrect
assert parenthesage_correct(expression) is False

expression = "(()"  # trop d'ouvrants
assert parenthesage_correct(expression) is False