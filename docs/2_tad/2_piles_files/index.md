---
title: Piles et files
---

Voyez-vous le point commun entre la façon dont vous avez rangé votre lave-vaisselle et la gestion de l'historique de votre navigateur ? Entre la façon dont vous avez rangé vos courses dans votre réfrigérateur et la façon dont votre ordinateur choisit à quel processus allouer du temps de calcul ?

![Pile et File](Stack-and-Queue.png){width=30% .center .autolight}

## Les Piles

### Définition et utilisation

???+ info "Pile"

    Une ***pile***, ***stack*** en anglais (comme [Stack Overflow !](https://stackoverflow.com/)), est une structure de données reposant sur le modèle du ***Last In, First Out*** (***LIFO***), ou ***Dernier Entré, Premier Sorti*** (***DEPS***) en français.

    ![Pile en programmation](stack.png){width=50% .center .autolight}
    
La façon la plus simple de se représenter une *pile* est une pile (!) d'assiettes : lorsqu'on les range, on empile les assiettes les unes sur les autres. Lorsque l'on met la table, on pioche dans la pile en commençant par le dessus et donc par la dernière assiette rangée !

![Pile d'assiette](pile_assiette.png){width=50% .center .autolight}

???+ info "Méthodes primitives"

    Les méthodes primitives essentielles d'une *pile* sont :

    * `creation` : crée et retourne une *pile* vide ;
    * `est_vide` : indique si la *pile* est vide ou non ;
    * `empiler` : ajoute un élément en haut de la *pile*. Les anglo-saxons disent *push* ;
    * `depiler` : retire, et retourne, le premier élément de la *pile*. Les anglo-saxons disent *pop*.

    Il est aussi possible d'ajouter les méthodes suivantes mais qui ne sont pas indispensables :

    * `taille` : retourne le nombre d'éléments dans la *pile* ;
    * `tete` : retourne le premier élément sans le dépiler ;
    * `vider` : dépile tous les éléments.

### Implantation

Il existe plusieurs implémentations des *piles*. 

Le plus simple en Python est d'utiliser des objets du type `list` comme illustré ci-dessous.

{{ IDE('pile_list.py')}}

On peut aussi utiliser des listes chaînées :

{{ IDE('Pile.py')}}

### Utilisation


La *pile* intervient très souvent dans la vie quotidienne et en programmation :

* la façon dont un professeur pioche la prochaine copie à corriger (le dernier élève à rendre est le premier corrigé !) ;

* la gestion d'un camion de livraison : on charge dans l'ordre inverse de la livraison, le dernier carton chargé est le  ;premier livré

* le ``ctrl-Z`` de l'ordinateur ;

* la gestion de l'historique de navigation sur internet ;

* déterminer si une expression compte le bon nombre de parenthèses (on empile les ouvrantes et dépile les fermantes, voir plus bas).

Une utilisation classique permet de vérifier qu'une expression est bien parenthésée. On considère une expression contenant différents types de *délimiteurs*, des *ouvrants*  et des *fermants*. Chaque ouvrant est associé à un fermant précis.

Une expression est *bien parenthésée* si, pour chaque catégorie de délimiteur :

* la chaîne compte autant d'ouvrants que de fermants de cette catégorie ;
* à chaque emplacement de l'expression, le nombre d'ouvrant de cette catégorie est supérieur ou égal ou nombre de fermants.

{{ IDE('parentheses.py')}}

## Les Files

### Définition et utilisation

???+ info "File"

    Une ***file***, ***queue*** en anglais, est une structure de données reposant sur le modèle du ***First In, First Out*** (***FIFO***), ou ***Premier Entré, Premier Sorti*** (***PEPS***) en français.

    ![File d'éléments](Data_Queue.png){width=30% .center .autolight}

La façon la plus simple de se représenter une *file* est la queue à un guichet : la première personne arrivée sera la première servie.


![File d'attente](file.png){width=40% .center }

???+ info "Méthodes primitives"

    Les méthodes primitives essentielles d'une *file* sont :

    * `creation` : crée et retourne une *file* vide ;
    * `est_vide` : indique si la *file* est vide ou non ;
    * `enfiler` : ajoute un élément à la queue de la *file*. Les anglo-saxons disent *enqueue* ;
    * `defiler` : retire, et retourne, l'élément à la tête de la *file*. Les anglo-saxons disent *dequeue*.

### Implantation

Là encore, il existe plusieurs implantations.

La plus rudimentaire en Python est d'utiliser le type `list`. Notons immédiatement que cette façon de faire n'est pas efficace du fait du `file.pop(0)` dont le coût est linéaire en la longueur de la file.

{{ IDE('file_list.py')}}

Une autre solution est d'utiliser des listes chaînées :

{{ IDE('File.py')}}

Notons enfin que Python propose dans le module `collections`, la classe `deque` (pour *double ended queue*) qui permet de mettre en oeuvre efficacement les files.

{{ IDE('file_deque.py')}}

### Utilisation

Voici des exemple d'utilisation de *files* :

* file d'attente ;

* gestion des appels dans un standard ;

* gestion des tâches par le processeur ;

* tri des paquets lors d'une transmission asynchrone (les paquets des différents messages arrivent dans le désordre, ils sont réassemblés au fur et à mesure dans leur ordre d'arrivée).

On peut aussi, en utilisant une pile et une file, tester si un chaîne est un palindrome[^1] :

[^1]: un palindrome est une chaîne de caractères se lisant de la même façon de gauche à droite et de droite à gauche. Par exemple `#!py "kayak"`.

* on remplit une pile et une file avec les caractères dans l'ordre de lecture ;
* on dépile un caractère et on défile un caractère puis on les compare. Les structures de la file et de la pile assurent que l'on compare des éléments provenant du début (pour la file) ou de la fin (pour la pile) de la chaîne. Ces caractères doivent donc être égaux ;
* si on arrive à la fin de la pile/file sans erreur c'est que la chaîne est un palindrome.

{{ IDE("palindrome.py")}}