# Création d'une file vide
file = []
print(f"(Tête) {file} (Queue)")

# Enfilement de valeurs
file.append(1)
print(f"(Tête) {file} (Queue)")
file.append(2)
print(f"(Tête) {file} (Queue)")
file.append(3)
print(f"(Tête) {file} (Queue)")

# défilement d'une valeur
premier = file.pop(0)  # opération coûteuse !
print(premier)
print(f"(Tête) {file} (Queue)")