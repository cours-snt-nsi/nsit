class Maillon:
    """
    Classe décrivant les maillons utilisés dans les piles
    Un maillon possède :
        - une donnée (de type quelconque)
        - un successeur (un autre élément)
    """

    def __init__(self, donnee):
        self.donnee = donnee
        self.successeur = None

    def __repr__(self):
        """Affichage"""
        resultat = f"{self.donnee}"
        if self.successeur is not None:
            resultat = f"{self.successeur}, " + resultat
        return resultat


class Pile:
    """
    Classe décrivant une pile
    La pile est caractérisée par sa seule tête
    """

    def __init__(self):
        """Constructeur : renvoie la pile vide"""
        self.tete = None

    def est_vide(self):
        """Détermine si la pile est vide ou non"""
        return self.tete is None

    def empile(self, x):
        """Empile la valeur x"""

        nouveau = Maillon(x)
        nouveau.successeur = self.tete
        self.tete = nouveau

    def depile(self):
        """Supprime et renvoie la valeur en haut de la pile"""
        if self.est_vide():
            raise ValueError("La pile est vide")

        maillon = self.tete
        self.tete = self.tete.successeur
        return maillon.donnee

    def __repr__(self):
        """Affichage"""
        if self.est_vide():
            return "(Bas) [] (Haut)"

        return f"(Bas) [{self.tete}] (Haut)"


pile = Pile()
print(pile)

pile.empile(1)
print(pile)

pile.empile(2)
print(pile)

pile.empile(3)
print(pile)

element = pile.depile()
print(element)
print(pile)
