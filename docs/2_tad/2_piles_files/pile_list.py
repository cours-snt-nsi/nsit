# Création d'une pile vide
pile = []
print(f"(Bas) {pile} (Haut)")

# Empilement de valeurs
pile.append(1)
print(f"(Bas) {pile} (Haut)")
pile.append(2)
print(f"(Bas) {pile} (Haut)")
pile.append(3)
print(f"(Bas) {pile} (Haut)")

# dépilement d'une valeur
dernier = pile.pop()
print(dernier)