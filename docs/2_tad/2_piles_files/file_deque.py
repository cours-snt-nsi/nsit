from collections import deque

# Création d'une file vide
file = deque([])
print(f"(Tête) {list(file)} (Queue)")

# Enfilement de valeurs
file.append(1)
print(f"(Tête) {list(file)} (Queue)")
file.append(2)
print(f"(Tête) {list(file)} (Queue)")
file.append(3)
print(f"(Tête) {list(file)} (Queue)")

# défilement d'une valeur
premier = file.popleft()  # opération efficace
print(premier)
print(f"(Tête) {list(file)} (Queue)")
