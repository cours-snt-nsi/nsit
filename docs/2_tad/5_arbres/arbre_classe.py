class Noeud:
    def __init__(self, valeur, enfants=None):
        self.valeur = valeur
        if enfants is None:
            enfants = []
        self.enfants = enfants

    def __repr__(self):
        return f"[{self.valeur}, {[self.enfants]}]"


class Arbre:
    def __init__(self, racine):
        self.racine = racine

    def __repr__(self):
        return f"{self.racine}"


print(
    r"""
     a
    /  \
   b    c
 / | \   \
d  e  f   g
"""
)
d = Noeud("d")
e = Noeud("e")
f = Noeud("f")
g = Noeud("g")
b = Noeud("b", [d, e, f])
c = Noeud("c", [g])
a = Noeud("a", [b, c])

arbre = Arbre(a)

# l'arbre
print(arbre)

# la valeur de la racine
print(arbre.racine.valeur)

# les enfants de la racine
print(arbre.racine.enfants)
