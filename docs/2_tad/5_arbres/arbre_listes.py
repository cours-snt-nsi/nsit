print(
    r"""
     a
    /  \
   b    c
 / | \   \
d  e  f   g
"""
)

d = ["d", []]
e = ["e", []]
f = ["f", []]
g = ["g", []]
b = ["b", [d, e, f]]
c = ["c", [g]]
arbre = ["a", [b, c]]


# l'arbre
print(arbre)

# la valeur de la racine
print(arbre[0])

# les enfants de la racine
print(arbre[1])
