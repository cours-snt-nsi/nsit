def hauteur(arbre):
    """Renvoie la hauteur d'un arbre non vide passé sous la forme [valeur, liste des enfants]"""
    _, enfants = arbre
    if enfants == []:
        return 1
    
    return 1 + max([hauteur(sous_arbre) for sous_arbre in enfants])

r"""
     a
    /  \
   b    c
 / | \   
d  e  f
   |
   g
"""

g = ["g", []]
d = ["d", []]
e = ["e", [g]]
f = ["f", []]
b = ["b", [d, e, f]]
c = ["c", []]
arbre = ["a", [b, c]]

assert hauteur(arbre) == 4