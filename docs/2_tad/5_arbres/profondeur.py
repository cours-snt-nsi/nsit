def prefixe(arbre):
    valeur, enfants = arbre
    resultat = [valeur]
    for sous_arbre in enfants:
        resultat += prefixe(sous_arbre)
    return resultat


def suffixe(arbre):
    valeur, enfants = arbre
    resultat = []
    for sous_arbre in enfants:
        resultat += suffixe(sous_arbre)
    resultat += [valeur]
    return resultat


r"""
     a
    /  \
   b    c
 / | \   
d  e  f
   |
   g
"""

g = ["g", []]
d = ["d", []]
e = ["e", [g]]
f = ["f", []]
b = ["b", [d, e, f]]
c = ["c", []]
arbre = ["a", [b, c]]

print(prefixe(arbre))
print(suffixe(arbre))
