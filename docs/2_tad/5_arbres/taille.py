def taille(arbre):
    """Renvoie la taille d'un arbre non vide passé sous la forme [valeur, liste des enfants]"""
    valeur, enfants = arbre
    if enfants == []:
        return 1
    
    t = 1
    for sous_arbre in enfants:
        t += taille(sous_arbre)
    return t 

r"""
     a
    /  \
   b    c
 / | \   \
d  e  f   g
"""

d = ["d", []]
e = ["e", []]
f = ["f", []]
g = ["g", []]
b = ["b", [d, e, f]]
c = ["c", [g]]
arbre = ["a", [b, c]]

assert taille(arbre) == 7