# --- PYODIDE:env --- #
class Maillon_File:
    """
    Classe décrivant les maillons utilisés dans les files
    Un maillon possède :
        - une donnée (de type quelconque)
        - un successeur (un autre élément)
    """

    def __init__(self, donnee):
        self.donnee = donnee
        self.successeur = None

    def __repr__(self):
        """Affichage"""
        resultat = f"{self.donnee}"
        if self.successeur is not None:
            resultat += f", {self.successeur}"
        return resultat


class File:
    """
    Classe décrivant une file
    La file est caractérisée par sa tête (pour le défilement)
    et sa queue (pour l'enfilement)
    """

    def __init__(self):
        """Constructeur : renvoie la file vide"""
        self.tete = None
        self.queue = None

    def est_vide(self):
        """Détermine si la file est vide ou non"""
        return self.tete is None

    def enfile(self, x):
        """Enfile la valeur x"""
        # Cas particulie : la file est vide
        nouveau = Maillon_File(x)
        if self.est_vide():
            self.queue = nouveau
            self.tete = nouveau
        else:
            self.queue.successeur = nouveau
            self.queue = nouveau

    def defile(self):
        """Supprime et renvoie la valeur en tête de la file"""
        if self.est_vide():
            raise ValueError("La file est vide")

        # Cas particulier : la file ne contient qu'un élément
        if self.tete.successeur is None:
            maillon = self.tete
            self.tete = None
            self.queue = None
            return maillon.donnee

        maillon = self.tete
        self.tete = self.tete.successeur
        return maillon.donnee

    def __repr__(self):
        """Affichage"""
        if self.est_vide():
            return "(Tête) [] (Queue)"

        return f"(Tête) [{self.tete}] (Queue)"


# --- PYODIDE:code --- #
# Les files sont déjà importées


class ArbreVide:
    def __init__(self):
        pass

    def est_vide(self):
        return True

    def taille(self):
        return 0

    def hauteur(self):
        return 0

    def largeur(self):
        return []

    def prefixe(self):
        return []

    def infixe(self):
        return []

    def suffixe(self):
        return []

    def __repr__(self):
        return "∅"


class ArbreBinaire:
    def __init__(self, valeur, gauche=None, droite=None):
        self.valeur = valeur
        if gauche is None:
            gauche = ArbreVide()
        if droite is None:
            droite = ArbreVide()
        self.gauche = gauche
        self.droite = droite

    def est_vide(self):
        return False

    def taille(self):
        return 1 + self.gauche.taille() + self.droite.taille()

    def hauteur(self):
        return 1 + max(self.gauche.hauteur(), self.droite.hauteur())

    def largeur(self):
        file = File()
        resultat = [self.valeur]
        file.enfile(self.gauche)
        file.enfile(self.droite)
        while not file.est_vide():
            sous_arbre = file.defile()
            if not sous_arbre.est_vide():
                resultat.append(sous_arbre.valeur)
                file.enfile(sous_arbre.gauche)
                file.enfile(sous_arbre.droite)
        return resultat

    def prefixe(self):
        return (
            [self.valeur]
            + self.gauche.prefixe()
            + self.droite.prefixe()
        )

    def infixe(self):
        return (
            self.gauche.infixe()
            + [self.valeur]
            + self.droite.infixe()
        )

    def suffixe(self):
        return (
            self.gauche.suffixe()
            + self.droite.suffixe()
            + [self.valeur]
        )

    def __repr__(self):
        return f"{self.gauche, self.valeur, self.droite}"


r"""
    a
   /  \
  b    c
 / \    \ 
d   f    g
"""

d = ArbreBinaire("d")
f = ArbreBinaire("f")
g = ArbreBinaire("g")
b = ArbreBinaire("b", d, f)
c = ArbreBinaire("c", ArbreVide(), g)
a = ArbreBinaire("a", b, c)

print(a)
print(a.largeur())
print(a.prefixe())
print(a.infixe())
print(a.suffixe())
