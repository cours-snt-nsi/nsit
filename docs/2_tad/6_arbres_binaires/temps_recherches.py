import matplotlib.pyplot as plt
from time import perf_counter
from statistics import mean
from arbre_binaire_recherche_classe import ArbreBinaireRecherche
from random import shuffle

repetition = 500

x = []
t_liste = []
t_ABR = []

for n in range(100, 10000, 100) :
    x.append(n)

    # création de la liste
    liste = [k for k in range(n)]

    shuffle(liste)

    # création de l'ABR
    arbre = ArbreBinaireRecherche(liste[0])
    for valeur in liste[1:] :
        arbre.insere(valeur)

    t_s = []
    for _ in range(repetition) :
        t_start = perf_counter()
        for valeur in liste :
            if valeur == liste[-1] :
                break
        t_end = perf_counter()
        t_s.append(t_end - t_start)
    t_liste.append(mean(t_s))

    t_s = []
    for _ in range(repetition) :
        t_start = perf_counter()
        arbre.contient(liste[-1])
        t_end = perf_counter()
        t_s.append(t_end - t_start)
    t_ABR.append(mean(t_s))



fig, ax = plt.subplots()

ax.plot(x, t_liste, label="Liste python")
ax.plot(x, t_ABR, label="Arbre binaire de recherche")
plt.title("Temps de recherche d'une valeur")
plt.xlabel("Taille des données")
plt.ylabel("Temps en µs")
plt.legend()
plt.show()