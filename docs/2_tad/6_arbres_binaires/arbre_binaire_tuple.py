# --- PYODIDE:env --- #
class Maillon_File:
    """
    Classe décrivant les maillons utilisés dans les files
    Un maillon possède :
        - une donnée (de type quelconque)
        - un successeur (un autre élément)
    """

    def __init__(self, donnee):
        self.donnee = donnee
        self.successeur = None

    def __repr__(self):
        """Affichage"""
        resultat = f"{self.donnee}"
        if self.successeur is not None:
            resultat += f", {self.successeur}"
        return resultat


class File:
    """
    Classe décrivant une file
    La file est caractérisée par sa tête (pour le défilement)
    et sa queue (pour l'enfilement)
    """

    def __init__(self):
        """Constructeur : renvoie la file vide"""
        self.tete = None
        self.queue = None

    def est_vide(self):
        """Détermine si la file est vide ou non"""
        return self.tete is None

    def enfile(self, x):
        """Enfile la valeur x"""
        # Cas particulie : la file est vide
        nouveau = Maillon_File(x)
        if self.est_vide():
            self.queue = nouveau
            self.tete = nouveau
        else:
            self.queue.successeur = nouveau
            self.queue = nouveau

    def defile(self):
        """Supprime et renvoie la valeur en tête de la file"""
        if self.est_vide():
            raise ValueError("La file est vide")

        # Cas particulier : la file ne contient qu'un élément
        if self.tete.successeur is None:
            maillon = self.tete
            self.tete = None
            self.queue = None
            return maillon.donnee

        maillon = self.tete
        self.tete = self.tete.successeur
        return maillon.donnee

    def __repr__(self):
        """Affichage"""
        if self.est_vide():
            return "(Tête) [] (Queue)"

        return f"(Tête) [{self.tete}] (Queue)"


# --- PYODIDE:code --- #
# Les files sont déjà importées


def est_vide(arbre):
    return arbre is None


def taille(arbre):
    if est_vide(arbre):
        return 0
    sag, _, sad = arbre
    return 1 + taille(sag) + taille(sad)


def hauteur(arbre):
    if est_vide(arbre):
        return 0
    sag, _, sad = arbre
    return 1 + max(hauteur(sag), hauteur(sad))


def largeur(arbre):
    if est_vide(arbre):
        return []
    file = File()
    resultat = []
    file.enfile(arbre)
    while not file.est_vide():
        arbre = file.defile()
        if not est_vide(arbre):
            sag, valeur, sad = arbre
            resultat.append(valeur)
            file.enfile(sag)
            file.enfile(sad)
    return resultat


def prefixe(arbre):
    if arbre is None:
        return []
    sag, valeur, sad = arbre
    return [valeur] + prefixe(sag) + prefixe(sad)


def infixe(arbre):
    if arbre is None:
        return []
    sag, valeur, sad = arbre
    return infixe(sag) + [valeur] + infixe(sad)


def suffixe(arbre):
    if arbre is None:
        return []
    sag, valeur, sad = arbre
    return suffixe(sag) + suffixe(sad) + [valeur]


r"""
    a
   /  \
  b    c
 / \    \ 
d   f    g
"""

arbre = (
    ((None, "d", None), "b", (None, "f", None)),
    "a",
    (None, "c", (None, "g", None)),
)

assert taille(arbre) == 6
assert hauteur(arbre) == 3

print(largeur(arbre))
print(prefixe(arbre))
print(infixe(arbre))
print(suffixe(arbre))
