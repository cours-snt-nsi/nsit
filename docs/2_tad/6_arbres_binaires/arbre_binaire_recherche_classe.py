# --- PYODIDE:env --- #
class ArbreVide:
    def __init__(self):
        pass

    def est_vide(self):
        return True

    def taille(self):
        return 0

    def hauteur(self):
        return 0

    def largeur(self):
        return []

    def prefixe(self):
        return []

    def infixe(self):
        return []

    def suffixe(self):
        return []

    def __repr__(self):
        return "∅"

# --- PYODIDE:code --- #
# La classe ArbreVide est déjà importée

# On a allégé le code en supprimant certains parcours

class ArbreBinaireRecherche:
    def __init__(self, valeur, gauche=None, droite=None):
        self.valeur = valeur
        if gauche is None:
            gauche = ArbreVide()
        if droite is None:
            droite = ArbreVide()
        self.gauche = gauche
        self.droite = droite

    def est_vide(self):
        return False

    def insere(self, x):
        if x < self.valeur:
            if self.gauche.est_vide():
                self.gauche = ArbreBinaireRecherche(x)
            else:
                self.gauche.insere(x)
        elif x > self.valeur:
            if self.droite.est_vide():
                self.droite = ArbreBinaireRecherche(x)
            else:
                self.droite.insere(x)
    
    def contient(self, x):
        if x < self.valeur:
            if self.gauche.est_vide():
                return False
            else:
                return self.gauche.contient(x)
        elif x > self.valeur:
            if self.droite.est_vide():
                return False
            else:
                return self.droite.contient(x)
        else:
            return True

    def infixe(self):
        return (
            self.gauche.infixe()
            + [self.valeur]
            + self.droite.infixe()
        )

    def __repr__(self):
        return f"{self.gauche, self.valeur, self.droite}"


from random import sample

nombres = sample(list(range(100)), 10)
nombres_presents = nombres.copy()

abr = ArbreBinaireRecherche(nombres.pop())
while nombres != []:
    abr.insere(nombres.pop())
    
print(abr)
print(abr.infixe())

for x in nombres_presents:
    assert abr.contient(x) is True
    
assert abr.contient(200) is False
