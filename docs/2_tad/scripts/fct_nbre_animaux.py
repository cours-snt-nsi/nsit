def nb_animaux(ferme, animal):
    ...


ferme_gaston = [["lapin", 5], ["vache", 7], ["cochon", 2], ["cheval", 4]]
assert nb_animaux(ferme_gaston, "vache") == 7
assert nb_animaux(ferme_gaston, "castor") == 0

print("Bravo !")
