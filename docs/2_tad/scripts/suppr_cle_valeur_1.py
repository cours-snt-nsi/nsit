ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4}

# Suppression du couple associé à la clé "cochon"
del ferme_gaston["cochon"]

# Affichage (pour vérifier)
print(ferme_gaston)