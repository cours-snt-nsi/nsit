def histogramme(caracteres):
    ...

# Tests
assert histogramme("brontosaurus") == {'b': 1, 'r': 2, 'o': 2, 'n': 1,
                                       't': 1, 's': 2, 'a': 1, 'u': 2}

