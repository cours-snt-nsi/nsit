def nombre_recettes(recettes, ingredient):
    ...


mes_recettes = {
    "Gâteau au chocolat": ["chocolat", "oeuf", "farine", "sucre", " beurre"],
    "Gâteau au yaourt": ["yaourt", "oeuf", "farine", "sucre"],
    "Crêpes": ["oeuf", "farine", "lait", "bière"],
    "Quatre-quarts": ["oeuf", "farine", "beurre", "sucre"],
    "Kouign amann": ["farine", "beurre", "sucre"],
}

# Tests
assert nombre_recettes(mes_recettes, "sucre") == 4
