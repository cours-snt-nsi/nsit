def intitule(les_notes, note):
    ...


notes_Alice = {"test_1": 14, "test_2": 16, "test_3": 18}

# Tests
assert intitule(notes_Alice, 18) == "test_3"
assert intitule(notes_Alice, 20) == "Cette note n'a pas été obtenue"
