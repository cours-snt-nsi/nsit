class Maillon:
    """
    Classe décrivant les maillons utilisés dans les listes châinées
    Un maillon possède :
        - une donnée (de type quelconque)
        - un successeur (un autre élément)
    """

    def __init__(self, donnee):
        self.donnee = donnee
        self.successeur = None

    def __repr__(self):
        """Affichage"""
        resultat = f"{self.donnee}"
        if self.successeur is not None:
            resultat+= f" --> {self.successeur}"
        return resultat


class Liste_Chainee:
    """
    Classe décrivant une liste simplement chaînée
    La liste est caractérisée par sa seule tête
    """

    def __init__(self):
        """Constructeur : renvoie la liste vide"""
        self.tete = None

    def est_vide(self):
        """Détermine si la liste est vide ou non"""
        return self.tete is None

    def inserer(self, nouveau):
        """Insertion d'un élément à la tête de la liste"""
        # précondition
        assert isinstance(nouveau, Maillon), "nouveau doit être un Maillon"
            
        nouveau.successeur = self.tete
        self.tete = nouveau

    def supprime_tete(self):
        """Supprime et renvoie le maillon de tête"""
        if self.est_vide():
            raise ValueError("La liste est vide")

        maillon = self.tete
        self.tete = self.tete.successeur
        maillon.successeur = None
        return maillon      
    
    def supprime_maillon(self, donnee):
        """Supprime et renvoie le premier maillon rencontré contenant la donnée indiquée
        Génère une erreur si aucun maillon ne contient cette donnée
        """
        if self.est_vide():
            raise ValueError("Aucun maillon ne contient cette donnée")
        
        # Cas particulier où il faut supprimer la tête
        if self.tete.donnee == donnee:
            return self.supprime_tete()
        
        actuel = self.tete
        while actuel.successeur is not None:
            # Le successeur de l'élément actuel est à supprimer
            if actuel.successeur.donnee == donnee:
                maillon = actuel.successeur
                # On saute le maillon à supprimer
                actuel.successeur = actuel.successeur.successeur
                maillon.successeur = None
                return maillon
            actuel = actuel.successeur
        
        raise ValueError("Aucun maillon ne contient cette donnée")

    def __repr__(self):
        """Affichage"""
        if self.est_vide():
            return "La liste est vide"

        return f"{self.tete}"


liste = Liste_Chainee()
print(liste)

elt1 = Maillon(1)
liste.inserer(elt1)
print(liste)

elt2 = Maillon(2)
liste.inserer(elt2)
print(liste)

elt3 = Maillon(3)
liste.inserer(elt3)
print(liste)

premier = liste.supprime_tete()
print(premier.donnee)
print(liste)

un = liste.supprime_maillon(1)
print(un.donnee)
print(liste)
