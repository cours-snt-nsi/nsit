# --- PYODIDE:code --- #

def parcours_profondeur(graphe, depart, deja_vu):
    resultat = [depart]
    deja_vu[depart] = True
    for voisin in graphe[depart]:
        if not deja_vu[voisin]:
            resultat += parcours_profondeur(graphe, voisin, deja_vu)
    return resultat

graphe = {
    "A" : ["B", "C", "D"],
    "B" : ["A", "D", "E"],
    "C" : ["A", "D"],
    "D" : ["A", "B", "C", "E"],
    "E" : ["B", "D"]
}

for sommet in graphe:
    deja_vu = {s: False for s in graphe}  # nouveau parcours -> nouveau dictionnaire deja_vus
    print(f"Le parcours en profondeur issu de {sommet} est {parcours_profondeur(graphe, sommet, deja_vu)}")