def indice(s, sommets):
    """
    Renvoie l'indice de s dans la liste des sommets
    Provoque une erreur si s n'est pas dans sommets
    """
    for i in range(len(sommets)):
        if sommets[i] == s:
            return i
    raise ValueError(f"{s} n'est pas dans la liste {sommets = }")


sommets = ["A", "B", "C", "D", "E"]

matrice_adj = [
    [0, 1, 1, 1, 0],
    [1, 0, 0, 1, 1],
    [1, 0, 0, 1, 0],
    [1, 1, 1, 0, 1],
    [0, 1, 0, 1, 0],
]

# Les sommets du graphes
print(sommets)

# nombre de sommets
taille = len(sommets)
print(taille)

# Les voisins de A
indice_A = indice("A", sommets)
voisins_A = [sommets[i] for i in range(len(taille)) if matrice_adj[indice_A][i]]
print(voisins_A)

# Le degré de A
degre_A = sum(matrice_adj[indice_A])
print(degre_A)
