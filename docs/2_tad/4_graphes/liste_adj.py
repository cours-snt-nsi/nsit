liste_adj = {
    "A" : ["B", "C", "D"],
    "B" : ["A", "D", "E"],
    "C" : ["A", "D"],
    "D" : ["A", "B", "C", "E"],
    "E" : ["B", "D"]
}

# Les sommets du graphes
sommets = [s for s in liste_adj]
print(sommets)

# nombre de sommets
taille = len(sommets)
print(taille)

# Les voisins de A
voisins_A = liste_adj["A"]
print(voisins_A)

# Le degré de A
degre_A = len(liste_adj["A"])
print(degre_A)