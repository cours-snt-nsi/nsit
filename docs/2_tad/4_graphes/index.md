---
title: Les graphes
---

En 1929, le hongrois Frigyes Karinthy énonce l'hypothèse suivante : « *Toute personne sur le globe peut en contacter une autre en s'adressant à moins de six individus* ». Dans les années 1960, le psychologue américaine Stanley Milgram (celui de l'[expérience](https://fr.wikipedia.org/wiki/Exp%C3%A9rience_de_Milgram)) teste l'hypothèse : il fournit à des volontaires des courriers à faire suivre à une personne qu'**ils ne connaissent pas** vivant à 2500 km de chez elles et en ne les autorisant simplement à les faire passer à une personne **de leur connaissance**... La première lettre arriva après quatre jours. En comptant les intermédiaires il apparaît qu'en moyenne, les lettres étaient passées par 5 intermédiaires !

![Les six degrés](6.jpg){width=40% .center .autolight}

Ce nombre fut confirmé par des études plus récentes sur les réseaux sociaux...

Quel rapport avec un cours d'informatique ? Ces expériences portent sur les réseaux, sur le *degré de connexion* du réseau des communications humaines.

L'outil conceptuel permettant de modéliser ces objets est le **graphe**.

## Théorie et vocabulaire

???+ info "Définition"

    Un *graphe* est constitué de sommets et d'arêtes les reliant (*nodes* et *edges* en anglais). Les sommets peuvent contenir des valeurs (on parle aussi d'*étiquettes*).
    
    Il s'agit donc de deux ensembles :
    
    * un ensemble $S$ contenant les sommets ;
    * un ensemble $A$ contenant les couples de sommets reliés par des arêtes.

Par exemple, le graphe de la figure ci-dessous correspond aux ensembles $S = \{E, F, G, H\}$ et $A = \{(E, F), (E, G), (E, H), (F, H), (G, H)\}$.

<center>

```mermaid
flowchart LR
    E(E) --- F(F)
    E --- G(G)
    E --- H(H)
    G --- H
    F --- H
```
</center>

Ce graphe possède 4 sommets et 5 arêtes.

???+ info "Graphe simple"

    Un graphe est dit *simple* s'il n'existe pas :
    
    * d'arête liant un sommet à lui-même ;
    * de multiples arêtes liant les mêmes sommets.
    
Le graphe de l'exemple est simple.

???+ info "Sommets voisins"

    Deux sommets sont *adjacents*, ou *voisins*, si une arête les relie.
    
$E$ et $G$ sont voisins mais pas $G$ et $F$.

???+ info "Degré d'un sommet"

    Le *degré* d'un sommet est le nombre d'arête en partant.

Ainsi le degré de $E$ est $3$, celui de $G$ est $2$.

???+ info "Chemin"

    Un chemin dans un graphe est une suite d'arêtes reliant deux sommets.
    
Il existe plusieurs chemins reliant $E$ et $H$ :

* $E \rightarrow H$ ;

* $E \rightarrow G \rightarrow H$ ;

* $E \rightarrow F \rightarrow H$.

???+ info "Graphe connexe"

    Un graphe est dit *connexe* s'il existe un chemin liant chaque paire de sommets du graphe.

Le graphe ci-dessous n'est pas connexe.

<center>

```mermaid
flowchart LR
    L(L) --- M(M)
    I(I) --- J(J)
    I --- K(K)
    J --- K
```
</center>

???+ info "Cycle"

    Un cycle est un chemin reliant un sommet à lui-même.

Il existe plusieurs cycles issus de $E$ dont $E \rightarrow H \rightarrow G \rightarrow E$.

???+ info "Graphe (non) orienté"

    Un graphe peut être **orienté** ou non :

    * dans un graphe non orienté, l'arête $A \rightarrow B$ est identique à l'arête $B \rightarrow A$. Si $A$ connaît $B$ alors $B$ connaît $A$ ;

    * dans un graphe orienté, les arêtes peuvent différer : dans une ville, il peut exister un chemin allant de $A$ vers $B$ mais pas de $B$ vers $A$, il suffit d'avoir un sens interdit !

Dans les faits on représente souvent les graphes orientés avec des flèches comme ci-dessous :

<center>

```mermaid
flowchart LR
    E(E) --> F(F)
    E <--> G(G)
    H(H) --> E
    H --> G
    F --> H
```
</center>

???+ info "Circuit"

    Un circuit est un cycle dans un graphe orienté.

Le chemin $E \rightarrow F \rightarrow H \rightarrow E$ est un circuit.

???+ info "Graphe pondéré (ou pas !)"

    Un graphe peut être pondéré ou non. Dans un graphe pondéré, on associe un nombre, un *poids*, à chaque arête.

Les graphes pondérés permettent par exemple de déterminer le plus court chemin dans un réseau (routier ou informatique).
    
<center>

```mermaid
flowchart LR
    E(E) ---|1| F(F)
    E ---|3| G(G)
    E ---|5| H(H)
    G ---|1| H
    F ---|2| H
```
</center>

## Implantation

On peut représenter informatiquement les graphes de deux façons classiques : 

* avec des listes d'adjacence ;

* avec une matrice d'adjacence.

On considère dans cette partie le graphe suivant :

<center>

```mermaid
flowchart LR
  A(A) --- B(B)
  A --- D(D)
  A --- C(C)
  B --- E(E)
  C --- D
  B --- D
  D --- E
```
</center>

### Liste d'adjacence

???+ info "Liste d'adjacence"

    Pour représenter un graphe à l'aide d'une liste d'adjacence, on fournit, pour chaque sommet, la liste des ses voisins

???+ note "Graphe orienté"

    Si le graphe est orienté alors il existe au moins un sommet qui apparaît dans la liste des voisins d'un autre sans que cet autre sommet soit dans la sienne.

Avec Python, on utilise souvent des dictionnaires.

{{ IDE("liste_adj.py")}}



Cette représentation a l'avantage d'être concise (pas de valeurs nulles contrairement aux matrices d'adjacences). Elle ne se prête par contre pas naturellement aux graphes pondérés [^1].

[^1]: quoique... On peut tout à fait inclure les poids des arêtes dans les listes : `#!py graphe = {"A": [("B", 1)], "B": [("A", 3)]}`.

### Matrice d'adjacence

???+ info "Matrice d'adjacence"

    Pour représenter un graphe à l'aide d'une matrice d'adjacence, on commence par numéroter les sommets.
    
    Puis on fournit une matrice (un tableau de nombres) dans laquelle la valeur à la ligne $i$ et la colonne $j$ décrit la relation entre les sommets d'indices $i$ et $j$.
    
    Cette valeur vaut $1$ s'il existe une arête allant du sommet d'indice $i$ à celui d'indice $j$, $0$ si cette arête n'existe pas.

La matrice du graphe étudié est :

$$
\begin{pmatrix}
    0&1&1&1&0\\
    1&0&0&1&1\\
    1&0&0&1&0\\
    1&1&1&0&1\\
    0&1&0&1&0\\
\end{pmatrix}
$$

???+ note "Graphe orienté"

    Si le graphe est orienté alors cette matrice de valeur est symétrique par rapport à la diagonale allant d'en haut à gauche à en bas à droite.

Avec Python, on utilise souvent des listes de listes.

{{ IDE("matrice_adj.py")}}

Cette représentation peut occuper beaucoup de place inutile (si un graphe comporte de très nombreux sommets mais peu d'arêtes). Elle permet par contre de représenter naturellement les graphes pondérés : il suffit de placer le poids d'une arête dans la matrice.

Par exemple pour le graphe ci-dessous en numérotant les sommets dans l'ordre lexicographique :

<center>

```mermaid
flowchart LR
    E(E) ---|1| F(F)
    E ---|3| G(G)
    E ---|5| H(H)
    G ---|1| H
    F ---|2| H
```
</center>

$$
\begin{pmatrix}
0 & 1 & 3 & 5 \\
1 & 0 & 0 & 2 \\
3 & 0 & 0 & 1 \\
5 & 2 & 1 & 0 \\
\end{pmatrix}
$$

### Conversions

Il n'est pas très compliqué de passer d'une représentation à l'autre. Le point d'attention principal est la correspondance entre les noms des sommets (utilisés dans les listes d'adjacence) et leurs indices (utilisés dans les matrices d'adjacence). Avant les conversions, on crée donc un dictionnaire `sommet_indice = {sommet: indice}`.


{{ IDE("conversions.py")}}

## Algorithmes classiques

### Parcours en largeur

???+ info "Parcours en largeur"

    Le parcours en largeur d'un graphe issue d'un de ses sommets visite :

    * en premier lieu ce sommet de départ...
    * ...puis tous les voisins de ce sommet,
    * ...puis les voisins de ces voisins...
    * etc.

Le parcours en largeur issu de $A$ dans le graphe ci-dessous est $A \rightarrow B \rightarrow C \rightarrow D \rightarrow E$. On a fait le choix de parcourir les voisins dans l'ordre lexicographique.

<center>

```mermaid
flowchart LR
  A(A) --- B(B)
  A --- D(D)
  A --- C(C)
  B --- E(E)
  C --- D
  B --- D
  D --- E
```
</center>

???+ info "Algorithme"

    On peut mettre en oeuvre un tel parcours à l'aide d'une file. Il faut bien veiller à garder trace des sommets déjà visités au risque de tomber dans une boucle infinie.
    
    ```
    Fonction largeur(graphe, depart) :
        file est une File vide
        Enfiler depart dans file
        Marquer depart comme visité
        Tant que file est non vide :
            Défiler un sommet
            Traiter ce sommet
            Pour chaque voisin de ce sommet :
                Si ce voisin n'a pas été visité
                    Enfiler ce voisin dans file
                    Marquer ce voisin comme visité
    ```

En Python, dans le cas d'une implantation avec des listes d'adjacences, on peut avoir :

{{ IDE('largeur.py')}}

Les parcours en largeur interviennent dans :

* la recherche du chemin du plus court d'un graphe non pondéré ;

* la recherche d'un arbre couvrant de taille minimale pour un graphe non pondéré (arbre contenant tous les sommets du graphe) ;

* les réseaux peer-to-peer afin de déterminer les sommets voisins ;

* l'indexation des pages web à l'aide de *bots* ;

* construire la carte des connaissances de niveau $k$ d'une personne dans un réseau (on parcourt $k$ largeur à partir de la personne étudiée) ;

* la détection de cycle (*cf. infra*) ;

* ...


### Parcours en profondeur

???+ info "Parcours en profondeur"

    Le parcours en profondeur d'un graphe issue d'un de ses sommets visite :

    * en premier lieu ce sommet de départ...
    * ...puis l'un de ses voisins,
    * ...puis l'un des voisin de ce voisin...
    * etc. jusqu'à ce qu'il n'y ait plus de voisins ou que des voisins déjà visités ,
    * dans ce cas, on revient en arrière au sommet précédent et on passe au prochain voisin pas encore visité.

Le parcours en largeur issu de $A$ dans le graphe ci-dessous est $A \rightarrow B \rightarrow D \rightarrow E \rightarrow C$. On a fait le choix de parcourir les voisins dans l'ordre lexicographique.

<center>

```mermaid
flowchart LR
  A(A) --- B(B)
  A --- D(D)
  A --- C(C)
  B --- E(E)
  C --- D
  B --- D
  D --- E
```
</center>

???+ info "Algorithme itératif"

    Une première approche est de mettre en oeuvre un tel parcours à l'aide d'une pile. L'algorithme est quasiment identique à celui du parcours en largeur.
    
    ```
    Fonction parcours_profondeur(graphe, depart) :
        pile est une Pile vide
        Empiler depart dans pile
        Marquer depart comme visité
        Tant que pile est non vide :
            Dépiler un sommet
            Traiter ce sommet
            Pour chaque voisin de ce sommet :
                Si ce voisin n'a pas été visité
                    Empiler ce voisin dans pile
                    Marquer ce voisin comme visité
    ```

En Python, dans le cas d'une implantation avec des listes d'adjacences, on propose le code ci-dessous. Vous noterez que la structure de Pile implique que les voisins sont parcourus dans l'ordre inverse de leur déclaration dans le graphe (*dernier entré, premier sorti*) :

{{ IDE('profondeur_iter.py')}}

???+ info "Algorithme récursif"

    Une seconde approche est de mettre en oeuvre un algorithme récursif.
    
    ```
    Fonction parcours_profondeur(graphe, depart) :
        Marquer depart comme visité
        Traiter le sommet depart
        Pour chaque voisin de ce depart :
            Si ce voisin n'a pas été visité
                Exécuter parcours_profondeur(graphe, voisin)
    ```

En Python, dans le cas d'une implantation avec des listes d'adjacences, on propose le code ci-dessous. Cette fois-ci l'ordre du parcours des voisins correspond bien à leur ordre de déclaration.

{{ IDE('profondeur_rec.py')}}

Le parcours en profondeur intervient dans :

* la recherche du chemin le plus court d'un graphe non pondéré ;

* la recherche d'un arbre couvrant de taille minimale pour un graphe non pondéré (arbre contenant tous les sommets du graphe) ;

* la détection de cycle dans un graphe ;

* l *tri topologique* des sommets d'un graphe. Ce tri permet de classer les sommets selon une hiérarchie (le premier ancêtre, son descendant...). Cela peut servir par exemple dans un tableur à déterminer quelle est la cellule à recalculer en priorité et dont dépendent les autres valeurs ;

* ...


### Détermination de cycles

La recherche de cycles dans un graphe non orienté n'est pas simple. La figure ci-dessous présente trois graphes : un graphe orienté présentant un *circuit*, un graphe non orienté cyclique et un autre acyclique.

<center>

```mermaid
flowchart LR
    A(A) --> B(B)
    B --> C(C)
    C --> A
    
    AA(A)
    BB(B)
    CC(C)
    DD(D)
    EE(E)
    BB --- DD
    AA --- BB
    AA --- CC
    AA --- DD
    BB --- EE
    CC --- DD
    DD --- EE

    AAA(A)
    BBB(B)
    CCC(C)
    DDD(D)
    EEE(E)
    FFF(F)
    GGG(G)
    HHH(H)
    AAA --- BBB
    AAA --- CCC
    AAA --- DDD
    BBB --- EEE
    BBB --- FFF
    CCC --- GGG
    GGG --- HHH
```
</center>

Notons tout d'abord que la recherche de *circuit* dans un graphe orienté est plus simple : un simple parcours en profondeur (ou en largeur) permet de répondre à la question.

Dans le cas d'un graphe non orienté, le sommet de départ est toujours le voisin de ses voisins. Se demander sans discernement si l'on rencontre un sommet déjà rencontré est donc une mauvaise approche car, à chaque étape du parcours, le sommet précédent est un voisin possible.

À chaque étape du parcours, on va garder trace du sommet d'origine : si l'on a parcouru $A \rightarrow B \rightarrow C$, il faut garder trace que $C$ est précédé par $B$.

La bonne question à se poser est alors : « *le voisin du sommet actuel est-il différent du précédent sommet ? Si oui a-t-il déjà été rencontré ?* ».

On obtient l'algorithme ci-dessous qui s'apparente à un parcours en profondeur.

{{ IDE('cycle.py')}}