# --- PYODIDE:env --- #
class Maillon_File:
    """
    Classe décrivant les maillons utilisés dans les files
    Un maillon possède :
        - une donnée (de type quelconque)
        - un successeur (un autre élément)
    """

    def __init__(self, donnee):
        self.donnee = donnee
        self.successeur = None

    def __repr__(self):
        """Affichage"""
        resultat = f"{self.donnee}"
        if self.successeur is not None:
            resultat += f", {self.successeur}"
        return resultat


class File:
    """
    Classe décrivant une file
    La file est caractérisée par sa tête (pour le défilement)
    et sa queue (pour l'enfilement)
    """

    def __init__(self):
        """Constructeur : renvoie la file vide"""
        self.tete = None
        self.queue = None

    def est_vide(self):
        """Détermine si la file est vide ou non"""
        return self.tete is None

    def enfile(self, x):
        """Enfile la valeur x"""
        # Cas particulie : la file est vide
        nouveau = Maillon_File(x)
        if self.est_vide():
            self.queue = nouveau
            self.tete = nouveau
        else:
            self.queue.successeur = nouveau
            self.queue = nouveau

    def defile(self):
        """Supprime et renvoie la valeur en tête de la file"""
        if self.est_vide():
            raise ValueError("La file est vide")

        # Cas particulier : la file ne contient qu'un élément
        if self.tete.successeur is None:
            maillon = self.tete
            self.tete = None
            self.queue = None
            return maillon.donnee

        maillon = self.tete
        self.tete = self.tete.successeur
        return maillon.donnee

    def __repr__(self):
        """Affichage"""
        if self.est_vide():
            return "(Tête) [] (Queue)"

        return f"(Tête) [{self.tete}] (Queue)"


class Maillon_Pile:
    """
    Classe décrivant les maillons utilisés dans les piles
    Un maillon possède :
        - une donnée (de type quelconque)
        - un successeur (un autre élément)
    """

    def __init__(self, donnee):
        self.donnee = donnee
        self.successeur = None

    def __repr__(self):
        """Affichage"""
        resultat = f"{self.donnee}"
        if self.successeur is not None:
            resultat = f"{self.successeur}, " + resultat
        return resultat


class Pile:
    """
    Classe décrivant une pile
    La pile est caractérisée par sa seule tête
    """

    def __init__(self):
        """Constructeur : renvoie la pile vide"""
        self.tete = None

    def est_vide(self):
        """Détermine si la pile est vide ou non"""
        return self.tete is None

    def empile(self, x):
        """Empile la valeur x"""

        nouveau = Maillon_Pile(x)
        nouveau.successeur = self.tete
        self.tete = nouveau

    def depile(self):
        """Supprime et renvoie la valeur en haut de la pile"""
        if self.est_vide():
            raise ValueError("La pile est vide")

        maillon = self.tete
        self.tete = self.tete.successeur
        return maillon.donnee

    def __repr__(self):
        """Affichage"""
        if self.est_vide():
            return "(Bas) [] (Haut)"

        return f"(Bas) [{self.tete}] (Haut)"


# --- PYODIDE:code --- #
# Les files sont déjà importées

def largeur(graphe, depart):
    resultat = []
    deja_vu = {sommet: False for sommet in graphe}
    file = File()
    file.enfile(depart)
    deja_vu[depart] = True
    while not file.est_vide():
        sommet = file.defile()
        resultat.append(sommet)
        for voisin in graphe[sommet]:
            if not deja_vu[voisin]:
                deja_vu[voisin] = True
                file.enfile(voisin)
    return resultat
        
graphe = {
    "A" : ["B", "C", "D"],
    "B" : ["A", "D", "E"],
    "C" : ["A", "D"],
    "D" : ["A", "B", "C", "E"],
    "E" : ["B", "D"]
}

for sommet in graphe:
    print(f"Le parcours en largeur issu de {sommet} est {largeur(graphe, sommet)}")