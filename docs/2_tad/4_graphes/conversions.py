def indice(s, sommets):
    """
    Renvoie l'indice de s dans la liste des sommets
    Provoque une erreur si s n'est pas dans sommets
    """
    for i in range(len(sommets)):
        if sommets[i] == s:
            return i
    raise ValueError(f"{s} n'est pas dans la liste {sommets = }")


def liste_en_matrice(liste_adj):
    """
    Convertit la liste d'adjacence graphe (dict)
    en un couple sommets, matrice dans lequel :
    - sommets est la liste des sommets
    - matrice est la matrice d'adajcence
    """
    n = len(liste_adj)
    sommets = [s for s in liste_adj]  # liste des sommets
    sommet_indice = {sommets[i]: i for i in range(n)}  # conversion sommet -> indice

    matrice = [[0 for _ in range(n)] for _ in range(n)]  # matrice de base
    for sommet in liste_adj:
        i = sommet_indice[sommet]
        for voisin in liste_adj[sommet]:
            j = sommet_indice[voisin]
            matrice[i][j] = 1
    return sommets, matrice


def matrice_en_liste(sommets, matrice):
    """
    Convertit la liste des noms des sommets et la matrice d'ajacence
    en une liste d'adajcence
    """
    n = len(sommets)
    liste_adj = dict()
    for i in range(n):
        sommet = sommets[i]
        liste_adj[sommet] = []
        for j in range(n):
            if matrice[i][j] == 1:
                voisin = sommets[j]
                liste_adj[sommet].append(voisin)
    return liste_adj


liste_adj = {
    "A": ["B", "C", "D"],
    "B": ["A", "D", "E"],
    "C": ["A", "D"],
    "D": ["A", "B", "C", "E"],
    "E": ["B", "D"],
}

sommets = ["A", "B", "C", "D", "E"]

matrice_adj = [
    [0, 1, 1, 1, 0],
    [1, 0, 0, 1, 1],
    [1, 0, 0, 1, 0],
    [1, 1, 1, 0, 1],
    [0, 1, 0, 1, 0],
]

assert liste_en_matrice(liste_adj) == (sommets, matrice_adj)
assert matrice_en_liste(sommets, matrice_adj) == liste_adj