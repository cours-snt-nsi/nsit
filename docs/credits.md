---
author: Steeve PYTEL
title: 👍Crédits 
tags:
    - credit
---

Le site est hébergé par [la forge des communs numériques éducatifs](https://docs.forge.apps.education.fr/){:target="_blank" }.

![AEIF](assets/logo_aeif_300.png){width=7%}    
Le modèle du site a été créé par l'  [Association des enseignantes et enseignants d'informatique de France](https://aeif.fr/index.php/category/non-classe/){target="_blank"}.  

Le site est construit avec [`mkdocs`](https://www.mkdocs.org/){target="_blank"} et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/){target="_blank"}. 

Le thème intégrant Pyodide [Pyodide-Mkdocs-Theme](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/){target="_blank"} est dû à Frédéric Zinelli.

La source pour les cours SNT : [Enseigner les SNT](https://ressources.forge.apps.education.fr/snt/intro/){target="_blank"}. 

La source des exercices : [Exercices de programmation par des professeurs d'informatique](https://codex.forge.apps.education.fr/){target="_blank"}. 

Vous trouverez sur ce site les cours de la Spécialité *Numérique et Sciences Informatiques* rédigés par [Nicolas Revéret](https://forge.apps.education.fr/nreveret).

😀 Un grand merci à Frédéric Zinelli, et Vincent-Xavier Jumel qui ont réalisé la partie technique de ce site.   
Merci également à Nathalie Weibel pour ses relectures attentives et ses conseils judicieux.
