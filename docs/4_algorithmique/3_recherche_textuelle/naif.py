def recherche_naive(texte, motif):
    """
    Recherche l'indice de la première orccurrence de motif dans texte
    On utilise une approche naïve
    Provoque une erreur si motif n'est pas présent
    """
    N = len(texte)
    P = len(motif)

    if len(motif) > len(texte):
        raise ValueError("Le motif est trop long pour être présent dans le texte")

    for i in range(N - P + 1):  # inutile de tester les derniers caractères
        j = 0
        while j < P and motif[j] == texte[i + j]:
            j += 1
        if j == P:
            return i

    raise ValueError("Le motif n'est pas présent dans le texte")


# Résultat positif
texte = "lundi je regarderai la lune avec la lunette."
motif = "lunette"
assert recherche_naive(texte, motif) == 36

# Motif trop long
try:
    recherche_naive(motif, texte)
except ValueError as erreur:
    print(erreur)

# Texte absent
texte = "lundi je regarderai la lune avec la lunette."
motif = "mer"
try:
    recherche_naive(texte, motif)
except ValueError as erreur:
    print(erreur)
