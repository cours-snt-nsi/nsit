# --- PYODIDE:env --- #
def calcule_decalages(motif):
    """
    Renvoie le dictionnaire {caractère: indice de la dernière occurrence}
    où les caractères sont ceux de motif
    """
    decalage = {}
    for i in range(len(motif)):
        caractere = motif[i]
        decalage[caractere] = i
    return decalage


# --- PYODIDE:code --- #
# La fonction calcule_decalages a été importée

def recherche_BMH(texte, motif):
    """
    Recherche l'indice de la première orccurrence de motif dans texte
    On utilise l'algorithme de Boyer-Moore-Horspool
    Provoque une erreur si motif n'est pas présent
    """
    N = len(texte)
    P = len(motif)

    if len(motif) > len(texte):
        raise ValueError("Le motif est trop long pour être présent dans le texte")

    decalages = calcule_decalages(motif)

    i = 0
    while i < N - P + 1:  # inutile de tester les derniers caractères
        j = P - 1
        while j >= 0 and motif[j] == texte[i + j]:
            j -= 1
        if j == -1:
            return i
        if texte[i + j] in decalages:
            i += max(1, P - 1 - decalages[motif[j]])
        else:
            i += P
    raise ValueError("Le motif n'est pas présent dans le texte")


# Résultat positif
texte = "lundi je regarderai la lune avec la lunette."
motif = "lunette"
assert recherche_BMH(texte, motif) == 36

# Motif trop long
try:
    recherche_BMH(motif, texte)
except ValueError as erreur:
    print(erreur)

# Texte absent
texte = "lundi je regarderai la lune avec la lunette."
motif = "mer"
try:
    recherche_BMH(texte, motif)
except ValueError as erreur:
    print(erreur)
