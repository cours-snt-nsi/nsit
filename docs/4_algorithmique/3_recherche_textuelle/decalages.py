def calcule_decalages(motif):
    """
    Renvoie le dictionnaire {caractère: indice de la dernière occurrence}
    On utilise l'algorithme de Boyer-Moore-Horspool
    où les caractères sont ceux de motif
    """
    decalage = {}
    for i in range(len(motif)):
        caractere = motif[i]
        decalage[caractere] = i
    return decalage


motif = "les fleurs"
assert calcule_decalages(motif) == {
    "l": 5,
    "e": 6,
    "s": 9,
    " ": 3,
    "f": 4,
    "u": 7,
    "r": 8,
}
