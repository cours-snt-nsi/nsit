---
title: Recherche Textuelle
---

L'ADN des êtres vivants est constitué des bases  l'*adénine*, la *cytosine*, la *guanine* et *thymine* que l'on abrège souvent en *A*, *C*, *G* et *T*. Le génome humain est ainsi constitué d'environ $3\,400$ millions de paires de bases azotées : une chaîne de caractères de $3\,400$ millions de lettres piochées dans l'alphabet `#!py {"A", "B", "C", "D"}` !

L'ADN permet de synthétiser les protéines constitutives des êtres vivants grâce à la correspondance $3 \textrm{ bases azotées} = 1 \textrm{ acide aminé}$. Considérons une protéine de $500$ acides aminés de long : son code génétique est une chaîne de $1\,500$ bases. Comment chercher l'emplacement de ce code génétique au sein de l'ensemble du génome d'un humain ? Comment chercher **efficacement** un mot de $1\,500$ lettres au sein d'un livre de $3\,400\,000\,000$ de lettres ?

Afin de simplifier les choses, nous allons considérer l'exemple suivant : on se donne le **texte** "lundi je regarderai la lune avec la lunette" et l'on va chercher au sein de cette chaîne le **motif** "lunette".

Le texte a une longueur de $43$ caractères, le motif de $7$. Chaque lettre d'une chaîne est indicée en partant de $0$. Ainsi `texte[1]` est `u`.

![Notre exemple](exemple.png){width=100% .center .autolight}

## Recherche naïve

L'approche naïve consiste à lire le texte du début à la fin en cherchant les correspondances avec le motif. Si la première lettre correspond, on essaie la deuxième, si elle correspond aussi, on passe à la troisième *etc*... Si les lettres ne correspondent pas, on décale le motif d'un rang et on recommence. 

![Recherche naïve](recherche_naive.png){width=100% .center .autolight}

On obtient une correspondance pour l'indice $36$.

??? example "Méthode naïve"

    {{ IDE('naif.py')}}
    
La boucle `#!py for` principale ne va pas jusqu'à la fin du texte car il faut laisser de la place pour le motif (un motif de taille $7$ ne tiendra pas sur les $6$ derniers caractères !).

Pour chaque rang ensuite on teste successivement les correspondances entre la lettre du texte et celle du motif. S'il y a correspondance on passe à la lettre suivante, sinon, on quitte les tests et on passe à l'indice suivant. Si toutes les lettres correspondent, on renvoie l'indice en question.

Quelle est le coût de cet algorithme ?

Le pire des cas serait celui d'un texte comportant une répétition du motif : par exemple le texte `#!py "aaaaaaaaab"` (neuf `#!py "a"` suivis d'un `#!py "ab"`) dans lequel on cherche le motif `#!py "ab"`. Dans ce cas, la boucle `#!py for` va effectuer $9$ itérations et pour chacune testera $2$ lettres. On aura donc $9\times2 = 18$ itérations.

De façon générale, si la longueur du texte est $N$ et celle du motif $P$, le coût sera proportionnel à $(N-P+1)\times P$.

Dans le cas d'une recherche d'un motif de $1\,500$ caractères dans un texte de $3\,400\,000\,000$ de caractères cela fait $5\,097\,751\,500$ itérations dans le pire des cas ! On doit pouvoir faire mieux...

## Algorithme de Boyer-Moore

Cet algorithme a été développé par Robert S. Boyer et J. Strother Moore en 1977.

Le point de départ est plutôt contre-intuitif : pour chercher le motif dans le texte, on commence par la dernière lettre du motif  ! En effet, **si la lettre du texte correspondante n'est pas la bonne, il est inutile de tester les autres lettres**. Cela ne change pas beaucoup par rapport à l'approche naïve.

![Le mauvais caractère n'est pas dans la clé, on saute au caractère suivant](boyer1.png){width=40% .center .autolight}

La vraie différence se situe dans l'action qui suit la comparaison des caractères:

**Si la lettre observée ne fait pas partie du motif**, on peut alors décaler la fenêtre de lecture directement de la longueur du motif. On évite ainsi de faire autant de tests que le motif est long.

Que se passe-t-il lorsque **la lettre du texte correspond à celle du motif** ? On étudie alors les lettres précédentes jusqu'à avoir soit fini le motif (et donc trouvé une correspondance parfaite), soit trouvé un mauvais caractère.

Dernier cas de figure : le caractère lu n'est pas correct mais **il apparaît toutefois à une autre position dans le motif**. Dans ce cas on doit décaler la fenêtre de lecture afin de faire correspondre ce caractère et celui du motif situé le plus à droite (la dernière occurrence de la lettre). C'est ce qui se passe entre les étapes 1 et 2 de la figure ci-dessous.

![Les différents sauts](boyer2.png){width=35% .center .autolight}

Il peut toutefois arriver que la dernière occurrence du caractère cherché soit située plus loin dans le motif : le saut ferait alors reculer la fenêtre de lecture ce qui est inefficace. Dans ce cas, on se contente de faire un saut de $1$ caractère. C'est ce qui se passe entre les étapes 2 et 3.

<center>

```mermaid
graph LR
  A(comparaison)
  B(correspondance)
  C(différence)
  D(succès)
  E(on passe au caractère précédent)
  F("le caractère n'est\npas dans le motif")
  G("le caractère est\ndans le motif")
  H(décalage de la longueur du motif)
  I(alignement des caractères)
  J(décalage de 1)
  K(échec)
  
  A --> B
  B --> E
  B -->|début du motif ?| D
  A --> C
  C --> F
  C --> G
  F --> H
  G --> I
  G -->|recul ?| J
  C -->|fin du texte ?| K
  
  style D stroke:green,stroke-width:4px
  style K stroke:red,stroke-width:4px
```
</center>

L'algorithme de Boyer-Moore nécessite donc avant d'effectuer les comparaisons de savoir quels sont les décalages à effectuer. Pour ce faire il doit construire un dictionnaire `decalages` reprenant l'indice dans le motif de la dernière occurrence de chacun de ses caractères.

??? example "Calcul des décalages"

    On crée ici un dictionnaire dont les entrées sont les lettres du motif. On parcourt de la gauche vers la droite en ajoutant ou mettant à jour le décalage associé à chaque caractère.

    {{ IDE('decalages') }}
    
Ce dictionnaire étant créé, on peut écrire la fonction de recherche.

??? example "Recherche de *Boyer-Moore-Horspool*"

    {{ IDE('boyer') }}

L'analyse précise du coût de cet algorithme est trop technique. Il est toutefois intéressant de noter que, du fait des nombreux sauts, cette méthode de recherche ne lit pas le texte en entier.

On peut même montrer qu'elle lit en moyenne une fraction $\frac{\textrm{longueur du texte}}{\textrm{longueur du motif}}$ du texte. Ainsi, si la clé fait $10$ caractères de long, l'algorithme lira en moyenne $1$ caractère sur $10$. On est loin des nombreuses lectures de la recherche naïve !

??? note "Seconde table de décalage"

    L'algorithme initial de Boyer-Moore utilise aussi une autre façon de gérer les décalages mais cette seconde approche est trop technique pour ce cours et ne sera pas abordée.

![On a trouvé la lunette !](boyer3.png){width=100% .center .autolight}