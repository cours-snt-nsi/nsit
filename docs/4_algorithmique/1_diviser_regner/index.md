---
title: Diviser Pour Régner
---

Certains problèmes algorithmiques de grande taille peuvent être résolus à l'aide de sous-problèmes : si l'on souhaite par exemple déterminer le minimum d'une liste de nombres, on peut commencer par chercher les minimums de la partie gauche de la liste et celui de la partie droite puis comparer les deux. Le minimum général sera le plus petit des deux minimums.

Cette méthode est appelée *Diviser Pour Régner*. L'expression est attribuée à Philippe II de Macédoine, le père d'Alexandre le Grand. Il fallait alors l'entendre dans un sens politique.

![Philippe II de Macédoine](philippeII.jpg){width=20% .center .autolight}

## Présentation générale

On considère un problème dont l'entrée est de taille importante.

???+ info "Méthode *Diviser pour régner*"
    La méthode diviser pour régner (*divide and conquer* en anglais) consiste en trois étapes :

    1. Partager le problème en sous-problèmes de taille inférieure (*diviser*)
    2. Résoudre les sous-problèmes (*régner*)
    3. Combiner leurs solutions afin de résoudre le problème de départ (*combiner*)

    <center>

    ```mermaid
    flowchart LR
      A(Problème)
      B(Sous-problème)
      BB(Sous-problème)
      BBB(Sous-problème)
      C(Sous-problème)
      D(Sous-problème)
      E(Solution partielle)
      EE(Solution partielle)
      F(Solution partielle)
      G(Solution partielle)
      H(Solution générale)
      
      subgraph Diviser
      A --> B
      A --> C
      A --> D
      B ---> BB
      B ---> BBB
      end
      subgraph Régner
      BB --> E
      BBB --> EE
      C --> F
      D --> G
      end
      subgraph Combiner
      E --> H
      EE --> H
      F --> H
      G --> H
      end
    ```
    </center>

Bien souvent dans cette démarche on travaille **récursivement** : chaque sous-problème est lui aussi divisé en sous-problèmes de taille inférieure. On continue la division jusqu'à ce que le sous-problème ait une taille suffisante pour le résoudre facilement (c'est le **cas de base** de la récursion).

La méthode peut s'avérer plus efficace qu'une résolution *frontale* du problème. Le coût d'une telle méthode dépend de trois paramètres :

* le nombre de sous-problèmes générés à chaque *division* ;

* le coefficient par lequel est divisé la taille de chaque problème lors des divisions ;

* le coût de la combinaison des sous-problèmes.

Pour faire simple retenons que pour que la démarche soit efficace, il faut que la combinaison des sous-problèmes soit moins coûteuse que les étapes de divisions et de résolution.

## Mini-maxi

???+ note "Tri fusion"

    L'exemple classique de la méthode *Diviser pour régner* est le tri fusion.
    
    Il est étudié en détails sur [cette page](https://nreveret.forge.apps.education.fr/tris/04_rapides/01_fusion/).

Il est facile de déterminer la valeur minimale (maximale) dans un tableau de nombres : on lit tous les nombres et à chaque valeur lue, on la compare avec le minimum (maximum) actuel.

Il est possible d'adapter cette méthode afin de chercher le couple `(minimum, maximum)` néanmoins une méthode *Diviser pour régner* est (légèrement) plus efficace.

Comment procéder ?

Tout d'abord on remarque qu'il facile de trouver le couple `(minimum, maximum)` dans un tableau de deux valeurs : il suffit de comparer les deux éléments. Dans le même esprit, si le tableau ne contient qu'une seule valeur, elle est à la fois le `minimum` et le `maximum`. Ces observations forment l'étape *régner*.

Comment faire pour les tableaux plus grands ? On peut les partager en deux (les valeurs du début et celles de la fin) et chercher le couple `(minimum, maximum)` dans chaque moitié. C'est l'étape *diviser*.

Reste à *combiner*... À cette étape, on a récupéré les couples `(minimum, maximum)` provenant de deux sous-problèmes. Le vrai `minimum` est nécessairement l'un des deux `minimum` : il suffit de les comparer. On peut faire de même pour le maximum.

La figure ci-dessous illustre la résolution dans le cas d'un tableau de 8 éléments

<center>

  ```mermaid
  flowchart LR
    A(Problème)
    B(Moitié gauche)
    C(Moitié droite)
    D(Moitié gauche)
    E(Moitié droite)
    F(Moitié gauche)
    G(Moitié droite)
    H(Une comparaison)
    I(Une comparaison)
    J(Une comparaison)
    K(Une comparaison)
    L(Deux comparaisons)
    M(Deux comparaisons)
    N("Deux comparaisons\nSolution générale")
    
    subgraph Diviser
    A -->|4 nombres| B
    A -->|4 nombres| C
    B -->|2 nombres| D
    B -->|2 nombres| E
    C -->|2 nombres| F
    C -->|2 nombres| G
    end
    subgraph Régner
    D --> H
    E --> I
    F --> J
    G --> K
    end
    subgraph Combiner
    H --> L
    I --> L
    J --> M
    K --> M
    L --> N
    M --> N
    end
  ```
  </center>

{{ IDE('mini_maxi.py') }}

??? note "Plus efficace ?"

    Pour comparer les deux approches, on doit compter les comparaisons de nombres. Chercher un extremum dans un tableau de taille $2^n$ en demande $2^n-1$.

    La méthode naïve (classique) pour trouver le couple `(minimum, maximum)` effectue $2\times2^n-3$ comparaisons ($2^n-1$ pour trouver le minimum puis $2^n-2$ pour le maximum car on ne tient pas compte du `minimum`).
    
    L'approche *Diviser pour régner* effectue quant à elle $1,5\times 2^n-2$ comparaisons.
    
    Ce n'est pas grand-chose mais il y en a un peu moins !
    
    <center>
    <iframe src="https://www.desmos.com/calculator/kojgmytukf?embed" width="500" height="500" style="border: 1px solid #ccc" frameborder=0></iframe>
    </center>