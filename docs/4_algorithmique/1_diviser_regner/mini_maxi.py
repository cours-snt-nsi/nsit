def mini_maxi(tableau, debut, fin):
    """
    Renvoie le couple (minimum, maximum) des éléments de tableau
    dont les indices sont compris entre debut (inclus) et fin (exclu)
    """
    # Régner
    if fin - debut == 1:  # un seul élément
        return (tableau[debut], tableau[debut])
    elif fin - debut == 2:  # un seul élément
        a, b = tableau[debut], tableau[debut + 1]
        if a <= b:
            return (a, b)
        else:
            return (b, a)

    # Diviser
    milieu = (debut + fin) // 2
    (mini_gauche, maxi_gauche) = mini_maxi(tableau, debut, milieu)
    (mini_droite, maxi_droite) = mini_maxi(tableau, milieu, fin)

    # Combiner
    mini = min(mini_gauche, mini_droite)
    maxi = max(maxi_gauche, maxi_droite)

    return (mini, maxi)


from random import sample

taille = 50
tableau = sample(list(range(100)), taille)

assert mini_maxi(tableau, 0, taille) == (min(tableau), max(tableau))
