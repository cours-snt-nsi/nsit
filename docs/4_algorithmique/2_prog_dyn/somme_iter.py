def somme(grille):
    hauteur = len(grille)
    largeur = len(grille[0])
    maxis = [[0 for _ in range(largeur)] for _ in range(hauteur)]
    
    # origine
    maxis[0][0] = grille[0][0]
    
    # première ligne
    for i in range(1, hauteur):
        maxis[i][0] = grille[i][0] + maxis[i - 1][0]
    
    # première colonne
    for j in range(1, largeur):
        maxis[0][j] = grille[0][j] + maxis[0][j - 1]
    
    # partout ailleurs
    for i in range(1, hauteur):
        for j in range(1, largeur):
            maxis[i][j] = grille[i][j] + max(maxis[i - 1][j], maxis[i][j - 1])
    
    return maxis[hauteur - 1][largeur - 1]


grille = [[3, 5, 6], [4, 9, 1]]
assert somme(grille) == 18
grille = [[0, 7, 1, 3]]
assert somme(grille) == 11
grille = [[1]]
assert somme(grille) == 1
