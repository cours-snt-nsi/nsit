def somme(grille):
    hauteur = len(grille)
    largeur = len(grille[0])
    ligne_maxis = [0 for _ in range(largeur)]
    
    # origine
    ligne_maxis[0] = grille[0][0]
    
    # première ligne
    for j in range(1, largeur):
        ligne_maxis[j] = grille[0][j] + ligne_maxis[j - 1]
    
    # autres lignes
    for i in range(1, hauteur):
        suivante = [0 for _ in range(largeur)]
        
        # première case, on ne peut venir que du dessus
        suivante[0] = grille[i][0] + ligne_maxis[0]
        
        # autres cases
        for j in range(1, largeur):
            suivante[j] = grille[i][j] + max(ligne_maxis[j], suivante[j - 1])
        ligne_maxis = suivante
    
    return ligne_maxis[largeur - 1]


grille = [[3, 5, 6], [4, 9, 1]]
assert somme(grille) == 18
grille = [[0, 7, 1, 3]]
assert somme(grille) == 11
grille = [[1]]
assert somme(grille) == 1
