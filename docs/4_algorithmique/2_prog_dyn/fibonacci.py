from time import perf_counter

def fibo_recursif(n):
    global appels
    appels += 1
    if n <= 1:
        return n
    else:
        return fibo_recursif(n-1) + fibo_recursif(n-2)


def fibo_dynamique_bottom_up(n):
    global appels
    appels += 1
    F_bottom_up = [-1]*(n+1)
    F_bottom_up[0] = 0
    F_bottom_up[1] = 1
    for i in range(2, n+1):
        F_bottom_up[i] = F_bottom_up[i-1] + F_bottom_up[i-2]
    return F_bottom_up[n]


def fibo_dynamique_top_down(n):
    global appels
    appels += 1
    if F[n] == -1:
        if n <= 1:
            F[n] = n
        else:
            F[n] = fibo_dynamique_top_down(n-1) + fibo_dynamique_top_down(n-2)
    return F[n]


if __name__ == "__main__":
    n = 10

    print("Calculs pour n =",n)

    # Méthode récursive classique
    appels = 0
    t_debut = perf_counter()
    fibo_recursif(n)
    t_fin = perf_counter()
    print("Nombres d'appels (récursif classique) :",appels)
    print("Durée (récursif classique) :",(t_fin - t_debut)*1000,"ms")

    # Méthode dynamique ascendante
    appels = 0
    t_debut = perf_counter()
    fibo_dynamique_bottom_up(n)
    t_fin = perf_counter()
    print("Nombres d'appels (dynamique bottom-up):",appels)
    print("Durée (dynamique bottom-up) :",(t_fin - t_debut)*1000,"ms")

    # Méthode dynamique descendante
    appels = 0
    t_debut = perf_counter()
    F = [-1] * (n+1)
    fibo_dynamique_top_down(n)
    t_fin = perf_counter()
    print("Nombres d'appels (dynamique top-down):",appels)
    print("Durée (dynamique top-down) :",(t_fin - t_debut)*1000,"ms")
