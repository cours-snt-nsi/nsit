def somme(grille):
    def aux(i, j):
        if maxis[i][j] is None:
            # origine
            if i == 0 and j == 0:
                maxis[i][j] = grille[j][i]
            # première ligne
            elif i == 0:
                maxis[i][j] = grille[i][j] + aux(i, j - 1)
            # première colonne
            elif j == 0:
                maxis[i][j] = grille[i][j] + aux(i - 1, j)
            # partout ailleurs
            else:
                maxis[i][j] = grille[i][j] + max(aux(i - 1, j), aux(i, j - 1))
        
        return maxis[i][j]

    hauteur = len(grille)
    largeur = len(grille[0])
    maxis = [[None for _ in range(largeur)] for _ in range(hauteur)]
    return aux(hauteur - 1, largeur - 1)


grille = [[3, 5, 6], [4, 9, 1]]
assert somme(grille) == 18
grille = [[0, 7, 1, 3]]
assert somme(grille) == 11
grille = [[1]]
assert somme(grille) == 1
