fibo = {0:0, 1:1}

def fibonacci(n):
    """
    Calcule récursivement le n-ième terme de la suite de Fibonnacci
    On utilise le dictionnaire fibo pour "mémoïser"
    """
    if n not in fibo:
        fibo[n] = fibonacci(n - 1) + fibonacci(n - 2)
    return fibo[n]


attendus = [0, 1, 1, 2, 3, 5, 8, 13, 21]
for n in range(len(attendus)):
    assert fibonacci(n) == attendus[n]

assert fibonacci(50) == 12586269025
assert fibonacci(100) == 354224848179261915075