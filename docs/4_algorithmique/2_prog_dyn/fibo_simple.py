def fibonacci(n):
    """Calcule récursivement le n-ième terme de la suite de Fibonnacci"""
    if n <= 1:
        return n
    return fibonacci(n - 1) + fibonacci(n - 2)


attendus = [0, 1, 1, 2, 3, 5, 8, 13, 21]
for n in range(len(attendus)):
    assert fibonacci(n) == attendus[n]

# L'appel ci-dessous prend beaucoup de temps !
# fibonacci(50)