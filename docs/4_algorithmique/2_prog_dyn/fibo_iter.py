def fibonacci(n):
    """
    Calcule itérativement le n-ième terme de la suite de Fibonnacci
    """
    fibo = [0, 1]
    while len(fibo) <= n + 1:
        dernier = fibo[-1]
        avant_dernier = fibo[-2]
        fibo.append(dernier + avant_dernier)
    return fibo[n]


attendus = [0, 1, 1, 2, 3, 5, 8, 13, 21]
for n in range(len(attendus)):
    assert fibonacci(n) == attendus[n]

assert fibonacci(50) == 12586269025
assert fibonacci(100) == 354224848179261915075