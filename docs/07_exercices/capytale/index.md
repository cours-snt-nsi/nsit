---
author: Steeve PYTEL
title: 📋 CAPYTALE
---

# Exercices

!!! abstract "Débuter avec les Outils en ligne"

    1. [Boucles](https://capytale2.ac-paris.fr/web/c/073b-742357){target='_blank'}
    2. [Si](https://capytale2.ac-paris.fr/web/c/6961-922493){target='_blank'}  
    3. [While](https://capytale2.ac-paris.fr/web/c/0285-1279188){target='_blank'}
    4. [Dessin](https://capytale2.ac-paris.fr/web/c/0285-1279188){target='_blank'}
    5. [Mélange](https://capytale2.ac-paris.fr/web/c/0285-1279188){target='_blank'}
    6. [Découverte Python](https://capytale2.ac-paris.fr/web/c/ca10-632362){target='_blank'}

!!! abstract "Activitées Pixel'Art"

    1. [Easy](https://capytale2.ac-paris.fr/web/c/dbee-644319){target='_blank'}
    2. [Drapeau](https://capytale2.ac-paris.fr/web/c/125f-1447786){target='_blank'}
    3. [Zig](https://capytale2.ac-paris.fr/web/c/4cc1-567778){target='_blank'}
    4. [Echec](https://capytale2.ac-paris.fr/web/c/ab28-802220){target='_blank'}
   
!!! absctract " Internet"
   
!!! abstract "Le web"

    1. [page vide](https://capytale2.ac-paris.fr/web/c/910d-980620){target='_blank'}
    2. [Jeu](https://capytale2.ac-paris.fr/web/c/59e2-2069430){target='_blank'}
    3. [Html 1/2](https://capytale2.ac-paris.fr/web/c/60a6-2549442){target='_blank'}
    4. [Html 2/2](https://capytale2.ac-paris.fr/web/c/fdad-2518372){target='_blank'}
    5. [Pomme Poire](https://capytale2.ac-paris.fr/web/c/ee80-936514){target='_blank'}

!!! abstract "Les Objets "

    1. [Microbit](https://capytale2.ac-paris.fr/web/c/c2d5-1767097){target='_blank'}

!!! abstract "Les Reseaux Sociaux "

    1. [Graphes 1/2](https://capytale2.ac-paris.fr/web/c/cc08-3406821){target='_blank'}
    2. [Graphes 2/2](https://capytale2.ac-paris.fr/web/c/9dcd-3133354){target='_blank'}

!!! abstract "Localisation et Cartographie "

    1. [EXIF 1](https://capytale2.ac-paris.fr/web/c/e0b9-1658411){target='_blank'}
    2. [EXIF 2](https://capytale2.ac-paris.fr/web/c/339d-1658359){target='_blank'}
    3. [EXIF 3](https://capytale2.ac-paris.fr/web/c/23a6-1489513){target='_blank'}

!!! abstract "Image"

    1. [Coloriage](https://capytale2.ac-paris.fr/web/c/073e-1952982){target='_blank'}
    2. [Manipulation](https://capytale2.ac-paris.fr/web/c/6271-3574308){target='_blank'}
    3. [Cache-cache](https://capytale2.ac-paris.fr/web/c/9cdd-3700092){target='_blank'}
    4. [Cache-Console](https://capytale2.ac-paris.fr/web/c/6218-1490234){target='_blank'}


