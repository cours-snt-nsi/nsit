# --- PYODIDE:env --- #
# Import de matplotlib (installation lors du 1er lancement)
import matplotlib.pyplot as plt
import numpy as np



# Précision du backend à utiliser
matplotlib.use("module://matplotlib_pyodide.html5_canvas_backend")

# Insertion de la courbe dans une div spécifiée (id="cible_2")
from js import document 
document.pyodideMplTarget = document.getElementById("cible_2")
# On vide la div
document.getElementById("cible_1").textContent = ""

# --- PYODIDE:code --- #





# Définition des dimensions de l'image
nbLignes = 500
nbColonnes = 900

# Création de l'image 'drapeau'
drapeau = np.zeros((nbLignes, nbColonnes, 3), dtype=np.uint8)

# Remplissage de l'image
for i in range(nbColonnes):
    for j in range(nbLignes):
        """Entrez les instructions pour construire le drapeaux concerné"""

# Affichage de l'image
plt.imshow(drapeau)
plt.axis('off')
plt.show()