

# --------- PYODIDE:code --------- #

factorielle_mem = [1]


def factorielle(n):
    i = len(factorielle_mem)
    while n >= i:
        fact_im1 = ...
        fact_i = ...
        factorielle_mem.append(...)
        i = ...
    return ...

# --------- PYODIDE:corr --------- #

factorielle_mem = [1]


def factorielle(n):
    i = len(factorielle_mem)
    while n >= i:
        fact_im1 = factorielle_mem[i - 1]
        fact_i = fact_im1 * i
        factorielle_mem.append(fact_i)
        i += 1
    return factorielle_mem[n]

# --------- PYODIDE:tests --------- #

assert factorielle(5) == 120
assert factorielle(10) == 3628800

# --------- PYODIDE:secrets --------- #


# autres tests


def f(n):
    if n == 0:
        return 1
    else:
        return n * f(n - 1)


for n in [0, 1, 2, 3, 4, 17, 42, 11, 42, 31]:
    attendu = f(n)
    assert factorielle(n) == attendu, f"Erreur avec {n=}"