---
author:
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Aire sous une courbe (2)
tags:
    - en travaux
    - maths
difficulty: 230
maj: 08/04/2024
---

Soient $a$ et $b$ deux nombres réels ($a < b$) et $f$ une fonction continue telle que l'on ait $f(x)\ge0$ sur $\left[a~;~b\right]$. On appelle $C_f$ la représentation graphique de $f$ dans un repère orthonormé.

On cherche à calculer une valeur approchée de l'aire du domaine situé entre :

* l'axe des abscisses ;

* la courbe $C_f$ ;
* les droites d'équations $x=a$ et $x=b$.

La figure ci-dessous (à gauche) illustre le problème avec :

* $f(x) = \frac12x^3 - \frac12 x^2 - \frac12 x + 1$ ;

* $a=-1$ ;
* $b = 2$.

<div style="display: flex;justify-content: space-around;">
    <img src="base.svg" class="autolight" width=30%>
    <img src="seize.svg" class="autolight" width=30%>
</div>

Il est possible de calculer cette valeur approchée en utilisant la *méthode des rectangles* [^1] suivante :

[^1]: On propose ici une version simplifiée de cette méthode qui considère des rectangles ayant tous la même largeur.

* on découpe l'intervalle $\left[a~;~b\right]$ en $n>0$ subdivisions régulières. Chaque subdivision a pour largeur $\text{d}x=\dfrac{b-a}n$ ;
* il est possible de numéroter chaque subdivision avec un entier $0 \leqslant k < n$ ;
* dans la $k$-ième subdivision, on dessine le rectangle de largeur $\text{d}x$ et de hauteur $f\left(a+k\text{d}x\right)$ ;
* la somme des aires de tous les rectangles donne une valeur approchée de l'aire du domaine étudié.

La figure de droite illustre cette méthode dans le cas $n=16$. La valeur approchée obtenue vaut alors :

$$f(-1)\times\text{d}x+f(-1+\text{d}x)\times\text{d}x+f(-1+2\text{d}x)\times\text{d}x+\dots+f(-1+15\text{d}x)\times\text{d}x\simeq2,4888$$

Il est clair que plus le nombre de subdivisions est important, plus la valeur calculée est proche de la valeur exacte de l'aire cherchée.

Écrire la fonction `aire` qui prend en paramètres les entiers `a` et `b` ainsi que l'entier `n` strictement positif et renvoie la valeur approchée renvoyée par la méthode des rectangles décrite utilisant $n$ rectangles.

{{ remarque('assertion') }}

{{ remarque('proximite_flottants') }}

???+ example "Exemples"

    ```pycon title=""
    >>> a = -1
    >>> b = 2
    >>> aire(a, b, 1)
    1.5
    >>> aire(a, b, 2)
    1.78125
    >>> aire(a, b, 4)
    2.1328125
    ```

=== "Version vide"
    {{ IDE('exo_vide') }}
=== "Version à compléter"
    {{ IDE('exo_trous') }}

