

# --------- PYODIDE:code --------- #

# Initialisation da la suite des résultats
catalan_mem = [1]


def nb_arbres_binaires(n):
    if n >= len(catalan_mem):
        somme_cas = ...
        for i in range(...):
            somme_cas += nb_arbres_binaires(...) * ...
        # Ici, catalan_mem sera de longueur n
        catalan_mem.append(...)
    return catalan_mem[...]

# --------- PYODIDE:corr --------- #

# Initialisation da la suite des résultats
catalan_mem = [1]


def nb_arbres_binaires(n):
    if n >= len(catalan_mem):
        somme_cas = 0
        for i in range(n):
            somme_cas += nb_arbres_binaires(i) * nb_arbres_binaires(n - 1 - i)
        # Ici, catalan_mem sera de longueur n
        catalan_mem.append(somme_cas)
    return catalan_mem[n]

# --------- PYODIDE:tests --------- #

assert nb_arbres_binaires(3) == 5
assert nb_arbres_binaires(4) == 14
assert nb_arbres_binaires(5) == 42

# --------- PYODIDE:secrets --------- #


# autres tests


assert nb_arbres_binaires(8) == 1430
assert nb_arbres_binaires(6) == 132


CATALAN = [
    1,
    1,
    2,
    5,
    14,
    42,
    132,
    429,
    1430,
    4862,
    16796,
    58786,
    208012,
    742900,
    2674440,
    9694845,
    35357670,
    129644790,
    477638700,
    1767263190,
    6564120420,
    24466267020,
    91482563640,
    343059613650,
    1289904147324,
    4861946401452,
    18367353072152,
    69533550916004,
    263747951750360,
    1002242216651368,
    3814986502092304,
]

for (n, catalan_n) in enumerate(CATALAN):
    assert nb_arbres_binaires(n) == catalan_n, f"Erreur avec n = {n}"