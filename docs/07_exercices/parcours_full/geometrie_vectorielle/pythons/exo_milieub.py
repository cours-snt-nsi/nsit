

# --------- PYODIDE:code --------- #

def milieu(x_A, y_A, x_B, y_B):
    x_M = ...
    y_M = ...
    return (..., ...)

# --------- PYODIDE:corr --------- #

def milieu(x_A, y_A, x_B, y_B):
    x_M = (x_A + x_B) / 2
    y_M = (y_A + y_B) / 2
    return (x_M, y_M)

# --------- PYODIDE:tests --------- #

assert milieu(0, 2, 3, 6) == (1.5, 4.0)
assert milieu(10, 4, 5, -8) == (7.5, -2.0)

# --------- PYODIDE:secrets --------- #


# tests secrets
from random import randrange

for _ in range(10):
    x_A = randrange(-200, 200) / 2
    x_B = randrange(-200, 200) / 2
    y_A = randrange(-200, 200) / 2
    y_B = randrange(-200, 200) / 2
    x_attendu = (x_A + x_B) / 2
    y_attendu = (y_A + y_B) / 2
    assert milieu(x_A, y_A, x_B, y_B) == (
        x_attendu,
        y_attendu,
    ), f"Erreur avec A {x_A, y_A} et B {x_B, y_B}"