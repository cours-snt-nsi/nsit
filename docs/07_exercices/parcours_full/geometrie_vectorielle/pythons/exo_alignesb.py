

# --------- PYODIDE:env --------- #

def colineaires(x_u, y_u, x_v, y_v):
    return x_u * y_v - y_u * x_v == 0


def vecteur(x_A, y_A, x_B, y_B):
    x_AB = x_B - x_A
    y_AB = y_B - y_A
    return (x_AB, y_AB)

# --------- PYODIDE:code --------- #

def alignes(x_A, y_A, x_B, y_B, x_C, y_C):
    x_AB = ...
    ...
    x_AC = ...
    ...
    if ...:
        return ...
    else:
        return ...

# --------- PYODIDE:corr --------- #

def alignes(x_A, y_A, x_B, y_B, x_C, y_C):
    x_AB = x_B - x_A
    y_AB = y_B - y_A
    x_AC = x_C - x_A
    y_AC = y_C - y_A
    if x_AB * y_AC - y_AB * x_AC == 0:
        return True
    else:
        return False

# --------- PYODIDE:tests --------- #

assert alignes(-2, 2, 1, 1, 7, -1) is True
assert alignes(-2, 2, 1, 1, 7, -2) is False

# --------- PYODIDE:secrets --------- #


# tests secrets
from random import randrange

for _ in range(10):
    x_A = randrange(-200, 200) / 2
    x_B = randrange(-200, 200) / 2
    x_C = randrange(-200, 200) / 2
    y_A = randrange(-200, 200) / 2
    y_B = randrange(-200, 200) / 2
    y_C = randrange(-200, 200) / 2
    attendu = (x_B - x_A) * (y_C - y_A) - (y_B - y_A) * (x_C - x_A) == 0
    assert (
        alignes(x_A, y_A, x_B, y_B, x_C, y_C) is attendu
    ), f"Erreur avec A {x_A, y_A}, B {x_B, y_B} et  C {x_C, y_C}"