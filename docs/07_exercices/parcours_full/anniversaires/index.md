---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Anniversaires
tags:
    - dictionnaire
difficulty: 170
---

# Anniversaires

On dispose d'un dictionnaire Python dans lequel :

* les clés sont les prénoms de différentes personnes. Il n'y a aucun prénom en double
* les valeurs sont les mois de naissance de ces personnes stockées sous forme de nombres entiers (`#!py 1` pour janvier, ... `#!py 12` pour décembre)

Par exemple :

```python
naissances = {'Nicolas': 10, 'Antoine': 7, 'Camille': 7}
```

Vous devez écrire une fonction `anniversaires(naissances, mois)` prenant en arguments le dictionnaire décrit ci-dessus ainsi qu'un numéro d'un mois et renvoyant une liste contenant les prénoms des personnes nées durant ce mois.

La liste renvoyée doit contenir les prénoms attendus dans un ordre quelconque.

???+ example "Exemples"

    ```pycon title=""
    >>> anniversaires({'Nicolas': 10, 'Antoine': 7, 'Camille': 7}, 1)
    []
    >>> anniversaires({'Nicolas': 10, 'Antoine': 7, 'Camille': 7}, 10)
    ['Nicolas']
    >>> anniversaires({'Nicolas': 10, 'Antoine': 7, 'Camille': 7}, 7)
    ['Antoine', Camille]
    >>> anniversaires({'Nicolas': 10, 'Antoine': 7, 'Camille': 7}, 13)
    []
    >>> anniversaires({}, 1)
    []
    ```

???+ note "Tests sur les résultats triés"

    En Python, la comparaison de listes prend compte de l'ordre des éléments. Ainsi, les listes `#!py [0, 1]` et `#!py [1, 0]` diffèrent, quand bien même elles contiennent les mêmes valeurs.

    Afin de s'assurer que la liste renvoyée par la fonction `anniversaires` contient bien les éléments attendus sans avoir à leur imposer un ordre, on compare donc la version triée du résultat à la valeur attendue. Ceci explique les tests sous la forme `#!py assert sorted(anniversaires(naissances, mois)) == résultat_attendu`.

{{ IDE('exo') }}
