# --- exo, while_  --- #
def est_premier(n):
    ...


# --- vide, while_ --- #
def est_premier(n):
    if ... < ...:
        return ...
    d = ...
    while ...:
        if ...:
            return ...
        d = ...
    return True


# --- corr, while_ --- #
def est_premier(n):
    if n < 2:
        return False
    d = 2
    while d ** 2 <= n:
        if n % d == 0:
            return False
        d += 1
    return True
# --- exo, for_ --- #
from math import sqrt


def est_premier(n):
    ...


# --- vide, for_ --- #
from math import sqrt


def est_premier(n):
    if ... < ...:
        return ...
    for d in range(..., int(...) + ...):
        if ...:
            return ...
    return ...


# --- corr, for_ --- #
from math import sqrt


def est_premier(n):
    if n < 2:
        return False
    for d in range(2, int(sqrt(n)) + 1):
        if n % d == 0:
            return False
    return True
# --- tests, for_, while_ --- #
assert est_premier(0) is False
assert est_premier(1) is False
assert est_premier(2) is True
assert est_premier(3) is True
assert est_premier(4) is False
assert est_premier(31) is True
# --- secrets, for_, while_ --- #
premiers = {
    2,
    3,
    5,
    7,
    11,
    13,
    17,
    19,
    23,
    29,
    31,
    37,
    41,
    43,
    47,
    53,
    59,
    61,
    67,
    71,
    73,
    79,
    83,
    89,
    97,
    101,
    103,
    107,
    109,
    113,
    127,
    131,
    137,
    139,
    149,
    151,
    157,
    163,
    167,
    173,
    179,
    181,
    191,
    193,
    197,
    199,
    211,
    223,
    227,
    229,
    233,
    239,
    241,
    251,
    257,
    263,
    269,
    271,
    277,
    281,
    283,
    293,
    307,
    311,
    313,
    317,
    331,
    337,
    347,
    349,
    353,
    359,
    367,
    373,
    379,
    383,
    389,
    397,
    401,
    409,
    419,
    421,
    431,
    433,
    439,
    443,
    449,
    457,
    461,
    463,
    467,
    479,
    487,
    491,
    499,
    503,
    509,
    521,
    523,
    541,
    547,
    557,
    563,
    569,
    571,
    577,
    587,
    593,
    599,
    601,
    607,
    613,
    617,
    619,
    631,
    641,
    643,
    647,
    653,
    659,
    661,
    673,
    677,
    683,
    691,
    701,
    709,
    719,
    727,
    733,
    739,
    743,
    751,
    757,
    761,
    769,
    773,
    787,
    797,
    809,
    811,
    821,
    823,
    827,
    829,
    839,
    853,
    857,
    859,
    863,
    877,
    881,
    883,
    887,
    907,
    911,
    919,
    929,
    937,
    941,
    947,
    953,
    967,
    971,
    977,
    983,
    991,
    997,
}
from random import randrange

for _ in range(20):
    n = randrange(10, 1000)
    attendu = n in premiers
    assert est_premier(n) is attendu, f"Erreur avec {n = }"
# --- rem, for_, while_ --- #
""" # skip
Le test proposé ici est extrêmement rudimentaire. Il ne peut pas être mis en œuvre dans le cas de grands nombres entiers.

A titre d'exemple, en février 2024, le plus grand nombre premier connu est $2^{82\,589\,933} − 1$. 
Ce nombre s'écrit avec $24\,862\,048$ chiffres en base $10$.

Appliquée à ce nombre, la méthode proposée ici nécessiterait environ $\sqrt{2^{82\,589\,933} − 1}$ vérifications... Ce nombre s'écrit avec plus de $12$ millions de chiffres !
"""  # skip
