Le test proposé ici est extrêmement rudimentaire. Il ne peut pas être mis en œuvre dans le cas de grands nombres entiers.

A titre d'exemple, en février 2024, le plus grand nombre premier connu est $2^{82\,589\,933} − 1$. 
Ce nombre s'écrit avec $24\,862\,048$ chiffres en base $10$.

Appliquée à ce nombre, la méthode proposée ici nécessiterait environ $\sqrt{2^{82\,589\,933} − 1}$ vérifications... Ce nombre s'écrit avec plus de $12$ millions de chiffres !
