C'est la version correspondant à l'énoncé.

En effet, pour dénombrer `nb_k_sommes(n, k)`, on fera deux cas :

- Les sommes dont le plus petit terme est `1`.
    - Il reste `k - 1` termes, pour une somme de `n -1`.
    - il y a donc `nb_k_sommes(n - 1, k - 1)` telles partitions.
- Les sommes dont tous les termes sont supérieurs à `1`. On enlève `1` à chaque terme, ce qui donne une somme égale à `n - k` en `k` parties. Réciproquement, chaque partition de `n - k`  en `k` termes correspond à une partition de `n` en `k` termes où chacun est plus grand que 1. Il y en a `nb_k_sommes(n - k, k)`

# Variante avec une autre méthode

En représentant les sommes de $n$ en $k$ termes, avec un diagramme de Ferres, comme avec la somme $7 = 3 + 2 + 1+ 1$ (somme en **4** parties)

    # # # #
    # #
    #

On peut aussi basculer cette représentation en $7 = 4 + 2 +1$

    # # #
    # #
    #
    #

Et dire qu'il s'agit d'une somme où le plus grand terme est **4**.

**Théorème** : Le nombre de partitions de $n$ en $k$ parties est égal au nombre de partitions de $n$ dont la plus grande partie est égale à $k$.


Montrons comment établir la même relation de récurrence avec cet angle de vue.


Les partitions de $n$ peuvent aussi se dénombrer comme 

- Les sommes de `n` en termes dont le plus grand terme est égal à `k`, et ce, pour tout `k` inférieur ou égal à `n`.
- Comment calculer le nombre de sommes de `n` en termes dont le plus grand terme est égal à `k` de manière générale ?
    - Si `n < k`, il n'y a aucune façon.
    - Si `k = 1`, il n'y a qu'une seule façon : que des 1.
    - Sinon, les sommes se partagent en deux catégories :
        - `k` est seul en tant que maximum, on lui enlève 1, on obtient une partition de `n` dont le plus grand terme est `k-1` (et réciproquement).
        - `k` n'est pas le seul en tant que maximum, on enlève cette première colonne, on obtient une partition de `n - k` dont le terme maximal est `k` (celui de l'ancienne deuxième colonne).

On retrouve très exactement la même récurrence.


# Mémoïsation avec le cache LRU

:warning: Cette section est uniquement pour les experts. :warning:

On peut utiliser un décorateur de fonction ! Il en existe pour automatiser la mémoïsation et ne conserver qu'une certaine quantité d'antécédents ; les plus récemment utilisés.

Avantages : c'est plus facile à écrire, c'est automatique, c'est très efficace.
Problème : Ne pas utiliser un jour d'examen, sauf si vous êtes capable d'expliquer parfaitement le fonctionnement !

```python
from functools import lru_cache

@lru_cache
def nb_sommes(n):
    @lru_cache
    def nb_k_sommes(n, k):
        if n < k:
            return 0
        elif k == 1:
            return 1
        else:
            return nb_k_sommes(n - 1, k - 1) + nb_k_sommes(n - k, k)
    
    return sum(nb_k_sommes(n, k) for k in range(1, 1 + n))
```

Remarque : à partir de Python 3.9, le décorateur `cache` remplace avantageusement le décorateur `lru_cache` pour les situations (comme ici) où on ne met pas de limite à la zone tampon pour mémoriser les images.


