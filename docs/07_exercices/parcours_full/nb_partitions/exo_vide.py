# --------- PYODIDE:code --------- #
def nb_sommes(n):
    ...


# --------- PYODIDE:corr --------- #
memo = dict()


def nb_k_sommes(n, k):
    if (n, k) not in memo:
        if n < k:
            memo[(n, k)] = 0
        elif k == 1:
            memo[(n, k)] = 1
        else:
            memo[(n, k)] = nb_k_sommes(n - 1, k - 1) + nb_k_sommes(n - k, k)
    return memo[(n, k)]


def nb_sommes(n):
    return sum(nb_k_sommes(n, k) for k in range(1, 1 + n))


# --------- PYODIDE:tests --------- #

assert nb_sommes(3) == 3
assert nb_sommes(5) == 7

# --------- PYODIDE:secrets --------- #

attendus = [
    -1,
    1,
    2,
    3,
    5,
    7,
    11,
    15,
    22,
    30,
    42,
    56,
    77,
    101,
    135,
    176,
    231,
    297,
    385,
    490,
    627,
    792,
    1002,
    1255,
    1575,
    1958,
    2436,
    3010,
    3718,
    4565,
    5604,
    6842,
    8349,
    10143,
    12310,
    14883,
    17977,
    21637,
    26015,
    31185,
    37338,
    44583,
    53174,
]

for n in range(1, 43):
    attendu = attendus[n]
    assert nb_sommes(n) == attendu, f"Erreur avec {n = }"
