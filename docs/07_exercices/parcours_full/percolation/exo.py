

# --------- PYODIDE:env --------- #


def nouveau_sol(nom="base"):
    """
    Renvoie un sol sec
    Trois sols sont proposés
    Par défaut on récupère le sol 'base'
    """
    sols = {
        "base": [
            [1, 1, 1, 1, 1, 0, 1, 1, 1, 1],
            [1, 1, 0, 1, 0, 0, 0, 0, 0, 1],
            [1, 1, 0, 1, 0, 1, 1, 1, 0, 1],
            [1, 1, 1, 1, 0, 1, 1, 0, 0, 1],
            [1, 0, 1, 0, 1, 0, 0, 0, 1, 1],
            [1, 0, 1, 1, 1, 1, 0, 1, 1, 1],
            [1, 1, 1, 0, 0, 1, 1, 1, 0, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        ],
        "puits": [
            [1, 0, 1],
            [1, 0, 1],
            [1, 0, 1],
            [1, 0, 1],
            [1, 1, 1],
        ],
        "diagonale": [
            [1, 0, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 0, 0, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 0, 0, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 0, 0, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 0, 0, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 0, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        ],
    }
    return [ligne.copy() for ligne in sols[nom]]




# --------- PYODIDE:code --------- #

TERRE = 1
VIDE = 0
EAU = 2


def percolation(sol, i, j, prof_max):
    sol[i][j] = ...

    if ... == ...:
        return True
    else:
        if ... == VIDE:
            if percolation(sol, ..., ..., prof_max):
                return True

        ...

        ...

        return False

# --------- PYODIDE:corr --------- #

TERRE = 1
VIDE = 0
EAU = 2


def percolation(sol, i, j, prof_max):
    sol[i][j] = EAU

    if i == prof_max:
        return True
    else:
        if sol[i + 1][j] == VIDE:
            if percolation(sol, i + 1, j, prof_max):
                return True

        if sol[i][j - 1] == VIDE:
            if percolation(sol, i, j - 1, prof_max):
                return True

        if sol[i][j + 1] == VIDE:
            if percolation(sol, i, j + 1, prof_max):
                return True

        return False


def nouveau_sol(nom="base"):
    """
    Renvoie un sol sec
    Trois sols sont proposés
    Par défaut on récupère le sol 'base'
    """
    sols = {
        "base": [
            [1, 1, 1, 1, 1, 0, 1, 1, 1, 1],
            [1, 1, 0, 1, 0, 0, 0, 0, 0, 1],
            [1, 1, 0, 1, 0, 1, 1, 1, 0, 1],
            [1, 1, 1, 1, 0, 1, 1, 0, 0, 1],
            [1, 0, 1, 0, 1, 0, 0, 0, 1, 1],
            [1, 0, 1, 1, 1, 1, 0, 1, 1, 1],
            [1, 1, 1, 0, 0, 1, 1, 1, 0, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        ],
        "puits": [
            [1, 0, 1],
            [1, 0, 1],
            [1, 0, 1],
            [1, 0, 1],
            [1, 1, 1],
        ],
        "diagonale": [
            [1, 0, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 0, 0, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 0, 0, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 0, 0, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 0, 0, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 0, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        ],
    }
    return [ligne.copy() for ligne in sols[nom]]

# --------- PYODIDE:tests --------- #

# Test profondeur 0 (déjà atteinte)
sol = nouveau_sol()
assert percolation(sol, 0, 5, 0) is True
# Test profondeur 1 (atteignable)
sol = nouveau_sol()
assert percolation(sol, 0, 5, 1) is True
# Test profondeur 5 (atteignable)
sol = nouveau_sol()
assert percolation(sol, 0, 5, 5) is True
# Test profondeur 6 (non atteignable)
sol = nouveau_sol()
assert percolation(sol, 0, 5, 6) is False

# --------- PYODIDE:secrets --------- #

# Tests
from copy import deepcopy

base_sol = [
    [1, 1, 1, 1, 1, 0, 1, 1, 1, 1],
    [1, 1, 0, 1, 0, 0, 0, 0, 0, 1],
    [1, 1, 0, 1, 0, 1, 1, 1, 0, 1],
    [1, 1, 1, 1, 0, 1, 1, 0, 0, 1],
    [1, 0, 1, 0, 1, 0, 0, 0, 1, 1],
    [1, 0, 1, 1, 1, 1, 0, 1, 1, 1],
    [1, 1, 1, 0, 0, 1, 1, 1, 0, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
]

# Test profondeur 0 (déjà atteinte)
sol = deepcopy(base_sol)
assert percolation(sol, 0, 5, 0) is True
# Test profondeur 1 (atteignable)
sol = deepcopy(base_sol)
assert percolation(sol, 0, 5, 1) is True
# Test profondeur 5 (atteignable)
sol = deepcopy(base_sol)
assert percolation(sol, 0, 5, 5) is True
# Test profondeur 6 (non atteignable)
sol = deepcopy(base_sol)
assert percolation(sol, 0, 5, 6) is False

# Tests supplémentaires
base_sol = [
    [1, 0, 1],
    [1, 0, 1],
    [1, 0, 1],
    [1, 0, 1],
    [1, 1, 1],
]

# Test profondeur 0 (déjà atteinte)
sol = deepcopy(base_sol)
assert percolation(sol, 0, 1, 0) is True
# Test profondeur 3 (atteignable)
sol = deepcopy(base_sol)
assert percolation(sol, 0, 1, 1) is True
# Test profondeur 4 (non atteignable)
sol = deepcopy(base_sol)
assert percolation(sol, 0, 1, 4) is False

base_sol = [
    [1, 0, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 0, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 0, 0, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 0, 0, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 0, 0, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 0, 0, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 0, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
]

# Test profondeur 0 (déjà atteinte)
sol = deepcopy(base_sol)
assert percolation(sol, 0, 1, 0) is True
# Test profondeur 3 (atteignable)
sol = deepcopy(base_sol)
assert percolation(sol, 0, 1, 1) is True
# Test profondeur 6 (atteignable)
sol = deepcopy(base_sol)
assert percolation(sol, 0, 1, 1) is True
# Test profondeur 7 (non atteignable)
sol = deepcopy(base_sol)
assert percolation(sol, 0, 1, 7) is False
