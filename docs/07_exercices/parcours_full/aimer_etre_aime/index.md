---
author:
    - Guillaume Connan
    - Nicolas Revéret
    - Pierre Marquestaut
hide:
    - navigation
    - toc
title: Aimer et être aimé
difficulty: 220
tags:
    - dictionnaire
    - graphe
maj: 06/06/2024
---

Sur le réseau social *Immediam*, des utilisateurs peuvent s'abonner au compte d'autres utilisateurs. Si l'utilisateur A s'abonne au compte de B, on dit que A « suit » B.

Ces relations sont schématisées ci-dessous. Dans cette représentation, une flèche allant d'un sommet Α vers un
sommet B indique que l'utilisateur A « suit » l'utilisateur B.

<center>

```mermaid
flowchart LR
    A([Anna]) --> B([Billy])
    B --> A
    B --> E([Eroll])
    C([Carl]) --> B
    D([Dora]) --> G([Gaby])
    E --> B
    E --> D
    E --> G
    E --> F([Flynn])
    F --> G
    G --> E
```
</center>

On peut observer que Eroll suit Billy, Dora, Flynn et Gaby.

On souhaite établir, pour chaque utilisateur du réseau, la liste des personnes qui le suivent. Ce sont ses *abonnés*.

Les abonnés de Eroll sont Billy et Gaby. Les abonnés de Gaby sont Dora, Eroll et Flynn.

On représente ce réseau *Immediam* en machine par un dictionnaire dans lequel :

- les clés sont les chaînes de caractères correspondant aux noms des utilisateurs,
  
- les valeurs associées sont des listes de chaînes de caractères représentant les personnes suivies par cet utilisateur.

Écrire une fonction  `etre_aime` qui :

- prend en argument un dictionnaire `reseau` représentant un tel réseau *Immediam* ;

- renvoie un dictionnaire qui associe à chaque membre du réseau (représenté par une chaine de caractères) la liste de ses abonnés. 

??? note "Ordre indifférent"

    La liste des abonnés associée à chaque membre pourra contenir les membres dans n'importe quel ordre.
    
    La fonction `#!py compare` utilisée dans les tests permet de valider des ordres différents.


???+ example "Exemples"  

    ```pycon title=""
    >>> petit = {"Anna": ["Basile"], "Basile": []}
    >>> etre_aime(petit)
    {'Anna': [], 'Basile': ['Anna']}
    >>> immediam = {
    ...     "Anna":  ["Billy"],
    ...     "Billy": ["Anna", "Eroll"],
    ...     "Carl":  ["Billy"],
    ...     "Dora":  ["Gaby"],
    ...     "Eroll": ["Billy", "Dora", "Flynn", "Gaby"],
    ...     "Flynn": ["Gaby"],
    ...     "Gaby":  ["Eroll"],
    ... }
    >>> etre_aime(immediam)
    {'Billy': ['Anna', 'Carl', 'Eroll'], 'Anna': ['Billy'], 'Eroll': ['Billy', 'Gaby'], 'Gaby': ['Dora', 'Eroll', 'Flynn'],'Dora': ['Eroll'], 'Flynn': ['Eroll']}
    ```

{{ IDE('exo') }}
