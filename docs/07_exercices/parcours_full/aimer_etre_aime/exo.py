# --------- PYODIDE:env --------- #


def comparer(dico1, dico2):
    for cle in dico1:
        if cle not in dico2:
            return False
        if not sorted(dico1[cle]) == sorted(dico2[cle]):
            return False
    return True


# --------- PYODIDE:code --------- #


def etre_aime(reseau, membre): ...


# --------- PYODIDE:corr --------- #


def etre_aime(reseau):
    dico = {}
    for personne in reseau:
        for aime in reseau[personne]:
            if aime in dico:
                dico[aime].append(personne)
            else:
                dico[aime] = [personne]
    return dico


# --------- PYODIDE:tests --------- #

# petit = {"Anna": ["Basile"], "Basile": []}
# assert etre_aime(petit) == {"Anna": [], "Basile": ["Anna"]}

immediam = {
    "Anna": ["Billy"],
    "Billy": ["Anna", "Eroll"],
    "Carl": ["Billy"],
    "Dora": ["Gaby"],
    "Eroll": ["Billy", "Dora", "Flynn", "Gaby"],
    "Flynn": ["Gaby"],
    "Gaby": ["Eroll"],
}


assert comparer(
    etre_aime(immediam),
    {
        "Billy": ["Anna", "Carl", "Eroll"],
        "Anna": ["Billy"],
        "Eroll": ["Billy", "Gaby"],
        "Gaby": ["Dora", "Eroll", "Flynn"],
        "Dora": ["Eroll"],
        "Flynn": ["Eroll"],
    },
)


# --------- PYODIDE:secrets --------- #


# Tests supplémentaires
immediam = {
    "Anna": ["Billy"],
    "Billy": ["Anna"],
    "Carl": ["Billy"],
    "Dora": ["Gaby"],
    "Eroll": ["Billy", "Dora", "Flynn", "Gaby"],
    "Flynn": ["Eroll", "Gaby"],
    "Gaby": ["Eroll", "Flynn"],
    "Charles": [],
}

assert comparer(
    etre_aime(immediam),
    {
        "Billy": ["Anna", "Carl", "Eroll"],
        "Anna": ["Billy"],
        "Gaby": ["Dora", "Eroll", "Flynn"],
        "Dora": ["Eroll"],
        "Flynn": ["Eroll", "Gaby"],
        "Eroll": ["Flynn", "Gaby"],
    },
)

def abonnes(reseau):
    resultat = {personne: [] for personne in reseau}
    for personne in reseau:
        for personne_suivie in reseau[personne]:
            resultat[personne_suivie].append(personne)
    return resultat


petit = {"Anna": ["Basile"], "Basile": []}
assert abonnes(petit) == {"Anna": [], "Basile": ["Anna"]}

immediam = {
    "Anna": ["Billy"],
    "Billy": ["Anna", "Eroll"],
    "Carl": ["Billy"],
    "Dora": ["Gaby"],
    "Eroll": ["Billy", "Dora", "Flynn", "Gaby"],
    "Flynn": ["Gaby"],
    "Gaby": ["Eroll"],
}


assert comparer(
    abonnes(immediam),
    {
        "Billy": ["Anna", "Carl", "Eroll"],
        "Anna": ["Billy"],
        "Eroll": ["Billy", "Gaby"],
        "Gaby": ["Dora", "Eroll", "Flynn"],
        "Dora": ["Eroll"],
        "Flynn": ["Eroll"],
    },
)


immediam = {
    "Anna": ["Billy"],
    "Billy": ["Anna"],
    "Carl": ["Billy"],
    "Dora": ["Gaby"],
    "Eroll": ["Billy", "Dora", "Flynn", "Gaby"],
    "Flynn": ["Eroll", "Gaby"],
    "Gaby": ["Eroll", "Flynn"],
    "Charles": [],
}

assert comparer(
    abonnes(immediam),
    {
        "Billy": ["Anna", "Carl", "Eroll"],
        "Anna": ["Billy"],
        "Gaby": ["Dora", "Eroll", "Flynn"],
        "Dora": ["Eroll"],
        "Flynn": ["Eroll", "Gaby"],
        "Eroll": ["Flynn", "Gaby"],
    },
)