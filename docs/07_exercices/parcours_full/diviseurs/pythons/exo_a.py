

# --------- PYODIDE:code --------- #

def diviseurs(n):
    liste_diviseurs = [1]
    if n == 1:
        return ...
    for d in range(..., n//2 + 1):
        if ...:
            liste_diviseurs.append(...)
    liste_diviseurs.append(...)
    return ...

# --------- PYODIDE:corr --------- #

def diviseurs(n):
    liste_diviseurs = [1]
    if n == 1:
        return liste_diviseurs
    for d in range(2, n//2 + 1):
        if n % d == 0:
            liste_diviseurs.append(d)
    liste_diviseurs.append(n)
    return liste_diviseurs

# --------- PYODIDE:tests --------- #

assert diviseurs(1) == [1]
assert diviseurs(15) == [1, 3, 5, 15]
assert diviseurs(17) == [1, 17]

# --------- PYODIDE:secrets --------- #


# tests secrets
assert diviseurs(25) == [1, 5, 25]
assert diviseurs(72) == [1, 2, 3, 4, 6, 8, 9, 12, 18, 24, 36, 72]
assert diviseurs(81) == [1, 3, 9, 27, 81]
assert diviseurs(7) == [1, 7]
assert diviseurs(49) == [1, 7, 49]
assert diviseurs(100) == [1, 2, 4, 5, 10, 20, 25, 50, 100]

possibles = {
    8245: [1, 5, 17, 85, 97, 485, 1649, 8245],
    9651: [1, 3, 3217, 9651],
    5356: [1, 2, 4, 13, 26, 52, 103, 206, 412, 1339, 2678, 5356],
    7542: [1, 2, 3, 6, 9, 18, 419, 838, 1257, 2514, 3771, 7542],
    3435: [1, 3, 5, 15, 229, 687, 1145, 3435],
    3791: [1, 17, 223, 3791],
    3155: [1, 5, 631, 3155],
    9684: [
        1,
        2,
        3,
        4,
        6,
        9,
        12,
        18,
        36,
        269,
        538,
        807,
        1076,
        1614,
        2421,
        3228,
        4842,
        9684,
    ],
    8475: [1, 3, 5, 15, 25, 75, 113, 339, 565, 1695, 2825, 8475],
    3742: [1, 2, 1871, 3742],
    6740: [1, 2, 4, 5, 10, 20, 337, 674, 1348, 1685, 3370, 6740],
    3705: [1, 3, 5, 13, 15, 19, 39, 57, 65, 95, 195, 247, 285, 741, 1235, 3705],
    9466: [1, 2, 4733, 9466],
    1033: [1, 1033],
    4316: [1, 2, 4, 13, 26, 52, 83, 166, 332, 1079, 2158, 4316],
    7622: [1, 2, 37, 74, 103, 206, 3811, 7622],
    3508: [1, 2, 4, 877, 1754, 3508],
    8639: [1, 53, 163, 8639],
    1903: [1, 11, 173, 1903],
    9040: [
        1,
        2,
        4,
        5,
        8,
        10,
        16,
        20,
        40,
        80,
        113,
        226,
        452,
        565,
        904,
        1130,
        1808,
        2260,
        4520,
        9040,
    ],
}

from random import choice

n = choice([x for x in possibles])
attendu = possibles[n]
assert diviseurs(n) == attendu, f"Erreur en factorisant {n = }"