Un programme plus simple serait:

```python
def diviseurs(n):
    diviseurs = [1]
    for d in range(2, n + 1):
        if n % d == 0:
            diviseurs.append(d)
    return diviseurs
```

Ou avec une définition de liste en compréhension:

```python
def diviseurs(n):
    return [d for d in range(1, n + 1) if n % d == 0]
```

Mais comme il n'y a pas de diviseurs compris strictement entre `#!py n // 2` et `n`, la moitié des tests effectués serait inutile.
