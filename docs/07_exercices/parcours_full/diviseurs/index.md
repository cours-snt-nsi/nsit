---
author: Serge Bays
hide:
    - navigation
    - toc
title: Diviseurs d'un entier positif
tags:
    - maths
    - liste/tableau
    - à trous
difficulty: 130
maj: 01/03/2024
---


???+ tip "Entiers strictement positifs"

    Tous les nombres considérés ici sont des entiers strictement positifs.

Le nombre $d$ est un diviseur du nombre $n$ s'il existe un nombre $q$ tel que $n = d \times q$, 
autrement dit, si le reste dans la division euclidienne de $n$ par $d$ est nul.

L'objectif est de construire la liste des diviseurs d'un nombre $n$ quelconque. Par exemple, la liste des diviseurs de $n = 15$ est $[1, 3, 5, 15]$.


Compléter la fonction `diviseurs` qui prend en paramètre un entier `n` strictement positif et renvoie la liste des diviseurs de `n` **triés dans l'ordre croissant**.

Pour cette fonction, on tient compte du fait qu'il n'y a aucun diviseur compris entre $\dfrac{n}{2}$ et $n$.

???+ example "Exemples"

    ```pycon title=""
    >>> diviseurs(1)
    [1]
    >>> diviseurs(15)
    [1, 3, 5, 15]
    >>> diviseurs(17)
    [1, 17]
    ```
    
=== "Version vide"
    {{ IDE('./pythons/exo_b') }}
=== "Version à compléter"
    {{ IDE('./pythons/exo_a') }}
