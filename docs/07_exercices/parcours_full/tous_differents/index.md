---
author: Romain Janvier
hide:
    - navigation
    - toc
title: Tous différents
tags:
    - boucle
    - booléen
    - liste/tableau
difficulty: 201
---

# Tableau avec des éléments tous différents

Un tableau peut contenir plusieurs fois le même élément. C'est le cas du tableau `tableau_1` ci-dessous :
```python
tableau_1 = [1, 2, 3, 6, 2, 4, 5]
```
La valeur `#!py 2` est deux fois dans ce tableau.

Au contraire, dans le tableau `tableau_2`, toutes les valeurs sont uniques :
```python
tableau_2 = ['chien', 'chat', 'lion', 'poisson']
```

Écrire une fonction `tous_differents` qui prend un tableau `tableau` et renvoie un booléen indiquant si toutes les valeurs de `tableau` sont différentes ou non.

!!! info "Taille des tableaux"
    Pour limiter le temps de calcul, on se limitera à des tests avec des
    tableaux de moins de 100 éléments.

??? tip "Utilisation de `#!py range(a, b)`"
    On pourra éventuellement utiliser `range(a, b)` qui itère sur toutes
    les valeurs entières allant de `a` inclus à `b` **exclu**.

???+ example "Exemples"

    ```pycon title=""
    >>> tableau_1 = [1, 2, 3, 6, 2, 4, 5]
    >>> tous_differents(tableau_1)
    False
    >>> tableau_2 = ['chien', 'chat', 'lion', 'poisson']
    >>> tous_differents(tableau_2)
    True
    ```
    
{{ IDE('exo') }}

