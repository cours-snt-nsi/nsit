

# --------- PYODIDE:env --------- #


class Wagon:
    def __init__(self, contenu):
        "Constructeur"
        self.contenu = contenu
        self.suivant = None

    def __repr__(self):
        "Affichage dans la console"
        return f"Wagon de {self.contenu}"

    def __str__(self):
        "Conversion en string"
        return self.__repr__()


# --------- PYODIDE:code --------- #

class Train:
    def __init__(self):
        "Constructeur"
        self.premier = None
        self.nb_wagons = ...

    def est_vide(self):
        """renvoie True si ce train est vide (ne comporte aucun wagon),
        False sinon
        """
        return ...

    def donne_nb_wagons(self):
        "Renvoie le nombre de wagons de ce train"
        return ...

    def transporte_du(self, contenu):
        """Détermine si ce train transporte du {contenu} (une chaine de caractères).
        Renvoie True si c'est le cas, False sinon
        """
        wagon = self.premier
        while wagon is not None:
            if wagon.contenu == ...:
                return ...
            ... = wagon....
        return ...

    def ajoute_wagon(self, nouveau):
        """Ajoute un wagon à la fin de ce train.
        L'argument est le wagon à ajouter
        """
        if self.est_vide():
            self.premier = ...
        else:
            wagon = self.premier
            while ....suivant is not None:
                wagon = ....suivant
            wagon.suivant = ...
        self.nb_wagons = ...

    def supprime_wagon_de(self, contenu):
        """Supprime le premier wagon de {contenu}
        Renvoie False si ce train ne contient pas de {contenu},
        True si la suppression est effectuée
        """
        # On parcourt le train afin de trouver le contenu
        precedent = None
        wagon = self.premier
        while wagon is not ... and wagon.contenu != ...:
            precedent = wagon
            wagon = wagon....

        if wagon is ...:  # on a parcouru tout le train sans trouver le contenu
            return ...
        if precedent is ...:  # le wagon supprimé est le premier du train
            self.premier = wagon....
        else:  # le wagon supprimé n'est pas le premier
            precedent.... = wagon....
        self.nb_wagons -= ...
        return ...

    def __repr__(self):
        "Affichage dans la console"
        contenus_wagons = ['']
        wagon = self.premier
        while wagon is not None:
            contenus_wagons.append(str(wagon))
            wagon = wagon.suivant
        return "Locomotive" + " - ".join(contenus_wagons)

    def __str__(self):
        "Conversion en string"
        return self.__repr__()

# --------- PYODIDE:corr --------- #

class Wagon:
    def __init__(self, contenu):
        "Constructeur"
        self.contenu = contenu
        self.suivant = None

    def __repr__(self):
        "Affichage dans la console"
        return f"Wagon de {self.contenu}"

    def __str__(self):
        "Conversion en string"
        return self.__repr__()


class Train:
    def __init__(self):
        "Constructeur"
        self.premier = None
        self.nb_wagons = 0

    def est_vide(self):
        """renvoie True si ce train est vide (ne comporte aucun wagon),
        False sinon
        """
        return self.nb_wagons == 0

    def donne_nb_wagons(self):
        "Renvoie le nombre de wagons de ce train"
        return self.nb_wagons

    def transporte_du(self, contenu):
        """Détermine si ce train transporte du contenu (une chaine de caractères).
        Renvoie True si c'est le cas, False sinon
        """
        wagon = self.premier
        while wagon is not None:
            if wagon.contenu == contenu:
                return True
            wagon = wagon.suivant
        return False

    def ajoute_wagon(self, nouveau):
        """Ajoute un wagon à la fin de ce train.
        L'argument est le wagon à ajouter
        """
        if self.est_vide():
            self.premier = nouveau
        else:
            wagon = self.premier
            while wagon.suivant is not None:
                wagon = wagon.suivant
            wagon.suivant = nouveau
        self.nb_wagons += 1

    def supprime_wagon_de(self, contenu):
        """Supprime le premier wagon de {contenu}
        Renvoie False si ce train ne contient pas de {contenu},
        True si la suppression est effectuée
        """
        # On parcourt le train afin de trouver le contenu
        precedent = None
        wagon = self.premier
        while wagon is not None and wagon.contenu != contenu:
            precedent = wagon
            wagon = wagon.suivant

        if wagon is None:  # on a parcouru tout le train sans trouver le contenu
            return False
        if precedent is None:  # le wagon supprimé est le premier du train
            self.premier = wagon.suivant
        else:  # le wagon supprimé n'est pas le premier
            precedent.suivant = wagon.suivant
        self.nb_wagons -= 1
        return True

    def __repr__(self):
        "Affichage dans la console"
        contenus_wagons = [""]
        wagon = self.premier
        while wagon is not None:
            contenus_wagons.append(str(wagon))
            wagon = wagon.suivant
        return "Locomotive" + " - ".join(contenus_wagons)

    def __str__(self):
        "Conversion en string"
        return self.__repr__()

# --------- PYODIDE:tests --------- #

train = Train()
w1 = Wagon("blé")
train.ajoute_wagon(w1)
w2 = Wagon("riz")
train.ajoute_wagon(w2)
train.ajoute_wagon(Wagon("sable"))
assert str(train) == 'Locomotive - Wagon de blé - Wagon de riz - Wagon de sable'
assert train.est_vide() is False
assert train.donne_nb_wagons() == 3
assert train.transporte_du('blé') is True
assert train.transporte_du('matériel') is False
assert train.supprime_wagon_de('riz') is True
assert str(train) == 'Locomotive - Wagon de blé - Wagon de sable'
assert train.supprime_wagon_de('riz') is False

# --------- PYODIDE:secrets --------- #

# Tests
train = Train()
w1 = Wagon("blé")
train.ajoute_wagon(w1)
w2 = Wagon("riz")
train.ajoute_wagon(w2)
train.ajoute_wagon(Wagon("sable"))
assert str(train) == "Locomotive - Wagon de blé - Wagon de riz - Wagon de sable"
assert train.est_vide() is False
assert train.donne_nb_wagons() == 3
assert train.transporte_du("blé") is True
assert train.transporte_du("matériel") is False
assert train.supprime_wagon_de("riz") is True
assert str(train) == "Locomotive - Wagon de blé - Wagon de sable"
assert train.supprime_wagon_de("riz") is False


# Tests secrets
train = Train()
for k in range(10):
    train.ajoute_wagon(Wagon("A"))
    train.ajoute_wagon(Wagon("B"))
    assert train.donne_nb_wagons() == 2 * (k + 1)
    assert train.nb_wagons == train.donne_nb_wagons()
assert str(train) == "Locomotive - " + " - ".join(["Wagon de A", "Wagon de B"] * 10)
assert train.transporte_du("A") is True
assert train.transporte_du("B") is True
assert train.transporte_du("C") is False
for k in range(10):
    assert train.supprime_wagon_de("A") is True
    assert train.donne_nb_wagons() == 20 - (k + 1)
assert str(train) == "Locomotive - " + " - ".join(["Wagon de B"] * 10)
assert train.supprime_wagon_de("A") is False
assert train.supprime_wagon_de("C") is False
