On utilise un parcours en largeur. Le stockage des sommets à visiter est effectué à l'aide d'une file (modèle *premier entré, premier sorti*).

Chaque sommet à traiter est empilé avec sa "distance" à l'origine sous la forme d'un tuple. Ainsi, pour l'origine on enfile `#!py (origine, 0)`.

Le parcours continue tant que la file est non-vide et que l'on n'a pas visité la destination :

* on défile le sommet à traiter et sa distance à l'origine (le nombre d'étapes),
* on modifie le booléen associé au nom du sommet dans le dictionnaire des personnes au courant,
* on teste si ce nom est la destination (auquel cas on renvoie la distance),
* si ce n'est pas le cas, on ajoute ses voisins pas encore visités à la file en incrémentant la distance

Si le parcours se termine sans sortie prématurée, cela signifie que la destination n'est toujours pas au courant de la rumeur. L'énoncé demande de renvoyer `#!py None`.
