On aurait pu également envisager une boucle `for` avec sortie anticipée si la distribution n'est pas circulaire :

```python
def est_circulaire(distribution):
    personnes = [prenom for prenom in distribution]
    premiere = personnes[0]
    personne = premiere
    effectif = len(distribution)
    for i in range(effectif - 1):
        personne = distribution[personne]
        if personne == premiere:
            return False
    return True
```

Cet exercice ressemble à un exercice de graphe mais les règles de distribution des cadeaux simplifient le parcours.
