

# --------- PYODIDE:code --------- #

def est_circulaire(distribution):
    ...

# --------- PYODIDE:corr --------- #

def est_circulaire(distribution):
    personnes = [prenom for prenom in distribution]
    premiere = personnes[0]
    destinataire = distribution[premiere]
    nb_destinataires = 1

    while destinataire != premiere:
        destinataire = distribution[destinataire]
        nb_destinataires = nb_destinataires + 1

    return nb_destinataires == len(distribution)

# --------- PYODIDE:tests --------- #

distribution_a = {
    "Anne": "Elodie",
    "Bruno": "Fidel",
    "Claude": "Denis",
    "Denis": "Claude",
    "Elodie": "Bruno",
    "Fidel": "Anne",
}
distribution_b = {
    "Anne": "Claude",
    "Bruno": "Fidel",
    "Claude": "Elodie",
    "Denis": "Anne",
    "Elodie": "Bruno",
    "Fidel": "Denis",
}
assert est_circulaire(distribution_a) is False
assert est_circulaire(distribution_b) is True

# --------- PYODIDE:secrets --------- #


# Autres tests
assert est_circulaire({"A": "B", "F": "C", "C": "D", "E": "A", "B": "F", "D": "E"}) is True
assert est_circulaire({"A": "B", "F": "A", "C": "D", "E": "C", "B": "F", "D": "E"}) is False
assert est_circulaire({"A": "E", "F": "C", "C": "D", "E": "B", "B": "F", "D": "A"}) is True
assert est_circulaire({"A": "A", "B": "B"}) is False
assert est_circulaire({"A": "B", "B": "A"}) is True
assert est_circulaire({"A": "B", "B": "C", "C": "D", "D": "A"}) is True
assert est_circulaire({"A": "B", "B": "C", "C": "D", "D": "A", "E": "E"}) is False

from string import ascii_uppercase
from random import sample, randrange, shuffle

# Permutations circulaires valides
for _ in range(20):
    taille = randrange(2, 27)
    donateurs = sample(ascii_uppercase, taille)
    destinataires = [donateurs[-1]] + donateurs[:-1]
    couples = [(do, de) for do, de in zip(donateurs, destinataires)]
    shuffle(couples)
    distribution = dict(couples)
    assert est_circulaire(distribution) is True, f"Erreur avec {distribution}"
# Permutations non circulaires
for _ in range(20):
    taille_1 = randrange(2, 25)
    taille_2 = randrange(2, 27 - taille_1)
    personnes = sample(ascii_uppercase, taille_1 + taille_2)
    donateurs_1 = personnes[:taille_1]
    donateurs_2 = personnes[taille_1:]
    destinataires_1 = donateurs_1.copy()
    destinataires_2 = donateurs_2.copy()
    shuffle(destinataires_1)
    shuffle(destinataires_2)
    couples = [(do, de) for do, de in zip(donateurs_1, destinataires_1)]
    couples += [(do, de) for do, de in zip(donateurs_2, destinataires_2)]
    shuffle(couples)
    distribution = dict(couples)
    assert est_circulaire(distribution) is False, f"Erreur avec {distribution}"