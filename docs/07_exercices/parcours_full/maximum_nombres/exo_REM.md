Il s'agit d'une recherche de maximum classique. La liste étant non-vide, on initialise la variable `maxi` avec la première valeur. Cela permet de garantir que le maximum est bien une valeur de la liste.

Par exemple, avec la liste `#!py [-78, -7, -15, -3]`, le maximum est
`-3`. Si on initialise `maxi` avec 0, alors il ne sera jamais dépassé et
la fonction renverra `0`, ce qui est faux.

On peut également utiliser la valeur `#!py -float("inf")` qui correspond
à $-\infty$ et qui sera donc dépassée à la première valeur regardée. Cela
permet également de gérer les listes vides en admettant que le maximum
d'une liste vide est $-\infty$.

Par contre, si on sait que les valeurs de `nombres` sont toutes
supérieures à une valeur donnée (par exemple 0 pour des notes), on peut
initialiser `maxi` avec cette valeur minimale.
