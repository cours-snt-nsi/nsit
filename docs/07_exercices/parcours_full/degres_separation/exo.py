# --------- PYODIDE:env --------- #

from collections import deque


class File:
    """Classe définissant une structure de file"""

    def __init__(self):
        self.valeurs = deque([])

    def est_vide(self):
        """Renvoie le booléen True si la file est vide, False sinon"""
        return len(self.valeurs) == 0

    def enfile(self, x):
        """Place x à la queue de la file"""
        self.valeurs.append(x)

    def defile(self):
        """Retire et renvoie l'élément placé à la tête de la file.
        Provoque une erreur si la file est vide
        """
        if self.est_vide():
            raise ValueError("La file est vide")
        return self.valeurs.popleft()

# --------- PYODIDE:code --------- #

def degres_separation(reseau, membre1, membre2):
    ...

# --------- PYODIDE:corr --------- #

def degres_separation(reseau, membre1, membre2):
    file = File()
    file.enfile((membre1,0))
    while not file.est_vide():
        membre, degre = file.defile()
        if membre == membre2:
            return degre
        for ami in reseau[membre]:
            file.enfile((ami, degre+1))
    return 0

# --------- PYODIDE:tests --------- #

immediam = {
    "Anna": ["Billy"],
    "Billy": ["Anna", "Eroll"],
    "Carl": ["Billy"],
    "Dora": ["Gaby"],
    "Eroll": ["Billy", "Dora", "Flynn", "Gaby"],
    "Flynn": ["Gaby"],
    "Gaby": ["Eroll"],
}
assert degres_separation(immediam, "Billy", "Billy") == 0
assert degres_separation(immediam, "Billy", "Eroll") == 1
assert degres_separation(immediam, "Carl", "Flynn") == 3

# --------- PYODIDE:secrets --------- #


# Tests supplémentaires
immediam = {
    "Anna": ["Billy"],
    "Billy": ["Anna"],
    "Carl": ["Billy"],
    "Dora": ["Gaby"],
    "Eroll": ["Billy", "Dora", "Flynn", "Gaby"],
    "Flynn": ["Eroll", "Gaby"],
    "Gaby": ["Eroll", "Flynn"],
    "Charles": [],
}

assert degres_separation(immediam, "Anna", "Anna") == 0
assert degres_separation(immediam, "Gaby","Flynn") == 1
assert degres_separation(immediam, "Dora", "Flynn") == 2