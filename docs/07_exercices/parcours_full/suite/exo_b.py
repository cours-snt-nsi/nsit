

# --------- PYODIDE:code --------- #

def terme(n):
    if n == ... :
        return ...
    else:
        return ...

# --------- PYODIDE:corr --------- #

def terme(n):
    if n == 0:
        return 3
    else:
        return 5 * terme(n-1) + 2

# --------- PYODIDE:tests --------- #

assert terme(0) == 3
assert terme(1) == 17
assert terme(2) == 87
assert terme(3) == 437

# --------- PYODIDE:secrets --------- #


# tests secrets
attendu = 437
n = 3
for _ in range(100):
    attendu = 5 * attendu + 2
    n += 1
    assert terme(n) == attendu, f"Erreur en calculant terme({n})"