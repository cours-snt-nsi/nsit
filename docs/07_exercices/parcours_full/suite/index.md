---
author:
    - Pierre Marquestaut
hide:
    - navigation
    - toc
title: Suite mathématique
tags:
    - maths
    - récursivité
difficulty: 190
maj: 01/03/2024
---

# Suite mathématique

Soit la suite définie par récurrence par :

\[
\left\{
          \begin{array}{ll}
            u_0 = 3 \\
            u_{n+1} = 5 \times u_n+2\\
          \end{array}
\right.
   \]

On souhaite créer la fonction `terme` qui calcule le terme de cette suite au rang `n`.

On pourra utiliser une méthode itérative ou récursive.

??? tips "Traduction d'une suite en algorithme"

    $u_0=3$ signifie que pour $n=0$ alors le terme $u =3$. On initialise donc les variables `n` et `u` aux valeurs correspondantes.

    Ensuite, connaissant le terme d'indice $n$, on calcule le terme suivant, celui d'indice $n+1$,  grâce à l'instruction :

    ```python
    u = 5*u + 2
    ```
    
    L'expression est exécutée en débutant par le membre de droite : `#!py 5*u + 2` est tout d'abord évalué, puis le résultat est affecté à la variable `u`. Cette variable contient donc successivement les différentes valeurs de $u_n$.

    Calculer un terme au rang `n` non nul revient donc à calculer chaque terme du rang $1$ au rang $n$.
    

{{ remarque('assertion') }}

{{ remarque('range') }}

=== "Version itérative"
    {{ IDE('exo_a') }}
=== "Version récursive"
    {{ IDE('exo_b') }}