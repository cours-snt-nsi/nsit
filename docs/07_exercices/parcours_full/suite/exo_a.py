

# --------- PYODIDE:code --------- #

def terme(n):
    u = ...
    for indice in range(... , ...):
        u = ...
    return u

# --------- PYODIDE:corr --------- #

def terme(n):
    u = 3
    for indice in range(1, n+1):
        u = 5*u + 2
    return u

# --------- PYODIDE:tests --------- #

assert terme(0) == 3
assert terme(1) == 17
assert terme(2) == 87
assert terme(3) == 437

# --------- PYODIDE:secrets --------- #


# tests secrets
attendu = 437
n = 3
for _ in range(100):
    attendu = 5 * attendu + 2
    n += 1
    assert terme(n) == attendu, f"Erreur en calculant terme({n})"