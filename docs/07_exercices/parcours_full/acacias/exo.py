

# --------- PYODIDE:code --------- #

def max_manger(feuilles):
    ...

# --------- PYODIDE:corr --------- #

def max_manger(feuilles):
    n = len(feuilles)
    if n == 0:
        return 0
    maxi_sans_i, maxi_avec_i = 0, feuilles[0]
    for i in range(1, n):
        maxi_sans_i, maxi_avec_i = (
            max(maxi_avec_i, maxi_sans_i),
            feuilles[i] + maxi_sans_i,
        )
    return max(maxi_avec_i, maxi_sans_i)

# --------- PYODIDE:tests --------- #

feuilles = [4, 25, 20, 8, 17]
assert max_manger(feuilles) == 42


feuilles = [4, 6, 5, 7, 4]
assert max_manger(feuilles) == 13

# --------- PYODIDE:secrets --------- #


# autres tests

assert max_manger([9]) == 9
assert max_manger([]) == 0

feuilles = [0, 1] * 100
assert max_manger(feuilles) == 100
feuilles = [2, 1] * 100
assert max_manger(feuilles) == 200

feuilles = [1, 2, 1] * 20
assert max_manger(feuilles) == 40

feuilles = [1, 2, 3] * 20
assert max_manger(feuilles) == 61

feuilles = [4, 25, 20, 8, 17, 1, 30, 2]
assert max_manger(feuilles) == 72

feuilles = []
total = 0
for i in range(13):
    total += 2 * 10
    if pow(7, i, 17) % 3:
        feuilles.extend([0, 0, 10, 4, 5, 10, 3])
    else:
        feuilles.extend([0, 0, 10, 2, 7, 2, 3])
assert max_manger(feuilles) == total