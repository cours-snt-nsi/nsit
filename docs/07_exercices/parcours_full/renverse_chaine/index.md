---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Renverser une chaine
tags:
    - string
    - ep1
difficulty: 120
---

# Renverser une chaine

Programmer une fonction `renverser` :

* prenant en paramètre une chaine de caractères `mot`,

* renvoyant une nouvelle chaine de caractères en inversant ceux de la chaine `mot`.

!!! warning "Contrainte"

    On s'interdit d'utiliser les fonctions `reverse` et `reversed`, ainsi que les tranches `mot[::-1]`.

???+ example "Exemples"

    ```pycon title=""
    >>> renverser('informatique')
    'euqitamrofni'
    >>> renverser('nsi')
    'isn'
    >>> renverser('')
    ''
    ```


{{ IDE('exo', SANS=".reverse, reversed") }}
