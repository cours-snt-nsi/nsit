

# --------- PYODIDE:code --------- #

def lplssc(texte_a, texte_b):
    def lplssc_rec(taille_a, taille_b):
        if taille_a == ... or ...:
            return ...
        if (taille_a, taille_b) not in ...:
            if texte_a[...] == texte_b[...]:
                longueurs[(taille_a, taille_b)] = ...
            else:
                longueurs[(taille_a, taille_b)] = ...
        return longueurs[...]

    longueurs = {}
    return lplssc_rec(..., ...)

# --------- PYODIDE:corr --------- #

def lplssc(texte_a, texte_b):
    def lplssc_rec(taille_a, taille_b):
        if taille_a == 0 or taille_b == 0:
            return 0
        if (taille_a, taille_b) not in longueurs:
            if texte_a[taille_a - 1] == texte_b[taille_b - 1]:
                longueurs[(taille_a, taille_b)] = 1 + lplssc_rec(taille_a - 1, taille_b - 1)
            else:
                longueurs[(taille_a, taille_b)] = max(
                    lplssc_rec(taille_a - 1, taille_b), lplssc_rec(taille_a, taille_b - 1)
                )
        return longueurs[(taille_a, taille_b)]

    longueurs = {}
    return lplssc_rec(len(texte_a), len(texte_b))

# --------- PYODIDE:tests --------- #

texte_a = "LAPIN"
texte_b = "CAPRIN"
assert lplssc(texte_a, texte_b) == 4

texte_a = "abcd"
texte_b = "abcde"
assert lplssc(texte_a, texte_b) == 4

texte_a = "aBaBaBaB"
texte_b = "aaa"
assert lplssc(texte_a, texte_b) == 3

# --------- PYODIDE:secrets --------- #


# tests secrets
texte_a = "bertrand roule en vélo"
texte_b = "bravo"
assert lplssc(texte_a, texte_b) == 5


texte_a = "resulta"
texte_b = "résultats"
assert lplssc(texte_a, texte_b) == 6


texte_a = "résultats"
texte_b = "résultats"
assert lplssc(texte_a, texte_b) == 9


texte_a = "résultats"
texte_b = ""
assert lplssc(texte_a, texte_b) == 0


texte_a = "LAPIN"
texte_b = "CIAPRN"
assert lplssc(texte_a, texte_b) == 3


texte_a = "LAPIN"
texte_b = "CIAPRN"
assert lplssc(texte_a, texte_b) == 3


texte_a = "eee"
texte_b = "eff"
assert lplssc(texte_a, texte_b) == 1

texte_a = "eff"
texte_b = "eee"
assert lplssc(texte_a, texte_b) == 1