

# --------- PYODIDE:code --------- #

def lplssc(texte_a, texte_b):
    ...

# --------- PYODIDE:corr --------- #

def lplssc(texte_a, texte_b):
    longueurs = [0 for _ in range(len(texte_a) + 1)]
    for taille_b in range(1, len(texte_b) + 1):
        temp = [0 for _ in range(len(texte_a) + 1)]
        for taille_a in range(1, len(texte_a) + 1):
            if texte_a[taille_a - 1] == texte_b[taille_b - 1]:
                temp[taille_a] = 1 + longueurs[taille_a - 1]
            else:
                temp[taille_a] = max(temp[taille_a - 1], longueurs[taille_a])
        longueurs = temp
    return longueurs[len(texte_a)]

# --------- PYODIDE:tests --------- #

texte_a = "LAPIN"
texte_b = "CAPRIN"
assert lplssc(texte_a, texte_b) == 4

texte_a = "abcd"
texte_b = "abcde"
assert lplssc(texte_a, texte_b) == 4

texte_a = "aBaBaBaB"
texte_b = "aaa"
assert lplssc(texte_a, texte_b) == 3

# --------- PYODIDE:secrets --------- #


# tests secrets
texte_a = "bertrand roule en vélo"
texte_b = "bravo"
assert lplssc(texte_a, texte_b) == 5


texte_a = "resulta"
texte_b = "résultats"
assert lplssc(texte_a, texte_b) == 6


texte_a = "résultats"
texte_b = "résultats"
assert lplssc(texte_a, texte_b) == 9


texte_a = "résultats"
texte_b = ""
assert lplssc(texte_a, texte_b) == 0


texte_a = "LAPIN"
texte_b = "CIAPRN"
assert lplssc(texte_a, texte_b) == 3


texte_a = "LAPIN"
texte_b = "CIAPRN"
assert lplssc(texte_a, texte_b) == 3


texte_a = "eee"
texte_b = "eff"
assert lplssc(texte_a, texte_b) == 1

texte_a = "eff"
texte_b = "eee"
assert lplssc(texte_a, texte_b) == 1