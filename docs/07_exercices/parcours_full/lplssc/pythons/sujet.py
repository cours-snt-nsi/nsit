# --- exo, rec_  --- #
def lplssc(texte_a, texte_b):
    def lplssc_rec(taille_a, taille_b):
        if taille_a == ... or ...:
            return ...
        if (taille_a, taille_b) not in ...:
            if texte_a[...] == texte_b[...]:
                longueurs[(taille_a, taille_b)] = ...
            else:
                longueurs[(taille_a, taille_b)] = ...
        return longueurs[...]

    longueurs = {}
    return lplssc_rec(..., ...)
    
# --- vide, rec_ --- #
def lplssc(texte_a, texte_b):
    def lplssc_rec(taille_a, taille_b):
        ...
    ...    


# --- corr, rec_ --- #
def lplssc(texte_a, texte_b):
    def lplssc_rec(taille_a, taille_b):
        if taille_a == 0 or taille_b == 0:
            return 0
        if (taille_a, taille_b) not in longueurs:
            if texte_a[taille_a - 1] == texte_b[taille_b - 1]:
                longueurs[(taille_a, taille_b)] = 1 + lplssc_rec(taille_a - 1, taille_b - 1)
            else:
                longueurs[(taille_a, taille_b)] = max(
                    lplssc_rec(taille_a - 1, taille_b), lplssc_rec(taille_a, taille_b - 1)
                )
        return longueurs[(taille_a, taille_b)]

    longueurs = {}
    return lplssc_rec(len(texte_a), len(texte_b))
# --- exo, iter_  --- #
def lplssc(texte_a, texte_b):
    longueurs = [0 for _ in range(len(texte_a) + 1)]
    for taille_b in range(..., ...):
        temp = [0 for _ in range(len(texte_a) + 1)]
        for taille_a in range(..., ...):
            if texte_a[...] == texte_b[...]:
                temp[taille_a] = ...
            else:
                temp[taille_a] = ...
        longueurs = temp
    return longueurs[...]

    
# --- vide, iter_ --- #
def lplssc(texte_a, texte_b):
    ...    


# --- corr, iter_ --- #
def lplssc(texte_a, texte_b):
    longueurs = [0 for _ in range(len(texte_a) + 1)]
    for taille_b in range(1, len(texte_b) + 1):
        temp = [0 for _ in range(len(texte_a) + 1)]
        for taille_a in range(1, len(texte_a) + 1):
            if texte_a[taille_a - 1] == texte_b[taille_b - 1]:
                temp[taille_a] = 1 + longueurs[taille_a - 1]
            else:
                temp[taille_a] = max(temp[taille_a - 1], longueurs[taille_a])
        longueurs = temp
    return longueurs[len(texte_a)]
# --- tests, rec_, iter_ --- #
texte_a = "LAPIN"
texte_b = "CAPRIN"
assert lplssc(texte_a, texte_b) == 4

texte_a = "abcd"
texte_b = "abcde"
assert lplssc(texte_a, texte_b) == 4

texte_a = "aBaBaBaB"
texte_b = "aaa"
assert lplssc(texte_a, texte_b) == 3
# --- secrets, rec_, iter_ --- #
texte_a = "bertrand roule en vélo"
texte_b = "bravo"
assert lplssc(texte_a, texte_b) == 5


texte_a = "resulta"
texte_b = "résultats"
assert lplssc(texte_a, texte_b) == 6


texte_a = "résultats"
texte_b = "résultats"
assert lplssc(texte_a, texte_b) == 9


texte_a = "résultats"
texte_b = ""
assert lplssc(texte_a, texte_b) == 0


texte_a = "LAPIN"
texte_b = "CIAPRN"
assert lplssc(texte_a, texte_b) == 3


texte_a = "LAPIN"
texte_b = "CIAPRN"
assert lplssc(texte_a, texte_b) == 3


texte_a = "eee"
texte_b = "eff"
assert lplssc(texte_a, texte_b) == 1

texte_a = "eff"
texte_b = "eee"
assert lplssc(texte_a, texte_b) == 1
# --- rem, rec_ --- #
""" # skip
Dans la fonction principale on commence par créer le dictionnaire `longueurs` initialement vide qui permettra de *mémoïser*.
On lance ensuite les appels récursifs en se demandant quelle est la longueur de la plus longue sous-suite commune si l'on considère
toutes les lettres de chaque texte : `lplsc_rec(len(texte_a), len(texte_b))` ?

Comme la sous-fonction est récursive, il est bon de débuter par un cas de base.
C'est ici le cas ou l'un des deux textes ne contient aucun caractère. Dans ce cas, on renvoie directement `#!py 0`.

On vérifie ensuite que le cas a été traité ou non. Pour cela on se demande si la clé `(taille_a, taille_b)`
n'est pas présente dans le dictionnaire `longueurs`.

Si c'est le cas, il faut traiter cette comparaison :

* si les derniers caractères considérés sont identiques, on passe aux caractères précédents et l'on ajoute `#!py 1` au résultat ;
* si les caractères diffèrent, on étudie les deux cas précisés par l'énoncé. On place dans le dictionnaire le résultat le plus grand.

Quel que soit le cas envisagé, la valeur est immédiatement stockée dans le dictionnaire `longueurs`.

En dernier lieu, on renvoie la valeur que l'on vient d'insérer dans le dictionnaire ou celle qui était déjà présente.
""" # skip
# --- rem, iter_ --- #
""" # skip
Cette fonction est itérative et construite autour d'une boucle `#!py for`.

Le tableau stockant les longueurs des différentes sous-suites communes devrait avoir `#!py 1 + len(texte_a)` colonnes et `#!py 1 + len(texte_b)` lignes.
On constate néanmoins que les formules de récurrence ne font appels qu'à des valeurs issues de la même ligne ou de la précédente.

On peut donc se contenter d'utiliser deux tableaux de `#!py 1` ligne.

La première ligne du tableau et la première colonne ne contiennent que des `#!py 0`.

Pour compléter les autres cases, on utilise les formules de récurrence.
""" # skip
