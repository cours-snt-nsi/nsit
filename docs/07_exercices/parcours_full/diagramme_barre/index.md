---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Diagramme en barres
tags: 
    - string
    - fonctions
    - liste/tableau
    - à trous
difficulty: 160
---

# Diagramme en barres

!!! tip "Plusieurs fonctions"

    Cet exercice est en plusieurs parties.
    
    Vous pouvez traiter celles-ci dans l'ordre que vous souhaitez.
    
    Il est possible dans une partie quelconque d'utiliser les fonctions demandées dans les parties précédentes 
    **même si vous ne les avez pas écrites**.

On souhaite construire un diagramme en barres à partir de deux listes :

* une liste `categories` contenant des chaînes de caractères,
  
* une liste `valeurs` contenant des nombres entiers positifs ou nuls.

On garantit que les deux listes ont la même longueur.

Si l'on prend par Exemples :

* `#!py categories = ["Anne", "Luc", "Patrick", "Sam"]` ,
* et `#!py valeurs = [10, 8, 5, 15]`,

on souhaite obtenir :

```pycon
Anne    : ##########
Luc     : ########
Patrick : #####
Sam     : ###############
```

La construction de ce diagramme passe par plusieurs étapes détaillées ci-dessous.

??? question "1. Longueur maximale"

    Comme on peut le voir, afin de construire un diagramme convenable, on doit faire en sorte que toutes les « barres » débutent au même niveau. Il faut donc compléter certaines catégories avec des espaces. La taille commune visée est la longueur maximale des chaines de caractères présentes dans `categories`.

    Écrire la fonction `longueur_max` qui prend en paramètre une liste non-vide de chaînes de caractères et renvoie la longueur de la plus grande d'entre elles.

    ???+ example "Exemples"

        ```pycon
        >>> chaines = [""]
        >>> taille_max(chaines)
        0
        >>> chaines = ["Toto", "Titi", "Tata"]
        >>> taille_max(chaines)
        4
        >>> chaines = ["Riri", "Fifi", "Loulou"]
        >>> taille_max(chaines)
        6
        >>> chaines = ["Pomme", "Abricot", "Poire", "Pêche"]
        >>> taille_max(chaines)
        7
        ```
    
    === "Version vide"
        {{ IDE('./pythons/exo_longueurb') }}
    === "Version à compléter"
        {{ IDE('./pythons/exo_longueura') }}

??? question "2. Allonger les chaînes"


    La longueur maximale de la chaîne étant connue, il faut désormais ajouter le bon nombre d'espaces aux différentes catégories.

    On demande désormais d'écrire la fonction `allonge` qui prolonge une chaîne de caractères `mot` avec des espaces jusqu'à ce que sa longueur soit égale au paramètre `longueur` (entier strictement positif).

    ???+ example "Exemples"

        ```pycon
        >>> allonge("Anne", 7)
        'Anne   '
        >>> allonge("Luc", 7)
        'Luc    '
        >>> allonge("Patrick", 7)
        'Patrick'
        >>> allonge("Patrick", 1)
        'Patrick'
        ```

    === "Version vide"
        {{ IDE('./pythons/exo_allongeb') }}
    === "Version à compléter"
        {{ IDE('./pythons/exo_allongea') }}


??? tip "`longueur_max` et `allonge` chargées !"

    Il est possible dans la prochaine partie d'utiliser les fonctions `longueur_max` et `allonge` **même si elles n'ont pas été complétées précédemment**.
    
    Une version valide de chaque fonction est déjà disponible dans l'éditeur.
    
??? question "3. Le diagramme complet"

    Il s'agit donc désormais d'écrire la fonction `diagramme` qui prend en paramètres la liste `categories`
     contenant les noms des catégories et la liste `valeurs` contenant les entiers associés à chaque catégorie.
     
     Cette fonction renvoie la **liste des chaînes de caractères** correspondant aux lignes du diagramme.
     
     On garantit que :
     
     * les deux listes ont la même taille ;
     * les deux listes sont non-vides ;
     * les valeurs sont toutes positives ou nulles.

    On notera que dans les règles de typographie française, on ajoute une espace entre la fin d'un mot et les deux points.
    On écrit ainsi `Patrick :` et non `Patrick:`.

    ???+ example "Exemples"

        ```pycon
        >>> categories = ["Anne", "Luc", "Patrick", "Sam"]
        >>> valeurs = [10, 8, 5, 15]
        >>> barres(categories, valeurs)
        ['Anne    : ##########', 'Luc     : ########', 'Patrick : #####', 'Sam     : ###############']
        >>> categories = ["A", "B", "", "EEEEE"]
        >>> valeurs = [1, 1, 0, 5]
        >>> barres(categories, valeurs)
        ['A     : #','B     : #', '      : ','EEEEE : #####']
        ```
        
        Les affichages des résultats sont toutefois plus lisibles en procédant ainsi :
        
        ```pycon
        >>> categories = ["Anne", "Luc", "Patrick", "Sam"]
        >>> valeurs = [10, 8, 5, 15]
        >>> for chaine in barres(categories, valeurs):
        ...    print(chaine)
        ...
        Anne    : ##########
        Luc     : ########
        Patrick : #####
        Sam     : ###############
        ```
        
    Compléter la fonction `barres` ci-dessous :

    === "Version vide"
        {{ IDE('./pythons/exo_diagrammeb') }}
    === "Version à compléter"
        {{ IDE('./pythons/exo_diagrammea') }}
