

# --------- PYODIDE:env --------- #

def allonge(mot, longueur):
    ...

# --------- PYODIDE:code --------- #

def allonge(mot, longueur):
    ...

# --------- PYODIDE:corr --------- #

def allonge(mot, longueur):
    return mot + " " * (longueur - len(mot))

# --------- PYODIDE:tests --------- #

assert allonge("Anne", 7) == "Anne   "
assert allonge("Luc", 7) == "Luc    "
assert allonge("Patrick", 7) == "Patrick"
assert allonge("Patrick", 1) == "Patrick"

# --------- PYODIDE:secrets --------- #


# tests secrets
for taille_mot in range(1, 5):
    mot = "a" * taille_mot
    for longueur in range(8):
        attendu = mot + " " * (longueur - len(mot))
        assert (
            allonge(mot, longueur) == attendu
        ), f"Erreur avec {mot = } et {longueur = }"