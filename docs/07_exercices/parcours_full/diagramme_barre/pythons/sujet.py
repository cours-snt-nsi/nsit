# --- exo, longueur | hdr, longueur --- #
def longueur_max(chaines):
    ...


# --- vide, longueur --- #
def longueur_max(chaines):
    maxi = 0
    for texte in ...:
        if len(...) > ...:
            ...
    return ...


# --- corr, longueur --- #
def longueur_max(chaines):
    maxi = 0
    for texte in chaines:
        if len(texte) > maxi:
            maxi = len(texte)

    return maxi

# --- tests, longueur --- #
chaines = [""]
assert longueur_max(chaines) == 0
chaines = ["Toto", "Titi", "Tata"]
assert longueur_max(chaines) == 4
chaines = ["Riri", "Fifi", "Loulou"]
assert longueur_max(chaines) == 6
chaines = ["Pomme", "Abricot", "Poire", "Pêche"]
assert longueur_max(chaines) == 7
# --- secrets, longueur --- #
from random import randrange, shuffle

attendu = randrange(1, 10)
chaines = ["a" * k for k in range(attendu + 1)]
shuffle(chaines)
assert longueur_max(chaines) == attendu, f"Erreur avec {chaines}"

# --- exo, allonge | hdr, allonge --- #
def allonge(mot, longueur):
    ...


# --- vide, allonge --- #
def allonge(mot, longueur):
    return ... + " " * (... - len(...))


# --- corr, allonge --- #
def allonge(mot, longueur):
    return mot + " " * (longueur - len(mot))


# --- tests, allonge --- #
assert allonge("Anne", 7) == "Anne   "
assert allonge("Luc", 7) == "Luc    "
assert allonge("Patrick", 7) == "Patrick"
assert allonge("Patrick", 1) == "Patrick"
# --- secrets, allonge --- #
for taille_mot in range(1, 5):
    mot = "a" * taille_mot
    for longueur in range(8):
        attendu = mot + " " * (longueur - len(mot))
        assert (
            allonge(mot, longueur) == attendu
        ), f"Erreur avec {mot = } et {longueur = }"

# --- hdr, diagramme --- #
def longueur_max(chaines):
    maxi = 0
    for texte in chaines:
        if len(texte) > maxi:
            maxi = len(texte)

    return maxi


def allonge(mot, longueur):
    return mot + " " * (longueur - len(mot))
# --- exo, diagramme --- #
def barres(categories, valeurs):
    ...
    
    
# --- vide, diagramme --- #
def barres(categories, valeurs):
    longueur = ...(categories)
    diagramme = []
    for i in range(...):
        ligne = ...(categories[i], longueur)
        ligne += " : "
        ligne += "#" * ...
        diagramme.append(ligne)

    return ...


# --- corr, diagramme --- #
def barres(categories, valeurs):
    longueur = longueur_max(categories)
    diagramme = []
    for i in range(len(categories)):
        ligne = allonge(categories[i], longueur)
        ligne += " : "
        ligne += "#" * valeurs[i]
        diagramme.append(ligne)

    return diagramme

# --- tests, diagramme --- #
categories = ["Anne", "Luc", "Patrick", "Sam"]
valeurs = [10, 8, 5, 15]
attendu = [
    "Anne    : ##########",
    "Luc     : ########",
    "Patrick : #####",
    "Sam     : ###############",
]
assert barres(categories, valeurs) == attendu
categories = ["A", "B", "", "EEEEE"]
valeurs = [1, 1, 0, 5]
attendu = ["A     : #", "B     : #", "      : ", "EEEEE : #####"]
assert barres(categories, valeurs) == attendu
# --- secrets, diagramme --- #
from random import shuffle, randrange

taille = 10
categories = ["a" * k for k in range(taille)]
valeurs = [randrange(0, 10) for _ in range(taille)]
shuffle(categories)
attendu = [categories[i].ljust(taille) + ": " + "#" * valeurs[i] for i in range(taille)]
assert (
    barres(categories, valeurs) == attendu
), f"Erreur avec {categories = } et {valeurs = }"

# --- rem, diagramme --- #
''' # skip
Il est aussi possible de créer le diagramme en utilisant une liste en compréhension :

```python
def barres(categories, valeurs):
    taille = longueur_max(categories)
    return [allonge(categories[i], taille) + " : " + "#" * valeurs[i] for i in range(len(categories))]
```
''' # skip
