

# --------- PYODIDE:env --------- #

def longueur_max(chaines):
    maxi = 0
    for texte in chaines:
        if len(texte) > maxi:
            maxi = len(texte)

    return maxi


def allonge(mot, longueur):
    return mot + " " * (longueur - len(mot))

# --------- PYODIDE:code --------- #

def barres(categories, valeurs):
    longueur = ...(categories)
    diagramme = []
    for i in range(...):
        ligne = ...(categories[i], longueur)
        ligne += " : "
        ligne += "#" * ...
        diagramme.append(ligne)

    return ...

# --------- PYODIDE:corr --------- #

def barres(categories, valeurs):
    longueur = longueur_max(categories)
    diagramme = []
    for i in range(len(categories)):
        ligne = allonge(categories[i], longueur)
        ligne += " : "
        ligne += "#" * valeurs[i]
        diagramme.append(ligne)

    return diagramme

# --------- PYODIDE:tests --------- #

categories = ["Anne", "Luc", "Patrick", "Sam"]
valeurs = [10, 8, 5, 15]
attendu = [
    "Anne    : ##########",
    "Luc     : ########",
    "Patrick : #####",
    "Sam     : ###############",
]
assert barres(categories, valeurs) == attendu
categories = ["A", "B", "", "EEEEE"]
valeurs = [1, 1, 0, 5]
attendu = ["A     : #", "B     : #", "      : ", "EEEEE : #####"]
assert barres(categories, valeurs) == attendu

# --------- PYODIDE:secrets --------- #


# tests secrets
from random import shuffle, randrange

taille = 10
categories = ["a" * k for k in range(taille)]
valeurs = [randrange(0, 10) for _ in range(taille)]
shuffle(categories)
attendu = [categories[i].ljust(taille) + ": " + "#" * valeurs[i] for i in range(taille)]
assert (
    barres(categories, valeurs) == attendu
), f"Erreur avec {categories = } et {valeurs = }"