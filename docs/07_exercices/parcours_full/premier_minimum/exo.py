

# --------- PYODIDE:code --------- #

def indice_arret(hauteurs):
    ...

# --------- PYODIDE:corr --------- #

def indice_arret(hauteurs):
    i = 0
    while hauteurs[i] >= hauteurs[i + 1]:
        i += 1
    return i

# --------- PYODIDE:tests --------- #

hauteurs = [3, 2, 5]
assert indice_arret(hauteurs) == 1

hauteurs = [3, 5]
assert indice_arret(hauteurs) == 0

hauteurs = [10, 8, 7, 5, 5, 4, 3, 6, 6, 5, 4, 12]
assert indice_arret(hauteurs) == 6

# --------- PYODIDE:secrets --------- #

from random import randint
# Tests supplémentaires
assert indice_arret([0, 1]) == 0
assert indice_arret([0] * 10 + [1]) == 9
for _ in range(100):
    hauteurs = [randint(1, 100) for _ in range(randint(1, 20))] + [101]
    i = [k for k in range(len(hauteurs) - 1) if hauteurs[k] < hauteurs[k + 1]][0]
    assert indice_arret(hauteurs) == i, f"Erreur avec {hauteurs = }"