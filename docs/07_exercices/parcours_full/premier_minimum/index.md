---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Premier minimum local
difficulty: 122
tags:
    - liste/tableau
    - glouton
---

Alors qu'elle joue sur un chemin dallé, Élodie laisse rouler une balle. En observant les dalles devant elle, elle se rend compte que certaines dalles sont plus basses que les précédentes, d'autres plus hautes.

Elle se pose la question suivante : "*Où va s'arrêter la balle ?*"

![le chemin dallé](images/exemple.svg)

On donne les hauteurs des dalles dans le chemin sous forme d'une liste de nombres entiers positifs. Cette liste compte au minimum deux valeurs.

On garantit que la hauteur de la dernière dalle est strictement supérieure celles de toutes les autres.

Par exemple :

```python
# indices    0  1  2  3  4  5  6  7  8  9 10  11
hauteurs = [10, 8, 7, 5, 5, 4, 3, 6, 6, 5, 4, 12]
```

Dans l'exemple précédent, illustré par la figure, la balle s'arrête sur la dalle d'indice `#!py 6`. En effet, la balle s'arrête sur **la première dalle dont la hauteur est strictement inférieure à celle de la suivante**.

On signale que lorsque deux dalles consécutives sont à la même hauteur, la balle continue de rouler.

Écrire la fonction `indice_arret` :

* qui prend en argument la liste des hauteurs des dalles (`hauteurs`),

* et qui renvoie l'indice de la dalle sur laquelle s'arrête la bille. La balle est initialement sur la dalle d'indice `#!py 0`.

???+ example "Exemples"

    ```pycon title=""
    >>> hauteurs = [3, 2, 5]
    >>> indice_arret(hauteurs)
    1
    >>> hauteurs = [3, 5]
    >>> indice_arret(hauteurs)
    0
    >>> hauteurs = [10, 8, 7, 5, 5, 4, 3, 6, 6, 5, 4, 12]
    >>> indice_arret(hauteurs)
    6
    ```

{{ IDE('exo') }}
