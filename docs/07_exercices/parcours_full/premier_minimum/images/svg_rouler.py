import drawSvg as draw


def dessiner(terrain, etapes, nom_fichier='exemple'):
    w, h = (len(terrain)+2)*LARGEUR_BARRE, HAUTEUR_BARRE*6
    d = draw.Drawing(w, h, origin=(0, 0))

    r = draw.Rectangle(0, 0, w, h, fill='white')
    d.append(r)

    for i, valeur in enumerate(terrain):
        r = draw.Rectangle(LARGEUR_BARRE+i*LARGEUR_BARRE,
                           HAUTEUR_BARRE,
                           LARGEUR_BARRE,
                           HAUTEUR_BARRE + valeur * HAUTEUR_BARRE//5,
                           fill=COULEUR_BARRE,
                           stroke_width=3,
                           stroke=CONTOUR_BARRE)
        d.append(r)

        d.append(draw.Text(str(i),
                           TAILLE_POLICE,
                           1.5*LARGEUR_BARRE+i*LARGEUR_BARRE,
                           0.7*HAUTEUR_BARRE,
                           text_anchor='middle',
                           valign='middle'))

    c = draw.Circle((etapes[0]+1.5)*LARGEUR_BARRE,
                    2*HAUTEUR_BARRE + terrain[etapes[0]] *
                    HAUTEUR_BARRE//5+LARGEUR_BARRE//2,
                    LARGEUR_BARRE//2,
                    fill=COULEUR_BALLE,
                    stroke_width=3,
                    stroke=CONTOUR_BARRE)
    cxs = []
    cys = []
    for i in etapes[:-1]:
        cxs.append(str((i+1.5)*LARGEUR_BARRE))
        cys.append(
            str(-2*HAUTEUR_BARRE-terrain[i]*HAUTEUR_BARRE//5-LARGEUR_BARRE//2))
    c.appendAnim(draw.Animate('cy', '5s', ';'.join(cys), calcMode='discrete',
                              repeatCount='indefinite'))
    c.appendAnim(draw.Animate('cx', '5s', ';'.join(cxs), calcMode='discrete',
                              repeatCount='indefinite'))

    d.append(c)

    d.saveSvg(f'{nom_fichier}.svg')


def rouler(terrain, position):
    deplacement = True

    etapes = [position]
    while deplacement:
        deplacement = False
        delta_droite = terrain[position+1]-terrain[position]
        if delta_droite <= 0:
            position += 1
            deplacement = True
        etapes.append(position)

    dessiner(terrain, etapes, 'exemple1')
    return position


LARGEUR_BARRE = 40
HAUTEUR_BARRE = 30
AJOUT_BARRE = 30
COULEUR_BARRE = '#c4687c'
CONTOUR_BARRE = '#555'
COULEUR_BALLE = '#ffff00'
TAILLE_POLICE = 20

terrain = [10, 8, 7, 5, 5, 4, 3, 6, 6, 5, 4, 12]

rouler(terrain, 0)
