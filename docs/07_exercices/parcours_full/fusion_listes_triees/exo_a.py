

# --------- PYODIDE:code --------- #

def fusion(liste_a, liste_b):
    ...

# --------- PYODIDE:corr --------- #

def fusion(liste_a, liste_b):
    taille_a = len(liste_a)
    taille_b = len(liste_b)
    liste_triee = []
    i_a = 0
    i_b = 0
    while (i_a < taille_a) and (i_b < taille_b):
        if liste_a[i_a] < liste_b[i_b]:
            liste_triee.append(liste_a[i_a])
            i_a += 1
        else:
            liste_triee.append(liste_b[i_b])
            i_b += 1
    while i_a < taille_a:
        liste_triee.append(liste_a[i_a])
        i_a += 1
    while i_b < taille_b:
        liste_triee.append(liste_b[i_b])
        i_b += 1
    return liste_triee

# --------- PYODIDE:tests --------- #

assert fusion([1, 6, 10], [0, 7, 8, 9]) == [0, 1, 6, 7, 8, 9, 10]

assert fusion([1, 6, 10], []) == [1, 6, 10]

assert fusion([], [0, 7, 8, 9]) == [0, 7, 8, 9]

# --------- PYODIDE:secrets --------- #


# tests secrets
assert fusion([3, 5], [2, 5]) == [2, 3, 5, 5]
assert fusion([-2, 4], [-3, 5, 10]) == [-3, -2, 4, 5, 10]
assert fusion([4], [2, 6]) == [2, 4, 6]

assert fusion([10, 10], [1, 1, 1, 1]) == [1, 1, 1, 1, 10, 10]
assert fusion([1, 2, 3, 6, 7, 8], [4, 5, 11, 11]) == [1, 2, 3, 4, 5, 6, 7, 8, 11, 11]
assert fusion([1, 2, 3, 6, 7, 8], [10, 11, 15, 15, 20]) == [
    1,
    2,
    3,
    6,
    7,
    8,
    10,
    11,
    15,
    15,
    20,
]
assert fusion([1, 3, 5, 7], [2, 4, 6, 8]) == [1, 2, 3, 4, 5, 6, 7, 8]
assert fusion([5, 6, 15, 16, 25, 26], [1, 2, 10, 11, 20, 21]) == [
    1,
    2,
    5,
    6,
    10,
    11,
    15,
    16,
    20,
    21,
    25,
    26,
]

assert fusion([8, 9, 15, 987, 17613], []) == [8, 9, 15, 987, 17613]
assert fusion([], [8, 9, 12, 987, 17614]) == [8, 9, 12, 987, 17614]
assert fusion([1, 2], [3, 4, 5, 6, 7, 8]) == [1, 2, 3, 4, 5, 6, 7, 8]
assert fusion([3, 4, 5, 6, 7, 8], [1, 2]) == [1, 2, 3, 4, 5, 6, 7, 8]
assert fusion([164646, 2321319], [3, 4, 5, 6, 7, 8]) == [3, 4, 5, 6, 7, 8, 164646, 2321319]
assert fusion([3, 4, 5, 6, 7, 8], [164646, 2321319]) == [3, 4, 5, 6, 7, 8, 164646, 2321319]