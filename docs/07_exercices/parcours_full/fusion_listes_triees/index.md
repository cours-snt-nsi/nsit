---
author: Franck Chambon
hide:
    - navigation
    - toc
title: Fusion de deux listes triées
tags:
    - à trous
    - tri
    - ep2
difficulty: 290
maj: 01/03/2024
---

# Fusion de deux listes triées

{{ version_ep() }}

On souhaite écrire le code de la fonction `fusion` qui prend en paramètres deux listes d'entiers `liste_a`, `liste_b`  **triées par ordre croissant** et les fusionne en une seule liste triée `liste_triee` qu'elle renvoie.

{{ remarque('tri_interdit')}}

???+ example "Exemples"

    ```pycon title=""
    >>> fusion([1, 6, 10], [0, 7, 8, 9])
    [0, 1, 6, 7, 8, 9, 10]
    ```

    ```pycon title=""
    >>> fusion([1, 6, 10], [])
    [1, 6, 10]
    ```

    ```pycon title=""
    >>> fusion([], [0, 7, 8, 9])
    [0, 7, 8, 9]
    ```

=== "Version vide"
    {{ IDE('exo_a', SANS=".sort, sorted") }}
=== "Verson à compléter"
    {{ IDE('exo_b', SANS=".sort, sorted") }}

