

# --------- PYODIDE:env --------- #

def coefficient_directeur(x_A, y_A, x_B, y_B):
    delta_y = y_B - y_A
    delta_x = x_B - x_A
    return delta_y / delta_x

# --------- PYODIDE:code --------- #

def ordonnee_origine(x_A, y_A, x_B, y_B):
    ...

# --------- PYODIDE:corr --------- #

def ordonnee_origine(x_A, y_A, x_B, y_B):
    a = coefficient_directeur(x_A, y_A, x_B, y_B)
    return y_A - x_A * a

# --------- PYODIDE:tests --------- #

assert abs(ordonnee_origine(1, 4, 9, 10) - 3.25) < 1e-6
assert abs(ordonnee_origine(3, 15, -2, 15) - 15) < 1e-6

# --------- PYODIDE:secrets --------- #


# tests secrets
from random import randrange

for test in range(10):
    x_A = randrange(-50, 50)
    y_A = randrange(-50, 50)
    x_B = randrange(-50, 50)
    y_B = randrange(-50, 50)
    if x_A == x_B:
        continue
    attendu = y_A - x_A * coefficient_directeur(x_A, y_A, x_B, y_B)
    assert (
        abs(ordonnee_origine(x_A, y_A, x_B, y_B) - attendu) < 1e-6
    ), f"Erreur en calculant ordonnee_origine{x_A, y_A, x_B, y_B}"