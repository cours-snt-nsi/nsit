# --------- PYODIDE:code --------- #
def aire(n):
    somme = ...
    largeur = ...
    for k in range(...):
        x = ... * ...
        somme = ... + ... * ...
    return ...


# --------- PYODIDE:corr --------- #
def aire(n):
    somme = 0
    largeur = 1 / n
    for k in range(n):
        x = k * largeur
        somme = somme + x**2 * largeur
    return somme
# --------- PYODIDE:tests --------- #
assert abs(aire(1) - 0.0) < 1e-6
assert abs(aire(2) - 0.125) < 1e-6
assert abs(aire(4) - 0.21875) < 1e-6
# --------- PYODIDE:secrets --------- #
n = 2**3
assert abs(aire(8) - 0.2734375) < 1e-6, f"Erreur avec {n = }"
n = 2**4
assert abs(aire(16) - 0.302734375) < 1e-6, f"Erreur avec {n = }"
n = 2**5
assert abs(aire(32) - 0.31787109375) < 1e-6, f"Erreur avec {n = }"
n = 2**6
assert abs(aire(64) - 0.3255615234375) < 1e-6, f"Erreur avec {n = }"
n = 2**7
assert abs(aire(128) - 0.329437255859375) < 1e-6, f"Erreur avec {n = }"
n = 2**8
assert abs(aire(256) - 0.33138275146484375) < 1e-6, f"Erreur avec {n = }"
n = 2**9
assert abs(aire(512) - 0.33235740661621094) < 1e-6, f"Erreur avec {n = }"
n = 2**10
assert abs(aire(1024) - 0.33284521102905273) < 1e-6, f"Erreur avec {n = }"
