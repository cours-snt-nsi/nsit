
Il est possible de présenter différemment la fonction `aire` :

```python
def aire(n):
    somme = 0
    x = 0       # abscisse du coin inf. gauche du rectangle initial
    dx = 1 / n  # largeur des rectangles
    for k in range(n):
        somme = somme + x**2 * dx
        x = x + dx
    return somme
```

Présentée en termes mathématiques, cette formule devient :

$$A_n=\left(\dfrac0n\right)^2\times \text{d}x+\left(\dfrac1n\right)^2\times \text{d}x+\left(\dfrac2n\right)^2\times \text{d}x+\dots+\left(\dfrac{n-1}n\right)^2\times \text{d}x$$

Lorsque $n$ tend vers $+\infty$ cette somme tend vers l'aire du domaine étudié. On note cette aire à l'aide d'une **intégrale** :

$$A=\int_0^1 x^2\text{d}x$$

La valeur exacte de cette intégrale est $\dfrac13$.

---------------------------------------

Il est possible de calculer une valeur approchée *par excès* de l'aire cherchée en « décalant » la valeur initiale de `x` :

```python
def aire_par_exces(n):
    somme = 0
    x = 1 / n   # abscisse du point « décalée »
    dx = 1 / n  # largeur des rectangles
    for k in range(n):
        somme = somme + x**2 * dx
        x = x + dx
    return somme
```

Cette somme tend elle aussi vers $\dfrac13$ quand $n$ tend vers $+\infty$.

---------------------------------------

Enfin, la régularité de la subdivision permet de simplifier le calcul en factorisant par les largeurs. Par exemple, encore dans le cas $n=4$ :

$$\left(0^2+0,25^2+0,5^2+0,75^2\right)\times0,25=0,21875$$

```python
def aire(n):
    somme = 0
    x = 0
    dx = 1 / n
    for k in range(n):
        somme = somme + x**2  # x**2 et plus x**2 * dx
        x = x + dx
    return somme * dx         # on multiplie l'ensemble de la somme apr dx
```