

# --------- PYODIDE:code --------- #

def correspond(motif, chaine, position):
    if ... > len(chaine):
        return False
    for i in range(len(motif)):
        if chaine[position + ...] != ...:
            return ...
    return True


def est_inclus(brin, gene):
    taille_gene = len(gene)
    taille_brin = len(brin)
    for i in range(... - ... + 1):
        if correspond(..., ..., ...):
            return True
    return False

# --------- PYODIDE:corr --------- #

def correspond(motif, chaine, position):
    if position + len(motif) > len(chaine):
        return False
    for i in range(len(motif)):
        if chaine[position + i] != motif[i]:
            return False
    return True


def est_inclus(brin, gene):
    taille_gene = len(gene)
    taille_brin = len(brin)
    for i in range(taille_gene - taille_brin + 1):
        if correspond(brin, gene, i):
            return True
    return False

# --------- PYODIDE:tests --------- #

assert correspond("AA", "AAGGTTCC", 4) is False
assert correspond("AT", "ATGCATGC", 4) is True

assert est_inclus("AATC", "GTACAAATCTTGCC") is True
assert est_inclus("AGTC", "GTACAAATCTTGCC") is False
assert est_inclus("AGTC", "GTACAAATCTTGCA") is False
assert est_inclus("AGTC", "GTACAAATCTAGTC") is True

# --------- PYODIDE:secrets --------- #


# autres tests
assert correspond("AA", "AAGGTTCC", 7) is False
assert correspond("CC", "AAGGTTCC", 6) is True
assert est_inclus("AAAA", "AAAAGTATCTTGCC") is True
assert est_inclus("GTAC", "GTACAAATCTTGCC") is True
assert est_inclus("AAAA", "GTACAAATCTAAAA") is True
assert est_inclus("AGTA", "GTACAAATCTAGTA") is True
assert est_inclus("AGTA", "GTACAAATCTAGTC") is False