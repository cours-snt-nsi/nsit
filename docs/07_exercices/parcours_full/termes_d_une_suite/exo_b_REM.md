La solution propose la construction du tableau par ajouts successifs.

Etant donné que l'on connaît en début de fonction le nombre d'éléments du tableau, la création du tableau aurait également pu se faire par construction :

```python
termes = [0 for k in range(N)]
```

On aurait ensuit complété le tableau en utilisant l'indice :

```python
termes[i] = 5 * termes[i - 1] - 8 * termes[i - 2]
```