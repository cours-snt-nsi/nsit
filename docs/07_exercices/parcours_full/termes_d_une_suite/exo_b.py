

# --------- PYODIDE:code --------- #

def liste_termes(N):
    termes = [..., ...]
    for n in range(..., N):
        termes.append(...)
    return termes

# --------- PYODIDE:corr --------- #

def liste_termes(N):
    termes = [0, 1]
    for n in range(2, N):
        termes.append(5 * termes[n - 1] - 8 * termes[n - 2])
    return termes

# --------- PYODIDE:tests --------- #

assert liste_termes(2) == [0, 1]
assert liste_termes(3) == [0, 1, 5]
assert liste_termes(4) == [0, 1, 5, 17]
assert liste_termes(10) == [0, 1, 5, 17, 45, 89, 85, -287, -2115, -8279]

# --------- PYODIDE:secrets --------- #


# tests secrets
assert liste_termes(100)[99] == -332554925882909994034962913837142688636462703