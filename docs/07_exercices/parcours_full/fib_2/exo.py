

# --------- PYODIDE:code --------- #

def fibonacci(n):
    ...

# --------- PYODIDE:corr --------- #

def fibonacci(n):
    """Renvoie (F_{n - 1}, F_{n})
        deux nombres de Fibonacci consécutifs
    """
    # cas de base
    if n == 1:
        return (0, 1)

    # cas général
    u, v = fibonacci(n//2)  # u = F_(k-1) et v = F_k
    # calcul de u = F_(2k-1) et v = F_2k
    v2 = v * v
    u, v = v2 + u*u, v2 + 2*u*v
    if n % 2 == 0:  # renvoie du couple (F_(2k-1), F_2k)
        return (u, v)
    if n % 2 == 1:  # renvoie du couple (F_2k, F_(2k+1))
        return (v, u + v)

# --------- PYODIDE:tests --------- #

assert fibonacci(1) == (0, 1)
assert fibonacci(2) == (1, 1)
assert fibonacci(3) == (1, 2)
assert fibonacci(9) == (21, 34)
assert fibonacci(4) == (2, 3)

# --------- PYODIDE:secrets --------- #


# Autres tests

fib_memo = [0, 1]
for _ in range(100):
    fib_memo.append(fib_memo[-1] + fib_memo[-2])

for n in range(1, 100):
    f_n, f_nm1 = (fib_memo[n - 1], fib_memo[n])
    assert fibonacci(n) == (f_n, f_nm1), f"Erreur avec {n=}"


def secret799_fib_2(n):
    """Renvoie (F_{n - 1}, F_{n})
        deux nombres de Fibonacci consécutifs
    on utilise les variables f_nm1 et f_n
    """
    # cas de base
    if n == 0:
        f_nm1 = 1
        f_n = 0
    elif n == 1:
        f_nm1 = 0
        f_n = 1

    # cas général
    elif n % 2 == 0:
        # n est pair
        k = n // 2
        f_km1, f_k = secret799_fib_2(k)
        f_nm1 = f_k * f_k + f_km1 * f_km1
        f_n = f_k * (f_k + 2 * f_km1)
    else:
        # n est impair
        k = n - 1
        f_km1, f_k = fib_2(k)
        f_nm1 = f_k
        f_n = f_km1 + f_k

    return f_nm1, f_n


# gros tests
for e in range(2, 7):
    n = 10**e
    attendu = secret799_fib_2(n)
    assert fibonacci(n) == attendu, f"Erreur avec {n=}"