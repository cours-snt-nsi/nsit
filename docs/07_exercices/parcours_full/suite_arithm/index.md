---
author:
    - Pierre Marquestaut
hide:
    - navigation
    - toc
title: Listes des termes d'une suite arithmétique
tags:
    - maths
    - en travaux
    - à trous
    - liste/tableau
difficulty: 210
---
# Listes des termes d'une suite arithmétique

Soit $(u_n)$ la suite arithmétique définie par :

\[
\left\{
          \begin{array}{ll}
            u_0 = 5 \\
            u_{n} = u_0 + n \times 10  \hspace{3ex} \text{pour} \hspace{1ex} n \ge 0 \\  
          \end{array}
\right.
   \]

On souhaite créer la fonction `arithmétique` qui renvoie une liste contenant les `N` premiers termes de cette suite.
On suppose que `N` est strictement supérieur ou égal à $0$.


=== "Construction par compréhension"
    {{ IDE('exo') }}
=== "Construction par ajouts successifs"
    {{ IDE('exo_b') }}
