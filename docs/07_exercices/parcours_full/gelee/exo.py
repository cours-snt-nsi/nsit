

# --------- PYODIDE:code --------- #

def gelees(temperatures):
    ...

# --------- PYODIDE:corr --------- #

def gelees(temperatures):
    plus_longue_periode = 0
    periode = 0
    for temp in temperatures:
        if temp <= 0:
            periode += 1
            if periode > plus_longue_periode:
                plus_longue_periode = periode
        else:
            periode = 0
    return plus_longue_periode

# --------- PYODIDE:tests --------- #

assert gelees([2, -3, -2, 0, 1, -1]) == 3
assert gelees([3, 2, 2]) == 0
assert gelees([]) == 0

# --------- PYODIDE:secrets --------- #


# autres tests
assert gelees([-2, -3, -2, 0, -1, -1]) == 6
assert gelees([0, 0, 0]) == 3
assert gelees([1] * 1000 + [-1] + [1] * 1000) == 1