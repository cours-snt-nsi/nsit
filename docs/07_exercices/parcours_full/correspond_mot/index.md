---
author:
    - Mireille Coilhac
    - Franck Chambon
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Mots qui se correspondent
tags:
    - string
    - booléen
    - ep1
difficulty: 210
---

# Mot qui correspond à un mot à trou

!!! info "Mots à trou"
    - Un mot est ici une chaine de caractères composée uniquement de lettres de l'alphabet.
    - Un mot à trous comporte également zéro, une ou plusieurs fois le caractère `#!py "."`.

    On dira que `mot_complet` correspond à `mot_a_trous`, si on peut remplacer chaque `#!py "."` de `mot_a_trous` par une lettre de façon à obtenir `mot_complet`. 

    - `#!py "INFO.MA.IQUE"` est un mot à trous,
    - `#!py "INFORMATIQUE"` est un mot qui lui correspond,
    - `#!py "AUTOMATIQUE"` est un mot qui ne lui correspond pas.

!!! abstract "Objectif"
    Écrire une fonction telle que `correspond(mot_complet, mot_a_trous)` renvoie un booléen qui détermine si « `mot_complet` correspond à `mot_a_trous` »


???+ example "Exemples"

    ```pycon title=""
    >>> correspond("INFORMATIQUE", "INFO.MA.IQUE")
    True
    >>> correspond("AUTOMATIQUE", "INFO.MA.IQUE")
    False
    >>> correspond("INFO", "INFO.MA.IQUE")
    False
    >>> correspond("INFORMATIQUES", "INFO.MA.IQUE")
    False
    ```

{{ IDE('exo') }}
