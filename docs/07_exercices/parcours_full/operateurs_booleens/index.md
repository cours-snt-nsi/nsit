---
author: Nicolas Revéret
hide:
    - navigation
    - toc
difficulty: 130
title: Opérateurs booléens
tags:
    - booléen
    - fonctions
---

# Opérateurs booléens

Certains opérateurs booléens tels que « $et$ », « $ou$ » et « $non$ » existent de façon native en Python. Leurs tables de vérités sont rappelées plus bas.

Le but de cet exercice est d'écrire des fonctions mimant leur fonctionnement en n'utilisant que des tests `if...elif...else`.

!!! tip "Astuce"

    On rappelle que si `a` est un booléen, l'instruction `if a == True:` peut-être allégée en `if a:`.

Les **seules structures autorisées** dans cet exercice sont  :

* les tests simples :

```python
if booleen:
    ...
elif booleen:
    ...
else:
    ...
```

* les tests imbriqués :

```python
if booleen_1:
    if booleen_2:
        ...
```

* l'utilisation dans les tests de fonction déjà écrites. Par exemple :

```python
if non(booleen):
    ...
```

!!! warning "Contrainte"

    Il est donc interdit dans cet exercice d'utiliser les opérateurs booléens natifs de Python : `not`, `and`, `or`.
    De la même façon on ne comparera pas les valeurs à l'aide de `==`, `!=`, `>`, ... 
    
    On s'interdira de la même façon d'utiliser la [bibliothèque `operator`](https://docs.python.org/fr/3/library/operator.html) ainsi que des conversions telles que `int(a)`...

On demande d'écrire les fonctions suivantes :

* `non` prend en argument le booléen `a` et renvoie `False` si `a` vaut `True`, `True` s'il vaut `False` ;
* `et` prend en argument les booléens `a` et `b` et ne renvoie `True` que s'ils valent l'un et l'autre `True` ; 
* `ou` prend en argument les booléens `a` et `b` et renvoie `True` dès que l'un ou l'autre vaut `True` (ou les deux) ; 
* `ou_exclusif` prend en argument les booléens `a` et `b` et renvoie `True` si les deux arguments ont des valeurs différentes ; 
* `non_ou` prend en argument les booléens `a` et `b` et ne renvoie `True` que s'ils valent l'un et l'autre `False`. ; 
* `non_et` prend en argument les booléens `a` et `b` et ne renvoie `False` que s'ils valent l'un et l'autre `True`. 

???+ example "Exemples"

    ```pycon title=""
    >>> non(True)
    False
    >>> et(True, False)
    False
    >>> ou(True, False)
    True
    >>> ou_exclusif(True, True)
    False
    >>> non_ou(False, True)
    False
    >>> non_et(False, True)
    True
    ```

=== "Opérateur `non`"

    La table de vérité de cet opérateur est :

    |   `a`   | `non(a)` |
    | :-----: | :------: |
    | `False` |  `True`  |
    | `True`  | `False`  |

=== "Opérateur `et`"

    La table de vérité de cet opérateur est :

    |   `a`   |   `b`   | `et(a, b)` |
    | :-----: | :-----: | :--------: |
    | `False` | `False` |  `False`   |
    | `False` | `True`  |  `False`   |
    | `True`  | `False` |  `False`   |
    | `True`  | `True`  |  `True`   |

=== "Opérateur `ou`"

    La table de vérité de cet opérateur est :

    |   `a`   |   `b`   | `ou(a, b)` |
    | :-----: | :-----: | :--------: |
    | `False` | `False` |  `False`   |
    | `False` | `True`  |   `True`   |
    | `True`  | `False` |   `True`   |
    | `True`  | `True`  |   `True`   |

=== "Opérateur `ou_exclusif`"

    La table de vérité de cet opérateur est :

    |   `a`   |   `b`   | `ou_exclusif(a, b)` |
    | :-----: | :-----: | :-----------------: |
    | `False` | `False` |       `False`       |
    | `False` | `True`  |       `True`        |
    | `True`  | `False` |       `True`        |
    | `True`  | `True`  |       `False`       |

=== "Opérateur `non_ou`"

    La table de vérité de cet opérateur est :

    |   `a`   |   `b`   | `non_ou(a, b)` |
    | :-----: | :-----: | :-----------------: |
    | `False` | `False` |       `True`       |
    | `False` | `True`  |       `False`        |
    | `True`  | `False` |       `False`        |
    | `True`  | `True`  |       `False`       |

=== "Opérateur `non_et`"

    La table de vérité de cet opérateur est :

    |   `a`   |   `b`   | `non_et(a, b)` |
    | :-----: | :-----: | :--------: |
    | `False` | `False` |  `True`   |
    | `False` | `True`  |  `True`   |
    | `True`  | `False` |  `True`   |
    | `True`  | `True`  |  `False`   |

{{ IDE('exo') }}
