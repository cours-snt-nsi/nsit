

# --------- PYODIDE:code --------- #

def tri_selection(tableau):
    ...

# --------- PYODIDE:corr --------- #

def position_minimum(tableau, i):
    "recherche et renvoie l'indice du minimum à partir de l'indice i"
    ind_mini = i
    for j in range(i + 1, len(tableau)):
        if tableau[j] < tableau[ind_mini]:
            ind_mini = j
    return ind_mini


def echange(tableau, i, j):
    tableau[i], tableau[j] = tableau[j], tableau[i]


def tri_selection(tableau):
    for i in range(len(tableau)):
        indice_minimum = position_minimum(tableau, i)
        echange(tableau, indice_minimum, i)

# --------- PYODIDE:tests --------- #

tab = [1, 52, 6, -9, 12]
tri_selection(tab)
assert tab == [-9, 1, 6, 12, 52], "Exemple 1"

tab_vide = []
tri_selection(tab_vide)
assert tab_vide == [], "Exemple 2"

singleton = [9]
tri_selection(singleton)
assert singleton == [9], "Exemple 3"

# --------- PYODIDE:secrets --------- #


# Autres tests

from random import sample

for i in range(10):
    nombres = list(sample(range(10**9), 100 + i))
    attendu = sorted(nombres)
    tri_selection(nombres)
    assert len(nombres) == len(
        attendu
    ), "Erreur, le tableau ne doit pas changer de taille"
    for (
        a,
        b,
    ) in zip(nombres, attendu):
        assert a == b, "Erreur lors du tri"