On peut aussi utiliser les définitions en compréhension des listes.

```python
def matrice_vers_liste(m):
    return [[k for k in range(len(m[i])) if m[i][k] == 1] for i in range(len(m))]
```
