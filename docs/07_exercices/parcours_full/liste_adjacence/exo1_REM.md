On peut aussi utiliser les définitions en compréhension des listes et dictionnaires.

```python
def matrice_vers_dict(m):
    return {i: [k for k in range(len(m[i])) if m[i][k] == 1] for i in range(len(m))}
```
