

# --------- PYODIDE:code --------- #

def est_trie(tableau):
    ...

# --------- PYODIDE:corr --------- #

def est_trie(tableau):
    for i in range(1, len(tableau)):
        if tableau[i - 1] > tableau[i]:
            return False
    return True

# --------- PYODIDE:tests --------- #

assert est_trie([0, 5, 8, 8, 9]) is True
assert est_trie([8, 12, 4]) is False
assert est_trie([-1, 4]) is True
assert est_trie([5]) is True
assert est_trie([]) is True

# --------- PYODIDE:secrets --------- #


# Autres tests

assert est_trie([5, 4, 3, 2, 1, 0]) is False
assert est_trie([1, 2, 3, 4, 0]) is False
assert est_trie([2, 1, 2, 3, 4]) is False
assert est_trie([5, 1, 2, 3, 4]) is False
assert est_trie([10, 10, 10, 10]) is True
assert est_trie([-9, -9, -8, -6, -3, -2, 0, 4, 6, 7, 7, 7]) is True