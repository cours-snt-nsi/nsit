On montre que dans ce cas de figure, le coefficient $a_{i,j}$ de la $k$-ième puissance de la matrice d'adjacence est égal au nombre de chemins de longueur $k$ reliant les sommets de numéros $i$ et $j$.

Les deux propriétés équivalentes citées dans l'énoncé sont donc aussi équivalentes à la propriété suivante :

il existe un indice $i$ tel que le coefficient diagonal $A^3_{i, i}$ n'est pas nul, $A^3$ désignant la matrice $A \times A \times A$.

Cette propriété n'est pas utilisée ici car elle nécessite le calcul de deux produits de matrices dont le coût est important.

De manière générale, s'il existe un indice $i$ tel que le coefficient diagonal $A^n_{i, i}$ n'est pas nul, on peut affirmer qu'il existe un cycle de longueur $n$.