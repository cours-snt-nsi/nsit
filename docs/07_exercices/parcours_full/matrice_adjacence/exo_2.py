# --------- PYODIDE:env --------- #


def produit(a, b):
    n = len(a)
    p = [[0] * n for i in range(n)]
    for i in range(n):
        for j in range(n):
            p_i_j = 0
            for k in range(n):
                p_i_j = p_i_j + a[i][k] * b[k][j]
            p[i][j] = p_i_j
    return p


# --------- PYODIDE:code --------- #


def triangle(a):
    ...


# --------- PYODIDE:corr --------- #


def triangle(a):
    n = len(a)
    a_carre = produit(a, a)
    for i in range(n):
        for j in range(n):
            if a[i][j] > 0 and a_carre[i][j] > 0:
                return True
    return False


# --------- PYODIDE:tests --------- #

a = [[0, 1, 1], [1, 0, 1], [1, 1, 0]]
assert triangle(a)
b = [
    [0, 1, 1, 1, 0],
    [1, 0, 0, 0, 1],
    [1, 0, 0, 1, 1],
    [1, 0, 1, 0, 0],
    [0, 1, 1, 0, 0],
]
assert triangle(b)
c = [[0, 1], [1, 0]]
assert not triangle(c)

# --------- PYODIDE:secrets --------- #
a = [[0, 1, 0, 1], [1, 0, 1, 0], [0, 1, 0, 1], [1, 0, 1, 0]]
attendu = False
assert triangle(a) is attendu, f"Erreur avec a = {a}"
a = [[0, 0, 0, 1], [0, 0, 0, 0], [0, 0, 0, 1], [1, 0, 1, 0]]
attendu = False
assert triangle(a) is attendu, f"Erreur avec a = {a}"
a = [[0, 1, 1, 0], [1, 0, 1, 1], [1, 1, 0, 0], [0, 1, 0, 0]]
attendu = True
assert triangle(a) is attendu, f"Erreur avec a = {a}"
a = [
    [0, 0, 1, 0, 0, 1, 0, 0],
    [0, 0, 1, 1, 0, 1, 1, 0],
    [1, 1, 0, 1, 1, 0, 1, 1],
    [0, 1, 1, 0, 0, 0, 0, 0],
    [0, 0, 1, 0, 0, 1, 0, 0],
    [1, 1, 0, 0, 1, 0, 1, 1],
    [0, 1, 1, 0, 0, 1, 0, 0],
    [0, 0, 1, 0, 0, 1, 0, 0],
]
attendu = True
assert triangle(a) is attendu, f"Erreur avec a = {a}"
a = [
    [0, 0, 0, 0, 1, 0, 1, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 0, 0],
    [0, 0, 0, 0, 1, 1, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [1, 0, 1, 0, 0, 0, 0, 0, 1],
    [0, 1, 1, 0, 0, 0, 1, 0, 0],
    [1, 0, 0, 0, 0, 1, 0, 1, 0],
    [0, 0, 0, 0, 0, 0, 1, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0, 0],
]
attendu = False
assert triangle(a) is attendu, f"Erreur avec a = {a}"

from random import randrange, choice

for _ in range(5):
    n_ = randrange(1, 10)
    a = [[0 for _ in range(n_)] for _ in range(n_)]
    for i in range(n_ - 1):
        for j in range(i + 1, n_):
            a[i][j] = choice([0, 0, 1])
            a[j][i] = a[i][j]
    a_cube = [[x for x in ligne] for ligne in a]
    for _ in range(2):
        a_cube = produit(a_cube, a)
    attendu = any(a_cube[i][i] > 0 for i in range(n_))
    assert triangle(a) is attendu, f"Erreur avec a = {a}"
