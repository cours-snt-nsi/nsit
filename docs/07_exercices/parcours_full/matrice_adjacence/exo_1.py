# --------- PYODIDE:env --------- #
def produit(a, b):
    ...


# --------- PYODIDE:code --------- #
def produit(a, b):
    n = len(...)
    p = [[...] * ... for i in range(...)]
    for i in range(...):
        for j ...:
            p_i_j = 0
            for ...
                p_i_j = ...
            p[i][j] = ...
    return ...
# --------- PYODIDE:corr --------- #


def produit(a, b):
    n = len(a)
    p = [[0] * n for i in range(n)]
    for i in range(n):
        for j in range(n):
            p_i_j = 0
            for k in range(n):
                p_i_j = p_i_j + a[i][k] * b[k][j]
            p[i][j] = p_i_j
    return p


# --------- PYODIDE:tests --------- #
a = [[0, 1], [1, 0]]
b = [[1, 0], [0, 1]]
assert produit(a, b) == a
assert produit(a, a) == b
a = [[1, 0, 1, 0], [1, 1, 1, 0], [0, 0, 0, 1], [1, 0, 0, 0]]
b = [[1, 0, 1, 0], [1, 0, 1, 1], [1, 1, 1, 1], [0, 0, 0, 0]]
p = [[2, 1, 2, 1], [3, 1, 3, 2], [0, 0, 0, 0], [1, 0, 1, 0]]
assert produit(a, b) == p

# --------- PYODIDE:secrets --------- #
A = [[0]]
B = [[0]]
P = [[0]]
assert produit(A, B) == P, f"Erreur avec a = {A} et b = {B}"
A = [[1]]
B = [[1]]
P = [[1]]
assert produit(A, B) == P, f"Erreur avec a = {A} et b = {B}"
A = [
    [1, 0, 0, 0, 1, 0, 0, 1, 1],
    [1, 0, 0, 1, 1, 0, 1, 0, 0],
    [0, 1, 1, 0, 1, 1, 0, 0, 0],
    [0, 1, 0, 1, 0, 1, 0, 0, 1],
    [0, 1, 0, 1, 0, 1, 0, 0, 0],
    [1, 1, 0, 1, 1, 1, 0, 0, 1],
    [0, 1, 1, 0, 0, 0, 0, 1, 1],
    [0, 0, 0, 0, 0, 1, 0, 0, 0],
    [1, 0, 0, 1, 0, 1, 0, 0, 0],
]
B = [
    [0, 1, 1, 1, 1, 1, 1, 1, 0],
    [0, 0, 0, 0, 0, 1, 1, 1, 1],
    [1, 1, 1, 1, 0, 1, 0, 1, 1],
    [1, 0, 1, 0, 1, 1, 0, 0, 0],
    [0, 1, 0, 0, 0, 0, 1, 1, 1],
    [1, 0, 1, 0, 1, 1, 0, 1, 0],
    [0, 0, 1, 0, 0, 1, 0, 1, 1],
    [1, 1, 0, 1, 0, 1, 0, 1, 0],
    [0, 0, 0, 1, 1, 1, 1, 0, 0],
]
P = [
    [1, 3, 1, 3, 2, 3, 3, 3, 1],
    [1, 2, 3, 1, 2, 3, 2, 3, 2],
    [2, 2, 2, 1, 1, 3, 2, 4, 3],
    [2, 0, 2, 1, 3, 4, 2, 2, 1],
    [2, 0, 2, 0, 2, 3, 1, 2, 1],
    [2, 2, 3, 2, 4, 5, 4, 4, 2],
    [2, 2, 1, 3, 1, 4, 2, 3, 2],
    [1, 0, 1, 0, 1, 1, 0, 1, 0],
    [2, 1, 3, 1, 3, 3, 1, 2, 0],
]
assert produit(A, B) == P, f"Erreur avec a = {A} et b = {B}"

from random import randrange, choice

for _ in range(5):
    n_ = randrange(1, 10)
    A = [[choice([0, 1]) for _ in range(n_)] for _ in range(n_)]
    B = [[choice([0, 1]) for _ in range(n_)] for _ in range(n_)]
    P = [
        [sum(a * b for a, b in zip(ligne_A, colonne_B)) for colonne_B in zip(*B)]
        for ligne_A in A
    ]
    assert produit(A, B) == P, f"Erreur avec a = {A} et b = {B}"
