Cet algorithme est relativement efficace car à chaque étape il coupe la zone de recherche en deux.

Ainsi, dans l'exemple précédent, la détermination d'une solution approchée à $10^{-6}$ près, en partant d'une zone
de recherche intiale de largeur $1$ nécessite $10$ étapes car $\left(\frac12\right)^{9} > 10^{-6} > \left(\frac12\right)^{10}$.
