
# --------- PYODIDE:code --------- #

def f(x):
    return -(x**3) + x**2 + x + 1

def dichotomie(debut, fin, precision):
    ...

# --------- PYODIDE:corr --------- #

def f(x):
    return -(x**3) + x**2 + x + 1

def dichotomie(debut, fin, precision):
    while fin - debut > precision:
        milieu = (debut + fin) / 2
        if f(milieu) * f(fin) > 0:
            fin = milieu
        elif f(milieu) * f(fin) < 0:
            debut = milieu
        else:
            return (milieu, milieu)
    return (debut, fin)

# --------- PYODIDE:tests --------- #

assert dichotomie(1, 2, 1) == (1, 2)  # la solution est comprise entre 1 et 2

print("Un encadrement de la solution de l'équation à 10^-6 près est :")
print(dichotomie(1, 2, 1e-6))

# --------- PYODIDE:secrets --------- #


# tests secrets
assert dichotomie(1, 2, 0.5) == (1.5, 2)
assert dichotomie(1, 2, 0.25) == (1.75, 2)
