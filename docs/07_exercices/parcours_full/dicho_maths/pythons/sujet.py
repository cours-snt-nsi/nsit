# --- exo, dichot  --- #
def f(x):
    return -(x**3) + x**2 + x + 1
    
def dichotomie(debut, fin, precision):
    ...
    
    
# --- vide, dichot --- #
""" # skip
def f(x):
    return -(x**3) + x**2 + x + 1

def dichotomie(debut, fin, precision):
    while ... > ...:
        milieu = (... + ...) / 2
        if f(...) * f(...) > ...:
            ... = ...
        elif f(...) * f(...) > ...:
            ... = ...
        else:
            return (..., ...)
    return (..., ...)
""" # skip


# --- corr, dichot --- #
def dichotomie(debut, fin, precision):
    while fin - debut > precision:
        milieu = (debut + fin) / 2
        if f(milieu) * f(fin) > 0:
            fin = milieu
        elif f(milieu) * f(fin) < 0:
            debut = milieu
        else:
            return (milieu, milieu)
    return (debut, fin)
# --- tests, dichot --- #
assert dichotomie(1, 2, 1) == (1, 2)  # la solution est comprise entre 1 et 2

print("Un encadrement de la solution de l'équation à 10^-6 près est :")
print(dichotomie(1, 2, 1e-6))
# --- secrets, dichot --- #
assert dichotomie(1, 2, 0.5) == (1.5, 2)
assert dichotomie(1, 2, 0.25) == (1.75, 2)
# --- rem, dichot --- #
""" # skip
Cet algorithme est relativement efficace car à chaque étape il coupe la zone de recherche en deux.

Ainsi, dans l'exemple précédent, la détermination d'une solution approchée à $10^{-6}$ près, en partant d'une zone
de recherche intiale de largeur $1$ nécessite $10$ étapes car $\left(\frac12\right)^{9} > 10^{-6} > \left(\frac12\right)^{10}$.
""" # skip
