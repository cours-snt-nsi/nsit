

# --------- PYODIDE:code --------- #

def noyaux(n):
    ...

# --------- PYODIDE:corr --------- #

def noyaux(n):
    v = 6e21
    resultats = [v]
    for jour in range(n):
        v = 0.995 * v + 1.5e19
        resultats.append(v)
    return resultats

# --------- PYODIDE:tests --------- #

assert noyaux(0) == [6000000000000000000000]
assert noyaux(1) == [6000000000000000000000, 5.985e21]
assert noyaux(5) == [
    6000000000000000000000,
    5.985e21,
    5.970075e21,
    5.955224625e21,
    5.940448501875e21,
    5.925746259365626e21,
]

# --------- PYODIDE:secrets --------- #


# tests secrets
assert noyaux(10) == [
    6000000000000000000000,
    5.985e21,
    5.970075e21,
    5.955224625e21,
    5.940448501875e21,
    5.925746259365626e21,
    5.911117528068798e21,
    5.896561940428454e21,
    5.882079130726312e21,
    5.867668735072681e21,
    5.853330391397318e21,
], "Erreur pour n = 10"
attendus = [6 * 1e21]
for _ in range(128):
    v = attendus[-1]
    attendus.append(0.995 * v + 1.5 * 10**19)
n = 128
assert noyaux(n) == attendus, f"Erreur en prenant {n = }"