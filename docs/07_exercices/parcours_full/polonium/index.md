---
author:
    - Pierre Marquestaut
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Noyaux de polonium
tags:
    - maths
    - à trous
difficulty: 190
maj: 01/03/2024
---

[Marie Skłodowska-Curie](https://fr.wikipedia.org/wiki/Marie_Curie) (1867-1934) est une physicienne, chimiste et mathématicienne polonaise naturalisée française.
Deux prix Nobel lui ont été décernés : un en Physique en 1903 (partagé avec son mari et Henri Becquerel) et un en Chimie en 1911 pour la découverte de deux nouveaux éléments, le polonium (nom donné en hommage à ses origines) et le radium.

On s'intéresse ici à la désintégration des noyaux atomiques de polonium au cours du temps.

Au début de l'expérience, on dispose d'un morceau de $2$ grammes de polonium.
On sait que $1$ gramme de polonium contient $3 \times 10^{21}$ noyaux atomiques.

On admet que, au bout de $24$ heures, $0,5$ % des noyaux se sont désintégrés et que, pour compenser cette disparition, on ajoute alors $0,005$ gramme de polonium.

??? note "Demie-vie"

    La demi-vie de l'atome de polonium 210 est de $128$ jours. Cela signifie que si l'on dispose d'une certaine quantité d'atomes de polonium 210, cette quantité aura été divisée par $2$ au bout de $128$ jours.
    
    Ce rythme de disparition correspond à une perte de $0,5$ % par jour.

On modélise la situation à l'aide de la suite $v_n$ dénombrant le nombre d'atomes encore présents $n$ jours après le début de l'expérience. On a donc :

\[
\begin{cases}
    v_0 = 6 \times 10^{21}  \\
    v_{n+1} = 0,995v_n + 1,5 \times 10^{19}\quad\text{ pour } n \geqslant 0
\end{cases}
\]

On demande d'écrire la fonction `noyaux` qui renvoie la liste contenant le nombre de noyaux durant les $n$ premiers jours de l'expérience.

On garantit que $n \leqslant 128$.

??? tip "Notation scientifique"

    Python permet d'abréger la notation `#!py 6 * 10**21` en `#!py 6e21`.

{{ remarque('append')  }}

???+ example "Exemples"

    ```pycon title=""
    >>> noyaux(0)
    [6000000000000000000000]
    >>> noyaux(1)
    [6000000000000000000000, 5.985e+21]
    >>> noyaux(5)
    [6000000000000000000000, 5.985e+21, 5.970075e+21, 5.955224625e+21, 5.940448501875e+21, 5.925746259365626e+21]
    ```

=== "Version vide"
    {{ IDE('exo_a') }}
=== "Version à compléter"
    {{ IDE('exo_b') }}