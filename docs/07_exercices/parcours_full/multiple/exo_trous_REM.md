La solution peut être allégée.

En effet, à deux reprises, on effectue un test et l'on renvoie `#!py True` si celui-ci est évalué à `#!py True` et `#!py False` s'il est évalué à `#!py False`.

On peut donc directement renvoyer le résultat du test :

```python
def est_multiple(a, b):
    if b == 0:
        return a == 0
    multiple = 0
    while multiple < a:
        multiple = multiple + b
    return multiple == a
```