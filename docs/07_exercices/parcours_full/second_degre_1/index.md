---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Autour du second degré (1)
tags:
    - maths
    - float
    - fonctions
difficulty: 60
maj: 27/04/2024
---

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/css/all.min.css" integrity="sha512-SnH5WK+bZxgPHs44uWIX+LLJAJ9/2PkPKZ5QiAj6Ta86w+fsb2TkcmfRyVX3pBnMFcV7oQPJkl9QevSCWr3W6A==" crossorigin="anonymous" referrerpolicy="no-referrer" />

Cette page regroupe différents exercices simples sur les polynômes du second degré.

On rappelle qu'une fonction *polynôme du second degré* est une fonction définie pour tout nombre réel $x$ par :

$$x \mapsto ax^2 + bx + c$$

dans laquelle $a$, $b$ et $c$ sont des nombres réels avec $a$ non nul.

Les exercices sont tous indépendants et peuvent être traités dans un ordre quelconque.

{{ remarque('assertion') }}

??? question "Définir la fonction"

    Le code contenu dans l'éditeur ci-dessous définit la fonction $f:x\mapsto 5x^2-9x+3$.
    
    Ajouter en dessous le code définissant la fonction $g:x\mapsto -3x^2+7x+5$.
      
    {{ IDE('./pythons/exo_fonction') }}

??? question "Tableau de valeurs"

    On souhaite dans cet exercice calculer les images de certains nombres par une fonction $f$.

    On considère donc une fonction $f$ définie dans le corps du programme ainsi qu'une fonction `valeurs` qui prend en paramètres :
    
    * la fonction dont on souhaite calculer les valeurs ;
    * deux nombres entiers `x_min` et `x_max`.

    La fonction `valeurs` renvoie la liste des images par la fonction passée en argument des entiers compris entre $x_{min}$ et $x_{max}$ (inclus l'un et l'autre).
    
    On garantit que $x_{min} \leqslant x_{max}$ et que la fonction polynôme a des coefficients entiers.
    
    ???+ example "Exemples"
    
        ```pycon title=""
        >>> def f(x):
        ...     return x**2
        >>> valeurs(f, 0, 1)  # images de 0 et 1 par x ↦ x^2
        [0, 1]
        >>> valeurs(f, 0, 3)  # images de 0, 1, 2 et 3 par x ↦ x^2
        [0, 1, 4, 9]
        ```
    
    === "Version vide"
        {{ IDE('./pythons/exo_valeurs_vide') }}
    === "Version à compléter"
        {{ IDE('./pythons/exo_valeurs_trous') }}
    

??? question "Représenter la fonction"

    On souhaite dans cet exercice représenter graphiquement une fonction définie dans le corps du programme.
    
    Pour cela on fournit la fonction `dessine` qui prend en paramètre une liste `points` contenant des couples de nombres correspondants aux points à placer dans le graphe et construit la représentation graphique correspondante.
    
    **Cette fonction est déjà importée dans l'éditeur.** Vous pouvez directement l'utiliser.
    
    Compléter la fonction `graphique` qui prend en paramètres la fonction à représenter ainsi que trois entiers `a`, `b` et `n`. Cette fonction représente la fonction passée en paramètre en construisant `n` de ses points dont les abscisses sont uniformément réparties entre `a` et `b`.
    
    
    Cette fonction ne renvoie rien et votre code ne sera donc pas testé. Assurez-vous néanmoins d'obtenir des graphiques correspondants à vos attentes.
    
    On garantit que `a` est strictement inférieur à `b` et que `n` est strictement positif.
    
    {{ IDE('./pythons/exo_graphe') }}
    
    <div id="graphe" class="center" style="display: flex;justify-content: center;align-content:center;flex-direction: column;margin:auto;min-height:5em;text-align:center;border:solid 1px var(--main-theme);border-radius: 0 8px 0 8px">
    Votre courbe sera ici
    </div>
    
??? question "Sommet de la parabole"

    La représentation graphique d'une fonction du second degré est une parabole.
    
    Si $h$ est la fonction définie sur $\mathbb{R}$ par $h(x)=ax^2+bx+c$, le sommet de cette parabole a pour coordonnées $\left(-\dfrac{b}{2a}~;~h\left(-\dfrac{b}{2a}\right)\right)$.
    
    Écrire la fonction `sommet` qui prend en paramètres les valeurs des coefficients $a$, $b$ et $c$ et renvoie les coordonnées du sommet de la représentation graphique de la fonction $x\mapsto ax^2+bx+c$.
    
    On rappelle qu'il est garanti que $a$ est non nul.
    
    ???+ example "Exemples"
    
        ```pycon title=""
        >>> sommet(1, 0, 0)
        (0.0, 0.0)
        >>> sommet(1, -8, 3)
        (4.0, -13.0)
        ```
        
    {{ IDE('./pythons/exo_sommet') }}

??? question "Variations"

    Les fonctions du second degré n'admettent que deux types de variations. Elles peuvent être :
    
    * décroissantes sur $\left]-\infty~;~-\dfrac{b}{2a}\right]$ puis croissantes sur $\left[-\dfrac{b}{2a}~;~+\infty\right[$;
    
    * croissantes sur $\left]-\infty~;~-\dfrac{b}{2a}\right]$ puis décroissantes sur $\left[-\dfrac{b}{2a}~;~+\infty\right[$.

    ![Variations possibles](variations.svg){.center .autolight width=40%}

    Écrire la fonction `variations` qui prend en paramètres les valeurs des coefficients $a$, $b$ et $c$ et renvoie :
    
    * `#!py "v"` si la fonction est décroissante puis croissante ;

    * `#!py "^"` si la fonction est croissante puis décroissante.
    
    ???+ example "Exemples"
    
        ```pycon title=""
        >>> variations(1, 0, 0)
        'v'
        >>> variations(-1, -8, 3)
        '^'
        ```
    
    {{ IDE('./pythons/exo_variations') }}
