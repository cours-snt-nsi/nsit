# --------- PYODIDE:env --------- #
def g(x):
    pass


# --------- PYODIDE:code --------- #
def f(x):
    return 5 * x**2 - 9 * x + 3


# compléter ci-dessous avec la définition de la fonction g
...


# --------- PYODIDE:corr --------- #
def g(x):
    return -3 * x**2 + 7 * x + 5


# --------- PYODIDE:tests --------- #
# la fonction f n'a pas été modifiée
assert f(0) == 3
assert f(2) == 5
# définition correcte de la fonction g
assert g(0) == 5
assert g(2) == 7


# --------- PYODIDE:secrets --------- #
def _g(x):
    return -3 * x**2 + 7 * x + 5


for x in range(-10, 11):
    attendu = _g(x)
    assert g(x) == attendu, f"Erreur sur la valeur obtenue pour g({x})"


