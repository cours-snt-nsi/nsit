

# --------- PYODIDE:code --------- #

def noircir(texte, noir):
    ...

# --------- PYODIDE:corr --------- #

def noircir(texte, noir):
    resultat = ""
    for caractere in texte:
        if caractere.isalpha():
            resultat += noir
        else:
            resultat += caractere

    return resultat

# --------- PYODIDE:tests --------- #

assert noircir("", "*") == ""
assert noircir("L'espion était J. Bond", "▮") == "▮'▮▮▮▮▮▮ ▮▮▮▮▮ ▮. ▮▮▮▮"
assert noircir("L'espion était J. Bond", "_") == "_'______ _____ _. ____"
assert noircir(";-)", "▮") == ";-)"
assert noircir("*", "*") == "*"

# --------- PYODIDE:secrets --------- #

from random import randint, choice, sample

# Tests
assert noircir("", "*") == ""
assert noircir("L'espion était J. Bond", "▮") == "▮'▮▮▮▮▮▮ ▮▮▮▮▮ ▮. ▮▮▮▮"
assert noircir("L'espion était J. Bond", "_") == "_'______ _____ _. ____"


# Tests supplémentaires
ALPHABET = "".join(chr(i) for i in range(ord("A"), ord("A") + 26))
ALPHABET += "".join(chr(i) for i in range(ord("a"), ord("a") + 26))
PONCTUATION = " ,;:!?.'"
NOIRS = "▮#@*/%$&"

for _ in range(10):
    nb_lettres = randint(1, 20)
    nb_ponctuation = randint(1, 20)
    texte = "".join([choice(ALPHABET) for _ in range(nb_lettres)])
    texte += "".join([choice(PONCTUATION) for _ in range(nb_ponctuation)])
    texte = "".join(sample(texte, len(texte)))
    noir = choice(NOIRS)
    attendu = "".join([noir if c.isalpha() else c for c in texte])
    assert noircir(texte, noir) == attendu, f"Erreur avec {texte}"
