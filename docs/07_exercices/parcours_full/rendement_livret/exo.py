

# --------- PYODIDE:code --------- #

def double_placement(placement, rendement):
    ...

# --------- PYODIDE:corr --------- #

def double_placement(placement, rendement):
    annee = 0
    capital = placement
    double = 2 * placement
    while capital < double:
        capital = capital + capital*rendement/100
        annee += 1
    return annee

# --------- PYODIDE:tests --------- #

assert double_placement(1000, 3.5) == 21
assert double_placement(1000, 100) == 1
assert double_placement(1000, 10) == 8

# --------- PYODIDE:secrets --------- #


# Autres tests
assert double_placement(1000, 50) == 2
assert double_placement(1000, 25) == 4
assert double_placement(1000, 40) == 3