---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: Rendement d'un livret
difficulty: 120
tags:
    - maths
maj: 01/03/2024
---

# Rendement d'un livret

Tom a ouvert un livret dans une banque, qui lui propose un rendement de 3.5 % par an.
Il y a placé la somme de 1000 € et se demande maintenant en combien d'années cette somme sera doublée.

On souhaite écrire la fonction `double_placement`. Cette fonction prend en paramètres un nombre strictement positif `placement` qui représente la somme placée, et un nombre `rendement`, compris entre 0 et 100, qui correspond au rendement exprimé en pourcents. Cette fonction renvoie un entier qui correspond au nombre d'années nécessaires pour le doublement du placement initial.


{{ IDE('exo') }}