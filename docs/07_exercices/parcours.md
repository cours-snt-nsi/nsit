---
hide:
    - toc
---

# 🚶 Parcours

On propose ici différents parcours permettant d'explorer un algorithme, une thématique ou une structure de données...

??? abstract "Algorithmes gloutons"

    On utilise des méthodes gloutonnes.

    - « {{ lien_exo("Premier minimum local", "premier_minimum" ) }} » : déterminer l'indice du premier minimum dans un tableau


??? abstract "Calcul de moyennes"

    On calcule ici les moyennes dans un tableau, ou dans des structures plus complexes.

    - « {{ lien_exo("Moyenne simple", "moyenne" ) }} » : calculer une moyenne
    - « {{ lien_exo("Moyenne olympique", "moy_olympic" ) }} » : calculer une moyenne en excluant la valeur maximale et la valeur minimale
    - « {{ lien_exo("Moyenne pondérée", "moyenne_ponderee" ) }} » : calculer une moyenne pondérée

??? abstract "Dictionnaires : construction"

    Des exercices demandant de construire des dictionnaires.

    - « {{ lien_exo("Valeurs extrêmes", "dict_extremes" ) }} » : extraire le dictionnaire `{"mini": m, "maxi": M}` d'un tableau
    - « {{ lien_exo("Dictionnaire d'occurrences", "dico_occurrences" ) }} » : compter les occurrences des caractères dans une chaine
    - « {{ lien_exo("Dictionnaire des antécédents", "antecedents" ) }} » : déterminer le dictionnaire « réciproque » d'un dictionnaire : `{valeur: [clés associées]}`


??? abstract "Dictionnaires: utilisation"

    On y parcourt des dictionnaires.

    - « {{ lien_exo("Anniversaires", "anniversaires" ) }} » : déterminer les clés dont les valeurs associées vérifient une certaine condition
    - « {{ lien_exo("Couleurs", "couleurs" ) }} » : convertir la représentation d'une couleur en hexadécimal à du RGB
    - « {{ lien_exo("L-système", "lsystem" ) }} » : « calculer » une nouvelle chaine de caractères en respectant les règles contenues dans un dictionnaire
    - « {{ lien_exo("Top-like", "top_like" ) }} » : déterminer la clé de valeur maximale

??? abstract "Graphes"

    <center>
    ```mermaid
    flowchart LR
    G ---|5| R
    G ---|2| A
    R ---|3| A
    A --- |1| P
    P --- |6| H
    P --- |6| E
    H --- |1| E
    ```
    </center>
    
    - « {{ lien_exo("La rumeur qui court", "rumeur" ) }} » : parcours en largeur
    - « {{ lien_exo("Degré de séparation", "degres_separation" ) }} » : illustration des petits mondes
    - « {{ lien_exo("Aimer et être aimé", "aimer_etre_aime" ) }} » : renverser un graphe orienté
    - « {{ lien_exo("Cadeaux circulaires", "permutation_circulaire" ) }} » : un graphe est-il cyclique ?
    - « {{ lien_exo("Liste et matrice d'adjacence", "liste_adjacence" ) }} » : passer d'une représentation d'un graphe à l'autre
    - « {{ lien_exo("Matrice d'adjacence", "matrice_adjacence" ) }} » : deux exercices autour des matrices d'adjacence
    - « {{ lien_exo("Mme Tortue et M. Lièvre", "lievre_tortue" ) }} » : algorithme de Floyd

??? abstract "Manipulation de chaines de caractères"

    Tout est dans le titre !

    - « {{ lien_exo("Dentiste", "dentiste" ) }} » : supprimer les voyelles d'une chaine de caractères
    - « {{ lien_exo("Mots qui se correspondent", "correspond_mot" ) }} » : comparer deux chaines de caractères
    - « {{ lien_exo("Renverser une chaine", "renverse_chaine" ) }} » : comme son nom l'indique !
    - « {{ lien_exo("Collage", "join" ) }} » : former une chaine à partir des éléments d'une liste... réécrire `" ".join(mots)` !
    - « {{ lien_exo("Découpe", "split" ) }} » : découper une chaine à chaque espace... réécrire `chaine.split(' ')` !
    - « {{ lien_exo("Code de César", "code_cesar" ) }} » : chiffrer une chaine de caractère à l'aide du code de César
    - « {{ lien_exo("Texte inclus", "est_inclus" ) }} » : recherche d'un motif dans une chaine de caractères
    - « {{ lien_exo("Texte brut", "brut" ) }} » : extraire le contenu textuel d'une source `HTML`

??? abstract "Maths : Seconde"

    - « {{ lien_exo("Fonctions affines", "fonctions_affines" ) }} » : plusieurs exercices autour des fonctions affines
    - « {{ lien_exo("Fonctions du 2nd degré", "second_degre_1" ) }} » : plusieurs exercices autour des fonctions polynômes du seconde degré
    - « {{ lien_exo("Géométrie vectorielle", "geometrie_vectorielle" ) }} » : plusieurs exercices de géométrie vectorielle
    - « {{ lien_exo("Résolution d'équation par balayage", "balayage" ) }} » : résolution approchée d'une équation
    - « {{ lien_exo("Moyenne simple", "moyenne" ) }} » : calculer une moyenne
    - « {{ lien_exo("Moyennes pondérée", "moy_ponderee" ) }} » : calculer une moyenne pondérée
    - « {{ lien_exo("Puissances d'un nombre", "seuil_puissances" ) }} » : déterminer la première puissance d'un nombre dépassant ou passant sous un seuil
    - « {{ lien_exo("Multiple", "multiple" ) }} » : déterminer si un entier est un multiple d'un autre
    - « {{ lien_exo("Diviseurs d'un entier", "diviseurs" ) }} » : déterminer les diviseurs d'un entier positif
    - « {{ lien_exo("Divisibilité par 3", "divisible_par_3" ) }} » : trois méthodes pour étudier la divisibilité par trois
    - « {{ lien_exo("Nombre Premier", "nombre_premier" ) }} » : déterminer si un (petit) nombre entier est premier

??? abstract "Maths : Première"

    - « {{ lien_exo( "Termes d'une suite", "suite" ) }} » : calculer $u_n$
    - « {{ lien_exo( "Désintégration", "polonium" ) }} » : modéliser la désintégration d'atomes par une suite
    - « {{ lien_exo( "Suite de Syracuse", "syracuse" ) }} » : la fameuse suite
    - « {{ lien_exo( "Rendement d'un livret", "rendement_livret" ) }} » : utiliser les suites dans le cadre d'un compte bancaire
    - « {{ lien_exo( "Suite arithmétique", "suite_arithm" ) }} » : utiliser une suite arithmétique
    - « {{ lien_exo( "Suite doublement récurrente", "termes_d_une_suite" ) }} » : utiliser une suite récurrente
    - « {{ lien_exo( "Somme des termes d'une suite", "somme_suite" ) }} » : sommer des termes consécutifs d'une suite
    - « {{ lien_exo( "Terme d'une suite constante", "suite_flottants" ) }} » : où il est bon de prendre un crayon avant de taper du code

??? abstract "Maths : Terminale"

    - «  {{ lien_exo("Seuil et suite explicite" , "seuil_explicite" ) }} » : limite d'un suite définie explicitement
    - «  {{ lien_exo("Seuil et suite récurrente" , "seuil_recurrence" ) }} » : limite d'un suite définie par récurrence
    - «  {{ lien_exo("Intégrale entre 0 et 1", "integrale_1" ) }} » : calcul approché de $\int_{0}^{1}x^2\text{d}x$ par la méthode des rectangles
    - «  {{ lien_exo("Intégrale quelconque", "integrale_2" ) }} » : calcul appoché de  $\int_{a}^{b}f(x)\text{d}x$ par la méthode des rectangles
    - «  {{ lien_exo("Résolution approchée d'équation par dichotomie" , "dicho_maths" ) }} »

??? abstract "Parcours de tableaux"

    Ici l'on filtre des tableaux, on vérifie qu'un tableau est trié.

    - « {{ lien_exo("Écrêtage", "ecretage" ) }} » : remplacer les valeurs extrêmes d'un tableau
    - « {{ lien_exo("Soleil couchant", "soleil_couchant" ) }} » : déterminer tous les *maxima* relatifs d'un tableau
    - « {{ lien_exo("Nombres puis double", "nb_puis_double" ) }} » : rechercher des couples de valeurs consécutives particulières dans un tableau
    - « {{ lien_exo("Est trié ?", "est_trie" ) }} » : le tableau est-il trié ?
    - « {{ lien_exo("Liste des différences", "liste_differences" ) }} » : comparer deux tableaux
    - « {{ lien_exo("Gelées", "gelee" ) }} » : longueur d'un sous-tableau particulier
    - « {{ lien_exo("Tous différents", "tous_differents" ) }} » : les éléments d'un tableau sont-ils tous différents ?


??? abstract "Programmation dynamique"

    - « {{ lien_exo("Communication des acacias", "acacias" ) }} » : somme maximale dans un tableau sous contrainte
    - « {{ lien_exo("Plus longues sous-chaines", "lplssc" ) }} » : plus longues sous-chaines communes à deux chaines

??? abstract "Programmation orientée objet"

    Il s'agit ici d'utiliser des classes proposées ou de les écrire.

    * « {{ lien_exo("Chien en POO", "poo_chien" ) }} » : compléter une classe représentant un chien
    * « {{ lien_exo("Train en POO", "poo_train" ) }} » : compléter une classe représentant un train
    * « {{ lien_exo("Hauteur et taille d'un arbre binaire", "arbre_bin" ) }} » : compléter les méthodes `hauteur` et `taille`
    * « {{ lien_exo("Arbre binaire de recherche", "ABR" ) }} » : compléter les méthodes `insere`, `est_present`, `affichage_infixe`

??? abstract "Recherche dans un tableau"

    On recherche ici les _extrema_ dans un tableau (minimum ou maximum), une valeur ou un indice particulier.

    - « {{ lien_exo("Maximum", "maximum_nombres" ) }} » : déterminer le maximum dans un tableau
    - « {{ lien_exo("Indice du minimum", "ind_min" ) }} » : déterminer l'indice du minimum dans un tableau
    - « {{ lien_exo("Premier minimum local", "premier_minimum" ) }} » : déterminer l'indice du premier minimum dans un tableau
    - « {{ lien_exo("Occurrences du minimum", "occurrences_du_mini" ) }} » : déterminer la valeur et les indices du minimum
    - « {{ lien_exo("Valeur et indices du max", "val_ind_max" ) }} » : déterminer la valeur et les indices du maximum

??? abstract "Récursivité"

    Voir « Récursivité »

    - « {{ lien_exo("Suite de Fibonacci (1)", "fib_1" ) }} » : une fonction très élémentaire, pour des indices inférieurs à 25
    - « {{ lien_exo("Anagrammes", "anagrammes" ) }} » : deux chaines sont-elles des anagrammes ?
    - « {{ lien_exo("Percolation", "percolation" ) }} » : écoulement d'eau dans un sol, ou plutôt parcours en profondeur dans une grille
    - « {{ lien_exo("Pavage possible avec triominos (2)", "pavage_triomino_2" ) }} » : Construire un pavage, si possible, d'un carré troué avec des triominos
    - « {{ lien_exo("Énumération des permutations", "nb_factoriel" ) }} » : les nombres factoriels
    - « {{ lien_exo("Énumération des chemins à Manhattan", "nb_chemins_grille" ) }} » : relier deux points en ne se déplaçant que vers la droite ou vers le haut
    - « {{ lien_exo("Énumération des chemins de Schröder", "nb_schroder" ) }} » : mouvements ↗ , →→, ou ↘
    - « {{ lien_exo("Énumération des chemins de Delannoy", "nb_delannoy" ) }} » : mouvements ↗, →, ou ↘
    - « {{ lien_exo("Énumération des arbres binaires de taille n", "nb_catalan_2" ) }} » : les nombres de Catalan
    - « {{ lien_exo("Énumération des arbres unaires-binaires à n+1 nœuds", "nb_motzkin" ) }} » : les nombres de Motzkin
    - « {{ lien_exo("Énumération des subdivisions de polygone", "nb_hipparque" ) }} » : Les nombres d'Hipparque
    - « {{ lien_exo("Suite de Fibonacci (2)", "fib_2" ) }} » :boom: :boom: : Calcul possible du millionième terme
    - « {{ lien_exo("Nombre de partitions d'un entier", "nb_partitions" ) }} »

??? abstract "Structures conditionnelles"

    Si c'est vrai il faut faire ceci, sinon cela...

    - « {{ lien_exo("Autour des booléens", "autour_des_booleens" ) }} » : écrire des tests tels que « $15$ est-il dans la table de $3$ et de $5$ ? »
    - « {{ lien_exo("Années bissextiles", "bissextile" ) }} » : déterminer si une année est bissextile ou non
    - « {{ lien_exo("Opérateurs booléens", "operateurs_booleens" ) }} » : écrire les opérateurs booléens sans utiliser les opérateurs booléens !
    - « {{ lien_exo("Multiplier sans *", "multiplication" ) }} » : multiplier deux entiers relatifs sans utiliser le symbole `*`
    - « {{ lien_exo("Suite de Syracuse", "syracuse" ) }} » : la fameuse suite $3n+1$
    - « {{ lien_exo("Tous différents", "tous_differents" ) }} » : les éléments d'un tableau sont-ils tous distincts (solution en temps quadratique)
    - « {{ lien_exo("Noircir un texte", "anonymat_1") }} » et « {{ lien_exo("Caviarder un texte", "anonymat_2" ) }} » : effacer les caractères alphabétiques dans un texte

??? abstract "Structures de données"

    On y utilise ou met en œuvre des listes chaînées, piles, files, arbres...

    * « {{ lien_exo("Train en POO", "poo_train" ) }} » : une liste chainée en réalité
    * « {{ lien_exo("Bien parenthésée 2", "expression_bien_parenthesee_2" ) }} » : une expression est-elle bien parenthésée ?
    * « {{ lien_exo("Hauteur et taille d'un arbre", "arbre_enracine" ) }} » : on y représente les arbres sous forme de liste de listes Python
    * « {{ lien_exo("Hauteur et taille d'un arbre binaire", "arbre_bin" ) }} » : on y représente les arbres à l'aide de la programmation orientée objet

??? abstract "Tris"

    Les classiques !

    - « {{ lien_exo("Tri par sélection", "tri_selection" ) }} »
    - « {{ lien_exo("Tri à bulles", "tri_bulles" ) }} »
    - « {{ lien_exo("Insertion dans une liste triée", "insertion_liste_triee" ) }} »
    - « {{ lien_exo("Fusion de listes triées", "fusion_listes_triees" ) }} »
    - « {{ lien_exo("Tas min", "tas_min" ) }} » : vérifier qu'un tableau représente un tas minimal

