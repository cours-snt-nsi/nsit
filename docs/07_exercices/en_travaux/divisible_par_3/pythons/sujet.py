# --- hdr, natif_, str_, int_  --- #
def divisible_par_3(n):
    pass


# --- vide, natif_, str_, int_  --- #
def divisible_par_3(n):
    ...


# --- exo, str_ --- #
def divisible_par_3(n):
    while n > ...:
        chaine = ...(n)
        somme = ...
        for chiffre in ...:
            if chiffre not in ...:
                somme = somme + ...(chiffre)
        n = ...
    return ... in (0, 3, 6, 9)


# --- exo, int_ --- #
def divisible_par_3(n):
    while n > ...:
        n = ...
    return ... in (0, 3, 6, 9)


# --- corr, str_ --- #
def divisible_par_3(n):
    while n > 10:
        chaine = str(n)
        somme = 0
        for chiffre in chaine:
            if chiffre not in "0369":
                somme = somme + int(chiffre)
        n = somme
    return n in (0, 3, 6, 9)
# --- corr, int_ --- #
def divisible_par_3(n):
    while n > 10:
        n = n // 10 + n % 10
    return n in (0, 3, 6, 9)
# --- corr, natif_ --- #
def divisible_par_3(n):
    return n % 3 == 0
# --- tests, natif_, str_, int_  --- #
assert divisible_par_3(0) is True
assert divisible_par_3(1) is False
assert divisible_par_3(3) is True
assert divisible_par_3(9230) is False
assert divisible_par_3(9231) is True
assert divisible_par_3(9232) is False
# --- secrets, natif_, str_, int_  --- #
from random import randrange
for _ in range(10):
    n = randrange(10**6, 10**9)
    attendu = (n % 3 == 0)
    assert divisible_par_3(n) is attendu, f"Erreur avec {n = }"
