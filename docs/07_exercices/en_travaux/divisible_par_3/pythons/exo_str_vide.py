# --------- PYODIDE:env --------- #
def divisible_par_3(n):
    pass


# --------- PYODIDE:code --------- #
def divisible_par_3(n):
    ...


# --------- PYODIDE:corr --------- #
def divisible_par_3(n):
    while n > 10:
        chaine = str(n)
        somme = 0
        for chiffre in chaine:
            if chiffre not in "0369":
                somme = somme + int(chiffre)
        n = somme
    return n in (0, 3, 6, 9)
# --------- PYODIDE:tests --------- #
assert divisible_par_3(0) is True
assert divisible_par_3(1) is False
assert divisible_par_3(3) is True
assert divisible_par_3(9230) is False
assert divisible_par_3(9231) is True
assert divisible_par_3(9232) is False
# --------- PYODIDE:secrets --------- #
from random import randrange
for _ in range(10):
    n = randrange(10**6, 10**9)
    attendu = (n % 3 == 0)
    assert divisible_par_3(n) is attendu, f"Erreur avec {n = }"
