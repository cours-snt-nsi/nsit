class Longueur:
    def __init__(self, cm):  # le constructeur
        """On passe simplement la longueur en centimètres"""
        self.cm = cm  # cm est un attribut

    def en_metre(self):  # accesseur
        """Renvoie la longueur convertie en mètres"""
        return self.cm / 100

    def en_millimetre(self):  # accesseur
        """Renvoie la longueur convertie en millimètres"""
        return self.cm * 10

    def allonge(self, x):  # mutateur
        """
        Ajoute x centimètres à la longueur
        x doit être strictement positif
        """
        assert x > 0, "x doit être strictement positif"
        self.cm += x

    def somme(self, autre):
        """
        Ajoute l'autre longueur à celle-ci
        Renvoie un nouvel objet dont la longueur est égal à la somme des deux
        """
        assert type(autre) is Longueur, "autre doit être une 'Longueur'"
        return Longueur(self.cm + autre.cm)

    def __repr__(self):
        """Renvoie la représentation de cette longueur sous forme d'un str"""
        return f"{self.cm} cm"


trois = Longueur(3)
cinq = Longueur(5)

# L'attribut cm de trois
print(f"{trois.cm = }")
# L'attribut cm de cinq
print(f"{cinq.cm = }")

# Conversions
print(f"{trois.en_metre() = }")
print(f"{trois.en_millimetre() = }")

# Modification de trois
trois.allonge(7)
print(f"{trois.cm = }")

# Somme des longueurs
quinze = trois.somme(cinq)  # en effet trois contient la valeur 10 !
print(f"{quinze.cm = }")