---
title: Paradigmes de programmation
---

Il existe de nombreux langages de programmation, chacun avec ses spécificités et ses adeptes... Il est néanmoins possibles de les classer selon leur **paradigme**, selon la façon dont ils manipulent les données et traitent les problèmes (les paragraphes ci-dessous sont inspirés de cette [source](https://docs.python.org/fr/3/howto/functional.html)) :

* Les langages les plus classiques suivent un paradigme **impératif**. Les programmes rédigés dans de tels langages  sont constitués de listes d'instructions qui détaillent les opérations que l'ordinateur doit appliquer aux entrées du programme. C, Pascal ou encore les interpréteurs de commandes Unix sont des langages procéduraux

* les langages **déclaratifs** permettent d'écrire la spécification, la description, du problème et laissent l'implémentation du langage trouver une façon efficace de réaliser les calculs nécessaires à sa résolution. SQL fait partie des langages déclaratifs : une requête `SQL` décrit le jeu de données que l'on souhaite récupérer et le moteur `SQL` choisit de parcourir les tables ou d'utiliser les indices, l'ordre de résolution des sous-clauses, etc...

* les programmes **orientés objet** manipulent des ensembles d'objets (!). Ceux-ci possèdent un état interne et des méthodes qui interrogent ou modifient cet état d'une façon ou d'une autre. Java est un langage orienté objet. C++ et Python gèrent la programmation orientée objet mais n'imposent pas l'utilisation de telles fonctionnalités

* Enfin, la programmation **fonctionnelle** implique de décomposer un problème en un ensemble de fonctions. Dans l'idéal, les fonctions produisent des sorties à partir d'entrées et ne possède pas d'état interne qui soit susceptible de modifier la sortie pour une entrée donnée. Les langages fonctionnels les plus connus sont ceux de la famille ML (Standard ML, OCaml et autres) et Haskell

![Paradigmes](paradigmes.png){width=65% .autolight .center}

Certains langages sont clairement associés à un paradigme.

Selon sa formation, son goût mais dans l'idéal selon le type de problème qu'il a à résoudre, un programmeur pourra choisir de rédiger son code dans un paradigme l'autre.

## La programmation impérative

C'est le paradigme de programmation le plus classique, celui que nous utilisons régulièrement en Python.

Dans ce paradigme, le programme consiste en un ensemble d'instructions décrivant comment modifier l'état (la mémoire, les relations avec les périphériques...) de l'ordinateur.

On peut, en programmation impérative utiliser les instructions suivantes :

* la séquence d'instructions : une suite d'actions que le système doit effectuer successivement (`ouvrir le cahier; écrire le cours; fermer le cahier;`),
* l'affectation de valeur à des variables du type `#!py a = 5`,
* l'instruction conditionnelle, les `#!py if`,
* la boucle non bornée (`#!py while condition:`) ou bornée (`#!py for elt in interable`).

Le langage machine (l'assembleur) peut être considéré comme faisant partie de la programmation impérative. Les courts scripts python que nous tapons en cours en font aussi partie bien souvent.

???+ info "La variable au centre"

    Le concept central dans la programmation impérative est la *variable* : écrire un programme c'est décrire l'état initial et les transformations subies par un ou des variables.

??? example "Exemple : soyons patients"

    On place 100 € sur un compte en banque rémunéré à 3 % par an. Combien d'années doit-on attendre avant d'avoir 200 € ?

    {{ IDE('imperatif.py')}}

## La programmation orientée objet

Le programme de Terminale NSI fait la part belle aux structures de données. Une structure de données permet par exemple de stocker des informations tout en autorisant certaines actions avec celles-ci. Il est aussi possible de faire interagir certaines structures de données entre elles.

Ce type de problèmes se prête bien au paradigme de la **programmation orientée objet** [^1] (POO).

[^1]: dans certaines classifications, la POO est placée dans le paradigme impératif.

???+ info "L'objet au centre"

    Comme son nom l'indique, le concept central dans la programmation orientée objet impérative est l'*objet* : écrire un programme c'est décrire les caractéristiques et les interactions de différents objets.

???+ info "Vocabulaire de la POO"

    ![POO](oop.jpg){width=30% .autolight align=right}
    
    Une **classe** permet de décrire le modèle général d'un objet. C'est le moule qui permettra de « forger » les objets.
    
    Une **instance** est un objet d'une certaine classe. C'est un objet « concret » réalisé à partir d'une classe.
    
    Les **attributs** sont les caractéristiques d'un objet. Il peuvent être différents d'un objet à l'autre, quand bien même tous ceux-ci seraient des instances de la même classe.
    
    Les **méthodes** sont les fonctions que l'on peut appliquer à un objet. Elles sont définies dans la classe.
    
    Le **constructeur** permet de créer l'objet. On lui passe les valeurs des attributs.
    
    Un **accesseur** est une méthode permettant de récupérer la valeur d'un attribut.
    
    Un **mutateur** est une méthode permettant de modifier la valeur d'un attribut.

L'avantage d'une telle organisation du code est la *formalisation* : en réfléchissant en amont aux attributs, aux comportements et aux interactions des objets, on gagne en hauteur de vue et, si l'on a bien travaillé, en clarté de code.

L'autre avantage est l'**encapsulation** des données : seul le concepteur de la classe connaît le fonctionnement interne. L'utilisateur final ne connaît que la surface, l'*interface*. Le plus souvent il n'a pas besoin de connaître les détails de l'implantation. Il faut par contre que la *documentation* soit claire !

??? example "Exemple : la classe `Longueur`"

    On fournit ci-dessous un exemple : une classe `Longueur` qui permet de représenter la longueur d'un objet en centimètres.

    {{ IDE('classe_longueur.py') }}

???+ "En python"

    La classe est introduite par `#!py class <NomDeLaClasse>` (en `CamelCase`).
    
    Le constructeur est impérativement nommé `__init__`.
    
    Le paramètre `self` permet d'indiquer qu'une méthode, un attribut, correspond à **cet objet** particulier. C'est (sauf cas particuliers[^2]) le premier paramètre de chaque méthode : `#!py def methode(self, param1, param2):`.
    
    [^2]: il existe toujours des cas particuliers. On peut par exemple imaginer des méthodes s'appliquant non pas à un objet particulier mais à **tous** les objets de la classe. Appeler une telle méthode sur une instance aura un effet sur tous les objets de cette classe.
    
    Contrairement à d'autres langages, Python autorise d'accéder directement aux attributs, sans nécessairement passer par des accesseurs. On peut néanmoins contourner ce comportement en utilisant des [*propriétés*](https://docs.python.org/fr/3/library/functions.html#property) (hors-programme).

    
## La programmation fonctionnelle

Les programmes du paradigme impératif et de la POO s'appuient sur l'*état* de la machine qui les exécute : centrés sur la notion de variables (qui peuvent être des objets), les programmes dépendent et modifient l'état de celles-ci.

Ce fonctionnement classique peut toutefois poser des problèmes : les **effets de bords**.

???+ info "Effet de bord"

    Il en existe de deux types :

    * effet de bord *entrant* : une variable extérieure à une fonction modifie le comportement de celle-ci ;

    * effet de bord *sortant* : fonction modifie une variable extérieure à celle-ci.

    Le premier cas de figure aura pour conséquence que le même appel `f(x)` ne renverra pas toujours le même résultat (en lien avec l'état de variables extérieures).
    
    Le second cas est plus pernicieux car un appel de fonction pourra modifier l'état général de la machine et donc avoir des conséquences sur une tout autres instructions.
    
    Dans les deux cas, ces effets de bords compliquent **l'étude de la correction du programme.**

??? example "Effet de bord *entrant*"

    Dans l'exemple ci-dessous, le même appel `#!py fois_k(5)` renvoie deux résultats différents car la fonction dépend de l'état de la variable `k` définie dans le corps principal du programme.
    
    {{ IDE('effet_bord_entrant.py' )}}

??? example "Effet de bord *sortant*"

    Afin de vérifier que tous les nombres de la liste sont pairs, on vide celle-ci. Les valeurs sont perdues !

    {{ IDE('effet_bord_sortant.py' )}}
    
Le paradigme de la programmation fonctionnelle vise, entre autres choses, à éliminer ces problèmes en interdisant toute modification de valeur de variables (dans les faits, bien des langages *fonctionnels* autorisent tout de même ces modifications).

Pour concevoir un programme, on ne raisonne pas en termes d'états de variables à faire évoluer mais plutôt en termes de **transformations indépendantes que l'on fait se succéder**.

???+ info "La fonction au centre"

    Le concept central dans la programmation fonctionnelle est donc la *fonction* (au sens mathématique du terme).
    
    Les fonctions utilisées sont *pures* : elles ne subissent ni ne créent pas d'effets de bords.
    
    Un programme est donc constitué d'une suite de fonctions qui s'enchaînent à partir d'une entrée initiale.
    
    Si chaque fonction est *correcte* et *pure*, leur succession est nécessairement *correcte*.
    
    $$x \xrightarrow[f]{} f(x) \xrightarrow[g]{} g\left[f(x)\right] \xrightarrow[h]{} h\left(g\left[f(x)\right]\right)$$

???+ note "Fonction d'ordre supérieur"

    Les fonctions sont au cœur de la programmation fonctionnelle : à tel point que des fonctions peuvent être des paramètres d'autre fonctions voir même des résultats.
    
    Ci-dessous :
    
    * l'appel `#!py fonction_fois(3)` renvoie  la fonction $x\mapsto 3x$ ;
    * l'appel `#!py fonction_ajoute(5)` renvoie la fonction $x\mapsto x+5$ ;
    * l'appel `#!py fonction_affine(fois_3, ajoute_5)` prend deux fonctions en paramètres et renvoie la fonction affine $x\mapsto 3x+5$.
    
    {{ IDE('ordre_superieur.py') }}


???+ note "En Python"

    La volonté de se passer au maximum de variables conduit à se passer de *boucles* : les programmes fonctionnels font un usage intensif de la **récursivité**.
    
    On utilise de plus couramment l'opérateur ternaire : `#!py a = truc if condition else bidule`.
    
    Enfin, le module `functools` contient des fonctions facilitant l'écriture de programmes fonctionnels.

    {{ IDE('fonctionnel.py') }}