def fonction_fois(a):
    def f(x):  # création d'une nouvelle fonction
        return a * x
    return f


def fonction_ajoute(b):
    return lambda x: x + b  # notation concise avec l'opérateur lambda


def fonction_affine(multiplication, addition):  # deux fonctions en paramètres
    return lambda x: addition(multiplication(x))


fois_3 = fonction_fois(3)
assert fois_3(6) == 18

ajoute_5 = fonction_ajoute(5)
assert ajoute_5(6) == 11

# Création de la fonction affine x ↦ 3x + 5
affine = fonction_affine(fois_3, ajoute_5)
assert affine(1) == 8
