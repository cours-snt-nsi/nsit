def maximum_fonctionnel(nombres, i=0):
    """
    Renvoie le maximum de nombres
    Provoque une erreur si la liste est vide
    """
    if i == len(nombres):
        return nombres[-1]

    temp = maximum_fonctionnel(nombres, i + 1)  # variable de stockage NON MODIFIEE
    # Utilisation de la récursivité
    return nombres[i] if nombres[i] >= temp else temp  # opérateur ternaire


assert maximum_fonctionnel([1, 3, 2, 5, 4]) == 5

# La fonction reduce applique la même fonction de proche en proche aux éléments d'un itérable
from functools import reduce


def maximum_reduce(nombres):
    """
    Renvoie le maximum de nombres
    Provoque une erreur si la liste est vide
    """
    maxi_2 = lambda x, y: x if x >= y else y
    return reduce(function=maxi_2, sequence=nombres, initial=nombres[0])


assert maximum_reduce([1, 3, 2, 5, 4]) == 5
