def tous_pairs(nombres):
    if nombres == []:
        return True
    dernier = nombres.pop()
    if dernier % 2 == 1:
        return False
    return tous_pairs(nombres)


nombres = [2, 4, 6, 8]
print(f"{nombres = }")
assert tous_pairs(nombres) is True
print(f"{nombres = }")