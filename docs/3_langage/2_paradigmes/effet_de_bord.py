def calculer(n: int) -> int:
    if liste[0] % 2 == 0:
        liste[0] = 1
        return 2*n
    else:
        liste[0] = 0
        return n // 2


def double_non_fonctionnelle(liste: list) -> None:
    for i in range(len(liste)):
        liste[i] = 2 * liste[i]


def double_fonctionnelle(liste: list) -> list:
    return [2*k for k in liste]


if __name__ == "__main__":
    liste = [112]
    print(calculer(8))
    print(calculer(8))

    liste = [3, 4, 5]
    double_non_fonctionnelle(liste)
    print(liste)

    liste = [3, 4, 5]
    liste_bis = double_fonctionnelle(liste)
    print(liste)
    print(liste_bis)
