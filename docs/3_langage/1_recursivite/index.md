---
title: Récursivité
---

## Définition

???+ info "Fonction récursive"

    Une fonction est dite *récursive* lorsqu'elle s'appelle elle-même, soit *directement*, soit *indirectement*.
    
    
    Une fonction récursive est souvent construite en deux temps :

    <center>

    ```mermaid
    flowchart LR
        A(Fonction récursive)
        B(Cas de base ?)
        C(Sortie)
        D(Appel récursif)
        
        A --> B
        B -->|Oui| C
        B -->|Non| D
        D --> A
    ```
    </center>

Considérons un exemple simple.

{{ IDE('exemple_base.py') }}

Le cas de base contient une **condition d'arrêt**. Par exemple :

* on a atteint la condition souhaitée,

* la structure étudiée est vide,

* ...

L'appel récursif appelle à nouveau la fonction **en modifiant la valeurs du ou des paramètres**. Par exemple :

* on diminue le paramètre d'une unité (et l'on s'arrêtera par exemple lorsque l'on atteint `#!py 0`),

* on supprime une valeur de la structure étudiée (et l'on s'arrêtera par exemple lorsque la structure est vide),

* ...

{{ IDE('exemple_base_2.py') }}

## Problèmes possibles

On peut rencontrer deux problèmes classiques.

??? note "Cas de base non atteint"

    Il arrive que l'on oublie le cas de base ou qu'on le formule mal.

    {{ IDE('pas_de_cas_de_base.py') }}
    

??? note "Profondeur trop importante"

    Python limite par défaut les appels récursifs à `#!py 1 000`.
    
    On peut avoir un code valide mais qui tarde à atteindre le cas de base.

    {{ IDE('harmonique.py') }}

??? note "Augmenter la profondeur"

    Python autorise toutefois de modifier la profondeur de récursivité en faisant :
    
    {{ IDE('harmonique_bis.py') }}

## Pile d'appels

Une bonne façon de décrire le fonctionnement d'appels est une *pile* : lors des appels successifs, le système les empile sur la **pile d'exécution**. À chaque nouvel empilement, l'exécution d'une fonction est mise en attente jusqu'à ce qu'elle soit de nouveau en haut de la pile.

Lorsqu'une fonction se termine (avec un `#!py return` en particulier), elle est dépilée et son résultat est transmis à la fonction qui l'avait appelée, fonction qui se retrouve alors au sommet de la pile et est exécutée....

La fenêtre ci-dessous illustre la pile des appels générée par `somme(5)` (il y en a six, de `#!py n = 5` à `#!py n = 0`) :

<iframe width="100%" height="500" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=def%20somme%28n%29%3A%0A%20%20%20%20%22%22%22Calcule%20la%20somme%200%20%2B%201%20%2B%202%20%2B%20...%20%2B%20n%22%22%22%0A%20%20%20%20if%20n%20%3D%3D%200%3A%20%20%23%20cas%20de%20base%0A%20%20%20%20%20%20%20%20return%200%0A%20%20%20%20return%20n%20%2B%20somme%28n%20-%201%29%20%20%23%20appel%20r%C3%A9cursif%0A%0Aassert%20somme%285%29%20%3D%3D%200%20%2B%201%20%2B%202%20%2B%203%20%2B%204%20%2B%205&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>

???+ note "Consommation"

    Cette visualisation permet de comprendre l'un des problèmes lié à la récursivité : elle consomme des ressources.
    
    En effet, chaque appel est lié dans le système d'exploitation à un espace mémoire qui permet de garder trace des paramètres de l'appel et autres informations.
    
    Multiplier les appels, va multiplier ces espaces mémoires.
    
    Dans bien des cas, il est préférable de construire un code *itératif* utilisant une structure de pile.

## Applications

On retrouve des fonctions récursives dans les cas suivants :

* parcours en profondeur d'abord d'un graphe ;
* [tri fusion](https://nreveret.forge.apps.education.fr/tris/04_rapides/01_fusion/) (on coupe le paquet de cartes en deux et on trie chacun des paquets) ;
* le calcul des termes de la suite de Fibonacci ($u_{n} = u_{n-1} + u_{n-2}$ même si c'est plutôt un contre-exemple...) ;
* les [tours de Hanoï](https://fr.wikipedia.org/wiki/Tours_de_Hano%C3%AF) ;
* les [L-systèmes](https://fr.wikipedia.org/wiki/L-Syst%C3%A8me) ;
* *etc*.

![Exemples de mauvaises herbes générées à l'aide de L-systèmes](L-systeme.jpg){width=55% .center .autolight}