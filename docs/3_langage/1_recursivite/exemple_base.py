def somme(n):
    """Calcule la somme 0 + 1 + 2 + ... + n"""
    if n == 0:  # cas de base
        return 0
    return n + somme(n - 1)  # appel récursif

assert somme(5) == 0 + 1 + 2 + 3 + 4 + 5
assert somme(6) == somme(5) + 6