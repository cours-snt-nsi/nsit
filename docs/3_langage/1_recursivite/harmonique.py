def harmonique(n, somme_partielle):
    """Somme 1 + 1/2 + 1/3 + ... + 1/n jusqu'à dépasser 8"""
    if somme_partielle >= 8:  # condition trop lointaine :(
        return n
    return harmonique(n + 1, somme_partielle + 1 / n)


# Cet appel est interrompu car il faut sommer 1675 termes pour dépasser 8
harmonique(n=1, somme_partielle=0)
