def hanoi(n, origine, destination, stockage):
    """
    Résout le problème pour n cylindres

    n est le nombre de cylindre
    origine est le nom de la tour de départ
    destination est le nom de la tour d'arrivée
    stockage est le nom de la tour de stockage
    """
    if n == 1:  # cas de base (régner)
        return f"{origine} → {destination}"

    # trois divisions
    aller = hanoi(n - 1, origine, stockage, destination)
    regner = hanoi(1, origine, destination, stockage)
    retour = hanoi(n - 1, stockage, destination, origine)

    # combinaison
    return f"{aller} ; {regner} ; {retour}"


print(hanoi(3, "A", "C", "B"))
