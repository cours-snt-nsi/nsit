def A(n: int):
    if n > 0:
        print(f"A({n}) appelle B({n-1})")
        retour = B(n-1)
        print(f"A({n}) récupère la main")
        return retour
    else:
        return f"A({n}) se termine"


def B(n: int):
    if n > 0:
        print(f"B({n}) appelle A({n-1})")
        retour = A(n-1)
        print(f"B({n}) récupère la main")
        return retour
    else:
        print(f"B({n}) se termine")
        return f"B({n}) s'est terminée"


def inc(n: int):
    if n == 5:
        return f"On s'arrête à n={n}"
    else:
        print(f"n vaut {n}")
        return inc(n+1)


def fact(n: int) -> int:
    if n == 1:
        return 1
    else:
        return n * fact(n-1)


if __name__ == "__main__":
    print(fact(7))
