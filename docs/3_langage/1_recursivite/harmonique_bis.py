def harmonique(n, somme_partielle):
    """Somme 1 + 1/2 + 1/3 + ... + 1/n jusqu'à dépasser 8"""
    if somme_partielle >= 8:
        return n
    return harmonique(n + 1, somme_partielle + 1 / n)


import sys
sys.setrecursionlimit(10_000)  # 10 000 appels récursifs autorisés

# cette fois-ci cela fonctionne :)
print(harmonique(n=1, somme_partielle=0))
