def somme(n):
    """Calcule la somme n + (n - 1) + (n - 2) + ... + -infini"""
    
    # pas de cas de base :(
    
    return n + somme(n - 1)  # appel récursif


# Cet appel fait planter Python
somme(5)