def somme_termes(valeurs):
    """Calcule la somme des termes contenus dans valeurs"""
    if len(valeurs) == 0:  # cas de base
        return 0
    dernier = valeurs.pop()  # suppression du dernier terme
    return dernier + somme_termes(valeurs)  # appel récursif, valeurs compte un élément de moins

assert somme_termes([1, 2, 10]) == 1 + 2 + 10
assert somme_termes([1 for _ in range(100)]) == 100  # 100 fois 1