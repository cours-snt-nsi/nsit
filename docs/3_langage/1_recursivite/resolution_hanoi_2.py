def hanoi_optimise(n, origine, destination, stockage):
    """
    Résout le problème pour n cylindres

    n est le nombre de cylindre
    origine est le nom de la tour de départ
    destination est le nom de la tour d'arrivée
    stockage est le nom de la tour de stockage
    """
    if n == 1:  # cas de base (régner)
        return f"{origine} → {destination}"

    # trois divisions
    aller = hanoi_optimise(n - 1, origine, stockage, destination)
    regner = hanoi_optimise(1, origine, destination, stockage)
    remplacement = {origine: stockage, stockage: destination, destination: origine}
    retour = ""
    for c in aller:
        if c in remplacement:
            c = remplacement[c]
        retour += c

    # combinaison
    return f"{aller} ; {regner} ; {retour}"


print(hanoi_optimise(3, "A", "C", "B"))
