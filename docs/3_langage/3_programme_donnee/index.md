---
title: Programme en tant que données
---

Nos ordinateurs sont très rapides, nous les utilisons tous les jours, parfois trop sous la forme de nos *smartphones*. Ils peuvent même, parfois, nous sembler intelligents !

Sont-ils vraiment capables de tout faire ? Dans la mesure où ils manipulent des nombres, sont-il capables de tout calculer ? Répondre à cette question nécessite de s'intéresser à la *calculabilité* des fonctions informatiques.

Identifier les problèmes qu'un ordinateur est effectivement capable de résoudre, revient à s'intéresser à leur *décidabilité*.  

## Les programmes sont des nombres comme les autres...

Prenons un programme Python simple :

```python
print("Hello World")
```
Et son presque jumeau (le `#!py "W"` ets devenu le `#!py "w"`) :

```python
print("Hello world")
```

Ces « programmes » sont enregistrés sous forme de nombres sur la machine : chaque caractère est associé à un code Unicode. Mis bout à bout, ces différents codes, forment un grand nombre en binaire.

??? example "Du code vers le nombre"

    {{ IDE('code_en_nombre.py') }}

Comme le montre les résultats, ces deux programmes sont associés à des nombres différents.

Il est possible de faire l'opération inverse : connaissant un nombre entier, on peut le convertir en un code (et à terme l'exécuter).

??? example "Du nombre vers le code"
    
    {{ IDE('nombre_en_code.py') }}

???+ info "Un programme est une donnée"

    Un programme informatique peut être converti en un nombre entier.
    
    Il peut aussi être utilisé comme « entrée » pour un autre programme. C'est le cas par exemple de nos programmes Python.
    
    <center>
    
    ```mermaid
    flowchart LR
      A[code source]
      B[compilateur]
      C["<span style='font-style: italic;color: var(--md-mermaid-label-fg-color);'>byte code</span>"]
      D[exécution]
      
      A --> B
      B --> C
      C --> D
    ```
    </center>
    
## Décidabilité et problème de l'arrêt

Quelques points de vocabulaire pour commencer.

???+ info "Problème de décision"

    Un problème de *décision* est une question à laquelle on peut répondre par « Oui » ou « Non » (« Vrai » ou Faux »).
    
    Par exemple : *le nombre $5$ est-il pair ?*. La réponse est « Oui ».

???+ info "Problème décidable"

    Un problème de *décision* est dit *décidable* s'il est possible de répondre à chaque *instance* du problème.
    
    Cette notion généralise la précédente : un problème est *décidable* s'il est possible de répondre « Oui » ou « Non » **quelle que soit la valeur d'entrée**.
    
    Par exemple : *Un nombre entier $n$ est-il pair ?*. La réponse est « Oui » car on peut construire un algorithme permettant de répondre à cette question (en calculant le reste de la division de $n$ par $2$ par exemple).

???+ info "Problème de la décision"

    **Le** problème de la *décision* consiste à ce demander si tous les énoncés de la logique du premier ordre sont *décidables* ?
    
    Formulé en termes simples, est-on capable de juger de la vérité de toutes les affirmations logiques ? Etant donné une affirmation logique, existe-t-il une méthode permettant de juger *à coup sûr* si elle est vraie ou fausse ?
    
    Ce problème, aussi appelé « [Entscheidungsproblem](https://fr.wikipedia.org/wiki/Probl%C3%A8me_de_la_d%C3%A9cision) », a été posé en 1928 par [David Hilbert](https://fr.wikipedia.org/wiki/David_Hilbert) et [Wilhelm Ackermann](https://fr.wikipedia.org/wiki/Wilhelm_Ackermann).
    
???+ info "Problème de l'arrêt"

    Afin de répondre au problème de la décision, Turing a posé un nouveau problème : « *existe-t-il un algorithme capable de juger, étant donné un autre algorithme et son entrée, si l'exécution de celui-ci avec cette entrée va s'arrêter ou non ?* »
    
Turing a démontré que la réponse au problème de l'arrêt est « Non » : il est impossible de concevoir un algorithme qui soit capables de juger l'arrêt de n'importe quel autre algorithme.

Ce faisant, il a mis en évidence une question non décidable et a donc aussi répondu « Non » au problème de la décision.

Sa démonstration repose sur un raisonnement par l'absurde. Si un tel algorithme existait, le faire fonctionner sur une version modifiée de lui-même conduirait à une absurdité.


## Les machines de Turing

???+ note "Hors-programme"

    Ce paragraphe n'est pas au programme.
    
    Il est toutefois important de connaître la machine de Turing lorsque l'on s'intéresse à l'informatique !

Dans sa preuve, Turing a présenté sa fameuse *machine de Turing* qui est un des modèles théoriques de l'ordinateur.

La machine de Turing est constituée :

* d'une bande de longueur infinie (ce qui limite d'emblée sa réalisation pratique...) divisé en cellules ;

* d'une tête de lecture/écriture pouvant lire ou écrire une valeur dans une cellule ;

* d'un moteur capable de décaler la bande d'une cellule vers la gauche ou la droite.

A ces éléments pratiques, on ajoute :

* un ensemble de caractères, que l'on appelle alphabet, que l'on a le droit d'écrire sur la bande (par exemple `0`, `1` et le symbole vide `_`) ;

* un ensemble d'états dans lequel peut se trouver la machine (notés souvent $q_1$ ... $q_n$) ;

* des règles de transitions du type :
  * si on est dans l'état $q_3$ et que l'on lit `1` dans la cellule alors on écrit `0` à la place, on décale la bande vers la droite (d'un pas) et on passe dans l'état $q_4$

<center>
![Le modèle de la machine de Turing](turing_machine.png){width=40%}
![Une machine de Turing](turing_machine.jpg){width=40%}
</center>

On retrouve le modèle d'un ordinateur :

* le bandeau joue le rôle de la *mémoire* ;

* la tête de lecture écriture est le *processeur* ;

* les règles de transition correspondent au *programme*.

Il reste tout de même un problème : dans cette description, une machine est associée à un alphabet, un ensemble de règles de transitions... La machine qui multiplie un nombre par deux est complètement différente de celle qui multiplie par trois !

Turing fait un pas supplémentaire : il imagine une machine universelle qui réserve une place sur son bandeau pour encoder la description d'une autre machine : son alphabet, ses règles de transitions... Une machine de Turing (un programme) peut ainsi être une donnée pour une autre machine à l'image des programmes de notre ordinateur qui sont des données pour le compilateur ou le système d'exploitation. L'ordinateur est né.

Malgré sa simplicité, une machine de Turing est capable de faire fonctionner tous les algorithmes imaginables. La [thèse de Church](https://fr.wikipedia.org/wiki/Th%C3%A8se_de_Church) postule même que tout calcul réalisable concrètement par un être humain est réalisable par une machine de Turing. **Un ordinateur peut donc calculer tout ce que peut calculer un être humain**.

Aujourd'hui encore, pour définir la puissance d'un langage informatique, on dit qu'il est *Turing-complet*. Un tel langage est capable de réaliser tout ce qu'une machine de Turing est capable de réaliser.

Python est Turing-complet, le langage C et même SQL le sont aussi. En tenant compte de la *redstone*, on peut même affirmer que *Minecraft* est Turing-complet !
