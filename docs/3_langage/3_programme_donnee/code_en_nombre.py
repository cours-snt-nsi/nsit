def code_en_entier(code):
    """Renvoie le nombre entier associé à code"""
    chaine = ""
    for c in code:
        code_ascii = ord(c)
        binaire = bin(code_ascii)[2:].zfill(8)  # le code en binaire, sur 8 bits
        chaine += binaire
    return int(chaine, 2)

code_1 = "print('Hello World')"
print(f"{code_en_entier(code_1) = }")

code_2 = "print('Hello world')"
print(f"{code_en_entier(code_2) = }")