"""
Implémentation d'une machine de Turing
"""

# Directions du déplacement
DROITE = +1
GAUCHE = -1
STOP = 0

# Création de la bande (remplie de 0 initialement)
bande = [0]*1000
largeur_affichage = 5
position = len(bande) // 2 # la position de lecture


def affiche_bande():
    """
    Fonction affichant la bande à la position de lecture
    """
    print("\n... ", end="")
    for i in range(-largeur_affichage, largeur_affichage + 1, 1) :
        if 0<= position + i <= 1000 - 1 :
            print(bande[position + i], end = " ")
    print("...")
    print("^".rjust(4 + 2*largeur_affichage + 1," "))


def step() :
    """
    Gestion d'une transition
    """
    print("Etat :", etat)
    print("Valeur lue :", bande[position])
    for r in regles:
        if etat == r[0] and bande[position] == r[1] :
            print("Valeur écrite :", r[2])
            print("Nouvel état :", r[4])
            if r[3] == GAUCHE :
                print("Déplacement vers la GAUCHE")
            elif r[3] == DROITE :
                print("Déplacement vers la DROITE")
            else :
                print("Pas de déplacement")
            bande[position] = r[2]
            return position + r[3], r[4]
    return position, "HALT"
    

# Les diférentes règles
# Au format etat lu / valeur lue / valeur écrite / direction déplacement / nouvel état
regles = [("q1", 0, 0, STOP, "stop"),
          ("q1", 1, 0, DROITE, "q2"),
          ("q2", 1, 1, DROITE, "q2"),
          ("q2", 0, 0, DROITE, "q3"),
          ("q3", 1, 1, DROITE, "q3"),
          ("q3", 0, 1, GAUCHE, "q4"),
          ("q4", 1, 1, GAUCHE, "q4"),
          ("q4", 0, 0, GAUCHE, "q5"),
          ("q5", 1, 1, GAUCHE, "q5"),
          ("q5", 0, 1, DROITE, "q1")
         ]

# Initialisation de la bande
bande[position] = 1
bande[position+1] = 1
bande[position+2] = 1

# Etat actuel
etat = "q1"

# Boucle générale
while etat != "stop" :
    affiche_bande()
    position, etat = step()
    
    input("Appuyer sur entrée pour passer à l'état suivant")