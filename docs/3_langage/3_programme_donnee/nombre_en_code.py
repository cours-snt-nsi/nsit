def entier_en_code(nombre):
    """Renvoie le code associé à un nombre entier"""
    code = ""
    binaire = bin(nombre)[2:]  # conversion en binaire
    while len(binaire) % 8 != 0:  # ajout de "0" en tête si besoin
        binaire = "0" + binaire
    for i in range(0, len(binaire), 8):
        octet = binaire[i : i + 8]  # récupération d'un octet
        entier = int(octet, 2)  # conversion en base 10
        code += chr(entier)  # ajout du caractère correspondant
    return code


entier_1 = 641958435660384650016559002527245425123770246953
print(f"{entier_en_code(entier_1) = }")

nombre_2 = 641958435660384650016559002527254432323024987945
print(f"{entier_en_code(nombre_2) = }")
