import math

def aire_disque(rayon) :
    """
    Fonction calculant l'aire d'un disque
    
    rayon est un entier ou un flottant positif ou nul
        
    Renvoie l'aire du disque de rayon donné float
    """
    # Préconditions
    if type(rayon) is not int and type(rayon) is not float:
        raise TypeError("rayon doit être de type int ou float")
    if rayon < 0:
        raise ValueError("rayon doit être positif ou nul")
    
    return math.pi * rayon * rayon

assert abs(aire_disque(2) - math.pi * 4) < 1e-9 