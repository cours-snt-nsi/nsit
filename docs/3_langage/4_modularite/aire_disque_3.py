import math

def aire_disque(rayon) :
    """
    Fonction calculant l'aire d'un disque
    
    rayon est un entier ou un flottant positif ou nul
        
    Renvoie l'aire du disque de rayon donné float
    """
    # Préconditions
    if type(rayon) is not int and type(rayon) is not float:
        raise TypeError("rayon doit être de type int ou float")
    if rayon < 0:
        raise ValueError("rayon doit être positif ou nul")
    
    return math.pi * rayon * rayon

if __name__ == "__main__":
    # Valeur classique
    assert abs(aire_disque(2) - math.pi * 4) < 1e-9 
    # Valeur nulle
    assert aire_disque(0) == 0.0
    # Erreur de type
    try:
        aire_disque("2")
    except TypeError:  # Si on récupère une TypeError...
        pass  # ... on passe car c'est ce que l'on attend
    except Exception:  # Si on récupère une autre erreur...
        raise Exception("Erreur de type non interceptée")  # ... on interrompt le programme
    # Valeur négative
    try:
        aire_disque(-2)
    except ValueError:  # Si on récupère une ValueError...
        pass  # ... on passe car c'est ce que l'on attend
    except Exception:  # Si on récupère une autre erreur...
        raise Exception("Erreur de valeur non interceptée")  # ... on interrompt le programme