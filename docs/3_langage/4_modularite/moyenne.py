def moyenne(tableau, colonne) :
    """
    Fonction calculant la moyenne de la colonne indiquée du tableau fourni en argument
    
    tableau est une liste de listes. Chaque ligne contient le même nombre de colonnes
    colonne est l'indice de la colonne dont on veut calculer la moyenne
    
    Renvoie la moyenne de la colonne concernée au format float
    """
    # Préconditions
    assert type(tableau) is list, "tableau doit être une liste"
    assert type(colonne) is int, "colonne doit être un entier"
    assert 0 <= colonne < len(tableau[0]), "colonne doit être un indice valide (entre 0 et len(tableau[0]) - 1)"

    somme = 0
    for ligne in tableau :
        somme += ligne[colonne]
    return somme / len(tableau)

tableau = [
    [0, 1],
    [1, 2],
    [2, 3]
]

assert moyenne(tableau, 0) == 1.0
assert moyenne(tableau, 1) == 2.0