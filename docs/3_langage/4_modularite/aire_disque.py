import math

def aire_disque(rayon) :
    """
    Fonction calculant l'aire d'un disque
    
    rayon est un entier ou un flottant positif ou nul
        
    Renvoie l'aire du disque de rayon donné float
    """
    # Préconditions
    assert type(rayon) is int or type(rayon) is float, "rayon doit être de type int ou float"
    assert rayon >= 0, "rayon doit être positif ou nul"
    
    return math.pi * rayon * rayon

assert abs(aire_disque(2) - math.pi * 4) < 1e-9 