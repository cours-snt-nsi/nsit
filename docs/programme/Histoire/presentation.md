---
author: Steeve PYTEL
title: 🏰 Histoire
---

!!! abstract "Présentation"

    Cette rubrique transversale se décline dans chacune des sept autres.  
    Comme toute connaissance scientifique et technique, les concepts de l’informatique ont une histoire et ont été forgés par des personnes.  
    Les algorithmes sont présents dès l’Antiquité, les machines à calculer apparaissent progressivement au XVIIe siècle, les sciences de l’information sont fondées au XIXe siècle, mais c’est en 1936 qu’apparaît le concept de machine universelle, capable d’exécuter tous les algorithmes, et que  es notions de machine, algorithme, langage et information sont pensées comme un tout cohérent.  
    Les premiers ordinateurs ont été construits en 1948 et leur puissance a ensuite évolué exponentiellement. Parallèlement, les ordinateurs se sont diversifiés dans leur taille, leur forme et leur emploi : téléphones, tablettes, montres connectées, ordinateurs personnels, serveurs, fermes de calcul, méga-ordinateurs. Le réseau  internet, développé depuis 1969, relie aujourd’hui ordinateurs et objets connectés.

|Contenus|Capacités attendues|
|:--- | :--- |
|Événements clés de l’histoire de l’informatique|Situer dans le temps les principaux événements de l’histoire de l’informatique et leurs protagonistes. Identifier l’évolution des rôles relatifs des logiciels et des matériels.|
