---
title: 🙋 Présentation
hide:
    -toc
---

# Enseigner la NSI pour préparer l'avenir 

!!! abstract "Présentation"

    La spécialité Numérique et Sciences Informatiques (NSI) en classe de terminale générale a pour objectif d'approfondir les concepts fondamentaux de l'informatique et du numérique introduits en première. Elle permet aux élèves d'acquérir des compétences approfondies dans les domaines clés des sciences informatiques, les préparant ainsi à de nombreuses poursuites d'études supérieures. 
    
!!! abstract "Programme de la spécialité NSI en terminale"

    Le programme de NSI en terminale se compose des thèmes suivants :

    1. [Thème 1 - Histoire de l'informatique](./Histoire/presentation.md)
    2. [Thème 2 - Structures de données](./Donnees/structures.md)
    3. [Thème 3 - Bases de données](./Donnees/bases_de_donnees.md)
    4. [Thème 4 - Architectures matérielles, systèmes d'exploitation et réseaux](./Materiel/presentation.md)
    5. [Thème 5 - Langages et programmation](./Programmation/presentation.md)
    6. [Thème 6 - Algorithmique](./Algorithmique/presentation.md)

??? note "Programme de la spécialité NSI en terminale"

	<div class="centre">
	<iframe 
	src="./documents/BO.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>


!!! abstract "Objectifs de la spécialité NSI en terminale"

    La spécialité NSI en terminale vise principalement à permettre aux élèves de :

    * Approfondir leur compréhension de l'évolution historique et des concepts fondamentaux de l'informatique
    * Maîtriser les principaux types de structures de données et leur utilisation en programmation
    * Concevoir et exploiter des bases de données relationnelles
    * Analyser les architectures matérielles, les systèmes d'exploitation et les réseaux informatiques
    * Développer des compétences avancées en programmation et en résolution de problèmes complexes
    * Appréhender les algorithmes fondamentaux et leurs applications dans divers domaines


!!! tip "Environ 30% du temps de la spécialité est consacré à des activités de programmation et de manipulation de systèmes informatiques en mode projet."

!!! question "Poursuites d'études après la spécialité NSI"

    Les élèves ayant suivi la spécialité NSI en terminale pourront envisager de poursuivre :

    * Bac Général avec la spécialité Numérique et Sciences Informatiques (NSI)
    * Classes préparatoires scientifiques (MPSI, PCSI, etc.)
    * BTS, DUT et licences dans les secteurs de l'informatique, du multimédia, des télécommunications
    * Écoles d'ingénieurs
    * Licences et masters dans les domaines des sciences informatiques, de l'intelligence artificielle, de la cybersécurité, etc.

!!! success "La spécialité NSI en terminale offre une formation approfondie aux sciences informatiques, ouvrant la voie à de nombreuses poursuites d'études supérieures dans ces secteurs en forte croissance."
