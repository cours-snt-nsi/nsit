# --- PYODIDE:env --- #
def echange(n):
    """
    Echange les 4 bits de poids forts avec les 4 de poids faibles
    n est un entier compris entre 0 et 255
    """
    assert 0<= n <= 255
    
    fort = n // (2**4)
    faible = n % (2**4)
    
    return faible * (2**4) + fort
# --- PYODIDE:code --- #
def chiffrement_echange(message):
    """
    Chiffre le message en échangeant les 4 bits de poids forts
    avec les 4 de poids faibles sur chaque caractère
    """
    resultat = ""
    for c in message:
        n = ord(c)
        m = echange(n)
        resultat += chr(m)
    return resultat

message = "Bonjour le monde !"
print(chiffrement_echange(message))

assert chiffrement_echange(chiffrement_echange(message)) == message