def echange(n):
    """
    Echange les 4 bits de poids forts avec les 4 de poids faibles
    n est un entier compris entre 0 et 255
    """
    assert 0<= n <= 255
    
    fort = n // (2**4)
    faible = n % (2**4)
    
    return faible * (2**4) + fort


n = int("11110000", 2)
m = int("00001111", 2)
assert echange(n) == m

n = int("00010000", 2)
m = int("00000001", 2)
assert echange(n) == m