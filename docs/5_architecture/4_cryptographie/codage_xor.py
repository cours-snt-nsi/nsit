def chiffrement_xor(message, cle):
    """
    Chiffre le message en faisant caactère XOR clé
    pour chaque carcatère du message
    La clé ets un entier entre 0 et 255
    """
    assert 0 <= cle <= 255
    resultat = ""
    for c in message:
        n = ord(c)
        resultat += chr(n ^ cle)
    return resultat


cle = int("10101010", 2)
message = "Bonjour le monde !"
print(chiffrement_xor(message, cle))

assert chiffrement_xor(chiffrement_xor(message, cle), cle) == message
