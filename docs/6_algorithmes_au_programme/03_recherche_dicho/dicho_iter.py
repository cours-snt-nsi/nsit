# saisir votre fonction ici

assert dichotomie_iter([5], 5) == 0
assert dichotomie_iter([5, 6, 7], 5) == 0
assert dichotomie_iter([5, 6, 7], 6) == 1
assert dichotomie_iter([5, 6, 7], 7) == 2
assert dichotomie_iter([5, 6, 7, 10, 100], 10) == 3
