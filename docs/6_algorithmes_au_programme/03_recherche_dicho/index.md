---
title : 🚤 Recherche dichotomique
hide:
    -toc
---


## Recherche dichotomique

On se donne un tableau de taille `n` **trié** et un valeur `cible`. Cette valeur est-elle dans le tableau ? Si oui on renvoie son indice, sinon on provoque une erreur.

Dans cette version, on utilise un méthode dichotomique :

* Tant que la zone de recherche a une longueur strictement positive :
    * on compare la valeur à l'élément du milieu de la zone de recherche :
        * s'il est supérieur, on cherche dans la zone de gauche,
        * s'il est inférieur on cherche dans la zone de droite,
        * s'il est égal à la valeur on renvoie son indice,

* On provoque une erreur.

On tire profit du fait que le tableau est déjà trié. Comme on partage la zone de recherche en $2$ à chaque étape, l'algorithme fera autant de comparaison qu'il est possible de couper la taille du tableau en $2$ jusqu'à obtenir $1$. La complexité esr donc **logarithmique** $\mathcal{O}(\log_2{n})$ ($n$ est la taille du tableau).

On propose ci-dessous deux versions de cette recherche dichotomique : itérative ou récursive.

!!! info "Version itérative"

    ```
    Fonction dichotomie_iter(tab : tableau, cible : valeur) -> Entier :
        # La zone de recherche
        debut = 0
        fin = longueur(tab) -1

        # Cas général, tant que la zone de recherche comprend plus de 2 valeurs
        Tant que debut < fin :
            # le milieu
            milieu = (debut + fin) // 2 # quotient entier
            Si cible < tab[milieu] :
                fin = milieu - 1
            Sinon si tab[milieu] < cible :
                debut = milieu + 1
            Sinon :
                Renvoyer milieu

        Provoquer une erreur
    ```

    ??? example "Éditeur"
    
        {{ IDE('dicho_iter')}}
        

!!! info "Version récursive"

    ```
    Fonction dichotomie_rec(tab : tableau, debut : entier, fin : entier, cible : valeur) -> Entier :
        # l'élément d'indice debut est inclus
        # l'élément d'indice fin est exclu
        
        # Cas de base : la zone de recherche ne contient qu'un seul élément
        Si fin - debut < 1 :
            Provoquer une erreur

        # Cas général
        # le milieu
        milieu = (debut + fin) // 2 # quotient entier
        Si cible < tab[milieu] :
            Renvoyer Dichotomie_re(tab, debut, milieu, cible)
        Sinon si tab[milieu] < cible :
            Renvoyer Dichotomie_re(tab, milieu + 1, fin, cible)
        Sinon :
            Renvoyer milieu
    ```
    
    ??? example "Éditeur"
    
        {{ IDE('dicho_rec')}}
        
