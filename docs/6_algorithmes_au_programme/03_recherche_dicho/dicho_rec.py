# saisir votre fonction ici

assert dichotomie_rec([5], 0, 1, 5) == 0
assert dichotomie_rec([5, 6, 7], 0, 3, 5) == 0
assert dichotomie_rec([5, 6, 7], 0, 3, 6) == 1
assert dichotomie_rec([5, 6, 7], 0, 3, 7) == 2
assert dichotomie_rec([5, 6, 7, 10, 100], 0, 5, 10) == 3
