"""
On représente les arbres binaires par :
 * None si l'arbre est vide
 * le tuple (sous-arbre gauche, valeur, sous-arbre droit)
 
  1
 /\
2  3

est représenté par

((None, 2, None), 1, (None, 3, None))
"""

# saisir votre fonction ici

ab = None
assert hauteur(ab) == 0

ab = ((None, 2, None), 1, (None, 3, None))
assert hauteur(ab) == 2

ab = ((None, 2, None), 1, (None, 3, (None, 4, None)))
assert hauteur(ab) == 3
