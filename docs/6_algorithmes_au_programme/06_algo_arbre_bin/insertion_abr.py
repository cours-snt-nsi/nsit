"""
On représente les arbres binaires par :
 * None si l'arbre est vide
 * la liste [sous-arbre gauche, valeur, sous-arbre droit]
 
  1
 /\
2  3

est représenté par

[[None, 2, None], 1, [None, 3, None]]
"""

# saisir vos fonctions ici

abr = [None, 5, None]

insertion_abr(abr, 2)
assert abr == [[None, 2, None], 5, None]

insertion_abr(abr, 8)
assert abr == [[None, 2, None], 5, [None, 8, None]]