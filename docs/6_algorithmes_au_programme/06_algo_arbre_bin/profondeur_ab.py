"""
On représente les arbres binaires par :
 * None si l'arbre est vide
 * le tuple (sous-arbre gauche, valeur, sous-arbre droit)
 
  1
 /\
2  3

est représenté par

((None, 2, None), 1, (None, 3, None))
"""

# saisir vos fonctions ici

ab = None
assert prefixe(ab) == []
assert infixe(ab) == []
assert suffixe(ab) == []

ab = ((None, 2, None), 1, (None, 3, None))
assert prefixe(ab) == [1, 2, 3]
assert infixe(ab) == [2, 1, 3]
assert suffixe(ab) == [2, 3, 1]

ab = ((None, 2, None), 1, (None, 3, (None, 4, None)))
assert prefixe(ab) == [1, 2, 3, 4]
assert infixe(ab) == [2, 1, 3, 4]
assert suffixe(ab) == [2, 4, 3, 1]
