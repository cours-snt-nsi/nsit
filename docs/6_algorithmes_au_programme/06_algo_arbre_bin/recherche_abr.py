"""
On représente les arbres binaires par :
 * None si l'arbre est vide
 * la liste [sous-arbre gauche, valeur, sous-arbre droit]
 
  1
 /\
2  3

est représenté par

[[None, 2, None], 1, [None, 3, None]]
"""

# saisir vos fonctions ici

abr = [[None, 2, None], 5, [[None, 6, None], 8, None]]
assert recherche_abr(abr, 5) is True
assert recherche_abr(abr, 2) is True
assert recherche_abr(abr, 6) is True
assert recherche_abr(abr, 8) is True
assert recherche_abr(abr, 9) is False