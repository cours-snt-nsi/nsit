---
title : 🚂 Algorithmes sur les arbres binaires
hide:
    -toc
---


## Algorithmes sur les arbres binaires

!!! note "Cours détaillé"
    
    Un cours détaillé sur les arbres binaires est disponible [sur cette page](../../2_tad/6_arbres_binaires/index.md).

!!! note "Arbres enracinés ?"

    On présente ci-dessous les algorithmes sur les *arbres binaires*.
    
    Les algorithmes analogues sur les [*arbres enracinés*](../../2_tad/5_arbres/index.md) (possédant un nombre quelconque d'enfants) sont similaires à deux nuances près :
    
    * le cas de base des appels récursifs est le cas où l'arbre est une *feuille* ;

    * on parcourt l'ensemble des enfants et pas seulement les sous-arbres gauche et droit.

Un arbre binaire est soit :

* un arbre vide ;

* un arbre qui possède exactement deux enfants (appelés sous-arbre gauche et sous-arbre droit), qui sont eux-mêmes des arbres (potentiellement vides).

!!! info "Hauteur d'un arbre binaire"

    On peut calculer la hauteur d'un arbre en utilisant une fonction récursive.

    ```
    Fonction hauteur(arbre : arbre binaire) -> entier :
        Si arbre est vide :
            Retourner 0
        Sinon :
            Retourner 1 + max(hauteur(sous-arbre gauche, hauteur(sous-arbre droit))
    ```

    ??? example "Éditeur"

        {{ IDE('hauteur_ab')}}

!!! info "Taille d'un arbre binaire"

    On rappelle que la taille d'un arbre est égale au nombre de nœuds non-vides qu'il contient. Là encore il est possible de déterminer la taille à l'aide d'une fonction récursive.

    ```
    Fonction taille(arbre : arbre binaire) -> entier :
        Si arbre est vide :
            Retourner 0
        Sinon :
            Retourner 1 + taille(sous-arbre gauche) + taille(sous-arbre droit)
    ```

    ??? example "Éditeur"

        {{ IDE('taille_ab')}}

!!! info "Parcours en largeur"

    <center>
    
    ```mermaid
    flowchart TD
        A(11)
        B(8)
        C(14)
        D(5)
        E(10)
        F(13)
        G(15)
        
        A --- B
        A --- C
        B --- D
        B --- E
        C --- F
        C --- G
    ```
    </center>

    Sur la figure, le parcours en largeur donnera l'ordre suivant (on convient que les enfants de gauche sont visités avant ceux de droite) :

    <center>11 → 8 → 14 → 5 → 10 → 13 → 15</center>

    On peut implémenter un parcours en largeur à l'aide d'une *file* :

    ```
    Fonction largeur(arbre : arbre binaire) -> Tableau de valeurs :
        f est une file vide
        Enfiler arbre dans f
        résultat est un tableau vide
        
        # Parcours proprement dit
        Tant que f est non vide
            Défiler un arbre a de f
            Si a est non vide :
                Ajouter a à la fin de résultat
                Enfiler le sous-arbre gauche de a dans f
                Enfiler le sous-arbre droit de a dans f
    ```

    ??? example "Éditeur"

        {{ IDE('largeur_ab')}}
        
!!! info "Parcours en profondeur"

    <center>
    
    ```mermaid
    flowchart TD
        A(11)
        B(8)
        C(14)
        D(5)
        E(10)
        F(13)
        G(15)
        
        A --- B
        A --- C
        B --- D
        B --- E
        C --- F
        C --- G
    ```
    </center>

    On distingue trois parcours en profondeur des arbres binaires :
    
    * les parcours *préfixe* dans lesquels le nœud est traité avant ses enfants : 11 → 8 → 5 → 10 → 14 → 13 → 15;

    * les parcours *infixe* dans lesquels le nœud est traité après l'enfant gauche mais avant le droit : 5 → 8 → 10 → 11 → 13 → 14 → 15;

    * et les parcours *postfixe* dans lequel le nœud est traité après ses enfants: 5 → 10 → 8 → 13 → 15 → 14 → 11.

    On utilise dans chacun des cas une fonction récursive. La différence entre les trois parcours se traduit dans le code dans l'ordre de traitement des enfants et du nœud en cours.

    ```
    Fonction préfixe(arbre : arbre binaire) -> Tableau de valeurs :
        Si arbre est vide:
            Renvoyer le tableau vide
        
        résultat est un tableau vide
        Ajouter la valeur de arbre à la fin de résultat
        Ajouter préfixe(sous-arbre gauche) à la fin de résultat
        Ajouter préfixe(sous-arbre droit) à la fin de résultat
        
        Renvoyer résultat
    
    Fonction infixe(arbre : arbre binaire) -> Tableau de valeurs :
        Si arbre est vide:
            Renvoyer le tableau vide
        
        résultat est un tableau vide
        Ajouter infixe(sous-arbre gauche) à la fin de résultat
        Ajouter la valeur de arbre à la fin de résultat
        Ajouter infixe(sous-arbre droit) à la fin de résultat
        
        Renvoyer résultat
    
    Fonction suffixe(arbre : arbre binaire) -> Tableau de valeurs :
        Si arbre est vide:
            Renvoyer le tableau vide
        
        résultat est un tableau vide
        Ajouter suffixe(sous-arbre gauche) à la fin de résultat
        Ajouter suffixe(sous-arbre droit) à la fin de résultat
        Ajouter la valeur de arbre à la fin de résultat
        
        Renvoyer résultat
    ```

    ??? example "Éditeur"

        {{ IDE('profondeur_ab')}}


!!! info "Insertion dans un arbre binaire de recherche"

    Afin d'insérer une valeur `x` dans un ABR non vide, on procède récursivement jusqu'à atteindre un nœud vide bien positionné.
    
    ```
    Fonction insertion_abr(arbre : arbre binaire de recherche, x : valeur) -> Rien:
        Si x est strictement inférieur à l'étiquette de la racine :
            Si le sous-arbre gauche est vide :
                Le sous-arbre gauche devient un ABR dont la racine a pour étiquette x
            Sinon :
                insertion_abr(sous-arbre gauche, x)
        Sinon si x est strictement supérieur à l'étiquette de la racine :
            Si le sous-arbre droit est vide :
                Le sous-arbre droit devient un ABR dont la racine a pour étiquette x
            Sinon :
                insertion_abr(sous-arbre droit, x)
    ```
    
    ??? example "Éditeur"

        {{ IDE('insertion_abr')}}

!!! info "Recherche dans un arbre binaire de recherche"

    Afin de rechercher une valeur `x` dans un ABR, on procède récursivement jusqu'à atteindre un nœud de valeur égale ou un arbre vide.
    
    ```
    Fonction recherche_abr(arbre : arbre binaire de recherche, x : valeur) -> booléen:
        Si arbre est vide :
            Renvoyer Faux
            
        Si x est strictement inférieur à l'étiquette de la racine :
            Renvoyer recherche_abr(sous-arbre gauche, x)
        Sinon si x est strictement supérieur à l'étiquette de la racine :
            Renvoyer recherche_abr(sous-arbre droit, x)
        Sinon :
            Renvoyer Vrai
    ```
    
    ??? example "Éditeur"

        {{ IDE('recherche_abr')}}