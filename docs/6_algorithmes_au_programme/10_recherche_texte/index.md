---
title : 🚚 Recherche textuelle
hide:
    -toc
---


## Recherche textuelle

!!! info "Algorithme de Boyer-Moore"

    !!! note "Cours détaillé"
    
        Un cours détaillé sur cet algorithme est disponible [sur cette page](../../4_algorithmique/3_recherche_textuelle/index.md).

    Cet algorithme a été développé par Robert S. Boyer et J. Strother Moore en 1977.

    Le point de départ est plutôt contre-intuitif : pour chercher un motif dans un texte, on commence par la dernière lettre du motif ! En effet, si la lettre du texte correspondante n'est pas la bonne, il est inutile de tester les autres lettres. Cela ne change pas beaucoup par rapport à l'approche naïve.

    La vraie différence se situe dans l'action qui suit la comparaison des caractères :

    * Si la lettre observée ne fait même pas partie du motif, on peut alors décaler la fenêtre de lecture directement de la longueur du motif. On évite ainsi de faire autant de tests que le motif est long.

    * Que se passe-t-il lorsque la lettre du texte correspond à celle du motif ? On étudie alors les lettres précédentes jusqu'à avoir soit :
        * fini le motif (et donc trouvé une correspondance parfaite),
        * soit trouvé un mauvais caractère.

    * Dernier cas de figure : le caractère lu n'est pas correct mais il apparaît toutefois à une autre position dans le motif. Dans ce cas on doit décaler la fenêtre de lecture afin de faire correspondre ce caractère et celui du motif situé le plus à droite (la dernière occurrence de la lettre).

    * Il peut toutefois arriver que la dernière occurrence du caractère cherché soit située plus loin dans le motif : le saut ferait alors reculer la fenêtre de lecture ce qui est inefficace. Dans ce cas, on se contente de faire un saut de $1$ caractère. C'est ce qui se passe entre les étapes 2 et 3.

    L'algorithme de Boyer-Moore nécessite donc avant d'effectuer les comparaisons de savoir quels sont les décalages à effectuer. Pour ce faire il doit construire un dictionnaire `décalages` reprenant l'indice dans le motif de la dernière occurrence de chacun de ses caractères.

    La fonction suivante crée ce dictionnaire :

    ```
    Fonction creation_décalage(motif : chaîne de caractères) -> dictionnaire[caractère, entier] :
        dico est un dictionnaire vide

        Pour chaque entier i entre 0 et la longueur du motif :
            dico[motif[i]] prend la valeur i            

        Renvoyer décalage
    ```

    On crée un dictionnaire dont les entrées sont les lettres du motif. On parcourt de la gauche vers la droite en ajoutant ou mettant à jour le décalage associé à chaque caractère.

    L'algorithme de Boyer-Moore peut alors être donné :

    ```
    Fonction recherche_BMH(texte : chaîne de caractères, motif : chaîne de caractères) -> liste :
        N est la longueur du texte
        P est la longueur du motif

        Si P > N :
            Renvoyer une erreur  # le motif est trop long

        décalages prend la valeur creation_décalage(motif)

        i prend la valeur 0
        Tant que i est strictement inférieur à N - P + 1 :
            j prend la valeur P - 1 :
            Tant que j est positif et que motif[j] est égal à texte[i + j] :
                Décrémenter j
            Si j vaut -1 :
                Renvoyer i  # Succès
            Si texte[i + j] est dans décalages :
                Augmenter i du maximum entre 1 et P - 1 - décalages[motif[j]]
            Sinon :
                Augmenter i de P

        Renvoyer une erreur  # le motif n'est pas présent
    ```
    
    ??? example "Éditeur"
    
        {{ IDE('boyer')}}