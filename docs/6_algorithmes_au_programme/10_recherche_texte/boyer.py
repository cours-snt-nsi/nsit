# Saisir vos fonctions ici

motif = "les fleurs"
assert calcule_decalages(motif) == {
    "l": 5,
    "e": 6,
    "s": 9,
    " ": 3,
    "f": 4,
    "u": 7,
    "r": 8,
}

# Résultat positif
texte = "lundi je regarderai la lune avec la lunette."
motif = "lunette"
assert recherche_BMH(texte, motif) == 36

# Motif trop long
try:
    recherche_BMH(motif, texte)
except ValueError as erreur:
    print(erreur)

# Texte absent
texte = "lundi je regarderai la lune avec la lunette."
motif = "mer"
try:
    recherche_BMH(texte, motif)
except ValueError as erreur:
    print(erreur)
