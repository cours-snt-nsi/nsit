---
title : 🎠 Algorithmes au programme de NSI
hide:
    -toc
---


Ce document regroupe différents algorithmes au programme de NSI. Il n'a pas la prétention d'être exhaustif.

On considère dans tout ce document qu'un tableau de longueur `n` a des indices allant de `0` à `n-1`.

Les classes de complexités citées concernent toutes la complexité en temps. On rappelle à ce titre quelques exemples de classes de complexités ainsi qu'une illustration de leurs "vitesses" relatives.

<center>

|          Temps          |          Type de complexité          | Temps pour $n = 5$ | Temps pour $n = 10$ | Temps pour $n = 10 000$ |               Exemple de problème                |
| :---------------------: | :----------------------------------: | :----------------: | :-----------------: | :---------------------: | :----------------------------------------------: |
|    $\mathcal{O}(1)$     |         complexité constante         |       10 ns        |        10 ns        |          10 ns          |          Accès à une cellule de tableau          |
| $\mathcal{O}(\log{n})$  |       complexité logarithmique       |       10 ns        |        10 ns        |          40 ns          |              Recherche dichotomique              |
| $\mathcal{O}(\sqrt{n})$ |         complexité racinaire         |       22 ns        |        32 ns        |          1 µs           |              Test de primalité naïf              |
|    $\mathcal{O}(n)$     |         complexité linéaire          |       50 ns        |       100 ns        |         100 µs          |                Parcours de liste                 |
| $\mathcal{O}(n\log{n})$ |      complexité linéarithmique       |       40 ns        |       100 ns        |         400 µs          |                    Tri fusion                    |
|   $\mathcal{O}(n^2)$    | complexité quadratique (polynomiale) |       250 ns       |        1 µs         |           1 s           |         Tris par insertion et sélection          |
|   $\mathcal{O}(n^3)$    |   complexité cubique (polynomiale)   |      1.25 µs       |        10 µs        |       2.7 heures        |         Multiplication matricielle naïve         |
|   $\mathcal{O}(2^n)$    |       complexité exponentielle       |       320 ns       |        10 µs        |           ...           |      Problème du sac à dos par force brute       |
|    $\mathcal{O}(n!)$    |        complexité factorielle        |       1.2 µs       |        36 ms        |           ...           | Problème du voyageur de commerce par force brute |

</center>

!["Vitesses" des différentes classes de complexité](complexite.jpg){width=60% .center .autolight}

On propose ci-dessous des algorithmes en langage naturel. Sous chacun d'eux un éditeur Python permet de transcrire cet algorithme.

