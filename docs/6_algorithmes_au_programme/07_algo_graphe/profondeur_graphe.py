"""
On représente les graphes sous forme de listes d'adjacences
 
A --- B -- D
 \    \   /
  \    \ /
   C -- E

est représenté par

graphe = {
    "A": ["B", "C"],
    "B": ["A", "D", "E"],
    "C": ["A", "E"],
    "D": ["B", "E"],
    "E": ["B", "C", "D"],
}
"""

# Les files sont déjà importées

# saisir votre fonction ici

graphe = {
    "A": ["B", "C"],
    "B": ["A", "D", "E"],
    "C": ["A", "E"],
    "D": ["B", "E"],
    "E": ["B", "C", "D"],
}
assert profondeur(graphe, "A") == ["A", "B", "D", "C", "E"]
assert profondeur(graphe, "B") == ["B", "A", "C", "D", "E"]
assert profondeur(graphe, "C") == ["C", "A", "B", "D", "E"]
