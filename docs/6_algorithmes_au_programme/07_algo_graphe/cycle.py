# saisir votre fonction ici


graphe_cyclique = {
    "A": ["B", "C", "D"],
    "B": ["A", "D", "E"],
    "C": ["A", "D"],
    "D": ["A", "B", "C", "E"],
    "E": ["B", "D"],
}

assert est_cyclique(graphe_cyclique) is True

graphe_acyclique = {
    "A": ["B", "C", "D"],
    "B": ["A", "E", "F"],
    "C": ["A", "G"],
    "D": ["A"],
    "E": ["B"],
    "F": ["B"],
    "G": ["C", "H"],
    "H": ["G"],
}

assert est_cyclique(graphe_acyclique) is False