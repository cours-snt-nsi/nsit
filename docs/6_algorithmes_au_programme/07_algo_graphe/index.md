---
title : 🚲 Algorithmes sur les graphes
hide:
    -toc
---


## Algorithmes sur les graphes

!!! note "Cours détaillé"

    Un cours détaillé sur les graphes est disponible [sur cette page](../../2_tad/4_graphes/index.md).

!!! info "Parcours en largeur"

    !!! note "Plus court chemin"
    
        Dans le cas d'un graphe non pondéré, le parcours en largeur permet de déterminer le plus court chemin entre deux sommets.
        
        Si le graphe est pondéré, un approche possible est de lister l'ensemble des chemins et de comparer leurs poids.

    On retrouve un algorithme proche de celui rencontré avec les arbres à cette différence qu'il faut ici garder trace des sommets déjà rencontrés afin de ne pas « tomber » dans un cycle.

    <center>

    ```mermaid
    flowchart LR
    A(A) --- B(B)
    A --- D(D)
    A --- C(C)
    B --- E(E)
    C --- D
    B --- D
    D --- E
    ```
    </center>

    Le parcours en largeur issu de A visite dans l'ordre : A → B → C → D → E.

    ```
    Fonction largeur(graphe : graphe, depart : sommet) -> Tableau de valeurs:

        f est une File vide
        Enfiler depart dans f
        
        visité est un dictionnaire vide
        Pour chaque sommet de graphe:
            visité[sommet] = Faux
    
        résultat est un tableau vide

        Tant que f est non vide :
            Défiler le sommet actuel de f
            Ajouter actuel à la fin de résultat
            visité[actuel] = Vrai
            Pour chaque voisin de actuel :
                Si visité[voisin] est Faux :
                    Enfiler voisin dans f
    ```
    
    ??? example "Éditeur"
    
        {{ IDE('largeur_graphe')}}
        
!!! info "Parcours en profondeur"

    <center>

    ```mermaid
    flowchart LR
    A(A) --- B(B)
    A --- D(D)
    A --- C(C)
    B --- E(E)
    C --- D
    B --- D
    D --- E
    ```
    </center>

    Le parcours en profondeur issu de A visite dans l'ordre : A → B → D → C → E.

    Pour le parcours en profondeur, on peut utiliser une fonction récursive ou une approche itérative à l'aide d'une *pile*. On propose ici la version récursive. Le dictionnaire des sommets `visités` est passé en paramètre.

    ```
    Fonction profondeur(graphe : graphe, depart : sommet, visités: dictionnaire) -> Tableau de valeurs :
        résultat est un tableau vide
        Ajouter départ à la fin de résultat
        visités[départ] = Vrai
        Pour chaque voisin de actuel :
            Si visité[voisin] est Faux :
                Ajouter profondeur(graphe, voisin, visités) à la fin de résultat
        Renvoyer résultat
    ```
    
    ??? example "Éditeur"
    
        {{ IDE('profondeur_graphe')}}

!!! info "Présence de cycles"

    On peut facilement repérer l'existence de cycles dans un graphe en utilisant un parcours en profondeur : si l'on rencontre un sommet déjà visité (autre que le sommet d'origine du déplacement ayant mené au sommet en cours d'étude), c'est qu'il existe un cycle.
    
    On propose un parcours en profondeur à l'aide d'une pile. A chaque étape, on empile le couple `(sommet à visiter, sommet d'origine)`.

    ```
    Fonction est_cyclique(graphe : graphe) -> booléen :
        Pour chaque sommet départ de graphe:
            p est une Pile vide
            visités est un dictionnaire vide
            Pour chaque sommet de graphe :
                visités[sommet] = Faux
            Empiler le couple (départ, None) dans p

            Tant que p est non vide :
                Dépiler le couple (actuel, origine) de p
                visités[actuel] = Varai
                Pour chaque voisin de actuel :
                    Si voisin est différent de origine:
                        Si visités[voisin]:
                            Renvoyer Vrai
                        Sinon :
                            Empiler le couple (voisin, actuel) dans p
            
            Renvoyer Faux
    ```
    
        
    ??? example "Éditeur"
    
        {{ IDE('cycle')}}