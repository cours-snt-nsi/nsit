---
title : 🚢 Recherche dans un tableau
hide:
    -toc
---


## Recherche dans un tableau

!!! info "Recherche du minimum/maximum"

    On se donne un tableau `tab`. Quel est l'indice de l'élément minimal ? Et le maximal ? Il faut lire l'ensemble des valeurs pour le déterminer : la complexité est linéaire $\mathcal{O}(n)$ ($n$ est la taille du tableau).

    ```
    Fonction minimum(tab : tableau) -> Entier :
        # Initialisation
        indice_mini = 0

        # Parcours des valeurs restantes à partir de la 1-ième
        Pour i allant de 1 à longueur(tab) - 1 (inclus) :
                Si tab[i] < tab[indice_mini] :
                    indice_mini = i

        Renvoyer indice_mini
    ```
    
    ??? example "Éditeur"
    
        {{ IDE('minimum')}}

!!! info "Recherche linéaire d'un élément"

    On se donne un tableau de taille `n` et un valeur `cible`. Cette valeur est-elle dans le tableau ? Si oui on renvoie son indice, sinon on provoque une erreur.

    Dans cette version, on effectue un parcours linéaire. Dans le cas où la valeur est située à la fin du tableau, il faut lire toutes les valeurs : la complexité est linéaire $\mathcal{O}(n)$ ($n$ est la taille du tableau).

    On peut adapter cet algorithme afin qu'il renvoie l'indice d'un élément respectant une certaine condition en modifiant le test portant sur `tab[i]`.

    ```
    Fonction recherche_linéaire(tab : tableau, cible : valeur) -> Entier :
        Pour i allant de 1 à longueur(tab) - 1 (inclus) :
            Si tab[i] == cible :
                Renvoyer i

        Provoquer une erreur
    ```

    ??? example "Éditeur"
    
        {{ IDE('lineaire')}}
        
