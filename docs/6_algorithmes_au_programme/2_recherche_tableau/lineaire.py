# saisir votre fonction ici

assert recherche_lineaire([5, 4], 4) == 1
assert recherche_lineaire([3, 5, 4], 4) == 2
assert recherche_lineaire([3, 5, 1, 4, 7], 4) == 3
assert recherche_lineaire([3, 5, 1, -3, 4], 3) == 0
