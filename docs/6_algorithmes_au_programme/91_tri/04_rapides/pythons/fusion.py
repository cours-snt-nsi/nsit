def fusion(gauche, droite):
    """
    Fusionne gauche et droite
    Les tableaux intiaux sont triés
    Le tableau résultat l'est aussi
    """
    taille_gauche = len(gauche)
    taille_droite = len(droite)
    nouveau = [None] * (... + ...)

    i_nouveau = 0
    i_gauche = 0
    i_droite = 0

    # Il reste des éléments à gauche ET à droite
    while i_gauche < ... and ... < taille_droite:
        if gauche[...] <= droite[...]:
            nouveau[i_nouveau] = gauche[...]
            i_gauche += 1
        else:
            nouveau[...] = ...[...]
            ...
        i_nouveau += 1

    # Il ne reste des éléments QUE à gauche
    while i_gauche < ...:
        nouveau[...] = gauche[...]
        i_gauche += 1
        i_nouveau += 1

    # Il ne reste des éléments QUE à droite
    while ...:
        ...
        ...
        ...

    return ...


# Même taille
gauche = [0, 2, 4]
droite = [1, 3, 5]
assert fusion(gauche, droite) == [0, 1, 2, 3, 4, 5], "Erreur avec gauche = [0, 2, 4] et droite = [1, 3, 5]"
# Un tableau plus court
gauche = [0, 2, 4]
droite = [1, 3]
assert fusion(gauche, droite) == [0, 1, 2, 3, 4], "Erreur avec gauche = [0, 2, 4] et droite = [1, 3]"
# Un tableau vide
gauche = []
droite = [1, 3, 5]
assert fusion(gauche, droite) == [1, 3, 5], "Erreur avec gauche = [] et droite = [1, 3, 5]"
# gauche est toujours plus petit
gauche = [0, 1]
droite = [2, 3]
assert fusion(gauche, droite) == [0, 1, 2, 3], "Erreur avec gauche = [0, 1] et droite = [2, 3]"


print("Bravo !")
