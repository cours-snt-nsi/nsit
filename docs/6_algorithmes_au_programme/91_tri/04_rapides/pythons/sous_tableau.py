def sous_tableau(tableau, debut, fin):
    """
    Renvoie une copie des éléments d'indices
    compris entre debut (inclus) et fin (exclu)
    de tableau
    """
    nouveau = [None] * (fin - debut)  # pas indispensable
    ...


tableau = [8, 9, 10, 11, 12, 13]

# Tests
assert sous_tableau(tableau, 0, 2) == [8, 9]
assert sous_tableau(tableau, 2, 6) == [10, 11, 12, 13]
assert sous_tableau(tableau, 0, 6) == [8, 9, 10, 11, 12, 13]
assert sous_tableau(tableau, 0, 0) == []
print("Bravo !")
