def sous_tableau(tableau, debut, fin):
    """
    Renvoie une copie des éléments d'indices
    compris entre debut (inclus) et fin (exclu)
    de tableau
    """
    return [tableau[valeur] for valeur in range(debut, fin)]


def fusion(gauche, droite):
    """
    Fusionne gauche et droite
    Les tableaux initiaux sont triés
    Le tableau résultat l'est aussi
    """
    taille_gauche = len(gauche)
    taille_droite = len(droite)
    nouveau = [None] * (taille_gauche + taille_droite)

    i_nouveau = 0
    i_gauche = 0
    i_droite = 0

    # Il reste des éléments à gauche ET à droite
    while i_gauche < taille_gauche and i_droite < taille_droite:
        if gauche[i_gauche] <= droite[i_droite]:
            nouveau[i_nouveau] = gauche[i_gauche]
            i_gauche += 1
        else:
            nouveau[i_nouveau] = droite[i_droite]
            i_droite += 1
        i_nouveau += 1

    # Il ne reste des éléments QUE à gauche
    while i_gauche < taille_gauche:
        nouveau[i_nouveau] = gauche[i_gauche]
        i_gauche += 1
        i_nouveau += 1

    # Il ne reste des éléments QUE à droite
    while i_droite < taille_droite:
        nouveau[i_nouveau] = droite[i_droite]
        i_droite += 1
        i_nouveau += 1

    return nouveau


def tri_fusion(tableau):
    """
    Trie tableau à l'aide du tri fusion
    Renvoie un nouveau tableau trié
    """
    ...


tableau_0 = [3, 1, 2]
assert tri_fusion(tableau_0) == [1, 2, 3], "Erreur avec [3, 1, 2]"

tableau_1 = [1, 2, 3, 4]
assert tri_fusion(tableau_1) == [1, 2, 3, 4], "Erreur avec [1, 2, 3, 4]"

tableau_2 = [-2, -5]
assert tri_fusion(tableau_2) == [-5, -2], "Erreur avec des valeurs négatives"

tableau_3 = []
assert tri_fusion(tableau_3) == [], "Erreur avec un tableau vide"

tableau_4 = [-7, 6, -8, 2, 9, 5, 10, 0]
assert tri_fusion(tableau_4) == [-8, -7, 0, 2, 5, 6, 9, 10], "Erreur avec [-7, 6, -8, 2, 9, 5, 10, 0]"

print("Bravo !")
