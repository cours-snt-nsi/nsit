def tri_rapide_hoare(tableau, debut, fin):
    """
    Trie tableau entre les indices 
    debut (inclus) et fin (exclu)
    """
    # Cas de base
    if fin - debut < 2:
        return

    pivot = tableau[debut]
    gauche = debut
    droite = fin - 1

    while ...:
        ...

    # Échange
    ...
    # Appels récursifs
    tri_rapide_hoare(tableau, debut, droite)
    tri_rapide_hoare(tableau, droite + 1, fin)


tableau_0 = [3, 1, 2]
tri_rapide_hoare(tableau_0, 0, len(tableau_0))
assert tableau_0 == [1, 2, 3], "Erreur avec [3, 1, 2]"

tableau_1 = [1, 2, 3, 4]
tri_rapide_hoare(tableau_1, 0, len(tableau_1))
assert tableau_1 == [1, 2, 3, 4], "Erreur avec [1, 2, 3, 4]"

tableau_2 = [-2, -5]
tri_rapide_hoare(tableau_2, 0, len(tableau_2))
assert tableau_2 == [-5, -2], "Erreur avec des valeurs négatives"

tableau_3 = []
tri_rapide_hoare(tableau_3, 0, len(tableau_3))
assert tableau_3 == [], "Erreur avec un tableau vide"

print("Bravo !")
