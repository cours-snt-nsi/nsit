def sous_tableau(tableau, debut, fin):
    return [tableau[valeur] for valeur in range(debut, fin)]


def inversions(tableau, debut, fin):
    ...


tableau_1 = [3, 8, 1]
assert inversions(tableau_1, 0, len(tableau_1)) == 2, "Erreur avec tableau_1"

tableau_2 = [30, 10, 23, 18, 23, 27, 20, 15, 21, 26, 24, 30, 15, 19]
assert inversions(tableau_2, 0, len(tableau_2)) == 44, "Erreur avec tableau_2"

print("Bravo !")
