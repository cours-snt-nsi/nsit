def plus_petits(tableau):
    n = len(tableau)
    bilan = [0] * n
    ...


tableau_1 = [3, 8, 1]
assert plus_petits(tableau_1) == [1, 1, 0], "Erreur avec tableau_1"

tableau_2 = [30, 10, 23, 18, 23, 27, 20, 15, 21]
assert plus_petits(tableau_2) == [8, 0, 4, 1, 3, 3, 1, 0, 0], "Erreur avec tableau_2"

print("Bravo !")
