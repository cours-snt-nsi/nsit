fevrier = [
    [-1, 4, 1,  0, -1, -1, 0],
    [-2, 2, 3, -2, -5,  0, 3],
    [0,  4, 4,  5,  1,  1, 2],
    [0,  5, 3, -3, -3, -1, 0]
]

effectifs = [0] * 4
...

assert effectifs == [3, 3, 0, 3], "Erreur, réessayez !"
print("Bravo !")
