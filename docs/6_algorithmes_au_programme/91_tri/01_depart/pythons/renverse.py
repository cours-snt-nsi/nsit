def renverse(tableau):
    ...


def echange(tableau, i, j):
    tableau[i], tableau[j] = tableau[j], tableau[i]


voyelles = ["a", "e", "i", "o", "u", "y"]
renverse(voyelles)
assert voyelles == ["y", "u", "o", "i", "e", "a"], "Les voyelles n'ont pas été correctement inversées"
print("Bravo !")
