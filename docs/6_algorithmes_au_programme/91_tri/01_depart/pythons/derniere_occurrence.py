def derniere_occurrence(tableau, cible):
    ...


nombres = [3, 8, 5, 4, 3, 8]

# Tests
assert derniere_occurrence(nombres, 3) == 4, "Erreur en cherchant 3"
assert derniere_occurrence(nombres, 8) == 5, "Erreur en cherchant 8"
assert derniere_occurrence(nombres, 5) == 2, "Erreur en cherchant 5"
assert derniere_occurrence(nombres, 0) is None, "Erreur en cherchant 0"
print("Bravo !")
