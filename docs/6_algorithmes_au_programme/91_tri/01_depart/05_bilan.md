---
author: Nicolas Revéret
title: Bilan
---

# Bilan sur les tableaux et les listes

!!! abstract "Tableau"

    En informatique un **tableau** :
    
    - est une structure de données linéaire dans laquelle des **éléments** sont repérés par leur **indice**,
  
    - contient des éléments de même type,
  
    - est de taille *fixe*.

!!! abstract "Listes Python"

    En Python on utilise le type `#!py list` pour représenter les tableaux.

    On peut **accéder** à un élément en faisant `#!py tableau[indice]`.
    
    On peut **modifier** un élément en faisant `#!py tableau[indice] = nouvel_element`.

    La **longueur** du tableau est donnée par `#!py len(tableau)`.


!!! abstract "Parcours"

    Python permet de parcourir les tableaux, comme `#!py neveux = ["Riri", "Fifi", "Loulou"]` :

    * selon les indices :

        ```pycon
        >>> for i in range(len(neveux)):
        ...     print("L'élément d'indice", i, "est", neveux[i])
        ...
        L'élément d'indice 0 est Riri
        L'élément d'indice 1 est Fifi
        L'élément d'indice 2 est Loulou
        >>>
        ```
    
    * selon les valeurs :

        ```pycon
        >>> for neveu in neveux:
        ...     print(neveu, "est un élément")
        ...
        Riri est un élément
        Fifi est un élément
        Loulou est un élément
        >>>
        ```

!!! abstract "Échanger des valeurs"

    Pour échanger des valeurs d'un `tableau`, on peut :

    * utiliser une variable tierce :

        ```python
        temporaire = tableau[i]
        tableau[i] = tableau[j]
        tableau[j] = temporaire
        ```
    
    * utiliser l'affectation multiple :
        
        ```python
        tableau[i], tableau[j] = tableau[j], tableau[i]
        ```
