---
author: Nicolas Revéret
title: Parcours de tableaux
---

# Parcours de tableaux

Considérons le tableau des dix langues les plus parlées dans le monde (en nombre de locuteurs dont c'est la langue maternelle - source : [Wikipédia](https://fr.wikipedia.org/wiki/Liste_de_langues_par_nombre_total_de_locuteurs)) :

```python
plus_parlees = [
    "mandarin",
    "espagnol",
    "anglais",
    "hindi",
    "bengali",
    "portugais",
    "russe",
    "japonais",
    "allemand",
    "cantonais",
]
```

Le mandarin est le premier élément du tableau : c'est la langue maternelle la plus importante. La valeur `#!py "mandarin"` est à l'indice `#!py 0` dans le tableau.

Il est très courant de parcourir des tableaux en utilisant une boucle « Pour », avec `#!py for` de Python.

Une première approche consiste à parcourir les indices :

```python
print("Les dix langues maternelles les plus importantes sont :\n")
for indice in range(len(plus_parlees)):
    print("-", plus_parlees[indice])
```

On obtient alors :

```output
Les dix langues maternelles les plus importantes sont :

- mandarin
- espagnol
- anglais
- hindi
- bengali
- portugais
- russe
- japonais
- allemand
- cantonais
```

Il est toutefois possible de parcourir directement les valeurs contenues dans le tableau. Ainsi, le code ci-dessous affichera les mêmes lignes :

```python
print("Les dix langues maternelles les plus importantes sont :\n")
for langue in plus_parlees:
    print("-", langue)
```

Comme on peut le voir, le code est allégé, plus facile à lire, car on n'utilise pas les indices.

Dans certains cas, il est toutefois nécessaire d'utiliser les indices et les valeurs :

```python
print("Les dix langues maternelles les plus importantes sont :\n")
for indice in range(len(plus_parlees)):
    print("-", plus_parlees[indices], "en n°", indice + 1)
```

On obtient alors :

```output
Les dix langues maternelles les plus importantes sont :

- mandarin en n° 1
- espagnol en n° 2
- anglais en n° 3
- hindi en n° 4
- bengali en n° 5
- portugais en n° 6
- russe en n° 7
- japonais en n° 8
- allemand en n° 9
- cantonais en n° 10
```

???+ question

    On considère le tableau `pointures = [38, 43, 44, 43, 37, 42, 39, 43, 40]`. On exécute le code ci-dessous :

    ```python linenums="1"
    for i in range(44):
        print(pointures[i])
    ```

    === "Cocher la ou les affirmations correctes"
        
        - [ ] Ce code affiche tous les indices du tableau
        - [ ] Ce code affiche toutes les pointures du tableau
        - [ ] Ce code affiche les trois premières pointures du tableau
        - [ ] Ce code provoque une erreur

    === "Solution"
        
        - :x: Ce code affiche tous les indices du tableau
        - :white_check_mark: Ce code affiche toutes les pointures du tableau... avant d'afficher une erreur !
        - :x: Ce code affiche les trois premières pointures du tableau
        - :white_check_mark: Ce code provoque une erreur

        En effet, la boucle parcourt les entiers entre `#!py 0` inclus et `#!py 44` exclu. On essaie ensuite dans la ligne 2 d'accéder à l'élément d'indice `#!py i` du tableau. Cela fonctionne pour les premières valeurs de `#!py i`, jusqu'à `#!py i = 8`. Ensuite, l'instruction `#!py pointures[i]`  entraîne une erreur car `#!py i` vaut `#!py 9` et `#!py pointures[9]` n'existe pas.

!!! note "Remarques"

    Avec un `#!py tableau` de longueur `l`, alors
    
    - `#!py tableau[l]` n'existe pas,
    - `#!py tableau[l - 1]` est le dernier élément, (si `l > 0`).

???+ question

    On considère toujours le tableau `pointures = [38, 43, 44, 43, 37, 42, 39, 43, 40]`. On exécute le code ci-dessous :

    ```python linenums="1"
    for i in pointures:
        print(i)
    ```

    === "Cocher la ou les affirmations correctes"
        
        - [ ] Ce code affiche tous les indices du tableau
        - [ ] Ce code affiche toutes les pointures du tableau
        - [ ] Ce code provoque une erreur
        - [ ] La variable `#!py i` est mal nommée

    === "Solution"
        
        - :x: Ce code affiche tous les indices du tableau
        - :white_check_mark: Ce code affiche toutes les pointures du tableau
        - :x: Ce code provoque une erreur
        - :white_check_mark: La variable `#!py i` est mal nommée

        Cette fois-ci on parcourt les valeurs du tableau. La première valeur de `#!py i` est donc `#!py 38`, la dernière `#!py 40`. On affiche ensuite chaque valeur de `#!py i` : des pointures.

        De plus, le nom de variable `#!py i` est souvent utilisé pour désigner des *indices* dans une boucle. Comme nous l'avons vu, la variable `#!py i` se voit ici affectée les *valeurs* du tableau. Mieux vaudrait ne pas utiliser ce nom de variable.

???+ question

    On dispose d'un tableau contenant des nombres entiers dont certains sont peut-être en double. Dans quels cas de figures peut-on utiliser un **parcours par valeurs** du tableau (sans utiliser les indices) ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] Rechercher le plus grand nombre
        - [ ] Déterminer si le nombre 0 est présent dans le tableau ?
        - [ ] Déterminer la position du minimum
        - [ ] Calculer la somme de tous les nombres

    === "Solution"
        
        - :white_check_mark: Rechercher le plus grand nombre
        - :white_check_mark: Déterminer si le nombre 0 est présent dans le tableau ?
        - :x: Déterminer la position du minimum
        - :white_check_mark: Calculer la somme de tous les nombres

        Seul le troisième cas nécessite de connaître l'indice du minimum. Dans les autres cas, on peut se contenter de parcourir par valeurs.

???+ question

    Compléter le script ci-dessous permettant de compter le nombre de pointures égales à 41 dans le tableau.
    
    L'avant-dernière ligne `#!py assert compteur == 2` permet de vérifier que votre code trouve bien `#!py 2`.
    
    - Si ce n'est pas le cas Python affichera une erreur.
    - Si, à l'inverse, le test est passé avec succès Python affichera `Bravo !`.

    {{ IDE('pythons/pointures') }}

    ??? success "Solution"

        On peut se contenter de parcourir les valeurs :

        ```python
        compteur = 0
        for p in pointures:
            if p == 41:
                compteur += 1
        ```

???+ question "Question : la fonction `#!py derniere_occurrence`"

    Compléter la fonction `#!py derniere_occurrence` prenant en argument `#!py tableau` de type  `list` ainsi qu'une valeur `#!py cible` et qui renvoie l'indice de la dernière occurrence de `#!py cible` dans `#!py tableau`.

    Si `#!py cible` n'est pas dans `#!py tableau`, la fonction renverra `#!py None`.

    {{ IDE('pythons/derniere_occurrence') }}

    ??? success "Solution"

        Sachant que l'on cherche la position d'un élément, il faut parcourir avec les indices :

        ```python
        def derniere_occurrence(tableau, cible):
            resultat = None
            for i in range(len(tableau)):
                if tableau[i] == cible:
                    resultat = i
            return resultat
        ```

        Une solution alternative et plus astucieuse consiste à parcourir le tableau en partant de la fin. Ainsi, dès que l'on rencontre la `#!py cible` on peut renvoyer l'indice correspondant :

        ```python
        def derniere_occurrence(tableau, cible):
            for i in range(len(tableau) - 1, -1, -1):
                if tableau[i] == cible:
                    return i
            return None
        ```
