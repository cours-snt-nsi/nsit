---
author: Nicolas Revéret
title: 🚩 Les tris en details
---

# Les tris

Ce site propose un cours d'introduction aux algorithmes de tris.

Il est destiné en premier lieu aux élèves de la spécialité *Numérique et Sciences Informatiques* de Première et Terminale.

Les seuls prérequis de ce cours sont les connaissances de bases en Python :

* Les variables : types élémentaires (`#!py int`, `#!py str`, `#!py list` et `#!py tuple`), affectations, calculs
* Les fonctions : utilisation et définition
* Structures conditionnelles : `#!py if... elif... else`
* Structures itératives : les boucles bornées, les « *Pour* », et non bornées, les « *Tant que* »
* Récursivité (pour la partie sur les tris efficaces)

Ce cours est divisé en plusieurs parties. Il n'est pas utile de toutes les lire.

1. [Prendre un bon départ](01_depart/01_tableaux.md) : rappels sur les tableaux, leurs modifications et leurs parcours
2. [Coût des algorithmes](02_complexite/01_recherches.md) : pourquoi certains algorithmes sont-ils plus efficaces que d'autres ?
3. [Étude de tris simples](03_lents/01_selection.md) : les tris par sélection en insertion (cours de Première NSI)
4. [Étude de tris efficaces](04_rapides/01_fusion.md) : en particulier le tri fusion (cours de Terminale NSI)
5. [Compléments](05_complements/01_tris_lineaires.md) : d'autres méthodes de tri