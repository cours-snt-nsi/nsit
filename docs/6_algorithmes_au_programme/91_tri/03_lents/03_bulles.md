---
author: Nicolas Revéret
title: Tri à bulles
---

# Tri à bulles

??? warning "Hors du programme de NSI"

    Le tri à bulle est intéressant à étudier mais il ne fait **pas partie du programme de NSI**

???+ abstract "Le tri à bulles en bref"

    On parcourt les paires d'éléments et on les échange si la valeur de gauche est strictement supérieure à celle de droite.

### Première approche

Le principe du tri à bulles s'apparente à celui du tri par insertion : il s'agit de décaler des valeurs jusqu'à ce qu'elles atteignent une position convenable.

L'idée est ici de faire « remonter » les grandes valeurs vers la fin du tableau. Comme les grosses bulles dans de l'eau pétillante ([source](https://www.pexels.com/fr-fr/photo/gouttes-d-eau-sur-fond-bleu-260551/)).

![Bulles](images/bulles.jpg){ .center width=30% }

L'algorithme est organisé autour de deux boucles :

* une boucle principale parcourant tous les indices `#!py i` du dernier `#!py len(tableau) - 1` (inclus) au deuxième `#!py 1` (inclus),

* une boucle secondaire qui, à chaque itération de la boucle principale, parcourt tous les indices `#!py j` entre le premier `#!py 0` (inclus) et `#!py i` (inclus). Lors de cette boucle, on compare deux éléments consécutifs d'indices `#!py j` et `#!py j + 1` et on les échange si celui d'indice `#!py j` est strictement supérieur à celui d'indice `#!py j + 1`.

Ainsi, lors de la première itération de la boucle principale, la boucle secondaire parcourt tous les éléments d'indice entre `#!py 0` et `#!py len(tableau) - 1` (inclus).

  * Lors de la première itération de cette boucle secondaire, on compare donc les valeurs d'indices `#!py 0` et `#!py 1` : si la première est strictement supérieure à la deuxième, on les échange.

  * Qu'il y ait eu échange ou non, on recommence ensuite en comparant les valeurs d'indices `#!py 1` et `#!py 2`. Là encore, si elles sont mal triées, on les échange.

  * On recommence de proche en proche jusqu'à avoir comparé les deux dernières valeurs.

* À l'issue de cette première itération de la boucle principale, la valeur maximale du tableau est remontée (comme une bulle) en fin de tableau.

* Lors de la deuxième itération de la boucle principale, la boucle secondaire parcourt tous les éléments d'indice entre `#!py 0` et `#!py len(tableau) - 2` (inclus). On effectue les échanges comme décrit plu haut.

* Lors du dernier tour de boucle principale, on n'effectue la comparaison que des deux premières valeurs (indices `#!py 0` et `#!py 1`).

Nous verrons plus bas qu'il est possible d'optimiser cet algorithme pour qu'il s'arrête dès que le tableau est trié. L'illustration ci-dessous met en oeuvre la version optimisée.

<section>
    <canvas id="canvas"></canvas>

    <span id="bouton-melange" onclick="initialisation('bulle')">
        Tableau aléatoire
    </span>

    <span id="bouton-melange" onclick="croissant('bulle')">
        Tableau croissant
    </span>

    <span id="bouton-melange" onclick="decroissant('bulle')">
        Tableau décroissant
    </span>

    <p>
        <span id="bouton-lancer"
            onclick="initialisation('bulle', document.getElementById('tableau').value)">
            Trier votre tableau
        </span>
        <input type="text" id="tableau" placeholder="[valeur1, valeur2,...]" value="[3,4,1,2]">
    </p>


    <script>
        $().ready(() => {
            setTimeout(
                initialisation('bulle'),
                500)
        }
        );
    </script>
</section>

???+ question "La fonction `#!py tri_bulles`"

    Compléter la fonction `#!py tri_bulles` prenant en argument un `#!py tableau` et le triant **en place** à l'aide du tri à bulle.

    On pourra utiliser la fonction `#!py echange`.

    {{ IDE('pythons/bulles') }}

    ??? tip "Astuce (1)"

        On doit utiliser deux boucles `#!py for` imbriquées.

    ??? tip "Astuce (2)"

        La boucle principale itère de `#!py len(tableau) - 1` inclus à `#!py 0` exclu.
    
    ??? tip "Astuce (3)"

        Si `#!py i` est la variable utilisée dans la boucle principale, la boucle secondaire itère de `#!py 0` inclus à `#!py i` exclu.

    ??? success "Solution"

        ```python
        def tri_bulles(tableau):
            for i in range(len(tableau) - 1, 0, -1):
                for j in range(0, i):
                    if tableau[j] > tableau[j + 1]:
                        echange(tableau, j, j + 1)
        ```

### Optimisation

L'algorithme trie bien les tableaux. Une question toutefois : que se passe-t'il lorsque le tableau est trié (à un moment quelconque du déroulement) ?

???+ question "Tri à bulle VS tableau trié"

    On fournit un tableau trié dans l'ordre croissant à l'algorithme de tri à bulles.
    
    === "Cocher la ou les affirmations correctes"
        
        - [ ] L'algorithme le trie dans l'ordre décroissant !
        - [ ] L'algorithme le trie dans un ordre aléatoire !
        - [ ] L'algorithme s'arrête à la fin de la première itération de la boucle principale
        - [ ] L'algorithme effectue toutes les itérations des deux boucles, toutes les lectures, les comparaisons, mais n'échange **aucune valeur**

    === "Solution"
        
        - :x: Non, l'algorithme gère bien ce cas de figure
        - :x: Non, l'algorithme gère bien ce cas de figure
        - :x: L'algorithme s'arrête à la fin de la première itération de la boucle principale
        - :white_check_mark: L'algorithme effectue toutes les itérations des deux boucles, toutes les lectures, les comparaisons, mais n'échange **aucune valeur**

Et oui, dans ce cas, l'algorithme effectue toutes les itérations, toutes les comparaisons mais n'échange **aucune valeur**. Beaucoup de travail pour pas grand-chose...

!!! note "Remarque"

    On  peut montrer que le tri à bulles est de coût quadratique, comme les tris par sélection ou insertion.

???+ question "Repérer un tableau trié"

    On exécute le tri à bulles. Comment savoir si le tableau est trié durant l'exécution de l'algorithme ?
    
    === "Cocher la ou les affirmations correctes"
        
        - [ ] Un tableau est trié si la valeur minimale est en première position
        - [ ] Un tableau est trié si la valeur maximale est en dernière position
        - [ ] Un tableau est trié si lors d'une itération de la boucle principale on n'a fait aucun échange
        - [ ] Un tableau est trié si lors d'une itération de la boucle principale on a fait un seul échange

    === "Solution"
        
        - :x: Cela peut se produire dès le début sans que le tableau soit trié. Par exemple avec $[1,\,3,\,2]
        - :x: Ce sera toujours le cas après une itération que le tableau soit trié ou non
        - :white_check_mark: En effet, c'est le bon critère !
        - :x: Cela signifie simplement que la valeur maximale était en avant-dernière position

Nous avons donc notre critère. Nous pouvons optimiser l'algorithme.

???+ question "La fonction `#!py tri_bulles_opt`"

    Compléter la fonction `#!py tri_bulles_opt` prenant en argument un `#!py tableau` et le triant **en place** à l'aide du tri à bulle.

    Le tri s'arrêtera dès qu'une itération se termine sans avoir effectué d'échange.

    On pourra utiliser la fonction `#!py echange`.

    {{ IDE('pythons/bulles_opt') }}

    ??? success "Solution"

        ```python
        def tri_bulles_opt(tableau):
            for i in range(len(tableau) - 1, 0, -1):
                tableau_trie = True
                for j in range(0, i):
                    if tableau[j] > tableau[j + 1]:
                        echange(tableau, j, j + 1)
                        tableau_trie = False
                if tableau_trie:
                    return
        ```

Désormais, face à un tableau initialement trié dans l'ordre croissant, l'algorithme n'effectuera qu'une seule itération. Le coût sera alors **linéaire**.