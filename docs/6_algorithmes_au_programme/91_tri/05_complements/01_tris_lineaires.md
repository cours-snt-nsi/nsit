---
author: Nicolas Revéret
title: Tris de coûts linéaires
---

# Tris de coûts linéaires

??? warning "Hors du programme de NSI"

    Les tris abordés dans cette partie ne font **pas** partie du programme de NSI (ni en Première, ni en Terminale).
    
Les différents algorithmes de tri abordés précédemment (par sélection, insertion, à bulle, fusion et rapide) sont tous des **tris par comparaison** : afin de trier le tableaux, ces algorithmes doivent comparer des éléments deux à deux.

On peut montrer que le coût de tels algorithmes ne peut pas descendre sous une certaine borne (les tris fusion et rapide atteignent cette borne dans certain cas, les tris par sélection, insertion et à bulles jamais).

Il existe toutefois des algorithmes de tris moins coûteux, plus rapides donc. Ils ne s'appuient toutefois pas sur les comparaisons d'éléments. Ces algorithmes profitent plutôt de particularités des tableaux à trier.

Nous allons en étudier deux dans cette partie, le tri par dénombrement et le tri du drapeau hollandais.

## Tri par dénombrement

???+ abstract "Le tri par dénombrement en bref"

    On l'utilise dans le cadre des tableaux d'entiers positifs ou nuls.

    On compte le nombre d'apparitions de chaque valeur entre `#!py 0` et le maximum du tableau (inclus l'un et l'autre) puis on trie le tableau initial en lisant ces apparitions dans l'ordre croissant

On se restreint aux tableaux de **nombres entiers positifs ou nuls**.

Les étapes du **tri par dénombrement** sont les suivantes :

* déterminer la valeur maximale dans le tableau,
  
* regrouper dans un autre tableau le nombre d'apparitions de chaque valeur entre `#!py 0` et le `#!py maximum` déterminé à l'étape précédente. Dans ce tableau d'effectifs, la valeur à l'indice `x` donne le nombre d'apparitions de `x` dans le tableau initial,

* parcourir le tableau des effectifs et écrire autant de fois que nécessaire chaque indice dans le tableau initial.

!!! example "Exemple"

    On souhaite trier le tableau `#!py nombres = [3, 3, 2, 0, 3, 0, 2, 0, 2, 3]` :

    * La valeur maximale est `#!py 3` ;

    * Le tableau des effectifs vaut :
  
    ```python
    # indice     0  1  2  3
    effectifs = [3, 0, 3, 4]
    ```

    Le `#!py 3` à l'indice `#!py 2` signifie que le nombre `#!py 2` apparaît `#!py 3` fois dans `nombres` ;
    
    * On parcourt `effectifs` :
        * à l'indice `0` on trouve la valeur `3` : on écrit trois fois `0` dans le tableau,
        * à l'indice `1` on trouve la valeur `0` : on n'écrit aucun `1` dans le tableau,
        * à l'indice `2` on trouve la valeur `3` : on écrit trois fois `2`,
        * à l'indice `3` on trouve la valeur `4` : on écrit quatre fois `3` ;

    * Le tableau est désormais trié : `#!py [0, 0, 0, 2, 2, 2, 3, 3, 3, 3]`.

Vous devez écrire deux fonctions :

* `maximum` prend en argument un `tableau` et renvoie la valeur maximale de celui-ci,

* `tri_denombrement` prend en argument un tableau `entiers` et le trie en utilisant le tri par dénombrement.

{{ IDE('pythons/denombrement') }}

??? success "Solution"

    ```python
    def maximum(tableau):
        maxi = 0
        for x in tableau:
            if x > maxi:
                maxi = x
        return maxi


    def tri_denombrement(entiers):
        maxi = maximum(entiers)

        effectifs = [0] * (maxi + 1)

        for x in entiers:
            effectifs[x] += 1

        i = 0
        for x in range(maxi + 1):
            for _ in range(effectifs[x]):
                entiers[i] = x
                i += 1
    ```

    On pourrait aussi utiliser `#!py max(nombres)`

## Tri du drapeau hollandais

???+ abstract "Le tri du drapeau hollandais en bref"

    On l'utilise dans le cadre des tableaux ne contenant que trois valeurs distinctes (petite, moyenne et grande).

    Il a pour avantage d'être de coût linéaire et de trier le tableau en un seul passage.

    On partage le tableau en quatre zones :


    * les petites valeurs,
    * puis les valeurs moyennes,
    * puis les valeurs pas encore triées,
    * enfin les grandes valeurs.

    ![Schéma](images/petit_drapeau.png#only-light){ width=50% .center }
       
    ![Schéma](images/petit_drapeau_dark.png#only-dark){ width=50% .center }
    
    L'algorithme s'arrête lorsque la zone des valeurs à trier est vide.
    

On souhaite trier un `tableau` ne contenant que trois valeurs `a`, `b` et `c` répétées un nombre quelconque de fois. On souhaite trier ce tableau afin de placer au début les `a` suivis des `b` et enfin des `c`.

Par exemple, avec `#!py nombres = [2, 0, 2, 1, 2, 1]` on a `#!py a = 0`, `#!py b = 1` et `#!py c = 2`. Le tableau trié est `#!py [0, 1, 1, 2, 2, 2]`.

Dans ce cas de figure, on peut utiliser le tri du drapeau hollandais.

??? note "Remarque"

    Ce tri a été inventé par [Edsger Dijkstra](https://fr.wikipedia.org/wiki/Edsger_Dijkstra) qui était de nationalité néelandaise.

    Il a donné son nom à l'algorithme en référence aux trois couleurs du drapeau néerlandais : Rouge, Blanc, Bleu.

    ![Drapeau hollandais](images/flag.jpg){ .center width=25% }

L'idée est de maintenir quatre zones dans le tableau :

* la première ne contient que des `a`,
* la deuxième que des `b`,
* la troisième des valeurs pas encore triées,
* enfin la dernière zone ne contient que des `c`.

Ces zones sont délimitées par les bornes suivantes (toutes incluses):

1. de l'indice `0` à `prochain_a - 1`,
2. de `prochain_a` à `actuel`,
3. de `actuel + 1` à `prochain_c`,
4. de `prochain_c + 1` à `#!py len(tableau) - 1`

![Les quatre zones](images/drapeau.png#only-light){.center width=100%}
![Les quatre zones](images/drapeau_dark.png#only-dark){.center width=100%}

L'algorithme fonctionne ainsi :

* Les variables `prochain_a` et `actuel` sont initialisées à `#!py 0`,
  
* La variable `prochain_c` est initialisée à `#!py len(tableau) - 1`,
  
* On répète les actions suivantes tant que la zone des valeurs restant à trier est non vide :
  
    * On lit l'élément à l'indice `actuel` :

        * Si c'est un `a`, on le rajoute à la fin de la zone des `a` en échangeant les éléments d'indices `prochain_a` et `actuel` (étape 3 sur les figures),
        * Si c'est un `b`, on la laisse à sa position  (étape 2 et 6),
        * Si c'est un `c`, on la place à la position précédant le début de la zone des `c` en échangeant les éléments d'indices `prochain_c` et `actuel` (étape 1, 4 et 5),
        * Dans tous les cas, on prend soin de mettre à jour les différents indices afin de refléter les nouvelles étendues des zones.

Les figures ci-dessous illustrent le tri du tableau `#!py [2, 0, 2, 1, 2, 1]` :

* Étape 1

    ![Étape 1](images/drapeau_1.png#only-light){ width=50% align=left}
    ![Étape 1](images/drapeau_1_dark.png#only-dark){ width=50% align=left}

    On lit un `#!py 2` : on le place au début de la zone des `c`.

    `prochain_c` est décrémenté.

---
* Étape 2

    ![Étape 2](images/drapeau_2.png#only-light){ width=50% align=left}
    ![Étape 2](images/drapeau_2_dark.png#only-dark){ width=50% align=left}

    On lit un `#!py 1` : on le laisse à cette position.

    `actuel` est incrémenté.

---
* Étape 3

    ![Étape 3](images/drapeau_3.png#only-light){ width=50% align=left}
    ![Étape 3](images/drapeau_3_dark.png#only-dark){ width=50% align=left}

    On lit un `#!py 0` : on le place à la fin de la zone des `a`.

    `prochain_a` et `actuel` sont incrémentés.

---
* Étape 4

    ![Étape 4](images/drapeau_4.png#only-light){ width=50% align=left}
    ![Étape 4](images/drapeau_4_dark.png#only-dark){ width=50% align=left}

    On lit un `#!py 2` : on le place au début de la zone des `c`.

    `prochain_c` est décrémenté.

---
* Étape 5

    ![Étape 5](images/drapeau_5.png#only-light){ width=50% align=left}
    ![Étape 5](images/drapeau_5_dark.png#only-dark){ width=50% align=left}

    On lit un `#!py 2` : on le place au début de la zone des `c`.

    `prochain_c` est décrémenté.

---
* Étape 6

    ![Étape 6](images/drapeau_6.png#only-light){ width=50% align=left}
    ![Étape 6](images/drapeau_6_dark.png#only-dark){ width=50% align=left}

    On lit un `#!py 1` : on le laisse à cette position.

---
* Étape 7

    ![Étape 7](images/drapeau_7.png#only-light){ width=50% align=left}
    ![Étape 7](images/drapeau_7_dark.png#only-dark){ width=50% align=left}

    `actuel` est strictement supérieur à `prochain_c`.
    
    L'algorithme se termine.

Vous devez écrire la fonction `tri_drapeau` qui prend en argument un `tableau` ainsi que les trois valeurs distinctes le composant `a`, `b` et `c`  et le trie **en place** en positionnant les `a` au début suivis des `b` et enfin des `c`.

{{ IDE('pythons/drapeau') }}

??? success "Solution"

    ```python
    def tri_drapeau(tableau, a, b, c):
        prochain_a = 0
        actuel = 0
        prochain_c = len(tableau) - 1

        while actuel <= prochain_c:
            if tableau[actuel] == a:
                tableau[prochain_a], tableau[actuel] = tableau[actuel], tableau[prochain_a]
                prochain_a = prochain_a + 1
                actuel = actuel + 1
            elif tableau[actuel] == b:
                actuel = actuel + 1
            else:
                tableau[prochain_c], tableau[actuel] = tableau[actuel], tableau[prochain_c]
                prochain_c = prochain_c - 1
    ```
