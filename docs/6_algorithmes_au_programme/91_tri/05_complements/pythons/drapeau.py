def tri_drapeau(tableau, a, b, c):
    prochain_a = 0
    actuel = 0
    prochain_c = len(tableau) - 1

    while ... <= ...:
        if tableau[...] == a:
            tableau[...], tableau[...] = tableau[...], tableau[...]
            prochain_a = ...
            actuel = ...
        elif tableau[...] == b:
            actuel = ...
        else:
            tableau[...], tableau[...] = tableau[...], tableau[...]
            prochain_c =...


# Tests
entiers = [2, 0, 2, 1, 2, 1]
a, b, c = 0, 1, 2
tri_drapeau(entiers, a, b, c)
assert entiers == [0, 1, 1, 2, 2, 2]

neveux = ["Fifi", "Loulou", "Riri", "Riri", "Fifi"]
a, b, c = "Riri", "Fifi", "Loulou"
tri_drapeau(neveux, a, b, c)
assert neveux == ["Riri", "Riri", "Fifi", "Fifi", "Loulou"]

print("Bravo !")
