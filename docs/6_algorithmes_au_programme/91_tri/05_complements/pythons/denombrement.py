def maximum(tableau):
    ...


def tri_denombrement(entiers):
    ...


nombres = [4, 5, 4, 2]
tri_denombrement(nombres)
assert nombres == [2, 4, 4, 5], "Erreur avec [4, 5, 4, 2]"

nombres = [3, 8, 7, 3, 5]
tri_denombrement(nombres)
assert nombres == [3, 3, 5, 7, 8], "Erreur avec [3, 8, 7, 3, 5] "

nombres = [1, 2, 3, 4, 5]
tri_denombrement(nombres)
assert nombres == [1, 2, 3, 4, 5], "Erreur avec [1, 2, 3, 4, 5]"

nombres = [5, 4, 3, 2, 1]
tri_denombrement(nombres)
assert nombres == [1, 2, 3, 4, 5], "Erreur avec [5, 4, 3, 2, 1] "

nombres = []
tri_denombrement(nombres)
assert nombres == [], "Erreur avec []"

print("Bravo !")
