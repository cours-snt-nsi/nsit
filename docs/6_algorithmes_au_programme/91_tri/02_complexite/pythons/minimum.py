def minimum(tableau):
    ...


assert minimum([3, 8, 5]) == 0, "Erreur avec [3, 8, 5]"
assert minimum([8, 5]) == 1, "Erreur avec [8, 5]"
assert minimum([8]) == 0, "Erreur avec [8]"
print("Bravo !")
