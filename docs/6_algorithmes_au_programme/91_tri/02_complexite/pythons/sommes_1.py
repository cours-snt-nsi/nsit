def sommes(tableau):
    cumuls = [0] * len(tableau)
    for i in range(len(tableau)):
        for j in range(0, ...):
            cumuls[i] = ...
    return ...


assert sommes([1, 2, 3]) == [1, 3, 6], "Erreur avec [1, 2, 3]"

resultats = [
    5, 10,  2,  8,  9, 13, 11, 7,  8, 5,
    13, 7, 11,  9, 15, 12, 17, 9, 10, 4, 0
]
assert sommes(resultats) == [
    5,  15,  17,  25,  34,  47,  58,  65,  73,  78,
    91, 98, 109, 118, 133, 145, 162, 171, 181, 185, 185, 
]
print("Bravo !")
