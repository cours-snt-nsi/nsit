def lineaire(tableau, x):
    ...


tableau = [8, 7, 2, 10, 3, 1, 6, 5, 9, 5, 4]
assert lineaire(tableau, 5) == 7, "Erreur en cherchant 5"
assert lineaire(tableau, 0) is None, "Erreur en cherchant 0"
print("Bravo !")
