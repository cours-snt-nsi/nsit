#--- HDR ---#
def lineaire_bis(tableau, x):
    nb_iterations = 0
    for i in range(len(tableau)):
        nb_iterations += 1
        if tableau[i] == x:
            return i
    return nb_iterations
#--- HDR ---#
# À modifier
tableau = [8, 7, 2, 10, 3, 1, 6, 5, 9, 5, 4]
x = 5

nb_itérations = lineaire_bis(tableau, x)
print(f"Il y a eu {nb_itérations} itération(s).")
