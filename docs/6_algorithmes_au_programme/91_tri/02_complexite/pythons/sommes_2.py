def sommes_lineaires(tableau):
    cumuls = [0] * len(tableau)
    cumuls[0] = tableau[0]
    for i in range(1, len(tableau)):
        ...
    return ...


assert sommes_lineaires([1, 2, 3]) == [1, 3, 6], "Erreur avec [1, 2, 3]"

resultats = [
    5, 10,  2,  8,  9, 13, 11, 7,  8, 5,
    13, 7, 11,  9, 15, 12, 17, 9, 10, 4, 0
]

# Tests
assert sommes_lineaires(resultats) == [
    5,  15,  17,  25,  34,  47,  58,  65,  73,  78,
    91, 98, 109, 118, 133, 145, 162, 171, 181, 185, 185, 
]
print("Bravo !")
