---
author: Nicolas Revéret
title: Recherches
---

# Recherches dans un tableau

## Rechercher une valeur

Considérons le tableau suivant : `#!py nombres = [8, 7, 2, 10, 3, 1, 6, 5, 9, 4]` et cherchons-y la valeur `#!py 5`.

Contrairement à nous, l'ordinateur n'a pas une vue d'ensemble des valeurs, il ne peut pas immédiatement « voir » le `#!py 5` vers la fin du tableau.

Son approche est plus rudimentaire : il passe en revue tous les éléments du tableau jusqu'à trouver le `#!py 5` :

![Recherche de 5](images/lineaire_fixe.gif){ width=75% .center}

Cette méthode est appelée la **recherche linéaire** ou méthode par balayage.

???+ question "La fonction `#!py lineaire`"

    Compléter la fonction `#!py lineaire` prenant en argument un `#!py tableau` et une valeur `#!py x` et renvoyant l'indice de la première occurrence de `#!py x` dans `#!py tableau`.

    Si `#!py x` n'est pas présent dans `#!py tableau`, la fonction renverra `#!py None`

    {{ IDE('pythons/recherche_lineaire') }}

    ??? success "Solution"

        ```python
        def lineaire(tableau, x):
            for i in range(len(tableau)):
                if tableau[i] == x:
                    return i
            return None
        ```

        En réalité, le `#!py return None` de la dernière ligne n'est pas nécessaire car en Python une fonction renvoie `#!py None` par défaut .

Étudions en détails le déroulement de cette fonction en comptant le nombre d'opérations:

* l'algorithme est construit autour d'une boucle « *Pour* »,
* à chaque itération, la variable `#!py i` se voit affecter une nouvelle valeur : $1$ affectation,
* on teste ensuite si `#!py tableau[i]` est égal à `#!py x` : $1$ accès à la valeur de `#!py tableau[i]` et $1$ comparaison,
* s'il y a égalité on renvoie `#!py i` : $1$ retour,
* en fin de boucle on renvoie `#!py None` : $1$ retour.

!!! note "Remarque"

    Ce décompte du nombre d'opérations est simpliste. En réalité, Python exécute plus d'opérations.

Si l'on fait le total, on se rend compte que chaque tour de boucle (sans tenir compte du `#!py return`) nécessite $3$ opérations. Le `#!py return` quant à lui, qu'il renvoie un indice ou `#!py None` rajoute $1$ opération à l'ensemble.

Dans l'exemple précédent, le `#!py 5` cherché était à l'indice `#!py 7`. Il a donc fallu $8$ itérations et $3\times 8 +1=25$ opérations au total.

???+ question "Compter les opérations ou les itérations"

    On considère toujours le tableau `#!py nombres = [8, 7, 2, 10, 3, 1, 6, 5, 9, 4]` et l'on effectue des recherches linéaires.

    === "Cocher la ou les affirmations correctes"
        
        - [ ] La recherche de `#!py 8` nécessite une seule **itération**
        - [ ] La recherche de `#!py 8` nécessite une seule **opération**
        - [ ] La recherche de `#!py 6` nécessite $19$ **opérations**
        - [ ] La recherche de `#!py 4` nécessite $31$ **opérations**

    === "Solution"
        
        - :white_check_mark: `#!py 8` est à l'indice `#!py 0`. Il faut donc bien une seule **itération**
        - :x: Non car chaque **itération** nécessite à elle seule $3$ **opérations**
        - :x: `#!py 6` est à l'indice `#!py 6`. Il faut donc $7$ **itérations** et $3\times 7 + 1= 22$ **opérations**
        - :white_check_mark: `#!py 4` est à l'indice `#!py 9`. Il faut donc $10$ **itérations** et bien $3\times 10 + 1= 31$ **opérations**

Le décompte précis des opérations peut être laborieux et très sensible au langage de programmation ou aux instructions saisies pour mettre en œuvre l'algorithme.

!!! info "Important"
    
    Nous allons donc désormais nous contenter de **compter les itérations** qui ne font que des opérations élémentaires.

???+ question "Compter les itérations"

    On a modifié la fonction `#!py lineaire` afin qu'elle renvoie le nombre d'itérations nécessaires effectuées lors de la recherche de `x` dans `tableau`. Cette nouvelle fonction s'appelle `lineaire_bis` 

    Si vous exécutez ce code, vous retrouverez le résultat précédent : rechercher `#!py 5` nécessite $8$ itérations.

    Modifier `#!py x` et `#!py tableau` (si besoin) afin que la recherche de `#!py x` dans `#!py tableau` nécessite :

    * 1 itération
    * 2 itérations
    * 5 itérations dans un tableau de 5 valeurs
    * 5 itérations dans un tableau de 6 valeurs
    * 5 itérations dans un tableau contenant 2 fois la valeur cherchée
    * 6 itérations dans un tableau de 6 valeurs **sans que `#!py x` ne soit présent dans `#!py tableau`**

    {{ IDE('pythons/compte_iterations') }}

    ??? success "Solution"

        * 1 itération : Par exemple `#!py lineaire([0], 0)`
  
        * 2 itérations : Par exemple `#!py lineaire([0, 1], 2)`
        * 5 itérations dans un tableau de 5 valeurs : Par exemple `#!py lineaire([1, 2, 3, 4, 5], 5)`
        * 5 itérations dans un tableau de 6 valeurs : Par exemple `#!py lineaire([1, 2, 3, 4, 5, 6], 5)`
        * 5 itérations dans un tableau contenant 2 fois la valeur cherchée : Par exemple `#!py lineaire([1, 2, 3, 4, 5, 5], 5)`
        * 6 itérations dans un tableau de 6 valeurs **sans que `#!py x` ne soit présent dans `#!py tableau`** : Par exemple `#!py lineaire([1, 2, 3, 4, 5, 6], 7)`

On l'a deviné, l'algorithme effectue un maximum d'itérations dans deux cas de figure :

* soit `#!py x` est le dernier élément de `#!py tableau`,
* soit `#!py x` n'est pas présent dans `#!py tableau`.

Ce sont les deux **pires des cas**.

![Cible en dernière position](images/lineaire_pire_present.gif){ width=49%}
![Cible absente](images/lineaire_pire_absent.gif){ width=49%}

Dans chacun d'eux, si le tableau compte $N$ éléments, il y aura $N$ itérations. Le nombre d'itérations est égal à la taille du tableau. On dit que l'algorithme de recherche linéaire est de **coût linéaire**.

## Rechercher le minimum

Le tri par sélection nécessite de trouver la valeur minimale du tableau afin de la placer en première position. Ceci fait on recommence à partir de la deuxième position, puis de la troisième, *etc*.

Nous nous intéressons dans cette partie à la recherche du minimum.

???+ question "Rechercher le minimum"

    === "Cocher la ou les affirmations correctes"
        
        - [ ] Dans le tableau $[3,\,8,\,7,\,1,\,2]$ la recherche du minimum nécessite $4$ itérations
        - [ ] Dans le tableau $[1,\,2,\,3,\,4]$ la recherche du minimum nécessite $4$ itérations
        - [ ] La recherche du minimum est plus courte s'il est placé au début du tableau
        - [ ] Dans le tableau $[8,\,8,\,8,\,8]$ la recherche du minimum nécessite $1$ itération

    === "Solution"
        
        - :x: Dans le tableau $[3,\,8,\,7,\,1,\,2]$ la recherche du minimum nécessite $4$ itérations
        - :white_check_mark: Dans le tableau $[1,\,2,\,3,\,4]$ la recherche du minimum nécessite $4$ itérations
        - :x: La recherche du minimum est plus courte s'il est placé au début du tableau
        - :x: Dans le tableau $[8,\,8,\,8,\,8]$ la recherche du minimum nécessite $1$ itération

        Il faut lire l'ensemble des valeurs une fois pour être sûr de bien déterminer le minimum. Donc pour un tableau de taille $N$, il faut faire $N$ itérations.


On l'a compris, la recherche du minimum nécessite de lire toutes les valeurs une seule fois : le coût est donc **linéaire** (proportionnel à la taille du tableau).

???+ question "La fonction `#!py minimum`"

    Compléter la fonction `#!py minimum` prenant en argument un `#!py tableau` et renvoyant l'indice de la première occurrence du minimum dans `#!py tableau`.

    On garantit que `#!py tableau` est non vide.

    {{ IDE('pythons/minimum') }}

    ??? success "Solution"

        ```python
        def minimum(tableau):
            i_mini = 0
            for i in range(1, len(tableau)):
                if tableau[i] < tableau[i_mini]:
                    i_mini = i
            return i_mini
        ```
