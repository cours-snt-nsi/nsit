---
title : 🚜 Programmation dynamique
hide:
    -toc
---


## Programmation dynamique

!!! note "Cours détaillé"
    
    Un cours détaillé sur cette méthode est disponible [sur cette page](../../4_algorithmique/2_prog_dyn/index.md).

La programmation dynamique est une méthode de programmation permettant de résoudre un problème en résolvant préalablement certains problèmes équivalents de taille inférieure. Cette méthode s'apparente donc à *Diviser Pour Régner*. Toutefois dans la programmation dynamique, on prend soin de conserver les résultats des calculs intermédiaires afin de ne pas les calculer à nouveau.

Considérons le problème consistant à calculer le $n$-ième terme de la suite de Fibonacci $(F_n)$ définie ci-dessous :

$$F_0 = 0$$

$$F_1 = 1$$

$$\forall n \geqslant 2,\,F_n=F_{n-1}+F_{n-2}$$

!!! info "Suite de Fibonacci descendante (avec mémoïsation)"

    Le calcul récursif du $n$-ième terme de la suite de Fibonacci fait intervenir à plusieurs reprises les mêmes appels. Par exemple $F_5=F_4+F_3=(F3+F_2)+(F_2+F_1)$.
    
    On peut alléger la démarche en gardant trace des calculs déjà effectués. Cette méthode est appelée *mémoïsation*.
    
    On utilise une approche récursive (descendante).

    ```
    mémoire est un dictionnaire
    
    Fonction fibonacci_rec(n : entier) -> entier :
        Si n <= 1 :
            Renvoyer n

        Si n n'est pas une clé de mémoire :
            mémoire[n] = fibonacci_rec(n - 1) + fibonacci_rec(n - 2)
        Renvoyer mémoire[n]
    ```

    ??? example "Éditeur"
    
        {{ IDE('fibo_rec')}}

!!! info "Suite de Fibonacci ascendante"

    Il est aussi possible d'utiliser une approche itérative, ascendante.

    ```
    Fonction fibonnacci_iter(n : entier) -> entier :
        F est un tableau initialisé à [0, 1]
        Tant que longueur(F) < n + 1 :
            dernier = F[longueur(F) - 1]
            avant_dernier = F[longueur(F) - 2] 
            Ajouter (dernier + avant_dernier) à la fin de F
        Renvoyer F[n]
    ```
    
    ??? example "Éditeur"
    
        {{ IDE('fibo_iter')}}