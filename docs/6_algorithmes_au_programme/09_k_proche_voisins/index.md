---
title : 🚗 k plus proches voisins
hide:
    -toc
---


## *k* plus proches voisins


L'algorithme des $k$ Plus Proches Voisins, *$k$ Nearest Neighbors* en anglais (*KNN*), fait partie des algorithmes de classification.

Le but de cet algorithme est d'attribuer une catégorie (une classe) à un individu. On se donne pour cela une base de données d'individus pour lesquels on connaît :

* des descripteurs numériques,

* la catégorie (qualitative ou quantitative).

Seules les valeurs des descripteurs sont connues pour l'individu « mystère ».

Le principe est illustré sur la figure ci-dessous :

![Algorithme des $k$ plus proches voisins](knn_algo.png){width=50% .center .autolight}

La démarche est donc la suivante :

* On calcule toutes les distances entre l'individu « mystère » et les individus de la base ;

* On trie la base dans l'ordre croissant des distances à l'individu « mystère » ;

* On observe les $k$ premiers voisins : la catégorie la plus représentée parmi ceux-ci est attribuée à l'individu « mystère ».

Dans l'algorithme ci-dessous on considère que le tableau `voisins` contient la liste non vide des catégories des voisins triée par distance au point « mystère » croissante. Ainsi, le premier élément de `voisin` est la catégorie du point le plus proche.

`k` est le nombre de voisins à prendre en considération. On suppose que `k` est inférieur ou égal au nombre de voisins.

```
Fonction knn(voisins : tableau de catégories, k : entier) -> catégorie :
    effectif est un dictionnaire vide
    
    catégorie_maxi = voisins[0]
    effectif_maxi = 1
    Pour i allant de 1 à k (exclu):
        catégorie = voisins[i]
        Si catégorie est une clé de effectif :
            effectif[catégorie] = effectif[catégorie] + 1
        Sinon :
            effectif[catégorie] = 1
        Si effectif[catégorie] > effectif_maxi:
            effectif_maxi = effectif[catégorie]
            catégorie_maxi = catégorie
    
    Renvoyer catégorie_maxi
```

??? example "Éditeur"

    {{ IDE('knn')}}
