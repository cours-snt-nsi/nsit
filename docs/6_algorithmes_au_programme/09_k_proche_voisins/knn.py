# saisir votre fonction ici

voisins = ["A", "B", "A", "B", "B", "C", "A", "A"]
assert knn(voisins, 1) == "A"
assert knn(voisins, 2) == "A"
assert knn(voisins, 3) == "A"
assert knn(voisins, 4) == "A"
assert knn(voisins, 5) == "B"
assert knn(voisins, 6) == "B"
assert knn(voisins, 7) == "A"
assert knn(voisins, 8) == "A"
