# saisir vos fonctions ici


gauche = [1, 3, 5]
droite = [2, 4]
assert fusion(gauche, droite) == [1, 2, 3, 4, 5]

gauche = [2, 4]
droite = [1, 3, 5]
assert fusion(gauche, droite) == [1, 2, 3, 4, 5]

gauche = [1]
droite = []
assert fusion(gauche, droite) == [1]

tab = [1]
assert tri_fusion(tab) == [1]

tab = [3, 4, 1, 2]
assert tri_fusion(tab) == [1, 2, 3, 4]

tab = [1, 1]
assert tri_fusion(tab) == [1, 1]
