---
title : 🚀 Algorithmes de tri
hide:
    -toc
---


## Algorithmes de tri

???+ note "Cours détaillés"

    Des cours détaillés sur les tris présentés ci-dessous sont disponibles [sur cette page](https://nreveret.forge.apps.education.fr/tris/).

!!! info "Tri par insertion"

    Cette méthode de tri consiste à parcourir l'ensemble du tableau (à partir du deuxième élément) et à faire « remonter » chaque élément jusqu'à ce qu'il ait atteint le début du tableau ou qu'il ne soit précédé que par des éléments qui lui sont inférieurs. C'est le tri utilisé par beaucoup de personnes pour trier des cartes.

    <center>
    ![Tri de cartespar insertion](insertion_cards_Cormen.png){width=30% .autolight style="vertical-align :top;margin-top :5%; margin-right :5%"}
    ![Tri par insertion](insertion_sort.jpg){width=30% .autolight}
    </center>

    La complexité de cet algorithme est **quadratique** $\mathcal{O}(n^2)$ ($n$ est la taille du tableau).

    Il s'agit d'un tri en place (les éléments sont triés directement dans le tableau de départ) et stable (deux éléments qui étaient déjà classés dans le tableau de départ ressortent dans le même ordre).

    ```title="Langage naturel"
    Fonction tri_insertion(tab : tableau) -> Rien :
        Pour i allant de 1 à longueur(tab) - 1 (inclus) :
            a_inserer = tab[i]
            j = i
            Tant que j > 0 et tab[j - 1] < a_inserer :
                tab[j] = tab[j - 1]
                j = j - 1
            tab[j] = a_inserer
    ```

    ??? example "Éditeur"
    
        {{ IDE('insertion')}}
        

!!! info "Tri par sélection"

    Cette méthode de tri consiste à parcourir l'ensemble du tableau (jusqu'à l'avant-denier élément) et, lors de chaque itération, à sélectionner le plus petit élément situé à droite de l'élément atteint. Ce minimum alors échangé avec l'élément en cours d'étude.

    ![Tri par sélection](selection_sort.jpg){width=40% .center .autolight}

    La complexité de cet algorithme est là-encore **quadratique** $\mathcal{O}(n^2)$ ($n$ est la taille du tableau).

    Il s'agit d'un tri en place et stable.

    ```
    Fonction tri_sélection(tab : tableau) -> Rien :
        Pour i allant de 0 à longueur(tab) - 2 (inclus) :
            indice_mini = i
            # Recherche de l'indice du minimum
            Pour j allant de i à longueur(tab) - 1 (inclus) :
                Si tab[j] < tab[indice_mini] :
                    indice_mini = j
            Échanger tab[i] et tab[indice_mini]
        
    ```

    ??? example "Éditeur"
    
        {{ IDE('selection')}}
        

!!! info "Tri fusion"

    Il s'agit d'un tri utilisant la méthode *Diviser Pour Régner*.

    Le principe est de couper récursivement le tableau en deux jusqu'à obtenir des tableaux de taille $1$. Une fois ceux-ci atteints, on fusionne ces tableaux (qui sont de fait déjà triés) en piochant à chaque étape de plus petit élément de chaque tableau.

    ![Tri fusion](merge_sort.png){width=35% .center .autolight}

    Cet algorithme est donc de complexité **linéarithmique** $\mathcal{O}(n\log{n})$ ($n$ est la taille du tableau).

    C'est un tri stable mais pas en place (dans la version présentée ici).
    
    Cette méthode est construite autour de deux fonctions :

    * `fusion` : fusionne les deux sous-tableaux passés en argument ;

    * `tri_fusion` : partage le tableau en deux sous-tableaux. C'est la fonction récursive.

    ```
    Fonction fusion(gauche : tableau, droite : tableau) -> tableau :
        # Le tableau qui sera renvoyé
        resultat = [Rien pour i allant de 0 à longueur(gauche) + longueur(droite) (exclu)]

        # les indices de lecture et l'indice d'écriture
        i_gauche = 0
        i_droite = 0
        i_resultat = 0

        Tant que i_gauche < longueur(gauche) et i_droite < longueur(droite) :
            Si gauche[0] <= droite[0] :
                resultat[i_resultat] = gauche[0]
                i_gauche = i_gauche + 1
            Sinon :
                resultat[i_resultat] = droite[0]
                i_droite = i_droite + 1
            i_resultat = i_resultat + 1
            
        Tant que i_gauche < longueur(gauche):
            resultat[i_resultat] = gauche[0]
            i_gauche = i_gauche + 1
            i_resultat = i_resultat + 1

        Tant que i_droite < longueur(droite) :
            resultat[i_resultat] = droite[0]
            i_droite = i_droite + 1
            i_resultat = i_resultat + 1

        Renvoyer resultat
    
    Fonction tri_fusion(tab : tableau) -> tableau :
        # Cas de base
        Si longueur(tab) == 1 :
            Renvoyer tab
        
        # Cas général
        # L'indice du "milieu" du tableau
        milieu = longueur(tab)// 2 # quotient entier

        # Les sous-tableaux de gauche et de droite
        gauche = [tab[i] pour chaque i entre 0 et milieu (exclu)]
        droite = [tab[i] pour chaque i entre milieu et longueur(tab) (exclu)]

        # Tri de gauche et droite
        gauche = tri_fusion(gauche)
        droite = tri_fusion(droite)

        # On renvoie leur fusion
        Renvoyer fusion(gauche, droite)
    ```

    ??? example "Éditeur"
    
        {{ IDE('fusion')}}