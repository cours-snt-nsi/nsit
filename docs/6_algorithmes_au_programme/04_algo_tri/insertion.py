# saisir votre fonction ici

tab = [1]
tri_insertion(tab)
assert tab == [1]

tab = [3, 4, 1, 2]
tri_insertion(tab)
assert tab == [1, 2, 3, 4]

tab = [1, 1]
tri_insertion(tab)
assert tab == [1, 1]
