# saisir votre fonction ici

# pièces européennes en centimes
# le tableau est trié dans l'ordre décroissant
pieces = [200, 100, 50, 20, 10, 5, 2, 1]

assert rendu_monnaie(200, pieces) == [200]
assert rendu_monnaie(114, pieces) == [100, 10, 2, 2]
assert rendu_monnaie(100, pieces) == [100]
assert rendu_monnaie( 17, pieces) == [10, 5, 2]
assert rendu_monnaie(  1, pieces) == [1]
assert rendu_monnaie(  0, pieces) == []
