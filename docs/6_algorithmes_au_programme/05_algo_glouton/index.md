---
title : 🚁 Algorithmes gloutons
hide:
    -toc
---


## Algorithmes gloutons

Les algorithmes **gloutons** sont une famille d'algorithmes permettant de résoudre des problèmes d'optimisation. Il s'agit de déterminer la solution optimale (la meilleure) à un problème.

Les algorithmes gloutons interviennent souvent lorsque la détermination immédiate de la solution optimale ne peut pas être faite et lorsque l'étude de toutes les solutions possibles est trop longue.

Dans ce cas, on utilise une méthode itérative qui détermine petit à petit une solution **satisfaisante**, à défaut d'être **optimale**. A chaque étape on fait le meilleur choix **à cet instant**. C'est donc une méthode *vorace*, *greedy* en anglais. L'expression *algorithme glouton* est d'ailleurs une traduction de *greedy algorithm*.

Il est courant qu'une algorithme glouton débute par trier des données. La complexité de ces algorithmes est donc **linéarithmique** $\mathcal{O}(n\log{n})$ ($n$ est la taille du tableau de données trié).

!!! info "Rendu de monnaie"

    Illustrons cette méthode par le problème du rendu de monnaie. Il s'agit de rendre une somme en utilisant un nombre minimum de pièces.

    On considère un jeu de pièces pour lequel la méthode gloutonne renvoie la solution optimale. Un tel jeu de pièces est dit *canonique*.
    
    On considère que le jeu de pièces est déjà trié dans l'ordre décroisant.

    ```
    Fonction rendu_monnaie(somme : entier, pièces : tableau d'entiers) -> tableau d'entiers :
        résultat est un tableau vide
        i = 0
        Tant que somme > 0 :
            valeur = pièces[i]
            Si valeur est inférieur ou égal à somme :
                Ajouter valeur à résultat
            Sinon :
                i = i + 1
        Renvoyer résultat
    ```
    
    ??? example "Éditeur"
    
        {{ IDE('monnaie')}}