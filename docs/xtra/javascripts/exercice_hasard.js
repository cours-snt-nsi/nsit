function exercice_hasard() {
    let classes = ["debutant", "facile", "moyen", "difficile"];

    for (const classe of classes) {
        const arrExos = exercices[classe]
        if(!arrExos.length) continue

        const i = Math.floor(Math.random() * arrExos.length)
        const exo = arrExos[i];
        $('#exo-' + classe).text(" ⇨ " + exo[0]);
        $('#exo-' + classe).attr("href", exo[1]);
    }
}